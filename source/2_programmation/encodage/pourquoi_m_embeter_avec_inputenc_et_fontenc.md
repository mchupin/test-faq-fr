# Pourquoi s'embêter avec les extensions « inputenc » et « fontenc » ?

## L'extension « inputenc »

Depuis 2018, les versions de LaTeX n'ont plus besoin que vous fassiez le chargement de l'extension <ctanpkg:inputenc> avec l'option `utf8` :

```latex
% !TEX noedit
\usepackage[utf8]{inputenc}
```

En effet, le fichier de format effectue ce chargement.

Le codage d'entrée standard pour l'Europe occidentale avant l'adoption généralisée d'Unicode était ISO 8859--1 (plus connu sous le nom de norme « Latin-1 »). Si vous enregistrez toujours des fichiers au codage Latin-1 (ou autre), vous devrez le déclarer avec une commande telle que :

```latex
% !TEX noedit
\usepackage[latin1]{inputenc}
```

## L'extension « fontenc »

Si vous utilisez pdfLaTeX ou LaTeX sans spécifier le chargement de l'extension <ctanpkg:fontenc> avec l'option `T1` :

```latex
% !TEX noedit
\usepackage[T1]{fontenc}
```

alors LaTeX utilisera par défaut l'encodage TeX `OT1` d'origine. Dans ce cas,

- pour rédiger un document en anglais, cela ne posera pas de problème ;
- pour rédiger un document dans une autre langue (en particulier le français), la situation sera tout autre : l'encodage `OT1` n'inclut pas de lettres accentuées (obtenir les lettres accentuées demandera d'utiliser par exemple la primitive `\accent`). De plus, les mots utilisant des accents ne pourront pas faire l'objet de césure. Ce sera donc *une mauvaise idée*.
