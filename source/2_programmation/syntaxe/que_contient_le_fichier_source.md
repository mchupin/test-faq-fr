# Que doit contenir un fichier source ?

Un fichier source (`fichier.tex`) doit comprendre un certain nombre de commandes (balises) LaTeX qui vont permettre au compilateur LaTeX de construire un fichier *device independent* (DVI). La plupart des commandes LaTeX se caractérisent par le fait qu'elles commencent par une « contre-oblique » `\`, que leurs arguments obligatoires apparaissent entre accolades (`{` et `}`) et que leurs arguments optionnels apparaissent entre crochets (`[` et `]`).

## Structure minimale

La structure minimale d'un document en LaTeX est schématisée ci-dessous :

```latex
% !TEX noedit
\documentclass{classe}
\begin{document}
   Votre texte...
\end{document}
```

Pour plus de détails sur cette structure, normalement beaucoup plus développée, voir la question « [Qu'est ce que le préambule d'un document LaTeX ?](/2_programmation/syntaxe/preambule) ».

## La classe du document

La commande `\documentclass` charge les fichiers décrivant la classe d'un document, sa structure, etc. Les principales classes de document disponibles (dites « classes standard ») sont :

- <ctanpkg:article> ;
- <ctanpkg:report> ;
- <ctanpkg:book> ;
- <ctanpkg:letter>.

Il existe également un certain nombre d'options qui permettent de modifier le style par défaut d'une classe (le format de papier avec `a4paper`, la taille de la fonte avec `12pt`, etc). Ces classes et options permettent de disposer d'une structure de base pour un document, mais libre à vous de définir vos propres structures grâce aux styles offerts, aux extensions disponibles notamment sur le CTAN et/ou à vos propres commandes TeX et LaTeX.

## Structuration du document

L'intérieur d'un document de classe <ctanpkg:article>, <ctanpkg:report> ou <ctanpkg:book> est ensuite structuré grâce aux balises disponibles de type : `\part`, `\chapter` (pas pour les articles), `\section`, `\subsection`, etc.

Les lettres et les transparents font appel à d'autres structures particulières.

:::{todo} Ce dernier paragraphe appelle une révision : « *Faire les liens vers les pages traitant de ces classes.* »
:::

Les informations présentées dans ces structures peuvent être mises sous différentes formes grâce à des environnements tels que `tabular` pour les tables ou `itemize` pour les listes.

## Un exemple

L'exemple ci-dessous illustre certains concepts présentés ci-dessus et ajoute quelques commandes dont vous pourrez observer les effets.

```latex
% !TEX noedit
\documentclass[12pt]{report}

\usepackage[french]{babel}

\title{Mon premier document \LaTeX{} \\
  Qu'il est beau !}
\author{C'est moi l'auteur.}

\begin{document}

\maketitle
\tableofcontents

\part{Une partie.}
\chapter{Un chapitre.}
Texte...
\section{Une section.}
Texte...
\section{Une autre section.}
Texte...
\subsubsection{Avec une sous-section.}
Texte...
\subsubsection{Plus une autre.}
\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{|c||c|}
      \hline
      données & données \\
      \hline
    \end{tabular}
    \caption{Titre table. \label{table-}}
  \end{center}
\end{table}

\part{Une courte deuxième partie.}
Texte...

\appendix
\chapter{Et une annexe pour finir.}
Texte...
\begin{itemize}
\item bla bla 1
\item bla bla 2
\end{itemize}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```
