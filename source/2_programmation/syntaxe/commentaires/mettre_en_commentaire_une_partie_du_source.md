# Comment commenter une partie d'un fichier source LaTeX ?

## Avec le caractère %

Tout ce qui se trouve entre le caractère `%` et la fin de la ligne (inclus tous les deux) est ignoré dans le résultat produit par TeX et LaTeX.

```latex
Que va-t-on % vraiment
voir dans le document produit ?
```

Bon nombre d'éditeurs offrent la possibilité de commenter des passages. Par exemple, "(un)comment-region" avec `Emacs`.

:::{warning}
Dans ce dernier exemple, faites bien attention à utiliser le `%` pour mettre en commentaire la fin de ligne après `\ifthenelse`. Si vous l'oubliez, vous aurez une espace en trop dans le rendu final, entre « simple » et « de faire » :

```latex
\documentclass{article}
  \usepackage{ifthen}
  \pagestyle{empty}

\begin{document}
Regardez comme c'est simple
\ifthenelse{\boolean{false}}
{Ne pas oublier le \usepackage{ifthen}
pour cette commande sous \LaTeX{}}{}
de faire disparaître du texte !
\end{document}
```
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,commentaire,cacher du texte,inclure des commentaires,annoter un document LaTeX
```
