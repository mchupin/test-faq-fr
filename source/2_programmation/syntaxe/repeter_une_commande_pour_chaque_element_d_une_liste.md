# Comment répéter une action pour chaque élément d'un ensemble ?

Comme expliqué en réponse à la question («[Comment répéter quelque chose N fois?](/2_programmation/syntaxe/repeter_une_commande_n_fois) »), TeX n'est pas vraiment conçu pour les opérations de programmation « ordinaire ». On doit ainsi recourir à des astuces pour effectuer la tâche apparemment simple de répéter une opération. Cette réponse propose des solutions pour répéter une opération pour chaque élément d'un ensemble donné d'objets.

L'extension <ctanpkg:etoolbox> permet d'itérer sur une liste d'éléments séparés par des virgules, grâce à ses macros `\docsvlist` et `\forcsvlist` (dans les noms de ces macros, `csv` voulant dire *comma separated values*); elles sont bien décrites dans la [documentation du paquet](texdoc:etoolbox). L'extension <ctanpkg:datatool> gère des «bases de données» (d'après les termes de [sa propre documentation](texdoc:datatool)) et vous pouvez itérer sur les entrées d'une telle base de données à l'aide de la commande `\DTLforeach`.

L'extension <ctanpkg:forarray> définit ses propres structures de liste (`list`) et de tableau (`array`), ainsi que les commandes `\ForEach` et `\ForArray` qui permettent d'exécuter une commande pour chaque élément d'une liste ou d'un tableau, respectivement.

L'extension <ctanpkg:dowith> définit une paire de macros `\DoWith` et `\StopDoing` qui applique la macro fournie en premier argument à chaque item passé ensuite entre elles; un exemple trivial d'utilisation pourrait être :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{dowith}

\begin{document}

  \newcommand{\foo}[1]{\message{#1+}}

  \DoWith\foo a{BBB}c\StopDoing
\end{document}
```

qui produit, dans le terminal, la sortie :

```text
a+ BBB+ c+
```

Ainsi, les macros ont trouvé 3 « items », dont un entouré d'accolades. (Les espaces interpolés proviennent de la primitive `\message`).

La seule contrainte est que toutes les commandes à l'intérieur de la structure doivent être « développables » (ce qui signifie, par exemple, que vous ne pouvez pas utiliser de commandes avec des arguments optionnels).

Du même développeur que <ctanpkg:dowith>, il y a aussi l'extension <ctanpkg:commado>, qui fournit les commandes `\DoWithCSL` (appliquer une commande à chaque élément d'une liste séparée par des virgules \[*comma-separated list*\]) et `\DoWithBasesExts` (appliquer une commande à chacun des fichiers d'un ensemble, définis par un nom de base et un suffixe \[*extension*\]). L'utilisation de ces macros `\DoWith*` est également développable (si la commande appliquée aux éléments de la liste est elle-même développable).

______________________________________________________________________

*Source :* [Repeating something for each 'thing' in a set](faquk:FAQ-repeat-set)

```{eval-rst}
.. meta::
   :keywords: LaTeX,boucles,répéter une commande,appliquer une commande à un ensemble,liste de commandes
```
