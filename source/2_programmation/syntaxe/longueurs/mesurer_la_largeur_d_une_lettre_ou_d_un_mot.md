# Comment mesurer la largeur d'un caractère, d'un mot ou d'une phrase ?

- Mettez le mot dans une boîte, puis mesurez simplement la largeur de la boîte. Par exemple :

```latex
% !TEX noedit
\newdimen\stringwidth
\setbox0=\hbox{Bonjour !}
\stringwidth=\wd0

<<\,Bonjour !\,>> mesure \the\stringwidth.
```

```latex
\newdimen\stringwidth
\setbox0=\hbox{Bonjour}
\stringwidth=\wd0

<<\,Bonjour\,>> mesure \the\stringwidth.
```

Notez que si le contenu de la `\hbox` est une phrase, on n'obtiendra que sa longueur « naturelle », c'est à dire sans les ajustements des espaces inter-mots qui se produisent normalement quand la phrase est insérée dans un paragraphe. Il faurt considérer que la mesure obtenue est une approximation de la longueur réelle.

- En LaTeX, on utiliserait plutôt la macro `\settowidth` :

```latex
% !TEX noedit
\newlength{\gnat}
\settowidth{\gnat}{\textbf{Bonjour}}
```

Cet exemple donne à la variable de longueur `\gnat` la valeur de la largeur du mot « \**Bonjour*\* » écrit en gras.

______________________________________________________________________

*Source :* [Finding the width of a letter, word, or phrase](faquk:FAQ-findwidth)

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,TeX,longueur d'un mot,longueur d'une phrase,longueur d'un texte,largeur d'un caractère,macros
```
