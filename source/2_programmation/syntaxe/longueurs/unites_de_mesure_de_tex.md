# Quelles sont les unités de mesure de TeX ?

Pour TeX, il existe 11 unités de longueur, plus une qui n'existe qu'en mode mathématique :

| Unité | Nom                         | Définition                                                         | Exemple d'une longueur d'une unité               |
|-------|-----------------------------|--------------------------------------------------------------------|--------------------------------------------------|
| `sp`  | *scaled point*              | La plus petite unité de TeX. 1 `sp` = 1/65536 `pt` soit 5,4 `nm`   | ![alt](/_static/images/svg/unites_mesure_01.svg) |
| `pt`  | point d'imprimeur (*point*) | 1 `pt` = 1/72,27 `in`, environ 0,351 `mm`                          | ![alt](/_static/images/svg/unites_mesure_02.svg) |
| `bp`  | gros point (*big point*)    | 1 `bp` = 1/72 `in`                                                 | ![alt](/_static/images/svg/unites_mesure_03.svg) |
| `dd`  | didot (*Didot point*)       | 1 `dd` = 1,07 `pt`, environ 0,376 `mm`                             | ![alt](/_static/images/svg/unites_mesure_04.svg) |
| `mm`  | millimètre                  | 1 `mm` = 0,001 `m`                                                 | ![alt](/_static/images/svg/unites_mesure_05.svg) |
| `pc`  | pica                        | 1 `pc` = 12 `pt`                                                   | ![alt](/_static/images/svg/unites_mesure_06.svg) |
| `cc`  | cicero                      | 1 `cc` = 12 `dd`                                                   | ![alt](/_static/images/svg/unites_mesure_07.svg) |
| `cm`  | centimètre                  | 1 `cm` = 0,01 `m`                                                  | ![alt](/_static/images/svg/unites_mesure_08.svg) |
| `in`  | pouce (*inch*)              | 1 `in` = 25,4 `mm`                                                 | ![alt](/_static/images/svg/unites_mesure_09.svg) |
| `ex`  | « hauteur d'x »             | La hauteur d'un « `x` » dans la fonte courante                     | ![alt](/_static/images/svg/unites_mesure_10.svg) |
| `em`  | « largeur d'M »             | La largeur d'un « `M` » dans la fonte courante                     | ![alt](/_static/images/svg/unites_mesure_11.svg) |
| `mu`  | « mu »                      | 1 `mu` = 1/18 `em` (unité valable en mode mathématique uniquement) | ![alt](/_static/images/svg/unites_mesure_12.svg) |

:::{note}
C'est le concepteur de la fonte qui décide la largeur `em` et peut tout à fait décider que ce ne sera pas exactement la largeur d'un « M ».
:::

En complément de ces mesures, pdfTeX définit deux autres mesures :

| Unité | Nom          | Définition          | Exemple d'une longueur d'une unité                                                                                                                       |
| ----- | ------------ | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `nd`  | *new didot*  | 1 `nd` = 0,375 `mm` | ![alt](/_static/images/svg/unites_mesure_13.svg) |
| `nc`  | *new cicero* | 1 `nc` = 12 `nd`    | ![alt](/_static/images/svg/unites_mesure_14.svg) |

```{eval-rst}
.. meta::
   :keywords: LaTeX,unité,unités en TeX,unités de mesure, unités de longueur, cm, pt, in, ex, em
```
