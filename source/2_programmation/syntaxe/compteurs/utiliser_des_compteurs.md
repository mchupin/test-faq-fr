# Comment gérer des compteurs ?

Un certain nombre de compteurs sont prédéfinis en LaTeX. Par exemple, les compteurs de page, de chapitre, de section, de note de bas de page...

## Créer un compteur

Pour utiliser un nouveau compteur, il faut avant tout le déclarer. Cela se fait par l'intermédiaire de la commande suivante :

```latex
% !TEX noedit
\newcounter{nom}[maitre]
```

L'argument obligatoire contient le *nom* du nouveau compteur (qui est un nom ne commençant pas par une contre-oblique), et l'argument optionnel est le nom d'un compteur « maître » (ici nommé *maitre*) qui, lorsqu'il est modifié, remet à zéro le compteur nouvellement défini (voir la question « [Comment définir un compteur dépendant d'un autre compteur ?](/2_programmation/syntaxe/compteurs/compteurs_maitres_et_esclaves) »).

La définition d'un compteur s'accompagne de la création d'une commande nommée `\thecompteur`, dont le rôle est d'afficher la valeur du compteur dénommé *compteur* (cette commande peut cependant être redéfinie comme indiquée plus bas mais il vaut mieux ici consulter la question « [Comment redéfinir les commandes de compteur \\the(...) ?](/2_programmation/syntaxe/compteurs/comment_fonctionnent_les_compteurs) »).

Par défaut, la valeur initiale d'un compteur est 0, comme le montre cet exemple :

```latex
\newcounter{moutons}
Comptons les moutons : \themoutons{} moutons !
```

## Changer la valeur d'un compteur

Pour incrémenter cette valeur, on peut utiliser une des deux commandes suivantes :

```latex
% !TEX noedit
\stepcounter{compteur}
\refstepcounter{compteur}
```

La différence est que, dans le deuxième cas, le compteur incrémenté sert de compteur « de référence » pour la prochaine commande `\label`. Cette dernière prend toujours la valeur du dernier compteur qui a été incrémenté par la commande `\refstepcounter`.

Une commande permet d'augmenter un compteur d'un certaine *valeur* (un entier positif ou négatif) :

```latex
% !TEX noedit
\addtocounter{compteur}{valeur}
```

Une autre commande lui donne une *valeur* (entière) particulière :

```latex
% !TEX noedit
\setcounter{compteur}{valeur}
```

Voici un exemple d'utilisation :

```latex
\newcounter{moutons}
\stepcounter{moutons}
Comptons les moutons :
\themoutons{} mouton,
\stepcounter{moutons}
\themoutons{} moutons,
\setcounter{moutons}{4}
\themoutons{} moutons...
\alph{moutons} moutons...
\addtocounter{moutons}{-1}
Ah non, \themoutons{} moutons.
```

## Obtenir la valeur d'un compteur

Pour récupérer la valeur d'un compteur, outre la commande `\thecompteur`, on peut utiliser les commandes `\arabic`, `\roman`, `\alph` et autres décrites à la question [Quels sont les différents styles de compteur ?](/2_programmation/syntaxe/compteurs/les_differents_compteurs). Ces commandes mettent en forme différemment le compteur cité.

```latex
\newcounter{moutons}
\stepcounter{moutons}
Comptons les moutons :
\themoutons{} mouton,
\stepcounter{moutons}
% en chiffres arabes (par défaut)
\arabic{moutons} moutons,
\stepcounter{moutons}
% en chiffres romains
\roman{moutons} moutons,
\stepcounter{moutons}
% en lettres
\alph{moutons} moutons.
```

En voici un exemple plus complexe avec un compteur avec dépendance. Le compteur *section* dépend du compteur *chapter* et sa numérotation sera celle du chapitre suivi d'un point et du numéro de section mis en chiffres romains :

```latex
% !TEX noedit
\newcounter{section}[chapter]
\renewcommand{\thesection}%
             {\thechapter.\roman{section}}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,compteurs,dépendance
```
