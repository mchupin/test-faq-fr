# Comment utiliser des étiquettes comme des valeurs de compteur ?

Les étiquettes sont des sources alléchantes de « nombres » : leur utilisation la plus courante, après tout, est simplement de composer un nombre. Cependant, leur simplicité apparente est trompeuse. Ainsi, par exemple, les extensions <ctanpkg:babel> et <ctanpkg:hyperref> modifient la définition de `\ref` et `\pageref`, ce qui empêche le bon fonctionnement de la commande suivante :

```latex
% !TEX noedit
\setcounter{compte}{\ref{truc}}
```

Aussi, cette technique ne devrait pas être utilisée. D'autant qu'il existe une solution.

## Avec l'extension refcount

L'extension <ctanpkg:refcount> (écrite par l'auteur de <ctanpkg:hyperref>) fournit quatre commandes, dont l'utilisation est assez intuitive pour qui connaît les commandes usuelles de manipulation des compteurs. Par exemple :

```latex
% !TEX noedit
\usepackage{refcount}
...
\label{truc}
...
\setcounterref{compte}{truc}
```

Les trois autres commandes sont `\addtocounterref`, `\setcounterpageref` et `\addtocounterpageref`.

L'extension fournit également une commande `\getrefnumber{⟨étiquette⟩}` qui peut être utilisée là où une valeur numérique est nécessaire. Par example :

```latex
% !TEX noedit
... \footnote{blabla...\label{unenote}}
...
\footnotemark[\getrefnumber{unenote}]
```

Cela vous permet de faire apparaître plusieurs fois un même renvoi à une note de bas de page. Bien entendu, l'extension met également à disposition une commande `\getpagerefnumber`.

Accesoirement, ces commandes pourraient être utilisées par une personne déterminée à ne pas utiliser <ctanpkg:changepage> pour déterminer si [une page est paire ou impaire](/3_composition/texte/pages/savoir_si_on_est_sur_une_page_paire_ou_impaire). Toutefois, il semble peu intéressant de ne pas recourir à une solution clé en main.

______________________________________________________________________

*Source :* [Using labels as counter values](faquk:FAQ-labelcount)

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programming
```
