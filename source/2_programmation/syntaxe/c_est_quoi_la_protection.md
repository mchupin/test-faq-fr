# À quoi sert la « protection » ?

Parfois, LaTeX enregistre des données qu'il relira plus tard. Ces données se retrouvent souvent dans l'argument d'une commande et elles sont, par extension, dénommées « arguments mouvants » (*moving arguments*). Le terme « mouvant » vient de l'idée que l'enregistrement des données les déplace. Les cas les plus courants sont :

- les arguments qui peuvent entrer dans la table des matières, la liste des figures, etc. à savoir les données écrites dans un fichier auxiliaire et lues ultérieurement ;
- les données qui peuvent apparaître dans les titres ou les bas de page. Les titres de section et les légendes des figures sont les exemples les plus marquants.

Une liste complète de ces arguments mouvants est donnée dans le livre de Leslie Lamport (voir « [Que lire sur LaTeX ?](/1_generalites/documentation/livres/documents_sur_latex) »).

## Ce qui se passe dans les coulisses

Les commandes dans les arguments mouvants sont normalement développées pendant le processus de sauvegarde. Parfois, ce développement génère un code TeX invalide qui s'affiche soit pendant ce développement, soit lorsque le code est à nouveau traité. Protéger une commande, en utilisant `\protect\cmd` dit à LaTeX de sauvegarder `\cmd` sous la forme `\cmd`, sans le développer du tout. Dès lors, se définissent deux types de commandes :

- une « commande fragile » se développe en code TeX invalide pendant le processus de sauvegarde ;
- une « commande robuste » se développe en code TeX valide pendant le processus de sauvegarde.

## La fragilité... de la fragilité

Le livre de Lamport indique pour chaque commande LaTeX si elle est « robuste » ou « fragile ». Il précise également que chaque commande avec un argument optionnel est fragile. Toutefois, la liste donnée n'est pas fiable, pas plus que l'assertion sur les arguments optionnels : ces indications peuvent avoir été vraies dans les premières versions de LaTeX mais ne le sont plus nécessairement :

- certaines commandes fragiles, telles que `\cite`, ont été rendues robustes dans les révisions ultérieures de LaTeX ;
- certaines commandes, telles que `\end` et `\nocite`, sont fragiles même si elles n'ont pas d'arguments optionnels ;
- la création de commande avec un argument optionnel par le biais de `\newcommand` ou de `\newcommand*` aboutit désormais à une commande robuste (bien que les commandes sans arguments optionnels puissent toujours être fragiles si elles font des actions elles-mêmes fragiles) ;
- un auteur d'extension peut toujours créer des commandes robustes avec des arguments optionnels dans le cadre de son extension ;
- certaines commandes robustes sont redéfinies par des extensions pour devenir fragiles (telle la commande `\cite`).

Par ailleurs, « cacher » une commande fragile dans une autre commande n'a aucun effet sur la fragilité. Donc, si `\truc` est fragile, et que vous écrivez :

```latex
% !TEX noedit
\newcommand{\machin}{\truc}
```

alors `\machin` est aussi fragile. Il existe cependant une variante de `\newcommand`, à savoir `\DeclareRobustCommand`, qui *crée toujours* une commande robuste (qu'elle ait ou non des arguments optionnels). La syntaxe de `\DeclareRobustCommand` est quasiment identique à celle de `\newcommand`, et si vous effectuez l'astuce d'encapsulation ci-dessus comme :

```latex
% !TEX noedit
\DeclareRobustCommand{\machin}{\truc}
```

alors `\machin` est robuste.

Enfin, l'extension <ctanpkg:makerobust> définit `\MakeRobustCommand` pour rendre une commande en robuste. Avec cette extension, l'encapsulation ci-dessus peut simplement être remplacée par :

```latex
% !TEX noedit
\MakeRobustCommand\truc
```

Ceci fait, `\truc` est robuste. L'utilisation de cette extension peut être envisagée si vous avez beaucoup de commandes fragiles à utiliser dans des arguments mouvants.

## En conclusion

Au final, la situation actuelle est assez déroutante. Personne ne la trouve satisfaisante : l'équipe LaTeX elle-même a d'ailleurs commencé à supprimer le besoin de protection de certaines fonctionnalités. Cependant, les méthodes disponibles actuellement dans LaTeX rendent ces modifications coûteuses. L’objectif d’éliminer tout besoin de « protection » reste donc un objectif de long terme.

______________________________________________________________________

*Source :* [What's the reason for "protection"?](faquk:FAQ-protect)

```{eval-rst}
.. meta::
   :keywords: LaTeX,commandes,protection,fragile,robuste,argument mouvant
```
