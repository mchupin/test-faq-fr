# À quoi sert la commande « \\savebox » ?

- Elle sert à définir des boîtes afin de les réutiliser ultérieurement. Une boîte doit être déclarée avant d'être utilisée. Par exemple :

Utilisation de `\savebox` :

```latex
% !TEX noedit
\newsavebox{\faq}
\savebox{\faq}{F.A.Q. de
              {\ttfamily fr.comp.text.tex}}
Ceci est la \usebox{\faq}.
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,enregistrer du contenu LaTeX,enregistrer une box,utiliser une box LaTeX
```
