# Comment visualiser des paramètres ?

- La commande `\show` permet d'afficher, lors de la compilation, la « valeur » d'une commande. La commande `\showthe` permet d'afficher la valeur d'un compteur, d'une longueur... Par exemple,

```latex
% !TEX noedit
\show\LaTeX
\showthe\baselinestretch
```

Après l'exécution d'une de ces commandes, LaTeX se met dans un état d'attente, afin que le message soit lisible. De plus, la compilation se termine par une erreur, même si le reste du document compile sans problème. Pour éviter cela, on pourra préférer la commande `message`, qui affiche un message à l'écran mais ne s'arrête pas. L'inconvénient étant que le message sera perdu au milieu des autres messages.

```latex
% !TEX noedit
\message{La valeur de baselinestretch est
\the\baselinestretch}
```

- Le package <ctanpkg:layout> fournit une commande, nommée `\layout`, qui affiche la valeur des dimensions de la page (`textwidth`, `textheight`, `paperwidth`, `paperheight`...)
- Le package <ctanpkg:showdim> permet d'afficher, dans le document, la valeur d'une dimension.
- Les packages <ctanpkg:showkeys> et <ctanpkg:showlabels> permettent d'afficher, dans la marge ou dans le texte, les labels ou les clefs de citations définis.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
