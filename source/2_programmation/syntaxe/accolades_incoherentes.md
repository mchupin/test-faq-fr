# Pourquoi mes crochets ne s'associent pas bien ?

TeX et LaTeX ont un mécanisme de bas niveau pour faire correspondre les accolades dans le texte d'un document. Cela signifie que vous pouvez taper quelque chose comme :

```latex
% !TEX noedit
\section{Tout \emph{semble} aller bien.}
```

Dans ce cas, la première accolade (pour l'argument de `\section`) sera mise en correspondance avec la dernière accolade, et la paire interne d'accolades (pour l'argument de `\emph`) sera également mise en correspondance.

Cependant, LaTeX a pour convention de mettre les arguments optionnels entre crochets, comme dans :

```latex
% !TEX noedit
\section[Bien]{Tout \emph{semble} aller bien.}
```

Les crochets ne suivent pas les règles des accolades, malgré la similitude apparente de leur utilisation. En conséquence, des cas simples comme le suivant ne vont pas fonctionner comme attendu :

```latex
% !TEX noedit
\section[Tout [semble] aller bien.]{Tout \emph{semble} aller bien.}
```

La commande `\section` comprend alors que :

- son argument optionnel se compose de « `Tout [semble` » ;
- son argument principal prend le caractère unique « `a` », celui du premier « `aller` », comme argument ;
- et, ce qui reste, « `ller bien.]{Tout \emph{semble} aller bien.}` » passe dans le texte...

Heureusement, les mécanismes de balayage de TeX nous aident en acceptant la syntaxe `{]}` pour « masquer » le crochet fermant du mécanisme de balayage utilisé par LaTeX. Aussi, pour bien rédiger l'exemple ci-dessus, il faut écrire :

```latex
% !TEX noedit
\section[Tout {[semble]} aller bien.]{Tout \emph{semble} aller bien.}
```

En plus de celle des crochets, LaTeX dispose d'une autre syntaxe d'argument, encore moins régulière, où l'argument est entre parenthèses. Elle est souvent utilisée pour indiquer des coordonnées dans des fonctions graphiques, tout particulièrement avec <ctanpkg:pstricks> :

```latex
% !TEX noedit
\put(1,2){Bien !}
```

Ce mécanisme présente les problèmes de correspondance entre parenthèses ouvrantes et fermantes. Cependant, ces problèmes se posent rarement car les arguments ne contiennent généralement pas de texte. Si cela devait survenir, la même solution (mettre les caractères posant difficulté entre accolades) résoudrait le problème.

______________________________________________________________________

*Source :* [My brackets don't match](faquk:FAQ-matchbrak)

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
