# Comment améliorer la qualité d'un code LaTeX ?

Avant tout chose, la lecture du tutoriel [Liste des péchés des utilisateurs de LaTeX2e](ctanpkg:l2tabu-french) (mentionné dans la question portant sur les [tutoriels sur LaTeX](/1_generalites/documentation/documents/tutoriels/tutoriaux)) est recommandée.

Cependant, il est souvent difficile de se souvenir systématiquement des choses que vous ne devez *pas* faire, quand il y a déjà tant à savoir sur ce que vous devez faire. Des automatisations sont heureusement possibles !

## Avec l'extension nag

L'extension <ctanpkg:nag> vous permet de faire un ensemble configurable de vérifications à votre document, lorsque vous le compilez avec LaTeX. Vous aurez alors des messages comme :

```latex
% !TEX noedit
Package nag Warning : Command \bf is an old LaTeX 2.09 command.
(nag)                Use \bfseries or \textbf instead on input line 30.
```

L'extension fournit d'ailleurs un fichier d'exemple illustrant la plupart des erreurs que vous pourriez faire (le cas ci-dessus étant l'une d'elles).

Notez bien que <ctanpkg:nag> vous alerte sur *d'éventuelles* erreurs de programmation. Vous ne devez pas oublier qu'il ne fait que commenter le *style* du code : n'imaginez pas qu'une erreur vue par <ctanpkg:nag> condamne votre code. Notez plutôt le problème et essayez de vous entraîner à ne pas faire la même chose la prochaine fois.

## Avec le programme lacheck

Le programme <ctanpkg:lacheck> analyse votre source et la commente. Sa vision de ce qui est « mauvais » est *très* subjective (comme le dit sa [documentation](texdoc:lacheck)) mais elle peut être utile.

______________________________________________________________________

*Source :* [The quality of your LaTeX](faquk:FAQ-latexqual)

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,code,programmation
```
