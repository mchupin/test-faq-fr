# Comment bien nommer ses commandes et environnements ?

## Les caractères autorisés dans les noms

Les commandes classiques sont de deux types :

- les caractères de contrôle. Dans ce cas, la contre-oblique est suivie d'un unique caractère qui n'est pas une lettre (par exemple `\!`) ;
- les mots de contrôle. Là, la contre-oblique est suivie d'une suite de lettres et le nom de la commande se termine au premier caractère non-lettre (typiquement un espace ou une accolade).

TeX reconnaît les 52 caractères de l'alphabet (majuscules et minuscules sont distinctes) comme des lettres. Les caractères accentués, les chiffres ou des caractères comme le «@'' » ne sont pas des lettres. Il reste possible de demander à TeX de changer sa notion de lettre : la question « [À quoi servent « \\makeatletter » et « \\makeatother » ?](/2_programmation/macros/makeatletter_et_makeatother) » présente le cas courant du «@'' ».

Pour les noms d'environnements, c'est plus simple : les caractères autorisés sont les lettres et le caractère « `*` » et ce, quelle que soit la longueur. Vous pouvez essayer d'utiliser d'autres caractères comme des espaces et cela marchera sans doute. Cependant, il n'y a aucune garantie que cela marchera encore à l'avenir et il n'est donc pas conseillé de le faire.

## Le sens du nom

Le deuxième élément à prendre en compte est le sens du nom. Cette recommandation est assez générale dans le monde informatique. Il faut qu'il soit compréhensible, quitte à ce qu'il soit un peu long : votre source gagnera en lisibilité, ce qui sans doute plus important que d'économiser quelques frappes de touches. Par ailleurs, choisissez toujours un nom qui se rapporte au sens de la commande et pas à sa mise en forme (par exemple, `\lebesgue` pour la mesure de Lebesgue, indépendamment du fait que vous la notiez `lambda` ou autre chose).

______________________________________________________________________

*Source :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#names>

```{eval-rst}
.. meta::
   :keywords: LaTeX,commande,environnement,lettre,nom
```
