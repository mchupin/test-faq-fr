# Comment enrichir la définition d'une commande déjà existante ?

- On peut ajouter des commandes dans la définition d'une macro en combinant l'utilisation de `\let` et `\def`. Par exemple :

```latex
% !TEX noedit
%% Incorrect : LaTeX entre dans une boucle
\def\LaTeX{\LaTeX\xspace}
%% Correct :
\let\oldLaTeX\LaTeX
\def\LaTeX{\oldLaTeX\xspace}
```

- Le package <ctanpkg:babel> fournit une commande nommée `\addto`, qui ajoute son deuxième argument à la fin de la commande passée en premier argument.

Par exemple :

```latex
% !TEX noedit
\addto{\LaTeX}{\xspace}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
