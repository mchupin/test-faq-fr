# Comment n'afficher que les deux derniers chiffres de l'année ?

Pour cela, vous pouvez définir une commande `\shortyear` :

```latex
% !TEX noedit
\newcommand{\ignoretwo}[2]{}
\newcommand{\shortyear}{\expandafter\ignoretwo\number\year}
```

Observez l'astuce utilisée :

- définir d'abord une commande `\ignoretwo` qui prend deux arguments et n'en fait rien.
- puis utiliser cette commande pour faire disparaître les deux premiers chiffres de l'année (ils lui sont passés comme arguments) et laisser les deux derniers.

```{eval-rst}
.. meta::
   :keywords: LaTeX,format de date,année sur deux chiffres
```
