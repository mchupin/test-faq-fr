# Pourquoi éviter d'utiliser des primitives TeX ou des commandes Plain TeX avec LaTeX ?

Les commandes LaTeX ont tendance à être plus complexes et à s'exécuter plus lentement que toute commande Plain TeX (ou primitive) qu'elles remplacent. Il est donc tentant d'utiliser ces commandes plus fondamentales. Néanmoins, la règle générale reste que vous devriez vous servir uniquement de commandes LaTeX.

La seule exception porte sur les cas où vous êtes sûr de connaître les différences entre les deux commandes et que vous savez que la version Plain TeX répond très précisément à votre besoin. Aussi, de préférence et par exemple :

- utilisez `\mbox` à la place de `\hbox` à moins que vous ne sachiez que les fonctionnalités additionnelles fournies par LaTeX dans `\mbox` causeraient des problèmes dans votre application ;
- utilisez `\newcommand` (ou l'une des commandes apparentées) à moins que vous n'ayez besoin de l'une des constructions qui ne peuvent pas être réalisées sans l'utilisation de `\def` (ou de commandes proches).

Voici quelques autres exemples d'écarts notables.

## Espacement

En règle générale, toute commande de texte LaTeX commence un nouveau paragraphe si besoin est. Mais ce n'est pas le cas des commandes Plain TeX, ce qui peut être source de confusion.

Par ailleurs, les commandes `\smallskip`, `\medskip` et `\bigskip` existent à la fois dans Plain TeX et LaTeX, mais se comportent légèrement différemment : elles terminent le paragraphe courant dans Plain TeX, mais dans LaTeX.

## Filets

La commande `\line` fait partie des commandes graphiques pour
LaTeX, alors qu'elle est définie comme `\hbox to \hsize` dans Plain
TeX. Il faut noter ici qu'il n'y a pas d'équivalent direct pour les
utilisateurs de la commande Plain TeX dans LaTeX ;
seul un équivalent interne existe avec la commande `@line`.

## Modes mathématiques

La commande `\(`...`\)` ne diffère pas vraiment de la primitive `$` ... `$`, sauf pour vérifier que vous n'essayez pas d'ouvrir un environnement mathématique alors que vous êtes déjà dans un tel environnement, ou d'essayer d'en fermer un quand vous n'y êtes pas. Cependant, `\[` ... `\]` a une différence plus significative avec `$$` ... `$$` : la version primitive, utilisée dans LaTeX, peut manquer l'effet de l'option de classe `fleqn`. De fait, [cette dernière n'est clairement pas recommandée](/4_domaines_specialises/mathematiques/arguments_contre_les_doubles_dollars) !

## Gestion des polices

La gestion des polices est, bien sûr, très différente dans Plain TeX et LaTeX. Cependant, la commande de chargement des polices de TeX (`\font\truc=⟨nom de la fonte⟩`) et son équivalent LaTeX (`\newfont`) doivent être *toutes les deux* évitées dans la mesure du possible. Elles ne sont sûres que dans les contextes les plus triviaux et sont des sources potentielles de grande confusion dans de nombreuses circonstances. Une discussion plus approfondie de ce problème peut être trouvée dans « [Pourquoi éviter d'utiliser la commande \\newfont ?](/3_composition/texte/symboles/polices/pourquoi_ne_pas_utiliser_newfont) ».

______________________________________________________________________

*Source :* [Using Plain or primitive commands in LaTeX](faquk:FAQ-plninltxstar)

```{eval-rst}
.. meta::
   :keywords: LaTeX,latex,macros,programming
```
