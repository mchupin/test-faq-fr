# Pourquoi préférer `\newcommand` à `\def` ?

Une activité essentielle dans l'usage de (La)TeX est la définition de macros. Pour cela, LaTeX propose des méthodes qui lui sont propres (`\newcommand`, `\renewcommand`, etc.) en plus des méthodes de TeX qui restent disponibles. Comme souvent, on a intérêt sauf raison particulière à préférer les méthodes de LaTeX pour définir les commandes.

La première raison est que LaTeX opère beaucoup d'opérations de contrôle pour vous empêcher de tout casser par maladresse. En l'occurrence, le danger est de redéfinir sans le savoir une commande qui existe déjà (vous ne connaissez sans doute pas par coeur la liste des 300 primitives de TeX et encore moins celle des commandes LaTeX). S'il s'agit d'une commande ayant un rôle essentiel (comme `\par` ou `\output`), les conséquences peuvent être catastrophique et difficiles à comprendre.

Pour cela, LaTeX distingue la définition d'une nouvelle commande qui se fait avec `\newcommand` de la redéfinition qui se fait avec `\renewcommand`. Par ailleurs, il faut savoir qu'en LaTeX il est dangereux de définir des commandes dont le nom commence par `\end` car elles sont utilisée en interne par les environnements. Là aussi, `\newcommand` se charge de la vérification et vous évite des problèmes.

Par ailleurs, une raison un peu plus personnelle de préférer `\newcommand` est sa syntaxe plus simple pour la spécification du nombre d'arguments, et en un sens plus puissante avec la possibilité de rendre optionnel le premier argument. Concernant les arguments, `\def` offre d'autres possibilités, comme les arguments délimités, dont on a rarement besoin sauf pour des techniques un peu avancées.

Dans les rares cas où on a vraiment besoin de \\def (ou d'autres commandes TeX comme `\let` ou `\edef`), un usage prudent est de faire précéder la définition de la macro (par exemple `\def\macro`) par un `\newcommand\macro{}` pour vérifier qu'on est bien autorisé à définir `\macro`.

*Archived copy:* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#defvsnc>
