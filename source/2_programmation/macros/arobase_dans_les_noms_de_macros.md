# À quoi sert « @ » dans les noms de commandes ?

Ce sujet est développé dans la question « [À quoi servent « \\makeatletter » et « \\makeatother » ?](/2_programmation/macros/makeatletter_et_makeatother) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,arobase,at dans les noms de commandes,A rond,commandes internes de LaTeX
```
