# À quoi servent les commandes « `\newcommand` » et « `\renewcommand` » ?

- Ces deux commandes servent à (re)définir des commandes. Il est nécessaire de savoir si la macro que l'on définit existait ou pas, afin de savoir laquelle de ces deux commandes utiliser.

La syntaxe est la même pour les deux commandes, à savoir :

```latex
% !TEX noedit
\newcommand{nom}[nbparam][defaut]{definition}
```

Le premier argument obligatoire est le nom de la commande, qui commencera obligatoirement par une contre-oblique. Le premier argument optionnel indique, s'il y en a, le nombre d'arguments que prendra la commande que l'on définit. Si le deuxième argument optionnel est présent, il indique que le premier argument de la commande nouvellement définie est optionnel, et donne sa valeur s'il était absent. Le deuxième argument obligatoire de `\newcommand` est la définition proprement dite de la commande. Dans cette définition, on peut faire référence aux arguments avec le symbole `#` : `#1` indique le premier argument (même s'il est optionnel), `#2` le deuxième, jusqu'à `#9`. Il ne peut y avoir plus de 9 arguments.

`\newcommand` vérifie qu'il n'existe pas déjà une commande portant le même nom que ce que vous lui demandez. Dans le cas où la commande existe déjà, il émet une erreur et la compilation du document s'arrête.

Pour **re**définir une commande, utilisez `\renewcommand`, qui remplacera (sans erreur) la définition existante.

:::{tip}
Si l'on veut définir une commande uniquement dans le cas où elle n'existe pas, on utilisera la commande `\providecommand`. Si la commande existait déjà, elle n'est pas modifiée.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,définir de nouvelles commandes,définir une macro,macrocommandes,écrire des macros LaTeX
```
