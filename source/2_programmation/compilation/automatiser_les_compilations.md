# Comment automatiser les compilations LaTeX ?

Quand un document comporte des références internes (avec `\label` et `\ref`), un index, une bibliographie, etc., il est nécessaire de compiler plusieurs fois le fichier, et éventuellement d'appeler des outils externes tels que `bibtex` ou `makeindex`.

Ce processus peut être géré automatiquement :

## Par un script

### Avec l'extension « latexmk »

Pendant longtemps, la seule extension s'occupant de ce problème a été <ctanpkg:latexmk>, un script Perl qui analyse les dépendances de votre source LaTeX, exécute `BibTeX` ou `makeindex` s'il constate que leurs données en entrée (des parties du fichier `.aux` ou du fichier `.idx`, respectivement) ont changé, et ainsi de suite.

Il fournit toujours une excellente solution et ses versions actuelles supportent XeLaTeX et lui permettent de fonctionner comme un [système WYSIWYG](/1_generalites/bases/wysiwyg), en mettant à jour la sortie dès que le document est modifié.

En général, `latexmk` a été installé en même temps que LaTeX, et vous avez juste à exécuter la commande :

```bash
latexmk -pdf nom_du_fichier_a_compiler
```

L'option `-pdf` indique que vous souhaitez obtenur un fichier PDF.

Pour nettoyer les fichiers intermédiaires, utilisez l'option `-c` (pour *clean*) :

```bash
latexmk -c
```

### Avec l'extension « arara »

Un concurrent sérieux et récent est <ctanpkg:arara>, écrit en Java. Selon sa [documentation](texdoc:arara), il est basé sur des « règles » et des « directives ». Son objectif est de déterminer ce qu'il faut faire à partir d'instructions explicites dans le code-source du document, plutôt que de sources secondaires telles que l'analyse du fichier journal. Cette extension est recommandée par un bon nombre d'experts.

### Avec l'extension « try »

Le script Python <ctanpkg:try> est encore plus récent et présente une structure similaire à <ctanpkg:arara> : il prend aussi ses instructions dans le code-source du document.

### Avec l'extension « AutoLaTeX »

Les scripts de l'extension <ctanpkg:AutoLaTeX> suivent des idées similaires. La documentation, un fichier `README`, correspond en fait à une page de manuel de type Unix et montre une grande attention aux détails du processus de production de documents.

### Avec le script « mk »

Le script <ctanpkg:mk> (apparemment, connu également sous le nom de `latex_maker`) génère le même type de fonctionnalité et est pensé pour fonctionner avec un autre script de l'auteur appelé `vpp` (qui affiche et imprime des fichiers PostScript/PDF).

### Avec le programme « texify »

Les utilisateurs du système MiKTeX peuvent utiliser l'application `texify` de ce système. Il traite des fonctionnalités de base de LaTeX, y compris la génération d'une bibliographie et d'un index. Il ne va cependant pas plus loin (évitant les éléments comme les bibliographies multiples, les index, les listes de terminologie, etc.), vous laissant le soin de recourir à <ctanpkg:AutoLaTeX>.

### Avec le programme « texi2dvi »

Le [système texinfo](/1_generalites/glossaire/qu_est_ce_que_texinfo) est livré avec un utilitaire similaire appelé `texi2dvi`, capable de « convertir » des fichiers LaTeX ou <ctanpkg:texinfo> en DVI (ou en PDF, en utilisant `pdfTeX`).

## Depuis un éditeur de texte

Pour les utilisateurs de l'éditeur `emacs`, le mode `AucTeX` indique les étapes successives à effectuer pour compiler un document.

## Avec un « Makefile »

Si vous êtes un développeur `C` ou Java, il doit vous sembler naturel de souhaiter utiliser `Make` aussi pour des documents LaTeX. Mais le processus de compilation par LaTeX est en fait difficile à exprimer dans un simple graphe de dépendances comme le fait `make`. En effet, LaTeX doit généralement être exécuté plusieurs fois pour différentes raisons et le simple fait que le fichier final soit plus récent que les fichiers intermédiaires ne garantit pas qu'il soit à jour.

C'est pour cela que les outils mentionnés ci-dessus ont été écrits. Ils lisent les fichiers `.log` et `.aux` produits par LaTeX pour décider de relancer une compilation si nécessaire, ou de lancer `bibtex`, `mkindex`...

Mais il y a [des essais avec Make,](/2_programmation/compilation/ecrire_un_makefile_pour_compiler_mon_document_latex) que vous pouvez tester.

______________________________________________________________________

*Sources :*

- [Makefiles for LaTeX documents](faquk:FAQ-make),
- [XeLaTeX et Latexmk](https://geekographie.maieul.net/206).

```{eval-rst}
.. meta::
   :keywords: LaTeX,makefile,latexmk,compilation
```
