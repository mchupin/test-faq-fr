# Comment compiler un document LaTeX en ligne ?

Deux grandes réponses :

- Si vous êtes un rédacteur de documents LaTeX, vous cherchez probablement un **éditeur qui vous permette d'écrire votre code LaTeX dans un navigateur web**;
- Si vous êtes un développeur de services en lignes, ou d'applications pour smartphones/tablettes, vous cherchez sans doute une **API vous permettant de compiler** un document sur un serveur distant, sans intervention humaine.

## Édition de documents LaTeX par le web

### Sites web commerciaux

Ces sites web proposent une interface pour éditer des documents LaTeX, les partager entre plusieurs rédacteurs (travail collaboratif) et les compiler pour obtenir un PDF de façon presque interactive :

- <https://www.overleaf.com/>
- <https://latexbase.com/>
- <https://papeeria.com/>
- <https://www.verbosus.com/>, qui propose une *app* pour smartphone et tablette.

Ils ont le plus souvent une offre gratuite (avec des limitations), mais restent des outils commerciaux.

D'autres liens disponibles sur le [TeXblog](https://texblog.net/latex-link-archive/online-compiler/) de Stefan Kottwitz.

### Sites web libres

- [LearnLaTeX](https://www.learnlatex.org/fr/) est un site d'autoformation à LaTeX. Son interface permet de compiler les exemples de code directement dans votre navigateur web et d'observer le résultat. Même si ce n'est pas son but premier, vous pouvez parfaitement y coller votre propre code LaTeX pour le compiler en ligne.
- Le [code-source d'Overleaf est open-source](https://github.com/overleaf/clsi), ce qui en permet l'installation sur n'importe quel serveur.

:::{todo} Y en a-t-il des instances ouvertes à tous sur des serveurs académiques ou associatifs ?
:::

- [L4T (LaTeX for technics) fournit une interface web](https://www.latex4technics.com/) pour éditer des formules mathématiques, et chercher des exemples dans une bibliothèque de code (plus de 235000 exemples disponibles en juillet 2020).

## API de compilation

- [LaTeX-on-HTTP](https://github.com/YtoTech/latex-on-http)

Par exemple en exécutant cette requête :

```html
https://latex.ytotech.com/builds/sync?content=\documentclass{article} \begin{document} Hello World Latex-on-HTTP \end{document}
```

vous devriez [obtenir le document PDF résultant.](https://latex.ytotech.com/builds/sync?content=documentclass%7Barticle%7D%20begin%7Bdocument%7D%20Hello%20World%20Latex-on-HTTP%20end%7Bdocument%7D)

- [TeXlive.net](https://texlive.net/) est le service de compilation utilisé notament par [LearnLaTeX](https://www.learnlatex.org/fr/). Il a également été appelé «LaTeX CGI server». Il est librement utilisable, il suffit de lire [la documentation de son API](https://davidcarlisle.github.io/latexcgi/) (en anglais). Vous pouvez également [l'installer sur votre propre serveur](https://github.com/davidcarlisle/latexcgi).
- [LaTeX.Online](https://latexonline.cc/), dont le [code-source](https://github.com/aslushnikov/latex-online) est disponible.

### MiniLaTeX

- [Le projet MiniLaTeX](https://minilatex.io/), de James Carslon, vise à permettre les auteurs de sites web d'écrire directement en LaTeX. MathJax s'occupe déjà des formules, mais laisse de côté tout le reste : sections, tableaux, références croisées, hyperliens... MiniLaTeX vise à corriger ça.
- [Une démonstration est disponible en ligne](https://demo.minilatex.app/),
- [Vous pouvez dès à présent l'utiliser sur vos sites web](https://minilatex.lamdera.app/).

### Conversion TeX/MathML

[MathTran](https://jonathanfine.wordpress.com/mathtran/), de Jonathan Fine, est (était?) un service web de transformation de formules mathématiques :

- conversion de formules TeX en images,
- traduction de formules TeX en [MathML](wpfr:MathML),
- traduction de formules MathML en TeX.

[Son code source est open source](https://sourceforge.net/projects/mathtran/) (dernière mise à jour en 2013).

______________________________________________________________________

*Sources :*

- <https://tex.stackexchange.com/questions/3/compiling-documents-online>

```{eval-rst}
.. meta::
   :keywords: LaTeX,internet,compilation,en ligne,serveur LaTeX,serveur de compilation,tablette,téléphone portable,smartphone
```
