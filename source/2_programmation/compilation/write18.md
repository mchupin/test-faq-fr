# Comment lancer un sous-processus pendant la compilation ?

Vous avez peut-être constaté que LaTeX vous parle de la commande `\write18` au début de chaque compilation. Cette commande sert à exécuter un sous-programme à partir de LaTeX. Voici comment :

## Que fait la commande « \\write18 » ?

La primitive TeX [\\write](/5_fichiers/ecrire_un_fichier_texte_a_partir_de_tex) est utilisée pour écrire dans différents « flux » de fichiers. TeX fait référence à chaque fichier ouvert par un numéro, non par un nom de fichier. À l'origine, TeX écrivait dans un fichier connecté à un flux numéroté de 0 à 15. Plus récemment, un « flux 18 » spécial a été implémenté : il n'écrit pas dans un fichier mais indique à TeX de demander au système d'exploitation de faire quelque chose. Pour exécuter une commande, il faut la placer comme argument de `\write18`. Ainsi, pour exécuter l'utilitaire `epstopdf` sur un fichier dont le nom est stocké sous la forme `\epsfilename`, nous écririons :

```latex
% !TEX noedit
\write18{epstopdf \epsfilename}
```

Lorsque vous utilisez par exemple l'extension <ctanpkg:epstopdf> (elle-même appelée quand vous utilisez <ctanpkg:pdftricks> pour utiliser du code [PStricks](texdoc:pstricks) avec pdfLaTeX), l'opération d'écriture « des flux » est cachée et vous n'avez pas à vous soucier de la manière exacte dont elle est faite.

## Cela pose-t-il des problèmes ?

Oui, il y a un problème de sécurité. Si vous téléchargez du code TeX ou LaTeX à partir d'Internet, il pourrait très bien contenir des commandes (peut-être cachées) pouvant nuire à votre ordinateur (comme supprimer tous les fichiers de votre disque dur ou envoyer son contenu à un serveur distant). Face à ce problème, MiKTeX et TeX Live ont, depuis un certain temps, désactivé `\write18` par défaut. Pour la réactiver, les deux distributions prennent en charge un argument supplémentaire lors du démarrage de TeX à partir de la ligne de commande :

```bash
(pdf)(la)tex --shell-escape ⟨file⟩
```

Le problème de cette méthode vient du fait que beaucoup d'utilisateurs utilisent TeX et LaTeX par le biais d'un éditeur graphique. Aussi, pour utiliser `\write18` avec un fichier, les paramètres de l'éditeur doivent être modifiés. Mais il faut penser à modifier ces paramètres après le traitement du fichier : sans cela, les bénéfices de la protection d'origine sont perdus...

Dans leurs versions récentes, ces distributions (MiKTeX à partir de la version 2.9 et TeX Live à partir de la version 2010) contournent ce problème en proposant une version « limitée » de `\write18`, prête à l'emploi. L'idée est de n'autoriser qu'une liste prédéfinie de commandes (par exemple, BibTeX, `epstopdf`, TeX lui-même, etc.). Hors de cette liste, toute commande (telle la suppression de fichiers) doit encore être autorisé par l'utilisateur. Cela semble être un bon équilibre : la plupart du temps, la plupart des gens n'auront pas du tout à se soucier de `\write18`, mais il sera disponible pour <ctanpkg:epstopdf> et autres.

Notez que le système TeX peut vous dire que le mécanisme est en cours d'utilisation, en indiquant au démarrage :

```
This is pdfTeX, Version 3.14159265-2.6-1.40.21 (TeX Live 2020) (preloaded format=pdflatex)
 restricted \write18 enabled.
```

## Comment récupérer la sortie du sous-processus ?

Dans son usage le plus commun, le sous-processus lancé depuis LaTeX va produire un résultat (code LaTeX, tableau de données...) que vous voudrez récupérer pour l'utiliser dans votre document.

On peut imaginer passer par des fichiers temporaires, que l'on lira avec la commande `\input` :

```latex
% !TEX noedit
\immediate\write18{ls *.data > listefichiers.tmp}
\input{listefichiers.tmp}
```

Ceci permet éventuellement de récupérer plusieurs fichiers.

Si le sous-processus envoie ses résultats sur sa sortie standard, il existe une syntaxe simple pour les récupérer sans passer par un fichier intermédiaire (ajoutée vers 2013 aux différents moteurs TeX) :

```latex
% !TEX noedit
\input|"ls -1 *.data"
```

Ceci inclura la sortie de la commande « `ls -1 *.data` » dans votre document.

:::{important}
Il reste indispensable d'utiliser l'option `-``-shell-escape` du compilateur pour autoriser le lancement du sous-processus, sinon vous aurez une erreur ressemblant à ceci :

```
runpopen command not allowed : ls

! I can't find file `"|ls -1 *.data"'.
l.4 \input|"ls -1 *.data"
```
:::

:::{note}
La commande `\immediate` fait que le `\write18` est exécuté immédiatement. Sinon, il ne le serait qu'au moment où la page en cours est envoyée vers la sortie (ce comportement par défaut de `\write` permet de n'écrire des fichiers externes qu'une fois que les numéros des pages sont fixés).
:::

______________________________________________________________________

*Sources :*

- [Spawning programs from (La)TeX : « \\write18 »](faquk:FAQ-spawnprog),
- [What are « \\immediate » & « \\write18 » and how does one use them?](https://tex.stackexchange.com/questions/20444/what-are-immediate-write18-and-how-does-one-use-them)
- [Documentation de la commande « \\write »](http://tug.ctan.org/info/latex2e-help-texinfo-fr/latex2e-fr.html#g_t_005cwrite) (*Manuel de référence officieux de LaTeX2ε*, version française).

```{eval-rst}
.. meta::
   :keywords: LaTeX,write18,lancer un sous-programme,lancer un programme,sous-processus,exécuter une commande
```
