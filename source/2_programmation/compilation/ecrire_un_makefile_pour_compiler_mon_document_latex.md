# Comment écrire un « Makefile » pour mes documents LaTeX ?

Si vous êtes un développeur `C` ou Java, vous utilisez sans doute des outils tels que [Make](wpfr:Make) pour automatiser la compilation de vos programmes. Ces logiciels s'occupent de lancer toutes les étapes de compilation les unes après les autres, de la façon la plus efficace possible, en fonction des modifications que vous avez apportées aux fichiers depuis la dernière compilation.

Il semble naturel de vouloir utiliser `Make` aussi pour des documents LaTeX. Mais le processus de compilation par LaTeX est en fait difficile à exprimer dans un simple graphe de dépendances comme le fait `make`. En effet, LaTeX doit généralement être exécuté plusieurs fois pour différentes raisons (par exemple : stabiliser la table des matières, stabiliser les références, ajouter la bibliographie, ajouter l'index, etc.), et le simple fait que le fichier final soit plus récent que les fichiers intermédiaires ne garantit pas qu'il soit à jour.

:::{tip}
Il est donc plutôt [conseillé d'utiliser les scripts développés pour LaTeX.](/2_programmation/compilation/automatiser_les_compilations) Ils lisent les fichiers `.log` et `.aux` produits par LaTeX pour décider de relancer une compilation si nécessaire, ou de lancer `bibtex`, `mkindex`...
:::

Ceci dit, il y a quelques essais d'utilisation de `Make` avec LaTeX :

## L'extension « latex-make »

L'extension <ctanpkg:latex-make> propose de vous aider à utiliser `make` avec LaTeX. Elle offre un fichier `LaTeX.mk`. Il suffit ensuite de créer un fichier `Makefile` qui contient cette unique ligne :

```make
include LaTeX.mk
```

Elle ne gère pas complètement les dépendances entre fichiers, et si vous modifiez des `\input{}` ou des `\include{}` dans votre document, il est conseillé de lancer une recompilation complète :

```bash
make distclean
make pdf
```

## L'extension « latexmake »

Une autre contribution est l'ensemble <ctanpkg:latexmake> qui propose un ensemble de règles `make` qui invoquent `texi2dvi` si nécessaire.

______________________________________________________________________

*Sources :*

- [Makefiles for LaTeX documents](faquk:FAQ-make),
- [Introduction à make avec (Xe)LaTeX](https://geekographie.maieul.net/58).

```{eval-rst}
.. meta::
   :keywords: LaTeX,automatiser la compilation,make,GNU make,imake,makefile
```
