# Comment détecter le moteur TeX utilisé ?

Détecter le [moteur](/1_generalites/glossaire/qu_est_ce_qu_un_moteur) TeX utilisé pour compiler un document peut être important, en particulier pour des documents partagés entre plusieurs utilisateurs. En effet, l'ensemble des fonctions disponibles diffère selon le moteur. Pour que vos commandes fonctionnent quelque soit le moteur, elles doivent « savoir » ce qu'elles peuvent et ne peuvent pas faire, ce qui dépend du moteur utilisé. Obtenir la bonne réponse à cette question est étonnamment tordu, comme vous pourrez le constater ci-dessous avec la construction d'un test simple.

## Les extensions qui traitent ce sujet

Il existe un ensemble complet d'extensions qui vous donnent cette réponse. Elles créent toutes une commande TeX conditionnelle :

- <ctanpkg:ifpdf> crée la commande `\ifpdf` ;
- <ctanpkg:ifxetex> crée la commande `\ifxetex` ;
- <ctanpkg:ifluatex> crée la command `\ifluatex`.

Ces commandes TeX peuvent être utilisées dans la structure conditionnelle de LaTeX à l'image de :

```latex
% !TEX noedit
\ifthenelse{\boolean{pdf}}{⟨si pdf⟩}{⟨sinon⟩}
```

L'extension <ctanpkg:ifxetex> fournit également une commande `\RequireXeTeX` qui crée une erreur si le code n'est pas exécuté avec [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex). Bien que les autres extensions ne fournissent pas une telle commande, il n'est pas réellement difficile d'en écrire une soi-même.

## Les méthodes faites maison

Pour ceux qui veulent faire le travail par eux-même, voici quelques réflexions sur le travail à effectuer pour pdfTeX et `\ifpdf`. Le programmeur motivé peut de la même manière regénérer `\ifxetex` ou `\ifluatex`. Mais tout ceci n'est pas recommandé...

Supposons que vous devez tester si le document que vous allez produire sera un PDF ou un DVI. L'idée la plus naturelle est de vérifier si vous avez accès à une primitive définie uniquement pour pdfTeX. Une bonne primitive à essayer (et pas des moindres puisqu'elle était présente dans les premières versions de pdfTeX) est `\pdfoutput`. Vous essayez donc :

```latex
% !TEX noedit
\ifx\pdfoutput\undefined
  ... % ce n'est pas pdfTeX qui est utilisé
\else
  ... % c'est pdfTeX qui est utilisé
\fi
```

Sauf qu'aucune des deux branches de ce test n'est totalement fiable. Ainsi, la première branche peut être trompeuse car un utilisateur « bizarre » pourrait avoir écrit

```latex
% !TEX noedit
\let\pdfoutput\undefined
```

de telle façon que votre test va se diriger par erreur la première branche. Bien que ce soit un problème théorique, il y a peu de chance qu'il soit majeur.

Plus important est le cas de l'utilisateur qui charge une extension qui utilise le test en style LaTeX pour détermine si le nom d'une commande existe (par exemple, l'extension <ctanpkg:graphics> de LaTeX). Une telle extension peut vous avoir devancé et demander à ce que votre test soit plus élaboré :

```latex
% !TEX noedit
\ifx\pdfoutput\undefined
  ... % ce n'est pas pdfTeX qui est utilisé
\else
  \ifx\pdfoutput\relax
    ... % ce n'est pas pdfTeX qui est utilisé
  \else
    ...% c'est pdfTeX qui est utilisé
  \fi
\fi
```

Si vous avez seulement besoin de savoir si une fonctionnalité pdfTeX (tel le [crénage](wpfr:Cr%C3%A9nage) marginal) est présent, vous pouvez vous arrêter ici : vous savez désormais tout ce dont vous avez besoin.

Toutefois, si vous avez besoin de savoir si vous créez un fichier de sortie en PDF, vous avez alors besoin de savoir quelle est la valeur de `\pdfoutput` :

```latex
% !TEX noedit
\ifx\pdfoutput\undefined
  ... % ce n'est pas pdfTeX qui est utilisé
\else
  \ifx\pdfoutput\relax
    ... % ce n'est pas pdfTeX qui est utilisé
  \else
    % c'est pdfTeX qui est utilisé avec
    \ifnum\pdfoutput>0
      ... % une sortie PDF
    \else
      ... % une sortie DVI
    \fi
  \fi
\fi
```

______________________________________________________________________

*Source :* [Am I using pdfTeX, XeTeX or LuaTeX?](faquk:FAQ-whatengine)

```{eval-rst}
.. meta::
   :keywords: LaTeX,luatex,xetex,pdftex,programmation,moteur,test,condition,if
```
