# Comment sont gérées les options d'extension ?

Lorsque vous utilisez plusieurs extensions, vous pouvez les regrouper dans l'argument d'une unique commande `\usepackage` en les séparant par des virgules :

```latex
% !TEX noedit
\usepackage{style1,style2}
```

Ceci peut paraître pratique, mais c'est à éviter lorsque vous souhaitez utiliser une option `option1` de l'extension `style1` qui n'existe pas pour l'extension `style2`. Mieux vaut alors écrire :

```latex
% !TEX noedit
\usepackage[option1]{style1}
\usepackage{style2}
```

Cela évite un message du type « *unknown option1 for style2* » (autrement dit « option1 n'est pas connue de style2 ») qui peut apparaître avec :

```latex
% !TEX noedit
\usepackage[option1]{style1,style2}
```

Par contre, si les options `option1` et `option2` sont attendues par plusieurs extensions, cette écriture fonctionne :

```latex
% !TEX noedit
\documentclass[option1,option2]{article}

\usepackage{style1,style2}
```

Cela permet de conserver l'ordre de chargement des extensions mais pas celui dans lequel seront prises en compte les options par les extensions. Les options de classe sont globales et se transmettent à toutes les extensions chargées, mais seules celles pour lesquelles elles sont définies les utiliseront (certaines options sont par nature globales comme `draft`, `final`, `a4paper`, etc.; d'autres sont souvent considérées comme globales, comme `french`).

______________________________________________________________________

*Sources :*

- [LaTeX document class options](https://www.tug.org/TUGboat/tb35-3/tb111thurnherr.pdf),
- [LaTeX « \\documentclass » options illustrated](https://texblog.org/2013/02/13/latex-documentclass-options-illustrated/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,options de classe,options de package,options d'extensions,combiner plusieurs usepackage
```
