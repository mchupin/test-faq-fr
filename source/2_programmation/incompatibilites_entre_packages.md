# Comment résoudre les incompatibilités entre extensions ?

Les problèmes surviennent, en général, lorsque deux extensions s'attaquent à la redéfinition de la même commande. Par exemple, si vous choisissez d'inclure à la fois les extensions <ctanpkg:bibentry> et <ctanpkg:backref>, vous allez inévitablement rencontrer des problèmes, puisque l'un et l'autre redéfinissent la commande `\bibitem` (en utilisant d'ailleurs tous les deux la commande interne `\BR@bibitem`).

Dans certains cas, il suffit de permuter l'ordre de chargement de certaines extensions. Il n'y a pas de règle générale, cependant.

## Conflits connus avec l'extension « hyperref »

:::{todo} Utiliser `la page de Freek Dijkstra <http://www.macfreek.nl/memory/LaTeX_package_conflicts>`__ pour compléter.
:::

______________________________________________________________________

*Sources :*

- [Packages that need to be included in a specific order](https://tex.stackexchange.com/questions/3090/packages-that-need-to-be-included-in-a-specific-order),
- [Which packages should be loaded after hyperref instead of before?](https://tex.stackexchange.com/questions/1863/which-packages-should-be-loaded-after-hyperref-instead-of-before)
- [LaTeX package conflicts](http://www.macfreek.nl/memory/LaTeX_package_conflicts).

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,conflits entre extensions,préambule,clash entre extension,clash entre package,ordre des extensions,ordre des packages
```
