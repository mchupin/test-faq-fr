# Que signifie l'erreur : « Improper argument for math accent : Extra braces must be added to prevent wrong output » ?

- **Message** : `Improper argument for math accent : Extra braces must be added to prevent wrong output`
- **Origine** : package *amsmath*.

La totalité d'une « sous-formule accentuée » doit être entourée d'accolades.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,accents en mode mathématique,accents mathématiques,package amsmath
```
