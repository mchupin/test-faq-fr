# Que signifie l'erreur : « I can't write on file \`⟨nom⟩' » ?

- **Message** : ''I can't write on file \`⟨*nom*⟩' ''
- **Origine** : *TeX*.

TeX n'est pas capable d'écrire des données dans le fichier `⟨nom⟩`. Il est probablement en lecture seule ou il n'y a pas de permission en écriture dans son répertoire. Sur certaines implémentations TeX (comme TeX Live), l'erreur peut être précédée d'une ligne telle que :

```
TeX : Not writing to /TeXmf/TeX/LaTeX/base/LaTeX.ltx (openout_any = p).
```

Ces installations TeX sont configurées par défaut pour être « paranoïaques » (d'où le « p ») en ce qui concerne l'écriture de fichiers. Elles ne permettent que l'écriture de fichiers situés sous le répertoire courant et interdisent l'écriture de fichiers spécifiés avec un chemin absolu ou dont le nom commence par un point. Ce comportement est modifiable en éditant le fichier `texmf.cnf`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,lecture seule,ecriture impossible,droits sur les fichiers,fichiers cachés
```
