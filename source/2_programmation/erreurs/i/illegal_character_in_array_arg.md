# Que signifie l'erreur : « Illegal character in array arg » ?

- **Message** : `Illegal character in array arg`
- **Origine** : *LaTeX*.

On obtient cette erreur lorsque la spécification de colonne dans les environnements `tabular` ou `array`, ou dans une commande `\multicolumn`, contient des caractères non définis comme spécificateurs de colonne.

Une cause fréquente est d'utiliser la syntaxe étendue de l'extension <ctanpkg:array>, décrite au chapitre 5 du *LaTeX Companion*

:::{todo}, en ayant oublié de la charger dans le préambule (par exemple, en ayant copié un tableau à partir d'un autre document).
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,définitions des colonnes,spécifications des colonnes,tableaux en LaTeX,problème avec un tableau
```
