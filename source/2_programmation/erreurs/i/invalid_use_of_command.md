# Que signifie l'erreur : « Invalid use of ⟨commande⟩ » ?

- **Message** : `Invalid use of ⟨commande⟩`
- **Origine** : package *amsmath*.

On a utilisé une commande de <ctanpkg:amsmath> à un endroit où elle n'a aucun sens. Il faut revoir la façon d'utiliser cette commande.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,mathématiques,package amsmath
```
