# Que signifie l'erreur : « Interruption » ?

- **Message** : `Interruption`
- **Origine** : *TeX*.

On obtient cette erreur après avoir interrompu la compilation LaTeX (avec Ctrl-C ou ce que le système d'exploitation propose) et cette erreur n'est donc pas surprenante. Pour forcer l'arrêt d'une compilation, on tape `x` suivi de *Entrée*. Si on ne tape que sur la touche *Entrée*, la compilation reprend.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème de compilation,sortir de LaTeX,interrompre la compilation,reprendre la compilation
```
