# Que signifie l'erreur : « `\include` cannot be nested » ?

- **Message** : `\include cannot be nested`

LaTeX a rencontré une commande `\include` à l'intérieur d'un fichier chargé avec `\include`. En raison des contraintes d'implémentation, il est impossible d'imbriquer ces commandes. On doit remplacer le `\include` intérieur par un `\input`, ou repenser la structure des fichiers afin que toutes les instructions `\include` soient regroupées dans le fichier principal du document.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=I>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,inclusion de fichier,fichiers imbriqués,inclusion récursive,include,includeonly,input
```
