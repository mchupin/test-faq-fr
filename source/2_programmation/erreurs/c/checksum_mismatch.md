# Que signifie l'erreur : « Checksum mismatch in font » ?

- **Message** : `Checksum mismatch in font`
- **Origine** : `dvips`

Lorsque MetaFont génère une police, il inclut une somme de contrôle (*checksum*) dans le fichier bitmap de la police et dans le fichier de métriques de la police (TFM). (La)TeX inclut également la somme de contrôle du fichier TFM dans le fichier DVI.

Lorsque `dvips` (ou d'autres pilotes DVI) traitent un fichier DVI, ils comparent les sommes de contrôle du fichier DVI à celles des polices bitmap utilisées pour les images de caractères. Si les sommes de contrôle ne correspondent pas, cela signifie que le fichier de métrique de police utilisé par (La)TeX n'a pas été généré à partir du même programme MetaFont qui a généré la police.

Cela se produit généralement lorsque vous traitez le fichier DVI de quelqu'un d'autre. Mais les polices de votre système peuvent également être en faute : il est possible que le nouveau TFM n'ait pas été installé, ou ait été installé dans un chemin d'un ancien fichier TFM, ou que vous ayez un cache personnel de bitmaps d'une ancienne version de la police.

Dans tous les cas, regardez le document qui a été généré car il y a de fortes chances que tout ait bien fonctionné. En effet, les métriques ont tendance à ne pas changer, même lorsque les bitmaps sont améliorés. De nombreux concepteurs de polices, Donald Knuth inclus, maintiennent les métriques quoi qu'il arrive.

Si la sortie *semble* mauvaise, votre seule chance est de régénérer les différentes polices (cela peut aussi revenir à vider votre cache bitmap, reconstruire le fichier TFM localement, etc).

______________________________________________________________________

*Source :* [Checksum mismatch in font](faquk:FAQ-checksum)

```{eval-rst}
.. meta::
   :keywords: LaTeX,errors
```
