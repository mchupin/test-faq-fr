# Que signifie l'erreur : « Cannot include graphics of type : ⟨ext⟩ » ?

- **Message** : `Cannot include graphics of type : ⟨ext⟩`
- **Origine** : package *graphics/graphicx*.

On obtient cette erreur lorsqu'on spécifie un type de graphique dans le second argument de `\DeclareGraphicsRule`, ou lorsqu'on utilise le mot-clef type de `\includegraphics` non supporté par le pilote graphique chargé.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,format d'images,includegraphics,fichier postscript,encapsulated postscript,fichier JPEG,fichier BMP,fichier PNG,fichier WMF
```
