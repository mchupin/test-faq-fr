# Que signifie l'erreur : « Cannot be used in preamble » ?

- **Message** : `Cannot be used in preamble`
- **Origine** : *LaTeX*.

Certaines commandes (`\nocite` par exemple) ne sont permises que dans le corps du document (c'est-à-dire après `\begin{document}`).

Il faut alors déplacer la déclaration à cet emplacement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=C>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,préambule,déclarations,usepackage,document LaTeX
```
