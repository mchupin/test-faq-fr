# Que signifie l'erreur : « Misplaced alignment tab character & » ?

- **Message** : `Misplaced alignment tab character &`
- **Origine** : *TeX*.

LaTeX a trouvé un caractère `&` en dehors des environnements d'alignement (`tabular`, `align`...). Pour obtenir un symbole « & », il faut saisir la commande `\&`.

Utiliser un environnement <ctanpkg:amsmath>, comme `cases` ou `matrix`, sans avoir chargé cette extension est une cause possible de cette erreur.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,tabulation,et commercial,esperluette,alignement,tableau,array,matrice
```
