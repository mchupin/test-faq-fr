# Que signifie l'erreur : « Missing = inserted for `\ifnum` » ?

- **Message** : `Missing = inserted for \ifnum`
- **Origine** : *TeX*.

TeX se plaint qu'une condition de bas niveau `\ifnum` n'est pas suivie de deux nombres séparés par `<`, `=` ou `>`.

Cette erreur peut également survenir lorsqu'on oublie l'opérateur de comparaison dans la commande `\ifthenelse` de l'extension <ctanpkg:ifthen>.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,ifthen,ifthenelse,condition,test,comparaison de nombres
```
