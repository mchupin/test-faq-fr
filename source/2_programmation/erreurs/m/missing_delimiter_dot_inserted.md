# Que signifie l'erreur : « Missing delimiter (. inserted) » ?

- **Message** : `Missing delimiter (. inserted)`
- **Origine** : *TeX*.

Un `\left`, un `\right`, ou l'une des commandes `\big...` n'est pas suivie de son délimiteur. TeX place un délimiteur vide « . » pour corriger cette erreur

:::{todo} (voir section 8.5.3 page 508 du *LaTeX Companion*).
:::
______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,parenthèse,délimiteur,grand délimiteur,mode mathématique,formule mathématique
```
