# Que signifie l'erreur : « Missing `\endcsname` inserted » ?

- **Message** : `Missing \endcsname inserted`
- **Origine** : *TeX*.

Cette erreur peut survenir en utilisant des commandes dans un nom de compteur ou d'environnement (par exemple, `\newenvironment{Ann\'ee}`).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,utiliser une commande dans le nom d'un environnement
```
