# Que signifie l'erreur : « Bad math environment delimiter » ?

- **Message** : `Bad math environment delimiter`
- **Origine** : *LaTeX*.

Cette erreur est déclenchée lorsqu'une commande `\(` ou `\[` est rencontrée à l'intérieur d'une formule, ou lorsque `\)` ou `\]` est rencontrée en dehors d'une formule mathématique. Vérifiez que ces commandes sont convenablement appariées dans le document.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=B>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,délimiteur mathématique,mode mathématique,balises,parenthèse,crochet,square bracket
```
