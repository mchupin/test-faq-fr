# Que signifie l'erreur : « Extra `}`, or forgotten `\endgroup` » ?

- **Message** : `Extra }, or forgotten \endgroup`
- **Origine** : *TeX*.

Le groupe courant a commencé avec `\begingroup` (utilisé, par exemple, par `\begin{...}`) mais TeX a trouvé un `}` fermant au lieu du `\endgroup` correspondant. On obtient cette erreur lorsqu'on laisse un `}` isolé à l'intérieur du corps d'un environnement.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=E>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème d'accolades,curly brackets,mode mathématique
```
