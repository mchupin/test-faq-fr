# Que signifie l'erreur : « '⟨caractère⟩' invalid at this point » ?

- **Message** : '' '⟨*caractère*⟩' invalid at this point''
- **Origine** : extension *calc*.

L'extension <ctanpkg:calc> est chargée et la formule utilisée dans une des commandes `\setcounter`, `\setlength`, `\addtocounter` ou `\addtolength` utilise une syntaxe incorrecte du point de vue de <ctanpkg:calc>. Voir section A.3.1 du *LaTeX Companion*

:::{todo} Ce dernier paragraphe appelle une révision : « pour plus de détails. »
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=-etoile-lt>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,package calc,caractère invalide
```
