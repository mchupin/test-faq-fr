# Que signifie l'erreur : « Option clash for package ⟨nom⟩ » ?

- **Message** : `Option clash for package ⟨nom⟩`
- **Origine** : *LaTeX*.

L'extension `⟨nom⟩` a été chargée deux fois avec des ensembles d'options qui entrent en conflit.

## Les cas non problématiques

Plusieurs cas ne génèrent aucune erreur. Le simple est le suivant :

```latex
% !TEX noedit
\usepackage[a]{truc}
...
\usepackage{truc}
```

De la même manière, il ne dira rien pour le cas suivant (le second chargement ne faisant rien) :

```latex
% !TEX noedit
\usepackage[a]{truc}
...
\usepackage[a]{truc}
```

Voici enfin le dernier cas où LaTeX ne réagit pas :

```latex
% !TEX noedit
\usepackage[a,b]{truc}
...
\usepackage[a]{truc}
```

De fait, la règle générale est la suivante : le premier chargement d'une extension définit un ensemble d'options ; si un autre `\usepackage` ou `\RequirePackage` appelle également l'extension, les options de cet appel peuvent ne pas étendre l'ensemble déclaré lors du premier chargement.

Il existe ici un cas particulier : l'extension <ctanpkg:fontenc> peut être chargée autant de fois que nécessaire avec différentes options. Voir section 7.5.3 page 369 du *LaTeX Companion*).

:::{todo} Le précédent paragraphe appelle une révision.
:::

## Les cas problématiques

### Le cas simple

Le cas suivant génère le message d'erreur, même dans le cas où les options `a` et `b` sont des alias (LaTeX ne poussant pas la vérification au-delà du simple nom) :

```latex
% !TEX noedit
\usepackage[a]{truc}
...
\usepackage[b]{truc}
```

Heureusement, l'erreur (dans ce genre de cas) est facilement réparable une fois que vous avez examiné le préambule de votre document.

### Le cas de l'extension qui appelle une autre extension

Ce cas donne du fil à retordre. Supposons que l'extension `truc` charge l'extension `machin` avec l'option `b` et que votre document indique (dans cet ordre ou l'ordre inverse) :

```latex
% !TEX noedit
\usepackage{truc}
...
\usepackage[a]{machin}
```

Alors, l'erreur sera détectée, même si vous n'avez chargé explicitement `machin` qu'une seule fois. En appuyant sur `H` lorsque LaTeX croise le problème lors de la compilation, les options en conflit sont affichées. Si cela ne marche pas, il faudra procéder à une analyse des fichiers journaux comme décrit dans la question « [Comment traiter les erreurs ?](/2_programmation/erreurs/interpreter_les_messages_d_erreur2) ». Il faut ici savoir que le processus de chargement de chaque fichier est mis entre parenthèses dans le journal. Aussi, si l'extension `truc` charge <ctanpkg:graphics>, le journal contiendra quelque chose comme :

```bash
(⟨path⟩/truc.sty ...
...
(⟨path⟩/graphics.sty ...
...)
...
)
```

Si nous avons affaire à une extension `truc` qui charge l'extension `machin` sans les options qui vous intéresse, vous devez demander à LaTeX de glisser des options lorsque `truc` le charge :

```latex
% !TEX noedit
\PassOptionsToPackage{⟨option1⟩,⟨option2⟩}{machin}
\usepackage{truc}
```

La commande `\PassOptionsToPackage` dit à LaTeX de se comporter comme si la liste des options données étaient passées quand il charge enfin l'extension.

### Le cas de la classe qui appelle une autre extension

Dans ce cas, on peut tenter une des solutions suivantes :

- spécifier les options requises en tant qu'options globales au niveau de `\documentclass` ;
- charger une extension avant `\documentclass` avec la commande `\RequirePackage`. Voir section 2.1.1 du *LaTeX Companion*

:::{todo} Ce dernier paragraphe appelle une révision : « pour plus de détails ; »
:::

- placer une commande `\PassOptionsToPackage` avant `\documentclass`.

### Le cas des options incomptibles

Si l'extension `truc` ou la classe `machin` charge une extension avec une option qui entre en conflit avec ce dont vous avez besoin, vous êtes bloqué. Un exemple d'options opposées est celui de `draft` et `final` pour l'extension <ctanpkg:graphics>. Dans un tel cas, vous devez modifier l'extension ou la classe (sous réserve des termes de sa licence). Il peut s'avérer utile de contacter l'auteur : il peut avoir une alternative utile à suggérer.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=O>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- [Option clash for package](faquk:FAQ-optionclash).

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,messages d'erreur de LaTeX,options incompatibles,charger des extensions,charger des packages
```
