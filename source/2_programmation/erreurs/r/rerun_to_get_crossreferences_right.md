# Que signifie le message : « Rerun to get crossreferences right » ?

- **Message** : `Rerun to get crossreferences right`
- **Origine** : *LaTeX*.

LaTeX avertit l'utilisateur que le document doit être compilé de nouveau («Exécutez de nouveau pour mettre à jour les références internes»). En effet, les étiquettes (insérées avec `\label`, par exemple) semblent avoir changé depuis l'exécution précédente. LaTeX compare de fait les étiquettes qu'il a créées cette fois-ci avec ce qu'il a trouvé lors de l'exécution précédente et il effectue cette comparaison au moment du `\end{document}`.

De temps en temps, le message ne disparaît pas : quel que soit le nombre de compilations, LaTeX vous indique toujours que « *Label(s) may have changed* » (« Les étiquettes peuvent avoir changé »). Ceci peut parfois être causé par une extension défectueuse : <ctanpkg:footmisc> (avec l'option `perpage`) et <ctanpkg:hyperref> ont été connues pour poser ce problème dans le passé : si vous utilisez l'une ou l'autre, vérifiez que vous avez la dernière version, et mettez-la à jour si nécessaire.

Cependant, il existe un cas rare où cette erreur peut se produire en raison d'une structure pathologique de votre document lui-même. Supposons que vous ayez des pages numérotées en chiffres romains, et que vous ajoutiez une référence à une étiquette située à la fin de la page « ix » (9). La présence de la référence pousse cette étiquette vers le haut de la page « x » (10), mais maintenant la référence est plus courte, donc l'étiquette revient au bas de la page « ix » à la compilation suivante. La séquence peut se répéter à l'infini...

La seule solution à ce problème est d'apporter une petite modification à votre document. Dans le cas décrit ici, l'ajout ou la suppression d'une virgule pourra êre suffisant.

:::{note}
Si vous utilisez l'extension <ctanpkg:varioref>, vous avez plus de chances de tomber sur un cas pathologique de ce genre, puisqu'elle insère des références plus longues (comme « figure page suivante » au lieu de juste « figure page ix »). Et vous devrez peut-être modifier plus qu'une virgule dans votre document pour résoudre le problème. Ce n'est pas une erreur dans le code de <ctanpkg:varioref>, uniquement une conséquence de ses fonctionnalités.
:::

______________________________________________________________________

*Source :* [Rerun messages won't go away](faquk:FAQ-rerun)

```{eval-rst}
.. meta::
   :keywords: LaTeX,étiquettes et références,boucle sans fin,réexécuter LaTeX,mettre à jour les références
```
