# Que signifie l'erreur : « Language definition file ⟨langue⟩.ldf not found » ?

- **Message** : `Language definition file ⟨langue⟩.ldf not found`
- **Origine** : package *babel*.

Lorsque LaTeX traite la liste d'options de <ctanpkg:babel> et lit une option `⟨langue⟩` inconnue, il essaie de charger un fichier dont le nom doit être `⟨langue⟩.ldf`.

Ce message s'affiche lorsque ce fichier n'est pas trouvé. L'erreur peut être due à une simple erreur de frappe, ou si le fichier est à un emplacement en dehors du chemin de recherche de LaTeX.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=L>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier non trouvé,package babel,langue non trouvée
```
