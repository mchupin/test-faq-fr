# Que signifie l'erreur « LaTeX2e command `\usepackage` in LaTeX 2.09 document » ?

- **Message** : `LaTeX2e command \usepackage in LaTeX 2.09 document`
- **Origine** : *LaTeX*.

:::{todo} À rédiger.
:::

Voir aussi la question : [Que signifie l'erreur « Entering compatibility mode» ?](/2_programmation/erreurs/e/entering_compatibility_mode)

______________________________________________________________________

*Source :* [How to convert LaTeX 2.09 to LaTeX2e](https://tex.stackexchange.com/questions/30128/how-to-convert-latex-2-09-to-latex2e)

```{eval-rst}
.. meta::
   :keywords: LaTeX,erreurs,ancien document LaTeX,compatibilité,ancienne version, documentstyle vs documentclass
```
