# Que signifie l'erreur : « Accent ⟨commande⟩ not provided by font family ⟨nom⟩ » ?

- **Message** : `Accent ⟨commande⟩ not provided by font family ⟨nom⟩`
- **Origine** : extension *textcomp*.

L'extension <ctanpkg:textcomp> implémente le codage `TS1` qui, malheureusement, n'est couvert entièrement que par une minorité de familles de fontes utilisables sous LaTeX. Aucun accent n'est imprimé.

Voir section 7.5.4 du *LaTeX Companion*

:::{todo} Ce dernier paragraphe appelle une révision : « pour savoir comment obtenir une autre représentation de ces accents. »
:::

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=A>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,accents,accent non fourni,acdent non disponible,encodage TS1,accents en LaTeX,lettres accentuées,problème avec les accents
```
