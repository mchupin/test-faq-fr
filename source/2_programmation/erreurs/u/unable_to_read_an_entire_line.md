# Que signifie l'erreur : « Unable to read an entire line » ?

- **Message** : `Unable to read an entire line`
- **Origine** : *TeX*.

TeX appartient à la génération d'applications écrites pour des environnements qui n'offraient pas de fonctions sophistiquées pour manipuler les chaînes de caractères et les entrées/sorties, alors qu'aujourd'hui, l'existence de ces fonctions semble aller de soi. En effet, TeX a été écrit en Pascal, et la norme Pascal initiale ne mentionnait même pas les entrées/sorties, de sorte que toutes les opérations, à part peut-être les plus triviales, étaient susceptibles de devoir être réimplémentées pour exécuter le programme sur un autre système.

Lorsque vous faites déborder le mécanisme d'entrée de TeX, vous avez ce message :

```
! Unable to read an entire line---bufsize=3000.
    Please ask a wizard to enlarge me.
```

(la valeur `3000` peut valoir autre chose --- l'exemple vient du groupe de discussion `comp.text.tex`, el la personne avait vraisemblablement une installation de TeX plutôt ancienne).

Comme le message l'indique, il y a (ce que TeX considère comme) une ligne dans votre entrée qui est « trop longue », au sens de TeX. Les distributions modernes ont généralement un tampon d'entrée de plusieurs dizaines de milliers de caractères, donc il est peu probable que ce message apparaisse vraiment pour cette raison. Voici des raisons possibles plus fréquentes :

- Le fichier a été transféré depuis un autre système sans conversion des fins de lignes. Avec la disparition des enregistrements de longueur fixe (qui existaient sur les mainframes) et l'intelligence accrue des distributions TeX pour reconnaître les caractères de fin de lignes des autres systèmes, cette cause est devenue plutôt rare, elle aussi.
- Un fichier graphique PostScript contient un aperçu en binaire, mais une extension le lit pour y trouver la définition de sa *boundingbox*. Là encore, les distributions TeX suffisamment récentes reconnaissent cette situation et ignorent les aperçus (qui n'ont d'intérêt que pour le prévisualisateur TeX, voire pas du tout).
- Un fichier graphique est complètement en binaire (au format PNG, JPEG...) mais il est lu comme du PostScript.

Le meilleur conseil est d'ignorer ce que TeX dit (notamment son conseil d'agrandir le tampon d'entrée), et de résoudre le problème à sa source.

Si le vrai problème est la longueur excessive des lignes de texte, la plupart des éditeurs de texte qui se respectent se feront un plaisir de diviser automatiquement les longues lignes (tout en préservant la structure des mots) de sorte qu'elles ne dépassent jamais une longueur donnée ; la solution consiste donc simplement à éditer le fichier, puis tenter une nouvelle compilation.

Si le problème est une section d'aperçu problématique dans un fichier PostScript, essayez d'utiliser [Ghostscript](https://www.ghostscript.com/) pour retraiter le fichier, en produisant un fichier au format EPS valide (la distribution de Ghostscript propose un script `ps2epsi`, qui régénérera l'aperçu si nécessaire). Les utilisateurs du programme [gsview](http://www.ghostgum.com.au/) trouveront sans mal des boutons pour effectuer la transformation requise du fichier affiché.

Enfin, si un fichier PNG ou JPG est lu comme du Postscript, c'est soit que vous ne compilez pas avec le bon moteur (vous utilisez la commande `latex` là où vous devriez utiliser `pdflatex`), ou que vous devriez [convertir le fichier en question au format EPS](/3_composition/illustrations/inclure_une_image/importer_des_graphiques_avec_dvips).

______________________________________________________________________

*Sources :*

- [Unable to read an entire line](faquk:FAQ-buffovl),
- [LinuxQuestions.org : « Unable to read an entire line » when trying graphics](https://www.linuxquestions.org/questions/linux-software-2/latex-unable-to-read-an-entire-line-when-trying-graphics-437744/).

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,buffer overflow,ligne trop longue,enregistrement trop long,séparateur de lignes
```
