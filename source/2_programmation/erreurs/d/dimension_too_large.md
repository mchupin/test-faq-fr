# Que signifie l'erreur : « Dimension too large » ?

- **Message** : `Dimension too large`

TeX ne peut gérer que des dimensions dont la taille ne dépasse pas 16383.99998 pt (environ 5,76 mètres) en valeur absolue. Même sur une page énorme, cet intervalle devrait être suffisant.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=D>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,dimension trop grande,longueur maximale de LaTeX,taille maximale en LaTeX
```
