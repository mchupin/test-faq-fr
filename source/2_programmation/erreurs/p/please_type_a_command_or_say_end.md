# Que signifie l'erreur : « (Please type a command or say \``\end`') » ?

- **Message** : `(Please type a command or say`\\end')\`
- **Origine** : *TeX*.

Parfois, lorsque vous exécutez TeX ou LaTeX, il s'arrête brusquement et vous présente une invite (par défaut, juste un caractère `*`). Beaucoup de gens appuient alors par réflexe sur la touche « Entrée », ce qui ne leur rend pas service car TeX affiche alors l'erreur ci-dessus, vous demandant de saisir à nouveau quelque chose.

Voir alors la question « [Que signifie le message : « * » ? Et que dois-je faire ?](/2_programmation/erreurs/0/etoile) »

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=P>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,étoile,compilation interactive,invite de commande LaTeX
```
