# Que signifie l'erreur : « Something's wrong--perhaps a missing `\item` » ?

- **Message** : `Something's wrong--perhaps a missing \item`
- **Origine** : *LaTeX*.

## Cas où il manque réellement une commande \\item

Parfois le message d'erreur indique bien l'erreur en cours : il manque une commande `\item`. L'exemple suivant va évidemment générer cette erreur :

```latex
% !TEX noedit
\begin{itemize}
Et hop !
\end{itemize}
```

Vous pouvez également faire apparaître l'erreur alors tout semble correct à première vue :

```latex
% !TEX noedit
\begin{tabular}{l}
  \begin{enumerate}
  \item et toc !\\
  \item et paf !
  \end{enumerate}
\end{tabular}
```

L'erreur tombe au moment où la commande `\\` tente d'agir. Si vous souhaitez numéroter des cellules dans une table, il ne faut surtout pas agir ainsi. Vous devez le faire un peu plus manuellement :

```latex
% !TEX noedit
\newcounter{tablecell}
...
\begin{tabular}{l}
  \stepcounter{tablecell}
  \thetablecell. et toc !\\
  \stepcounter{tablecell}
  \thetablecell. et paf !
\end{tabular}
```

Il y a ici moyen de mieux présenter ce code. Définissons ici une commande `\numbercell` :

```latex
% !TEX noedit
\newcounter{tablecell}
...
\newcommand*{\numbercell}{%
  \stepcounter{tablecell}%
  \thetablecell. % **
}
```

ce qui donnera le code principal suivant :

```latex
% !TEX noedit
\setcounter{tablecell}{0}
\begin{tabular}{l}
  \numbercell foo\\
  \numbercell bar
\end{tabular}
```

Notez l'introduction délibérée d'un espace dans le cadre de la commande, marqué par des astérisques.

## Cas où la commande \\item n'est pas liée à l'erreur

L'erreur apparaît également régulièrement lorsque vous n'auriez jamais pensé qu'une commande `\item` puisse être appropriée.

Le message d'erreur est produit par une commande `\addvspace` (qui ajoute un espace vertical) rencontrée en mode horizontal. Par exemple, en oubliant de refermer une accolade d'un `\mbox` :

```latex
% !TEX noedit
\mbox{Et voilà une \section{belle section} qui va tomber en erreur
```

Ici, la commande `\section` qui exécute `\addvspace` en interne est maintenant utilisée en mode horizontal. Il convient d'identifier la commande qui appelle le `\addvspace` provoquant l'erreur, et de comprendre pourquoi elle est mal utilisée.

En voici un exemple :

```latex
% !TEX noedit
\fbox{%
  \begin{alltt}
    Et toc !
  \end{alltt}%
}
```

L'erreur se produit aussi en remplaçant `\fbox` par `\mbox`, `\framebox` ou `\makebox`. En effet, l'environnement `alltt` utilise une liste « triviale », cachée dans sa définition (d'ailleurs, l'environnement `itemize` a également cette construction à l'intérieur de lui-même et `\begin{itemize}` ne fonctionnera pas non plus dans `\fbox`). La construction de liste cherche à se faire entre les paragraphes et crée un nouveau paragraphe de son propre chef. Dans la commande `\fbox`, cela ne fonctionne pas et les commandes suivantes supposent qu'il manque une commande `\item`.

Pour résoudre cette erreur plutôt cryptique, il faut mettre le `alltt` dans une boîte autorisant des paragraphes :

```latex
% !TEX noedit
\fbox{%
  \begin{minipage}{0.75\textwidth}
    \begin{alltt}
      Et toc !
    \end{alltt}
  \end{minipage}
}
```

Le code ci-dessus produit une boîte beaucoup trop large pour le texte. On peut vouloir utiliser quelque chose qui autorise des [boîtes de taille variable](/3_composition/texte/paragraphes/ajuster_la_taille_d_une_minipage) à la place de l'environnement `minipage`.

Curieusement, bien que l'environnement `verbatim` ne fonctionne pas dans un argument de commande `\fbox` (voir [verbatim dans les arguments de commande](/3_composition/texte/paragraphes/pourquoi_verbatim_ne_fonctionne_pas_toujours)), vous obtenez une erreur qui se plaint de la commande `\item` : la liste interne de l'environnement remonte l'anomalie avant même que '' verbatim '' n'ait eu la chance d'agir.

Une autre utilisation (apparemment) évidente de `\fbox` génère aussi cette erreur :

```latex
% !TEX noedit
\fbox{\section{Un titre encadré}}
```

C'est un cas où vous devez simplement être plus subtil : vous devriez soit écrire vos propres commandes pour remplacer les commandes de sectionnement de LaTeX, soit chercher une alternative dans les extensions évoquées dans la question « [Comment modifier le style des titres de sectionnement ?](/3_composition/texte/titres/modifier_le_style_des_titres) ».

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=S>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- [Perhaps a missing \\item?](faquk:FAQ-errmissitem).

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,itemize,enumerate,listes
```
