# Que signifie l'erreur : « TeX capacity exceeded (...) semantic nest size » ?

- **Message** : `TeX capacity exceeded, sorry [semantic nest size=⟨nombre⟩]`
- **Origine** : *TeX*.

TeX a dépassé la capacité de l'un de ses types de mémoires et arrête son travail. Dans le cas présent, l'imbrication sémantique (*semantic nest*) dont TeX parle correspond à la mécanique d'imbrication (*nesting*) de boîtes dans d'autres boîtes. Elle est ici débordée à force d'imbrications. Une commande stupide peut facilement produire cet effet :

```latex
% !TEX noedit
\def\stupide{\hbox{Voici une chose \stupide à faire}}
\stupide
```

## Utiliser les traces

L'utilisation d'une [méthode de trace](/2_programmation/erreurs/interpreter_les_messages_d_erreur2) peut rendre ici service même si elle donne beaucoup de texte à analyser. Dans le cas ci-dessus, cela donne :

```latex
% !TEX noedit
\stupide ->\hbox {
                  Voici une chose \stupide à faire}
```

puis une centaine de lignes de

```latex
% !TEX noedit
\stupide ->\hbox {Voici une chose \stupide
                                           à faire}
```

Les lignes répétées sont interrompues exactement au niveau de la commande fautive.

Bien sûr, la boucle n'est pas souvent aussi simple que cela --- si `\stupide` appelle `\insidieuse` qui met elle-même en boîte le contenu de `\stupide`, l'effet est le même. Des lignes alternées apparaissent alors dans la trace et sont interrompues à des positions alternées.

## Une précision

Deux éléments sont en fait utilisés quand vous imbriquez des boîtes. Celui non mentionné jusqu'ici est le niveau de groupement (*grouping level*). Le fait que vous épuisez l'imbrication sémantique ou le niveau de groupement dépend uniquement de leur taille respective dans la configuration de votre TeX.

______________________________________________________________________

*Sources :*

- [Capacity exceeded [semantic nest...](faquk:FAQ-semanticnest)\],
- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,taille mémoire de LaTeX,étendre la mémoire,augmenter la mémoire,imbrication
```
