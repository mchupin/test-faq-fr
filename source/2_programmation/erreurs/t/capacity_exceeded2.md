# Que signifie l'erreur : « TeX capacity exceeded (...) text input levels » ?

- **Message** : `TeX capacity exceeded, sorry [text input levels=`*(...)*`]`
- **Origine** : *TeX*.

L'erreur suivante est causée par l'imbrication trop profonde de votre entrée. Vous pouvez la provoquer avec le fichier trivial (en [Plain TeX](/1_generalites/glossaire/qu_est_ce_que_plain_tex)) `input.tex`, qui ne contient rien d'autre que :

```latex
% !TEX noedit
\input input
```

Hors ce cas d'école récursif, il est assez improbable que vous rencontriez cette erreur avec des distributions TeX modernes. Elles permettent en effet d'ouvrir un nombre important de fichiers à tout moment, ce qui est une valeur peu probable pour un document généré par un utilisateur classique.

Cependant, si cela devait survenir, certaines distributions offrent la possibilité d'ajuster le paramètre `max_in_open` dans un fichier de configuration.

______________________________________________________________________

*Source :* [Capacity exceeded --- input levels](faquk:FAQ-inputlev)

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,erreur,capacity exceeded,input levels
```
