# Que signifie l'erreur : « Too many math alphabets used in version ⟨nom⟩ » ?

- **Message** : `Too many math alphabets used in version ⟨nom⟩`

La présentation des mathématiques faite par TeX est l'une de ses caractéristiques les plus impressionnantes, mais la structure interne du mécanisme qui la produit est douloureusement compliquée et (dans un certain sens) pathétiquement limitée. Une de ses limitations est que l'utilisateur n'est autorisé à se servir que de 16 « alphabets mathématiques ».

LaTeX offre à l'utilisateur une grande flexibilité dans l'attribution d'alphabets mathématiques, mais peu de gens utilisent directement cette flexibilité. De fait, de nombreuses extensions qui fournissent des symboles, ou qui les manipulent, s’attribuent un ou plusieurs alphabet mathématique.

Si vous ne pouvez pas vous permettre de supprimer l'une de ces extensions, vous pourrez peut-être envisager d'utiliser [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex) ou [LuaTeX](/1_generalites/glossaire/qu_est_ce_que_luatex), qui disposent tous deux de 256 alphabets mathématiques. Toutefois, une telle modification n'est pas recommandée lorsque vous êtes sous pression pour terminer un document car d'autres problèmes, tels que la disponibilité des polices, pourraient rendre ce changement très complexe.

Il reste par ailleurs un espoir si, dans les extensions de votre document, se trouve <ctanpkg:bm> pour prendre en charge des [symboles mathématiques gras](/4_domaines_specialises/mathematiques/symboles/polices/lettres_grecques_en_gras) : cette extension d'ordinaire consomme beaucoup d'alphabets mathématiques. Cependant, elle définit deux commandes de limitation : `\bmmax` (pour *les symboles* gras, par défaut 4) et ''\\hmmax '' (pour // les symboles // plus gras encore (*heavy*) si vous en avez, par défaut 3), qui contrôlent le nombre d'alphabets à utiliser.

Toute réduction de ces variables ralentira <ctanpkg:bm> mais c'est sûrement mieux qu'un document ne fonctionnant pas du tout. Penser à réduire par test consécutifs ces valeurs, en commençant par ce qui suit, jusqu'à suppression de l'erreur :

```latex
% !TEX noedit
\newcommand{\bmmax}{3}
```

Si vous utilisez des polices mathématiques (telles que *Mathtime Plus*) qui présentent des graisses importantes, vous pouvez supprimer toute utilisation de familles lourdes par <ctanpkg:bm> en définissant de la même manière :

```latex
% !TEX noedit
\newcommand{\hmmax}{0}
```

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur,
- [Too many math alphabets](faquk:FAQ-manymathalph).

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,erreur,messages d'erreur de LaTeX,trop d'alphabets mathématiques,alphabets mathématiques
```
