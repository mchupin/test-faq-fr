# Que signifie l'erreur : « This may be a LaTeX bug » ?

- **Message** : `This may be a LaTeX bug`
- **Origine** : *LaTeX*.

À la connaissance des auteurs, jusqu'à maintenant, cette erreur n'a jamais indiqué un bogue de LaTeX. Elle signifie que LaTeX est tellement perdu en raison d'erreurs précédentes qu'il ne sait plus où il en est au niveau de sa structure de données qui gère les flottants.

Il vaut mieux dans ce cas interrompre la compilation et corriger les erreurs précédentes.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,bug de LaTeX,bogue de LaTeX,trop d'erreurs
```
