# Que signifie l'erreur : « Text line contains an invalid character » ?

- **Message** : `Text line contains an invalid character`
- **Origine** : *TeX*.

Le fichier source contient un caractère étrange, non imprimable, qui est rejeté par TeX. Cela peut survenir lorsqu'on utilise un éditeur de texte pour créer le fichier qui ne le sauvegarde pas en mode texte, ou si on enregistre un fichier en UTF-8 mais qu'on le compile sans en prévenir TeX.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=T>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,caractère non supporté,caractère unicode,UTF-8
```
