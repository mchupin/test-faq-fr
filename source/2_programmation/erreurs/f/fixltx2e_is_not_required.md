# Que signifie le message : « fixltx2e is not required with releases after 2015 » ?

- **Message** : `fixltx2e is not required with releases after 2015`
- **Origine** : Extension *fixltx2e* et *LaTeX*.

Pendant longtemps, des fonctionnalités considérées comme manquantes dans le noyau LaTeX, mais qui auraient fait perdre la compatibilité avec les anciens documents, ont été fournies sous forme de l'extension <ctanpkg:fixltx2e>.

Quelques exemples de ces fonctionnalités :

- une macro `\textsubscript` pour mettre du texte en indice (il existait `\textsuperscript` pour les exposants, mais rien pour les indices),
- la [correction de l'ordre des flottants en mode multicolonne](/3_composition/flottants/flottants_dans_le_desordre_en_mode_deux_colonnes),

```
*
```

:::{todo} Ce dernier paragraphe appelle une révision : « Donner d'autres exemples. »
:::

En 2015, toutes les corrections fournies par <ctanpkg:fixltx2e> ont été intégrées à LaTeX, donc il n'y a plus besoin de charger cette extension. Si un document fait encore appel à elle, cela n'empêche pas la compilation, mais vous pouvez lire cet avertissement dans le `log` :

```text
Package fixltx2e Warning : fixltx2e is not required with releases after 2015
(fixltx2e)                All fixes are now in the LaTeX kernel.
(fixltx2e)                See the latexrelease package for details.
```

## Mécanisme de compatibilité

À partir de 2015, la compatibilité complète de LaTeX de chaque nouvelle version avec les versions précédentes n'a donc plus été automatiquement garantie.

Mais si vous avez besoin de compiler un document ancien, qui utilisait des « fonctionnalités » antérieures à la version dont vous disposez, vous avez depuis 2015 la possibilité d'utiliser l'extension <ctanpkg:latexrelease>, qui restaure le comportement des versions précédentes de LaTeX.

Cette extention se charge *avant* la commande `\documentclass`, et prend en option la date de la version de LaTeX que l'on souhaite émuler. Ainsi en commençant votre document par :

```latex
% !TEX noedit
\RequirePackage[2014/05/01]{latexrelease}
\documentclass{article}
```

vous allez le compiler comme si vous utilisiez la version de LaTeX de mai 2014 (donc avant l'intégration de <ctanpkg:fixltx2e> au noyau).

:::{note}
En pratique, ça ne sert à rien de remonter avant `2014/05/01`, car justement le noyau a été extrêmement stable jusque là, et vous utiliserez plutôt des dates comprises entre 2014 et le moment présent.
:::

______________________________________________________________________

*Sources :*

- [What packages are no longer necessary after TeX Live 2015 kernel update?](https://tex.stackexchange.com/questions/287153/what-packages-are-no-longer-necessary-after-tex-live-2015-kernel-update)
- [How to use fixltx2e only when necessary?](https://tex.stackexchange.com/questions/287146/how-to-use-fixltx2e-only-when-necessary)
- [LATEX News : Issue 22, January 2015](https://www.latex-project.org/news/latex2e-news/ltnews22.pdf).

```{eval-rst}
.. meta::
   :keywords: messages d'avertissement de LaTeX,extension obsolète,2015,fix latex2e,LaTeX release,latexrelease
```
