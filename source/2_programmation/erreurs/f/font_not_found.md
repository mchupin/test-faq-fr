# Que signifie l'erreur : « Font ⟨nom⟩ not found » ?

- **Message** : `Font ⟨nom⟩ not found`

Les tables internes de LaTeX contiennent une mauvaise information et LaTeX devient incapable de trouver la fonte externe `⟨nom⟩` :

- soit cette fonte a été nouvellement installée et TeX ne peut pas trouver son fichier `.tfm` pour une raison quelconque,
- soit la déclaration `\DeclareFontShape` s'y référant contient une erreur de frappe.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,police non trouvée,fonte non trouvée,déclarer une famille de fontes
```
