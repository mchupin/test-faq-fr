# Que signifie l'erreur : « File ended while scanning ⟨quelque chose⟩ » ?

- **Message** : `File ended while scanning ⟨quelque chose⟩`
- **Origine** : *TeX*.

Cette erreur fait partie des erreurs du type « `Runaway...` ». Voir [les explications correspondantes](/2_programmation/erreurs/r/runaway_something).

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,fichier incomplet
```
