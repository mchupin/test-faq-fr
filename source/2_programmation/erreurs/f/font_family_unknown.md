# Que signifie l'erreur : « Font family ⟨enc⟩ + ⟨famille⟩ unknown » ?

- **Message** : `Font family ⟨enc⟩ + ⟨famille⟩ unknown`

L'erreur résulte d'un essai de déclaration d'un groupe de formes de fontes avec `\DeclareFontShape` sans que la fonte `⟨famille⟩` ait été préalablement déclarée disponible dans le codage `⟨enc⟩` en utilisant `\DeclareFontFamily`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,police non trouvée,fonte non trouvée,déclarer une famille de fontes
```
