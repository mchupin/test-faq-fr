# Que signifie l'erreur : « Font ⟨nom-interne⟩ = ⟨nom-externe⟩ not loadable : ⟨raison⟩ » ?

- **Message** : `Font ⟨nom-interne⟩ = ⟨nom-externe⟩ not loadable : ⟨raison⟩`
- **Origine** : *TeX*.

TeX est incapable de charger une fonte avec le nom LaTeX `⟨nom-interne⟩` ayant la structure `\⟨codage⟩/⟨famille⟩/⟨graisse⟩/⟨forme⟩/⟨taille⟩` en notation NFSS [^footnote-1] (c'est en fait un nom de commande unique mais en raison des barres obliques dans ce nom, on ne peut pas le spécifier directement dans le document). Par exemple, il peut s'agir de `\T1/cmr/m/it/10` (*Computer Modern* medium italique, 10 points, au codage T1). Cela donne une information intéressante pour savoir sur quelle fonte porte l'erreur, même si l'on ne peut pas en faire grand chose. Pour la `⟨raison⟩`, on a deux possibilités :

## « Bad metric (TFM) file »

Le fichier de métrique TeX de la fonte (c'est-à-dire `.tfm`) est corrompu. L'installation peut disposer de quelques programmes utilitaires permettant de vérifier les fichiers `.tfm` en détail, même si cela exige habituellement l'aide d'un expert.

## « Metric (TFM) file not found »

Le fichier de métrique TeX de la fonte (c'est-à-dire `.tfm`) n'a pas été trouvé. L'installation peut disposer d'une extension (par exemple, <ctanpkg:cmbright>) comme support d'une certaine famille de fontes, mais les fontes correspondantes ne sont pas disponibles ou pas correctement installées.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=F>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,police non trouvée,fonte non trouvée,charger une fonte,charger une police,fichiers de métrique,fichiers TFM
```

[^footnote-1]: **NFSS** signifie « *new font selection scheme*. »
