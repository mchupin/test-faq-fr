# Que signifie l'erreur : « No driver specified » ?

- **Message** : `No driver specified`
- **Origine** : packages *color/graphics/graphicx*.

Une des extensions <ctanpkg:graphics>, <ctanpkg:graphicx> ou <ctanpkg:color> a été chargée sans option de périphérique cible. Sur la plupart des installations, cette indication est donnée par les fichiers de configuration `graphics.cfg` et `color.cfg`.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,pilote graphique,pilote non trouvé,fichier non trouvé
```
