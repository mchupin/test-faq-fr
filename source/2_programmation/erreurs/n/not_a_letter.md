# Que signifie l'erreur : « Not a letter » ?

- **Message** : `Not a letter`
- **Origine** : *TeX*.

Une exception de coupure a été spécifiée avec `\hyphenation` avec un argument contenant un caractère que TeX ne considère pas comme une lettre. Par exemple, `\hyphenation{au-jourd'hui}` produit cette erreur puisque l'apostrophe'' ' '' n'est pas une lettre au sens de TeX.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=N>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,césure des mots,coupure des mots,hyphenation,motif de césure personnalisé
```
