# Installing a new font

Fonts are really "just another package", and so should be installed in the same sort of way as packages. However, fonts tend to be more complicated than the average package, and as a result it's sometimes difficult to see the overall structure.

Font files may appear in any of a large number of different formats; each format has a different function in a TeX system, and each is stored in a directory its own sub-tree in the installation's TDS tree; all these sub-trees have the directory `$TEXMF/fonts` as their root. A sequence of answers describes the installation of fonts : follow the list through the "next question" links at the bottom of this answer to view them all. Other answers discuss specific font families --- for example, [using the concrete fonts](/5_fichiers/fontes/utiliser_les_fontes_concrete)".

______________________________________________________________________

*Source :* [Installing a new font](faquk:FAQ-instfont)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installation de polices de caractères,installer une fonte
```
