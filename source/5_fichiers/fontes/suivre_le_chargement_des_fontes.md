# Comment suivre le chargement des polices ?

L'extension `tracefnt`, intégrée à LaTeX, permet de suivre le chargement des polices lors de la compilation d'un document. Vous pouvez invoquer `\usepackage{tracefnt}` avec plusieurs options présentées dans le *LaTeX Companion*, dont les principales sont les suivantes :

- `[infoshow]` pour avoir des informations sur le chargement des polices;
- `[errorshow]` permet de n'afficher que les erreurs.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
