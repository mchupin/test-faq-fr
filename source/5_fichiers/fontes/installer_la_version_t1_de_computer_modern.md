# Installing the Type 1 versions of the CM fonts

This is a specialised case of [installing a font](/5_fichiers/fontes/installer_une_nouvelle_police), but it is almost never necessary --- it's inconceivable that any (even remotely) recent system will *not* have the fonts already installed. You can confirm this (near-inevitable) fact by trying the fonts. On a system that uses `dvips` (almost all do), try the sequence :

```bash
latex sample2e
dvips -o sample2e.ps sample2e
```

at a "command prompt" (`shell` in a Unix-style system, "DOS box" in a Windows system).

If the command works at all, the console output of the command will include a sequence of Type 1 font file names, listed as `<path/cmr10.pfb>` and so on; this is `dvips` telling you it's copying information from the Type 1 font, and you need do no more.

If the test has failed, you need to install your own set of the fonts; the distribution (including all the fonts the AMS designed and produced themselves) is now described as <ctanpkg:amsfonts>. The bundle contains metric and map files --- all you need to [install the fonts](/5_fichiers/fontes/installer_une_police_t1).

______________________________________________________________________

*Source :* [Installing the Type 1 versions of the CM fonts](faquk:FAQ-inst1cm)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installing
```
