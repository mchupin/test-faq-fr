# Pourquoi `MakeTeXPK` est lancé alors que la police existe ?

- Voici la réponse de P. Terray : C'est un problème de mise à jour de la base liée à `kpathsea`. Normalement, `MakeTeXPK` met à jour la base `ls-R` de la TDS, à chaque ajout de police. Si cette base n'est pas autorisée en lecture pour tout le monde, ou si la variable TEXMF est mal réglée, `dvips` ou `xdvi` ne peuvent pas vérifier que cette police existe. Du coup, ils lancent `MakeTeXPK` pour la fabriquer. Et `MakeTeXPK` sachant par ailleurs où mettre la police, il vérifie qu'elle existe, et c'est pour ça qu'il donne le message comme quoi elle existe déjà.

Nous pouvons indiquer que l'ajout de polices dans un répertoire personnel ne sera pas pris en compte si l'utilisateur ne peut reconstruire la base en incorporant le nouveau répertoire.

Les solutions sous UNIX :

- vérifier que la variable d'environnement `$TEXMF` est bien réglée ;
- autoriser `ls-R`, qui se trouve dans le répertoire `texmf/`, en lecture/écriture pour tout le monde (ou pour le groupe users);
- reconstruire la base `ls-R` avec la commande `texhash` (tout court).

:::{warning}
Il faut avoir les droits de gestionnaire LaTeX (ou root) pour exécuter `texhash`.
:::

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,TeX directory structure
```
