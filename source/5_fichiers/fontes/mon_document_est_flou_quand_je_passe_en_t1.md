# Fonts go fuzzy when you switch to T1

You've been having problems with hyphenation, and someone has suggested that you should use `\usepackage[T1]{fontenc}` to help sort them out. Suddenly you find that your final PDF has become fuzzy. The problem may arise whether you are using PostScript output and then distilling it, or you are using pdfTeX for the whole job.

In fact, this is the same problem as most others about the [quality of PDF](/5_fichiers/pdf/generer_un_fichier_pdf_de_qualite) : you've abandoned your previous setup using Type 1 versions of the CM fonts, and `dvips` has inserted Type 3 versions of the EC fonts into your document output. (See [Adobe font types](/5_fichiers/fontes/formats_de_polices_adobe)" for details of these font types; also, note that the font *encoding* T1 has nothing directly to do with the font *format* Type 1).

However, as noted in [8-bit Type 1 fonts](/5_fichiers/fontes/fontes_t1_8bits)", Type 1 versions of CM-like fonts in T1 (or equivalent) encoding are now available, both as "real" fonts, and as virtual font sets. One solution, therefore, is to use one of these alternatives.

The alternative is to switch font family altogether, to something like `Times` (as a no-thought default) or one of the many more pleasing Adobe-encoded fonts. The default action of <ctanpkg:fontinst>, when creating metrics for such a font, is to create settings for both OT1 and T1 encodings, so there's little change in what goes on (at the user level) even if you have switched to T1 encoding when using the fonts.

______________________________________________________________________

*Source :* [Fonts go fuzzy when you switch to T1](faquk:FAQ-fuzzy-T1)

```{eval-rst}
.. meta::
   :keywords: LaTeX,fontes,polices,caractères flous,texte flou,texte pas net
```
