# Que sont les fichiers PK ?

Les fichiers PK (pour *packed* autrement dit compressé) contiennent des [polices matricielles](wpfr:Police_matricielle) compressées et utilisables par TeX. Le programme [MetaFont](/5_fichiers/fontes/utiliser_metafont) constitue des fichiers GF de police générique et l'utilitaire [gftopk](/5_fichiers/fontes/gftopk_et_pktogf) produit les fichiers PK à partir de ceux-ci.

Il y a potentiellement beaucoup de fichiers PK car il en faut un pour chaque fonte, c'est-à-dire autant que de combinaisons de grossissements, familles, corps et graisses pour chaque police. De plus, étant donné que les fichiers PK pour une imprimante ne fonctionnent pas nécessairement bien pour une autre, l'ensemble doit être dupliqué pour chaque type d'imprimante à disposition.

Si ce vaste ensemble de fichiers peut en principe fournir des fontes adaptées aux capacités des imprimantes, son trop grand nombre de fichiers (et la difficulté à le gérer) a été un puissant moteur de l'évolution vers des [polices vectorielles](wpfr:Police_vectorielle) telles que les [polices Adobe Type 1](/5_fichiers/fontes/formats_de_polices_adobe).

______________________________________________________________________

*Source :* [What are PK files?](faquk:FAQ-pk)

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,fontes,définition des polices
```
