# Quelle est la différence entre une fonte et une police ?

Extrait du *Cahiers GUTenberg*, n{sup}`o`49 (2007) :

- *Font*, *typeface* : en anglais, ces mots sont loin de faire l’unanimité et peuvent même avoir des sens assez différents selon qu’on est anglais ou américain, spécialiste du plomb ou de l'informatique, etc. En français, le mot « police » 10 ajoute encore à la confusion ! En principe, nous traduisons ici :
- *Font* par « fonte » lorsqu’il a le sens de Bringhurst [^footnote-1] et de Haralambous [^footnote-2], à savoir une collection (ou une base de données !) de procédures permettant de produire un ensemble donné de glyphes ; par exemple le Times ou le Fourier ; le mot « police » permettait de préciser soit un dessin (romain, italique) soit même un corps ou une gamme de corps (12 par exemple) ;
- *Typeface* par « famille de caractères » (par exemple tout le Time, avec toutes ses déclinaisons de graisse, corps, caractères spéciaux, etc.) mais souvent ce mot a une connotation graphique ; on utilise aussi alors le mot « caractère » (on dira « Fourier est un caractère de petit œil »).

______________________________________________________________________

*Sources :*

- [Lexique anglo-français du Companion](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_2007___49_19_0.pdf), Jacques André, Jean-Côme Charpentier. *Cahiers GUTenberg*, n{sup}`o`49 (2007).
- [Différence entre fonte et police](wpfr:Fonte_de_caractères),
- [Police et fonte](wpfr:Police_d'écriture).

```{eval-rst}
.. meta::
   :keywords: LaTeX,polices de caractères,fontes,vocabulaire typographique
```

[^footnote-1]: Robert Bringhurst, *The elements of typographic style*. Hartleys & Marks, 2{sup}`nd` edition, 1996. Page 291.

[^footnote-2]: Yannis Haralambous, *Fontes et codage*. O’Reilly, 2004.[Extrait disponible en ligne.](https://core.ac.uk/download/pdf/217848264.pdf)
