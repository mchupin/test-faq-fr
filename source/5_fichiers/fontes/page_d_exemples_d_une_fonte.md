# Comment visualiser tous les caractères d'une fonte ?

## Postscript, TrueType et OpenType

Pour la visualisation des fontes Postscript, TrueType et OpenType, utilisez les utilitaires fournis avec votre système d'exploitation. `Nautilus` sous UNIX/`GNOME` permet de visualiser les polices sous forme de vignettes et de table ; il existe également des logiciels spécialisés.

[FontForge](https://fontforge.org/) permet de visualiser et de modifier les polices Postscript, TrueType et OpenType, ce qui est utile si vous souhaitez ajouter des glyphes.

## Formats spécifiques à TeX

Avec TeX, lancer « TeX testfont » en ligne de commande, ensuite, au prompt, entrer le nom de la police (par exemple \\police\{cmr10}), puis `\table` et enfin `\end`.

Le résultat sera disponible dans le fichier `testfont.dvi`.

Avec LaTeX, lancer `latex nfssfont` (ou `pdflatex nfssfont`, etc...) en ligne de commande. Ce programme est une adaptation du programme original de D. Knuth. Il vous demandera d'indiquer d'abord le nom d'une fonte, ensuite les commandes pour générer le fichier. Dans le deuxième prompt, utilisez la commande `\help` pour avoir une présentation des commandes possibles.

Le résultat sera disponible dans le fichier `nfssfont.dvi` ou `nfssfont.pdf`, suivant le moteur utilisé. Les commandes sont identiques à celles du programme original avec quelques particularités.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX,Postscript
```
