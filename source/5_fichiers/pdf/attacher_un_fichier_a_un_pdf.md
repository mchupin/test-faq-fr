# Comment attacher un fichier à un PDF avec LaTeX ?

Le format PDF est bien pratique pour distribuer des documents électroniques qui auront toujours la même apparence, quelle que soit la plate-forme. Depuis la version 1.3 du format PDF (publiée en 1999), il est possible d'« attacher des fichiers » à un document PDF, c'est-à-dire d'y inclure des fichiers arbitraires, de la même façon qu'on peut attacher des fichiers à un email.

La personne qui lira le document PDF pourra extraire ces fichiers et les enregistrer sur son ordinateur (généralement, il suffit de cliquer sur l'icône représentant le fichier attaché).

## Avec l'extension « attachfile2 »

L'extension <ctanpkg:attachfile2> permet d'attacher un ou plusieurs fichiers à votre document PDF. Elle reprend et étend les fonctionnalités de l'extension <ctanpkg:attachfile> et vous avez intérêt à consulter [la documentation de cette dernière extension](texdoc:attachfile) pour en connaître la syntaxe.

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage{attachfile}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Longtemps, je me suis couché de bonne heure.

\noattachfile
\end{document}
```

Des options permettent de changer l'icône utilisée pour représenter le fichier ou d'afficher son nom.

______________________________________________________________________

*Sources :*

- [Portable document format](wpfr:Portable_Document_Format),
- [Links and attachments in PDFs](https://helpx.adobe.com/acrobat/using/links-attachments-pdfs.html).

```{eval-rst}
.. meta::
   :keywords: LaTeX,fichier PDF,fichier attaché,document attaché,document joint à un PDF,pièce jointe,Portable Document Format
```
