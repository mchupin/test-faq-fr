# Quality of PDF from PostScript

Any reasonable PostScript, including any output of `dvips`, may be converted to PDF, using (for example) a sufficiently recent version of [Ghostscript](https://www.ghostscript.com/), Frank Siegert's (shareware) [PStill](http://www.pstill.com/), or *Adobe Distiller* (commercial).

But, although the job may (almost always) be done, the results are often not acceptable : the most frequent problem is bad presentation of the character glyphs that make up the document. The following answers offer solutions to this (and other) problems of bad presentation. Issues covered are :

- [Wrong type of fonts used](/5_fichiers/fontes/mon_document_est_flou_a_cause_des_fontes_t3), which is the commonest cause of fuzzy text.
- [“Ghostscript” too old](/5_fichiers/fontes/mon_document_est_flou_a_cause_de_ghostscript), which can also result in fuzzy text.
- [Switching to font encoding T1 encoding](/5_fichiers/fontes/mon_document_est_flou_quand_je_passe_en_t1), which is yet another possible cause of fuzzy text.
- Another problem --- missing characters --- arises from an [aged version of « Adobe Distiller »](/5_fichiers/pdf/des_caracteres_manquent_dans_mon_fichier_pdf).
- Finally, there's the common confusion that arises from using the `dvips` configuration file `-Ppdf`, the [weird characters](/5_fichiers/fontes/caracteres_bizarres_avec_dvips).

It should be noted that *Adobe Reader* 6 (released in mid-2003, and later versions) does not exhibit the "fuzziness" that so many of the answers below address. This is of course good news : however, it will inevitably be a long time before every user in the world has this (or later) versions, so the remedies below are going to remain for some time to come.

The problems are also discussed, with practical examples, in Mike Shell's <ctanpkg:testflow> package, which these FAQs recommend as a [specialised tutorial](/1_generalites/documentation/documents/tutoriels/tutoriaux)".

______________________________________________________________________

*Source :* [Quality of PDF from PostScript](faquk:FAQ-dvips-pdf)

```{eval-rst}
.. meta::
   :keywords: LaTeX,produire un fichier PDF,fichiers Postscript,PS to PDF,convertir Postscript en PDF
```
