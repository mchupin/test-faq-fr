# Conversions entre divers formats

Certaines réponses sont [déjà disponibles sur une autre page.](/6_distributions/conversion/logiciels_de_conversion_de_formats_de_texte)

- Comment convertir du LaTeX en *Microsoft Word* ?
- Comment convertir du *Microsoft Word* en LaTeX ?
- Comment convertir du *Scribe* en LaTeX ?
- Comment convertir du *WordPerfect* en LaTeX ?
- Comment convertir du LaTeX en RTF ?
- Comment convertir du RTF en AllTeX ?
- Comment convertir du *Microsoft Excel* en LaTeX ?
- Comment convertir du TeX en *Framemaker* ?
- Comment convertir du *WinWord* en LaTeX ?
- Comment convertir un fichier 8 bits en fichier 7 bits ?
- Comment convertir un fichier *ChiWriter* en TeX ?
- Où trouver une FAQ de convertisseurs AllTeX/Traitement de texte ?
- Comment définir son propre format de sortie ?

```{eval-rst}
.. meta::
   :keywords: LaTeX,Microsoft Word,convertir en LaTeX
```
