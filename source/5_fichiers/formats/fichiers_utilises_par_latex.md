# Quels sont les fichiers utilisés par LaTeX ?

Pour pouvoir compiler un fichier source `tex`, LaTeX utilise des macros ou des styles définis dans :

- les fichiers `fmt` de format créés à l'installation de la distribution à partir de fichiers `ltx` ;
- le fichier `cls` qui définit la classe du document ;
- les fichiers des extensions `sty` qui contiennent des commandes ou des styles prédéfinis (modules distribués sous forme de fichiers `ins`, `dtx` à compiler, bien que ce ne soit pas toujours le cas, certaines extensions se limitant à un simple fichier `tex` ou `sty`) ;
- les fichiers de fontes qui décrivent les caractères utilisés : `tfm`, `mf`, `fd`, `pk`.

Après une première compilation, LaTeX crée plusieurs fichiers et il en utilisera certains lors des compilations suivantes (en général trois compilations seront suffisantes) :

- le fichier `log` qui contient des messages d'informations et les erreurs éventuelles de compilation ;
- le fichier `aux` qui servira en particulier pour les références ;
- le fichier `toc` qui contient la table des matières ;
- les fichiers `lot`, `lof` qui contiennent la liste des tables, des figures ;
- le fichier `dvi` résultat à visualiser (pdfLaTeX produit un `pdf`) qui pourra être transformé en `ps` ;
- les fichiers `bib`, `bst` et `bbl`, `blg` pour les bibliographies ;
- les fichiers `idx`, `ind`, `ilg` et `ist` pour l'index.

```{eval-rst}
.. meta::
   :keywords: Format DVI,LaTeX
```
