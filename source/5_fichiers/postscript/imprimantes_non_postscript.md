# Où trouver un pilote DVI pour cette imprimante ?

Dans les premières années où TeX a existé, des pilotes DVI ont été écrits pour tous les types d'imprimantes imaginables (à l'époque). Maintenant, on se demande plutôt pourquoi il y avait besoin de tant de pilotes différents... C'est lié à la standardisation des formats d'impression, grâce à PostScript, langage de description de page à la fois souple et puissant. Avec [ghostscript](https://www.ghostscript.com/) (et sa vaste gamme de pilotes d'imprimantes), il y a maintenant peu de demande pour de nouveaux développements autour du format DVI.

Actuellement, il est tout à fait sensé de [générer du PostScript](/5_fichiers/postscript/generer_un_fichier_postscript_a_partir_d_un_dvi) et d'utiliser [ghostscript](https://www.ghostscript.com/) pour envoyer la sortie PostScript résultante vers votre imprimante, quel que soit son modèle.

:::{note}
Si vous utilisez un système Linux ou Unix quelconque, vous utilisez sans doute [CUPS](wpfr:Common_Unix_Printing_System) pour piloter votre imprimante. Dans ce cas, c'est CUPS qui s'occupe de tout, éventuellement en appelant Ghostscript sans que vous ayez à vous soucier de quoi que ce soit.
:::

______________________________________________________________________

*Sources :*

- [Output to "other" printers](faquk:FAQ-otherprinters),
- [Printer Driver Basics](https://ghostscript.com/doc/cups/libs/filter/postscript-driver.shtml) : CUPS et Ghostscript.

```{eval-rst}
.. meta::
   :keywords: LaTeX,PostScript,imprimer un document LaTeX,pilote DVI,pilote d'impression
```
