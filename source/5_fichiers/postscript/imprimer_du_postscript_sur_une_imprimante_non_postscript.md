# Comment imprimer un fichier PostScript sur une imprimante non PostScript ?

- Il faut utiliser le logiciel `GhostScript` d'Artifex Software, Inc. et artofcode LLC. L'URL de la page d'accueil est <http://www.ghostscript.com/>. Ce logiciel existe sous deux formes, une sous license GPL et une autre, libre mais de license différente (voir les détails sur la page d'accueil). La version GPL est toujours un peu plus ancienne de quelques mois et, donc, ne supporte pas toujours les derniers modèles d'imprimantes.

Ce logiciel interprète le langage PostScript et le traduit dans le langage particulier de chaque imprimante ([PCL](wpfr:PCL_(langage_d'impression)) par exemple). Il est utilisé comme un filtre. Il est également utilisé par des visualiseurs qui le font travailler en arrière plan. La configuration est assurée par des « spoolers » d'imprimante, qui enchaînent les filtres de façon adéquate, et qui sont configurables, soit en ligne de commande, soit à travers une interface graphique. Voir [CUPS](wpfr:Common_Unix_Printing_System) par exemple, mais il y en a d'autres.

______________________________________________________________________

*Source :* [Ghostscript](wpfr:Ghostscript).

```{eval-rst}
.. meta::
   :keywords: LaTeX,PostScript,imprimer un document PostScript
```
