# Comment manipuler un fichier PostScript ?

- Les programmes suivants font partie du package <ctanpkg:psutils>. Ils ont utilisables en ligne de commande sous Unix et Windows.
- `psnup` permet de faire tenir plusieurs pages sur une seule, ce qui est très utile pour économiser du papier à l'impression.
- `pstops` retravaille le fichier PostScript de la même manière que `dvidvi` avec les `dvi` (*i.e.*, on peut redimensionner, retourner, réordonner les pages).
- `psbook` réordonne les pages pour l'impression de livres ou de livrets.
- `psmerge` concatène plusieurs fichiers PostScript en un seul.
- `psselect` permet de sélectionner un certain nombre de pages dans un fichier PostScript.
- `epssfit` met à l'échelle d'une « bounding-box » un fichier `epsf`.
- Le programme `GhostView` (save marked pages) permet lui aussi de sélectionner une partie d'un fichier. Pour l'obtenir, voir <http://www.ghostscript.com>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,PostScript,imprimer plusieurs pages par feuille,changer l'ordre d'un fichier PostScript
```
