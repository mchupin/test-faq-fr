# Comment changer le chemin recherche de fichiers de TeX/LaTeX ?

## Sous Unix ou Linux

Si l'on veut ajouter un répertoire/dossier au chemin de recherche de TeX et LaTeX, il faut redéfinir la variable d'environnement `$TEXINPUTS`.

Par exemple, les utilisateurs de `tcsh` taperont la ligne suivante pour ajouter le répertoire `/home/mespack/` au chemin de recherche de TeX et LaTeX :

```bash
setenv TEXINPUTS /home/mespack// :
```

Si les deux points (qui servent de séparateur) sont au début, à la fin ou bien doublés, alors `kpathsea` (le programme qui cherche les fichiers utilisés par TeX) remplace ceux-ci par le chemin d'accès défini précédement, qui est en général celui de `texmf.cnf`.

Et le fait d'utiliser la double barre `/``/` signifie que tous les sous-répertoires de `/home/mespack` seront parcourus.

```{eval-rst}
.. meta::
   :keywords: LaTeX,kpathsea,recherche de fichier LaTeX,répertoires LaTeX
```
