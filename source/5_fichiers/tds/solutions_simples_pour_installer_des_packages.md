# Existe-t-il des méthodes d'installation simples de fichiers ?

Quelques cas proposent des solutions où peuvent être contournés la plupart des problèmes d'installation :

- si vous êtes un utilisateur de `MiKTeX`, son [système de gestion d'extension](/6_distributions/installation/miktex) peut généralement vous aider ;
- de même, si vous êtes un utilisateur de TeX `Live`, [TeX Live manager](/6_distributions/installation/texlive) peut généralement vous aider ;
- enfin, l'extension qui vous intéresse existe peut-être déjà sous forme de fichier ZIP formaté pour permettre une [installation directe](/5_fichiers/tds/installer_a_partir_d_un_fichier_zip2).

______________________________________________________________________

*Source :* [Shortcuts to installing files](faquk:FAQ-inst-scut)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer un package LaTeX,installer une extension LaTeX
```
