# Comment convertir du WEB en LaTeX ?

:::{todo} // à rédiger //
:::

[WEB](wpfr:WEB)

- `SchemeWEB`, de J. Ramsdell, est disponible sur [SchemeWEB](ctanpkg:schemeweb).
- Il existe également des programmes pour de nombreux autres langages. Ainsi, `ocamlweb` (<http://www.lri.fr/~filliatr/ocamlweb/index.fr.html>) permet de faire de la programmation littéraire en CaML.

```{eval-rst}
.. meta::
   :keywords: LaTeX,conversion de format,programmation lettrée,programmation littéraire,TeX et Pascal,TANGLE,WEAVE
```
