# Comment mettre en page un manuel de référence ?

## Avec la classe « refman »

La classe <ctanpkg:refman> est faite pour ça. Vous pouvez ici consulter le [manuel de la classe refman](texdoc:refman) et l'[annexe concernant les classes refart et refrep](texdoc:layout_e).

## Avec la classe « memoir »

La classe <ctanpkg:memoir>, et son [excellente documentation](texdoc:memman), peut aussi être utilisée. Voir notamment [sa documentation sur les en-têtes de chapitres.](texdoc:MemoirChapStyle)

______________________________________________________________________

*Sources :*

- <https://tex.stackexchange.com/questions/3852/latex-template-for-a-technical-reference-manual-user-guide>

```{eval-rst}
.. meta::
   :keywords: LaTeX,classe,documentation technique
```
