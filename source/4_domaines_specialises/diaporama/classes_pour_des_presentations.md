Comment préparer une présentation (avec des « diapos ») ? =======================================================

:::{tip}
La réponse à cette question donne un aperçu historique détaillé. Mais si vous êtes pressé, regardez directement la classe <ctanpkg:beamer>, qui permet de préparer des présentations à projeter avec un vidéoprojecteur.
:::

La version originale de LaTeX, quand Leslie Lamport l'a conçue, proposait un programme séparé, `SliTeX` (comme *slide-TeX*), pour produire des diapositives ; cela date de l'époque où l'on faisait fabriquer de vraies [diapositives](wpfr:Diapositive) pour les conférences, ce qui demandait de séparer les couleurs pour l'impression. Le programme en question s'occupait de ça, et donnait le résultat attendu, à l'époque. Lorsque LaTeX2e est arrivé, SliTeX n'avait plus de raison d'être un programme séparé, et ses fonctionnalités ont été fournies par la classe <ctanpkg:slides>. C'est plus simple, mais ça ne change plus grand chose, car plus personne n'utilise de diapositives de nos jours.

Quand les diapositives ont cédé la place aux [transparents](wpfr:Transparent_(projection)) (ces feuilles de rhodoïd à placer sur la vitre d'un rétroprojecteur), les classes <ctanpkg:seminar> and [FoilTeX](ctanpkg:foiltex) ont été développées. Là encore, la technologie et les modes ont rendu ce type de support obsolète. Mais les fonctionnalités de ces classes ont été étendues pour s'adapter à l'usage d'un [vidéoprojecteur](wpfr:Vidéoprojecteur) (voir ci-dessous).

L'avènement de [Microsoft PowerPoint](wpfr:Microsoft_PowerPoint) (aussi médiocres qu'aient été ses premières versions) a créé une demande pour des diapos animées : des images qui révèlent leur contenu d'une manière plus élaborée qu'en remplaçant simplement une page par une autre, comme c'était la norme lorsque <ctanpkg:slides>, [FoilTeX](ctanpkg:foiltex) et <ctanpkg:seminar> ont été conçus.

# Les solutions modernes

## La classe « beamer »

[Beamer](ctanpkg:beamer) est une classe très puissante, mais relativement facile à maîtrise, Comme son nom l'indique [^footnote-1], elle a été conçue pour être utilisée avec des vidéoprojecteurs. Elle utilise l'extension [TikZ](ctanpkg:pgf) pour le support graphique. Il est également très simple de sortir une version imprimée de la présentation préparée avec Beamer.

De très nombreux thèmes sont été créés pour Beamer : [voici une galerie d'exemples](http://deic.uab.es/~iblanes/beamer_gallery/).

## La classe « elpres »

:::{todo} Il existe aussi <ctanpkg:elpres> parmi les packages actuellement maintenus.
:::

# Les solutions anciennes

Pour poursuivre l'aperçu historique de l'introduction, la classe <ctanpkg:prosper> s'appuie sur <ctanpkg:seminar> pour fournir des effets dynamiques et autres; elle permet à la fois de produire une présentation à projeter directement, ou d'imprimer des transparents. Le paquet complémentaire <ctanpkg:ppr-prv> ajoute des fonctions de "prévisualisation" (ce que l'on appelle communément « l'impression de polycopiés »). Le paquetage <ctanpkg:HA-prosper>, que vous chargez avec <ctanpkg:prosper>, corrige quelques bogues, ajoute plusieurs fonctions et fournit des thèmes pour les diapositives. La classe (plus récente) <ctanpkg:powerdot> est conçue comme un remplacement de <ctanpkg:prosper> et <ctanpkg:HA-prosper>, co-écrite par l'auteur de HA-prosper.

> <ctanpkg:Talk> est une autre classe très fonctionnelle, mais facile à apprendre, qui prétend différer de <ctanpkg:beamer> et autres systèmes mentionnés ci-dessus en ce qu'elle ne vous impose pas un style de diapositive. Vous pouvez spécifier plusieurs styles, et pouvez passer de l'un à l'autre entre les diapositives, selon vos besoins. La classe elle-même ne fournit qu'un seul style, sous le nom `greybars` : la suggestion de l'auteur est que les utilisateurs apportent leurs contributions, comme avec la Beamer Gallery.
>
> > <ctanpkg:Lecturer> est une solution générique (il fonctionne avec Plain TeX, LaTeX et ConTeXt mk ii, mais pas encore avec ConTeXt mk iv). En séparant les fonctionnalités nécessaires à une présentation (en utilisant TeX pour la composition, et les fonctions PDF pour la superposition et les effets dynamiques), une structure claire émerge. Bien qu'il n'ait pas la gamme de thèmes de Beamer, il semble être un candidat alternatif utile.
> >
> > <ctanpkg:Present> est conçu pour être utilisé avec Plain TeX uniquement ; sa conception est simple, à tel point que son auteur espère que les utilisateurs seront eux-mêmes capables de modifier ses macros.
> >
> > <ctanpkg:Ppower4> (communément appelé `pp4`) est un programme auxiliaire en Java qui post-traite le PDF, pour « animer » le fichier aux endroits que vous avez marqués avec des commandes de l'extension `pp4`. Attention, ces commandes ne fonctionnent que sur les PDF générés par pdfLaTeX, LaTeX, ou `dvipdfm` (sur la sortie de LaTeX); elles ne fonctionnent pas si vous êtes passé par `dvips`.
> >
> > > <ctanpkg:Pdfscreen> et <ctanpkg:texpower> sont des extentions complémentaires qui permettent des effets dynamiques dans des documents formatés dans des classes « plus modestes » ; <ctanpkg:pdfscreen> vous permettra même d'insérer des « effets de présentation » dans un document de classe <ctanpkg:article>.

Un examen plus détaillé des alternatives (y compris des exemples de code utilisant plusieurs d'entre elles) peut être trouvé sur [le site de Michael Wiedmann](http://www.miwie.org/presentations/presentations.html) (en anglais).

# Et avec ConTeXt ?

Les utilisateurs de ConTeXt verront que tout ce dont ils ont besoin ou presque se trouve déjà dans ConTeXt lui-même ; le wiki ConTeX garden fournit [un résumé utile de ce qui est disponible](http://wiki.ConTeXtgarden.net/Presentation_Styles), avec des exemples (en anglais).

______________________________________________________________________

*Sources :*

- [Producing presentations (including slides)](faquk:FAQ-slidecls),
- [Tutoriel : Créez vos diaporamas en LaTeX avec Beamer !](https://pub.phyks.me/sdz/sdz/creez-vos-diaporamas-en-latex-avec-beamer.html)
- [Beamer](wpfr:Beamer).

```{eval-rst}
.. meta::
   :keywords: LaTeX,PowerPoint,slides,diapositives,transparents,diaporamas,présentations,support de présentation
```

[^footnote-1]: *beamer* est un pseudo-anglicisme désignant un vidéoprojecteur en allemand, néerlandais et français de Suisse
