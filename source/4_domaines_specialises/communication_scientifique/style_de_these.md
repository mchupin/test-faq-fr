# Où trouver un style de thèse ?

- Les styles de thèse sont généralement spécifiques à chaque université, il vaut donc mieux commencer par se renseigner en interne sur ce qui existe : auprès de l'école doctorale, bien sûr, mais aussi à la bibliothèque universitaire, puisque beaucoup donnent des conseils pour la composition de thèses avec LaTeX, par exemple :
- [Bibliothèques de l'Université d'Aix-Marseille](https://bu.univ-amu.libguides.com/latexamu),
- [Bibliothèque de l'école polytechnique de Montréal](https://guides.biblio.polymtl.ca/c.php?g=479762&p=3280410).

Certaines universités ont [déposé leur classes ou canevas sur le CTAN](https://ctan.org/topic/dissertation).

N'oubliez pas qu'il est souvent difficile de produire une thèse qui soit à la fois belle et conforme au style demandé par votre université... Par exemple de nombreuses universités exigent encore des mémoires de thèse à double interligne, comme du temps des machines à écrire mécaniques ! Si vous devez vous y résoudre [cette page vous dit comment faire](/3_composition/texte/paragraphes/modifier_l_interligne).

Si votre école doctorale ne vous propose rien de particulier, le meilleur choix est sans doute de partir de la classe <ctanpkg:memoir>, qui étend la classe <ctanpkg:book> et est très facilement adaptable. [Sa documentation (en anglais)](texdoc:memman) est remarquable.

:::{important}
Si vous utilisez une classe peu répandue, vous aurez peut-être du mal à trouver de l'aide sur internet.
:::

## Classes dédiées aux thèses

- La classe <ctanpkg:yathesis>, basée sur `book`, vise à aider à mettre en forme son mémoire de thèse suivant les règles du ministère de l'enseignement supérieur français. Elle est entièrement [documentée en français](texdoc:yathesis) et propose des canevas pour commencer rapidement à rédiger son mémoire.
- La classe <ctanpkg:hecthese>, basée sur `memoir` et mise à disposition par HEC Montréal, est également [documentée en français](texdoc:hecthese). Elle prévoit le cas des thèses sur articles.
- Les universités de Californie et de Washington proposent chacune leur style de mémoire de thèse qui peut servir de point de départ :
- <ctanpkg:ucthesis>,
- <ctanpkg:uwthesis>.

:::{warning}
La classe <ctanpkg:thesis> est une classe de thèse basée sur la classe `report`, mais son développement s'est arrêté en 1995 et la plus grande partie de sa documentation est uniquement en allemand. Il vaut sans doute mieux ne pas perdre de temps à essayer de l'utiliser.
:::

______________________________________________________________________

*Sources :*

- [Formatting a thesis in LaTeX](faquk:FAQ-thesis),
- [Template complet pour manuscrit de thèse](https://blog.dorian-depriester.fr/latex/template-these/template-complet-pour-manuscrit-de-these) (blog de Dorian Depriester).

```{eval-rst}
.. meta::
   :keywords: LaTeX,manuscrit de thèse,tapuscrit de thèse,mémoire de thèse,page de garde
```
