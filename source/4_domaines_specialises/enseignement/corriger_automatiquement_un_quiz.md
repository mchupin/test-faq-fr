# Comment corriger automatiquement un quiz ?

- Avec l'extension <ctanpkg:eq-pin2corr>, basée sur <ctanpkg:exerquiz>, vous pouvez pré-corriger un quiz. Les réponses sont stockées dans le document, protégées par un mot de passe. Il suffit d'entrer ce mot de passe pour corriger le quiz et mettre la note.

Le mot de passe (un nombre à quatre chiffres) est stocké sous forme hachée dans le document. Pour calculer le hachage, utilisez <texdoc:get-hash-string> (à ouvrir avec Adobe Reader).

:::{important}
Cette extension utilise Javascript. Les documents produits sont consultables avec la plupart des visualisateurs PDF, mais, actuellement, seul Adobe Reader permet d'utiliser les fonctionnalités de remplissage et de correction du quiz.
:::

:::{todo} Ajouter un exemple (mais <ctanpkg:eq-pin2corr> n'est pas disponible sur TeXlive 20017).
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,enseignant,professeur,enseignement,examen,contrôle,QCM,corrigé avec mot de passe,solutions avec mot de passe
```
