# Comment réaliser des QCM ?

- La classe <ctanpkg:exam> permet de mettre en forme des sujets d'examen, et notamment des *questionnaires à choix multiples* :

```latex
% !TEX noedit
Combien le cheval possède-t-il de pattes ?
\begin{choices}
   \choice deux pattes
   \choice quatre pattes
   \choice zéro patte
\end{choices}
```

```latex
\documentclass{exam}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
Combien le cheval possède-t-il de pattes ?
\begin{choices}
   \choice deux pattes
   \choice quatre pattes
   \choice zéro patte
\end{choices}
\end{document}
```

Avec l'extension <ctanpkg:exam-randomizechoices>, il est de plus possible de rendre l'ordre des réponses aléatoire.

:::{important}
La classe <ctanpkg:exam> a d'abord été développée par son auteur, Philip Hirschhorn, sous forme d'une extension [exam.sty](http://tug.ctan.org/obsolete/macros/latex209/contrib/exam/exam.sty), pour LaTeX 2.09 (jusqu'en 1994).

Cette extension est maintenant complètement remplacée par la classe <ctanpkg:exam>.

Il existe deux autres classes de noms proches, mais incompatibles : <ctanpkg:exams> (de Hans van der Meer) et [exams.sty](https://www3.nd.edu/~taylor/ComputerStuff/TeX/stuff/2014-09-01-version/examsManual.pdf) (de Laurence R. Taylor).

Attention aux confusions !
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,questionnaires à choix multiples,examens,sujets d'examen aléatoires,formulaires
```
