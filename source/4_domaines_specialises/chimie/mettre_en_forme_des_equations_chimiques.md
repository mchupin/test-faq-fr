# Comment mettre en forme des formules chimiques ?

Il existe des packages pour écrire

- les équations de réaction : mhchem, chemmacros (et chemformula) notamment;
- d'autres pour représenter des structures de molécules (organiques) : chemfig notamment. Ces packages conviennet pour de petites structures, certainement moins pour un usage intensif.
- Le package <ctanpkg:chemfig> permet de représenter des **structures chimiques** à l'aide de TikZ. Il est maintenu et semble le plus utilisé.
- Les packages <ctanpkg:chemmacros> et <ctanpkg:chemformula> permettent notamment d'écrire des **réactions chimiques**.
- Le package <ctanpkg:ppchtex> permet d'écrire des formules chimiques. L'exemple ci-dessous présente son utilisation avec LaTeX et <ctanpkg:babel> :

:::{warning}
Il y a une incompatibilité avec le package <ctanpkg:babel> ou le shareware [French Pro](ctanpkg:french), due au fait que ces packages rendent actifs les deux-points. Il faut alors utiliser la commande `\nonfrench` de French Pro ou la commande `\selectlanguage` de <ctanpkg:babel> qui aura alors été appelé avec les deux options de package `francais` et `english`.
:::

```latex
\documentclass[twocolumn]{article}
\usepackage{etex}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[a4paper,margin=2.5cm]{geometry}

\usepackage{m-pictex,m-ch-en}
\usepackage[english,francais]{babel}

\begin{document}

Voici quelques exemples de ce que peut faire \texttt{ppchtex}
(repris de la page de Manuel Luque).

Attention~ : lorsqu'on utilise \texttt{babel},
il faut sortir du mode français avant de saisir
les formules chimiques.

\selectlanguage{english}
\startchemical[scale=big,%
               size=big,%
               width=fit,%
               height=fit]
 \bottext{Dioxyde de carbone}
 \chemical[ONE,Z0,DB15,SAVE,DIR1,3OFF1,Z0,EP28,
           RESTORE,DIR5,DIR5,2OFF1,Z0,EP46]%
          [C,O,O]
\stopchemical
\selectlanguage{french}

Et on revient en mode français après l'équation.

\selectlanguage{english}
\begingroup
\setupchemical[size=big,scale=big,width=fit]

\startchemical[height=fit,bottom=6000]
\chemical[ONE,DB1,Z01,SB3,MOV3,Z015,BB15,SB3,
          MOV3,Z015,BB15,SB3,MOV3,Z015,BB15,SB3,
          MOV3,Z0135,BB15,SB3]
         [HC,O,C,OH,H,C,H,HO,C,OH,H,C,OH,CH_2OH,H]
\bottext{\textit{Représentation de Cram}}
\stopchemical

\startchemical[height=fit,bottom=3000]
\chemical[SIX,B123,B56,ER5,RB1,RD236,R4,RZ123456,
          Z123456]%
     [OH,OH,OH,OH,O,OH,\TL{3 }{},
          \R{\hphantom{22} 4}{},%
      \T{5}{},\T{6}{},\BR{1}{},\B{2}{}]
\bottext{\textit{Représentation du D-glucose}}
\stopchemical
\endgroup

\begingroup
\setupchemical[size=big,scale=big]
\startchemical[height=4500,bottom=1500]
  \chemical[SIX,ROT4,B,C,R12,RBN]
  \chemical[PB:RZ1,ONE,Z0,SB5,MOV5,Z04,CZ6,SB4,%
            DB6,PE]
           [O,C,H_3C,O]
  \chemical[PB:RZ2,ONE,Z0,CZ6,SB1,DB6,MOV1,Z0,%
            SB2,DIR2,Z0,SB1,MOV1,3OFF1,Z0,5OFF1,%
            SB1,MOV1,Z07,SB17,MOV1,Z02,CZ8,SB2,%
            DB8,PE]%
           [C,O,O,NH,(CH_2)_4,CH,NH_2,C,OH,O]
\bottext{\textit{Acétylsalicylate de lysine}}
\stopchemical
\endgroup

\startchemical[scale=big,size=big,%
               width=fit,height=8000]
    \bottext{4-éthyl-2,5-diméthylhexane}
    % Commençons par le carbone 7
    \chemical[ONE,Z0,SB1]
             [\TL{7}{CH_3}]
    % poursuivons avec le C6
    \chemical[MOV1,Z0,SB1]
             [\TL{6}{CH_2}]
    % puis C5 et sa ramification
    \chemical[MOV1,Z03,SB13]
             [\TL{5}{CH},CH_3]
    %puis C4
    \chemical[MOV1,Z0,SB3]
             [\TL{4}{CH}]
    % puis la ramification vers C3...
    % SAVE pour sauvegarder la position initiale
    % avant de ramifier
    \chemical[SAVE,MOV3,Z0]
             [\TL{3}{CH_2}]
    \chemical[SAVE,MOV3,Z013,SB137]
             [\TL{2}{CH},\TL{1}{CH_3},CH_3]
    % on termine avec les deux derniers atomes
    % de carbone après être revenu au carbone 4
    \chemical[RESTORE,RESTORE,MOV1,Z01,SB15]
             [CH_2,CH_3]
\stopchemical
\selectlanguage{french}

Suite du texte en français.

\end{document}
```

- Le package <ctanpkg:mcf2graph> utilise le *Molecular Coding Format* pour décrire les structures chimiques de façon très compacte, puis les dessiner.
- Les utilisateurs de Windows peuvent utiliser [MDL Isis Draw](wpfr:ISIS/Draw) qui est gratuit pour toute utilisation personnelle ou académique. Ce logiciel permet de créer ses propres structures et de les sauver au format EPS. Il est disponible à : <http://www.mdli.com/downloads/downloadable/index.jsp>.
- Les utilisateurs de Linux peuvent jeter un coup d'œil à <http://www.randomfactory.com/lfc/lfc.html>.
- Il existe le package <ctanpkg:chemsym>.
- Le package <ctanpkg:XyMTeX> permet de définir des structures chimiques.

:::{note}
**Commentaire de F. Jacquet:**

- <ctanpkg:XyMTeX> est incompatible avec le shareware French Pro de Bernard Gaulle. Pour utiliser les deux dans un même document, il suffit de repasser en mode `english` avant la macros puis `french` après. Je ne sais pas comment cela fonctionne pour les `caption` mais en théorie, cela devrait marcher ! TODO : à verifier pour Babel french.
- <ctanpkg:XyMTeX> possède le gros défaut de ne pas pouvoir faire de longues chaînes aliphatiques si l'on ne sait pas programmer le nombre de points entre deux structures. Le plus simple dans ce cas est donc l'emploi de `Xfig` (ou autre);
- on ne peut pas imbriquer les formules, ce qui gêne considérablement son utilisation; en revanche, pour les cholestérols l'ensemble est très puissant !
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,chimie,formule développée,structure chimique,chimie organique
```
