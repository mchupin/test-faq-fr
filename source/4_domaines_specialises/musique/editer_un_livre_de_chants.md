# Comment éditer un livre de chants ?

- Le package <ctanpkg:songbook>, permet de composer un livre de chants. Quelques exemples sur \\Songbook sont donnés sur la page <http://rath.ca/Misc/Songbook/index.shtml>.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
