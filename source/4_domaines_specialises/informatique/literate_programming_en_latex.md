# Peut-on faire du « literate programming » avec LaTeX ?

La [programmation lettrée](wpfr:Programmation_lettrée) (*literate programming*) est un paradigme de programmation introduit par Donald Knuth dans lequel un programme informatique est associé à une explication de sa logique en langage naturel (comme l'anglais ou le français). Concrètement, le texte en langage naturel est entrecoupé du code source usuel, découpé en petits morceaux. Ainsi, à partir des mêmes fichiers, l'humain peut comprendre le code, et la machine peut le compiler. Cette approche est souvent utilisée dans l'informatique scientifique et en *data science*, avec l'idée que cela permet une analyse reproductible des données.

Knuth a appliqué les principes de la programmation lettrée au développement de TeX lui-même, qui est écrit en [WEB](wpfr:WEB), un dialecte de Pascal supportant ce paradigme.

- Pour votre code LaTeX, le package <ctanpkg:doc> ou sa réimplémentation <ctanpkg:xdoc> sont faits pour ça, avec l'utilitaire <texdoc:docstrip> fourni avec LaTeX.
- ConTeXt utilise une approche intéressante pour pour documenter le code des macros, avec différents types de commentaires. Par exemple, les lignes commençant par ''%D '' (suivi d'une espace) contiennent la documentation, les lignes commençant par `%C` contiennent la notice de copyright, et les lignes commençant par `%M` contiennent les définitions de macros qui sont utilisées pour compiler la documentation. Tous les fichiers de base de ConTeXt sont documentés de cette manière.

Voir aussi la question « [Qu'est-ce que la « programmation lettrée« ?](/5_fichiers/web/literate_programming) ».

______________________________________________________________________

*Sources :*

- [Different approach to literate programming for LaTeX](https://tex.stackexchange.com/questions/47237/different-approach-to-literate-programming-for-latex),
- [How to do literate programming in TeX?](https://tex.stackexchange.com/questions/46989/how-to-do-literate-programming-in-tex)
- [The Computer Science of TEX and LATEX](https://pages.tacc.utexas.edu/~eijkhout/Articles/TeXLaTeXcourse.pdf) (Victor Eijkhout; page 225 et suivantes).

```{eval-rst}
.. meta::
   :keywords: LaTeX,développement,programmation littérale,programmation littéraire,documentation de code,commentaires
```
