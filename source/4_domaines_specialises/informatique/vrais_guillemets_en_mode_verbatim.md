# Comment avoir des guillemets réalistes dans du code en verbatim ?

- La fonte `cmtt` a des guillemets « arrondis », qui sont jolis, mais ne correspondent pas vraiment à ce que l'on voit dans un `xterm` ou un éditeur de texte :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
\begin{verbatim}
`J'ai une touche', dit le clavier.
\end{verbatim}
\end{document}
```

L'apparence de ces guillemets est importante pour la compréhension des exemples de code, par exemple dans les livres didactiques. L'extension <ctanpkg:upquote> modifie le comportement de l'environnement `verbatim` et de la commande `\verb` pour que leur sortie soit une représentation plus fidèle de ce que l'utilisateur doit saisir :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage{upquote}
  \pagestyle{empty}

\begin{document}
\begin{verbatim}
`J'ai une touche', dit le clavier.
\end{verbatim}
\end{document}
```

## Avec l'extention « listings »

- Le package <ctanpkg:upquote> fonctionne également si vous utilisez <ctanpkg:listings> pour formatter votre code :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage{listings}
  \pagestyle{empty}

\begin{document}
\begin{lstlisting}
`J'ai une touche', dit le clavier.
\end{lstlisting}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage{listings}
  \usepackage{upquote}
  \pagestyle{empty}

\begin{document}
\begin{lstlisting}
`J'ai une touche', dit le clavier.
\end{lstlisting}
\end{document}
```

______________________________________________________________________

*Source :*

- [Realistic quotes for verbatim listings](faquk:FAQ-upquot),
- [How to have straight single quotes in lstlistings](https://tex.stackexchange.com/questions/145416/how-to-have-straight-single-quotes-in-lstlistings).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme,guillemets dans le code,guillemets en verbatim
```
