# Comment inclure un fichier verbatim ?

Le mode verbatim est exposé à la question « [Comment écrire en mode verbatim?](/4_domaines_specialises/informatique/ecrire_en_mode_verbatim) ». Ici, nous proposons des solutions pour inclure un fichier externe dans votre document.

- Un bon moyen est d'utiliser l'extension <ctanpkg:verbatim> de Rainer Schöpf, qui fournit une commande `\verbatiminput`, qui prend un nom de fichier comme argument :

```latex
% !TEX noedit
\usepackage{verbatim}
...
\verbatiminput{verb.txt}
```

- Une autre méthode consiste à utiliser l'environnement `alltt`, de l'extension <ctanpkg:alltt>. L'environnement insère son contenu essentiellement en mode verbatim, mais exécute tout de même toutes les commandes (La)TeX qu'il trouve :

```latex
% !TEX noedit
\usepackage{alltt}
...
\begin{alltt}
\input{verb.txt}
\end{alltt}
```

Bien sûr, cela n'est guère utile pour saisir du code source (La)TeX...

- L'extension <ctanpkg:moreverb> étend le paquet <ctanpkg:verbatim>, en fournissant un environnement `listing` et une commande `\listinginput`, qui numérote les lignes du texte du fichier. Cette extension propose également d'une commande `\verbatimtabinput`, qui respecte les caractères TAB dans l'entrée (l'environnement `listing` et la commande `\listinginput` de l'extension respectent également les caractères TAB).
- L'extension <ctanpkg:sverb> fournit de quoi lire un fichier verbatim (sans recourir à <ctanpkg:verbatim>) :

```latex
% !TEX noedit
\usepackage{sverb}
...
\verbinput{verb.txt}
```

- L'extension <ctanpkg:fancyvrb> réimplémente de façon configurable tout ce que <ctanpkg:verbatim>, <ctanpkg:sverb> et <ctanpkg:moreverb> proposent, et bien plus encore. C'est aujourd'hui l'extension de choix pour les amateurs de texte verbatim, mais sa richesse en fait un outil complexe et la lecture de [sa documentation](texdoc:fancyvrb) est fortement conseillée.

:::{note}
La classe <ctanpkg:memoir> intègre les fonctionnalités les plus utiles des extensions <ctanpkg:verbatim> et <ctanpkg:moreverb>.
:::

______________________________________________________________________

*Source :* [Including a file verbatim in LaTeX](faquk:FAQ-verbfile)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,inclure un code-source,inclure un programme,texte verbatim,inclure un script,inclure un programme
```
