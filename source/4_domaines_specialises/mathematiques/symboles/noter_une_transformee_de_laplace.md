# Comment obtenir le « L » de la transformée de Laplace ?

Pour obtenir le "L" de la [transformée de Laplace](wpfr:Transformation_de_Laplace), vous pouvez utiliser l'extension <ctanpkg:mathrsfs> puis la commande `\mathscr{L}`.

Si vous n'avez pas l'usage du caractère `\L` (le « L barré », Ł), vous pouvez le remplacer par ce fameux `L` de Laplace, avec un `\renewcommand` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{mathrsfs}

\renewcommand*{\L}{%
    \ensuremath{\mathscr{L}}}

\begin{document}
$\L$
\end{document}
```

```latex
\documentclass{article}
  \usepackage{mathrsfs}
  \pagestyle{empty}
  \renewcommand*{\L}{\ensuremath{\mathscr{L}}}

\begin{document}
\Huge$\L$
\end{document}
```

:::{important}
Cette extension fait appel à des polices de caractères particulières. Dans les anciennes distributions LaTeX, il fallait les installer volontairement. Ça ne semble plus être le cas aujourd'hui.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,symboles mathématiques,transformée de Laplace
```
