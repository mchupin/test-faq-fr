# Pourquoi le signe moins « - » descend-il sous la ligne de base ?

Avez-vous remarqué que le signe moins ("       $-$") a une profondeur non nulle ? Plus concrètement, il descend sous la ligne de base. Cela se remarque quand on le met sous un signe de racine :

```latex
\documentclass[14pt]{extarticle}
  \usepackage{lmodern}
  \usepackage{tikz}
  \pagestyle{empty}

\begin{document}
\begin{tikzpicture}
\path (0,0) node[inner sep=0pt] (sqrt) {\Huge$\displaystyle\sqrt{1} \quad \sqrt{-1}$} ;

\draw[line width=.2pt,blue] (sqrt.base west) -- (sqrt.base east) ;
\draw[line width=.2pt,green] ([yshift=.30ex]sqrt.south west) -- ([yshift=.30ex]sqrt.south east) ;
\draw[line width=.2pt,red] (sqrt.south west) -- (sqrt.south east) ;
\end{tikzpicture}
\end{document}
```

Vous pouvez voir que        $\sqrt{1}$ descend moins bas que        $\sqrt{-1}$.

- Lorsqu'il a dessiné la police [Computer Modern](wpfr:Computer_Modern), Donald Knuth a choisi de donner au signe moins ("       $-$") la même profondeur qu'au signe plus ("       $+$"), pour que l'alignement soit naturellement maintenu entre les expressions négatives et positives :

```latex
\documentclass[14pt]{extarticle}
  \usepackage{lmodern}
  \usepackage{tikz}
  \pagestyle{empty}

\begin{document}
\begin{tikzpicture}
\path (0,0) node[inner sep=0pt] (sqrt) {\Huge$\displaystyle\sqrt{1} \quad \sqrt{-1} \quad \sqrt{+1}$} ;

\draw[line width=.2pt,blue] (sqrt.base west) -- (sqrt.base east) ;
\draw[line width=.2pt,green] ([yshift=.30ex]sqrt.south west) -- ([yshift=.30ex]sqrt.south east) ;
\draw[line width=.2pt,red] (sqrt.south west) -- (sqrt.south east) ;
\end{tikzpicture}
\end{document}
```

C'est un choix du dessinateur de la police, et n'est pas une règle. Par exemple dans la police [Stix](ctanpkg:stix), le ("       $-$") n'a pas de profondeur tandis que le ("       $+$") en a une :

```latex
\documentclass[14pt]{extarticle}
  \usepackage{stix}
  \usepackage{tikz}
  \pagestyle{empty}

\begin{document}
\begin{tikzpicture}
\path (0,0) node[inner sep=0pt] (sqrt) {\Huge$\displaystyle\sqrt{1} \quad \sqrt{-1} \quad \sqrt{+1}$} ;

\draw[line width=.2pt,blue] (sqrt.base west) -- (sqrt.base east) ;
\draw[line width=.2pt,green] ([yshift=.19ex]sqrt.south west) -- ([yshift=.19ex]sqrt.south east) ;
\draw[line width=.2pt,red] (sqrt.south west) -- (sqrt.south east) ;
\end{tikzpicture}
\end{document}
```

:::{note}
En cliquant sur les images, vous pouvez avoir accès au fichier PDF, ce qui vous permet de zoomer pour mieux voir les détails.
:::

______________________________________________________________________

*Source :*

- [Why do « \\sqrt\{1} » and « \\sqrt\{-1} » have different heights?](https://tex.stackexchange.com/questions/340157/why-do-sqrt1-and-sqrt-1-have-different-heights)

```{eval-rst}
.. meta::
   :keywords: LaTeX,formules mathématiques,signe moins,négatif,profondeur,alignement des caractères
```
