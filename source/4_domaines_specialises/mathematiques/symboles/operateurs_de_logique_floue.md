# Où trouver des opérateurs de logique floue ?

Voici ce que propose Vincent Henn

```latex
%  Format de style permettant d'utiliser directement
% un certain nombre d'operateurs propres à la logique
% floue. Ces opérateurs sont généralement surlignés
% d'un tilde...
%
%    V.H., le 12 avril 1995

\RequirePackage{amsfonts}
\RequirePackage{xspace}

\message{Opérateurs flous}

\newcommand{\fmin}{\mathop{\flou{\min }}}
\newcommand{\fmax}{\mathop{\flou{\max }}}
\newcommand{\V}{\mathop{\mathrm V\kern 0pt}}
\newcommand{\ET}{\mathrel{\mathrm{ET}}}
\newcommand{\OU}{\mathrel{\mathrm{OU}}}
\newcommand{\Sim}{\mathop{\mathrm S \kern 0pt}}
\newcommand{\hauteur}{\mathop{\mathrm h\kern 0pt}}
\newcommand{\card}[1]{\| #1 \|}
\newcommand{\flou}[1]{\ensuremath{\widetilde{#1}}}
\newcommand{\R}{\ensuremath{\Bbb{R}}}
\newcommand{\cpp}{\ensuremath{\hbox{C}^{++}}\xspace}

\newcommand{\Poss}{\mathop{\Pi}}
%\newcommand{\Nec}{\mathop{{\cal{N}}}}
\newcommand{\Nec}{\mathop{\mathrm N\kern 0pt}}

\newcommand{\poss}{\operatoname{Poss}}
\newcommand{\nec}{\operatoname{Néc}}

\newcommand{\serie}[3]{%
%  #1 -> le nom de la variable
%  #2 -> l'indice de début
%  #3 -> l'indice de fin
\ensuremath{{#1}_{#2},\ldots,{#1}_{#3}}}

\newcommand{\DP}{\fsc{Dubois} et \fsc{Prade}\xspace}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
