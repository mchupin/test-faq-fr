# Comment obtenir un texte de théorème en caractères romains ?

Lorsque vous souhaitez utiliser la commande `\newtheorem` sans la contrainte d'un contenu du théorème affiché dans une police italique, plusieurs possibilités s'offrent à vous.

## Avec les commandes de base

L'exemple qui suit montre la manière d'obtenir un environnement dont le contenu est présenté en caractères romains (avec l'environnement « Lemme »). Il montre aussi la présentation classique (avec l'environnement « Remarque »).

```latex
% !TEX noedit
\documentclass{article}

\newtheorem{remarque}{Remarque}

\newtheorem{prelemme}{Lemme}
\newenvironment{lemme}{\begin{prelemme}\upshape}{\end{prelemme}}

\begin{document}
\begin{remarque}
Ce qui suit est un lemme.
\end{remarque}

\begin{lemme}
Si un ensemble $E$ possède une partition en $p$ sous-ensembles contenant chacun $r$ éléments,
alors $E$ contient $p \times r$ éléments.
\end{lemme}
\end{document}
```

```latex
\documentclass{article}

\newtheorem{remarque}{Remarque}

\newtheorem{prelemme}{Lemme}
\newenvironment{lemme}{\begin{prelemme}\upshape}{\end{prelemme}}
\pagestyle{empty}
\begin{document}
\begin{remarque}
Ce qui suit est un lemme.
\end{remarque}

\begin{lemme}
Si un ensemble $E$ possède une partition en $p$ sous-ensembles contenant chacun $r$ éléments,
alors $E$ contient $p \times r$ éléments.
\end{lemme}
\end{document}
```

## Avec l'extension « amsthm »

L'extension AMSLaTeX dédiée à la gestion des théorèmes, <ctanpkg:amsthm>, permet de faire une modification similaire.

## Avec l'extension « ntheorem »

L'extension <ctanpkg:ntheorem> permet de contrôler directement les polices utilisées par les environnements de théorème.

## Avec l'extension « theorem »

{octicon}`alert;1em;sd-text-warning` *L’extension* <ctanpkg:theorem> *est classée comme* [obsolète](/1_generalites/histoire/liste_des_packages_obsoletes)*. Ce qui suit est informatif.*

L'extension <ctanpkg:theorem> permettait aussi d'obtenir cette modification. L'auteur de l'extension recommande d'utiliser à présent <ctanpkg:amsthm> ou <ctanpkg:ntheorem>.

______________________________________________________________________

*Source :* [Theorem bodies printed in a roman font](faquk:FAQ-theoremfmt)

```{eval-rst}
.. meta::
   :keywords: LaTeX,théorèmes en caractères droits
```
