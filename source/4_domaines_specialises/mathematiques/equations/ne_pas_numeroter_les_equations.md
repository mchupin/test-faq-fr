# Comment ne pas numéroter les équations ?

Si la numérotation est une fonctionnalité par défaut de nombreux environnements traitant des équations et des [groupes d'équation](/4_domaines_specialises/mathematiques/equations/aligner_des_equations), elle peut être parfois non souhaitée (tout en conservant les autres propriétés de l'environnement), que ce soit pour un ensemble d'équations ou pour quelques équations mineures au sein d'un ensemble d'équations.

## Cas d'un groupe d'équations

Pour supprimer la numérotation des équations, il suffit d'ajouter le caractère `*` aux noms des environnements d'équation.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
Voici un exemple numéroté~ :
\begin{align}
   x + 4 & =  0 \\
   8 - y & =  0
\end{align}
et son équivalent non numéroté~ :
\begin{align*}
   x + 4 & =  0 \\
   8 - y & =  0.
\end{align*}
\end{document}
```

## Cas d'une ou de plusieurs équations au sein d'un groupe d'équations

Pour supprimer la numérotation d'une ligne particulière dans un groupe d'équations, il suffit d'utiliser la commande `\nonumber` dans la ligne de l'équation, avant la commande `\\`.

Avec l'extension <ctanpkg:mathtools> (ou <ctanpkg:amsmath>), il faut utiliser la commande `\notag`.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{mathtools}
\pagestyle{empty}
\begin{document}
Voici un exemple~ :
\begin{align}
   x + 4 & =  0 \notag \\
   8 - y & =  0.
\end{align}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équations,numérotation,numéroter,référence,label,ref
```
