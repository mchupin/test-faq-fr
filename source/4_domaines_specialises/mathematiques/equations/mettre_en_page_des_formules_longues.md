# Comment mettre en page des formules longues ?

## Avec l'extension « mathtools » (ou « amsmath »)

Les environnements `split` et `multline` de l'extension <ctanpkg:mathtools> (ou de <ctanpkg:amsmath>) permettent de couper une équation trop longue pour tenir sur une seule ligne. En voici des exemples :

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\usepackage{amsmath}
\pagestyle{empty}

\begin{document}
\begin{equation}
\begin{split}
  a & = b+c-d\\
    & \quad +e-f\\
    & = i
\end{split}
\end{equation}

\begin{multline}
  a+b+c+d+e+f\\
  +i+j+k+l+m+n
\end{multline}
\end{document}
```

[Leslie Lamport](wpfr:Leslie_Lamport) définit certaines conventions à ce sujet dans un article : [How to write a long formula](http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-119.pdf) (1994).

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équations,mise en forme des équations,équations longues,équations sur plusieurs lignes,comment couper une équation
```
