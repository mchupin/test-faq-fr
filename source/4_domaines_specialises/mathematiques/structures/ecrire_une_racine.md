# Comment écrire une racine (carrée ou autre) ?

La commande `\sqrt[⟨n⟩]{⟨argument⟩}` représente la racine \$n\\textsuperscript\{e}\$ de l`'⟨argument⟩`. `⟨n⟩` est un paramètre optionnel; s'il est absent, vous obtiendrez une racine carrée.

```latex
\documentclass{article}
\usepackage[body={8cm,8cm}]{geometry}
\usepackage{lmodern}
\pagestyle{empty}
\begin{document}
\[
\sqrt[3]{\sqrt{\sqrt[\gamma]{x+y}}}
\]
\end{document}
```

Notez que la taille du symbole racine s'ajuste automatiquement au contenu.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,structures mathématiques,racine carrée,racine cubique,symbole racine
```
