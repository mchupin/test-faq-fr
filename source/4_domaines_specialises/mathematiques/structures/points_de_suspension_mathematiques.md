# Comment obtenir des points de suspension dans des formules mathématiques ?

Des points de suspension peuvent être nécessaire dans des formules, et pas uniquement à l'horizontale. De base, LaTeX en fournit déjà pas mal :

| Commande | Résultat                               | Note                  |
|----------|----------------------------------------|-----------------------|
| `\dots`  | ![alt](/_static/images/svg/dots_1.svg) |                       |
| `\cdots` | ![alt](/_static/images/svg/dots_2.svg) | `c` pour « centré »   |
| `\vdots` | ![alt](/_static/images/svg/dots_3.svg) | `v` pour « vertical » |
| `\ddots` | ![alt](/_static/images/svg/dots_4.svg) | `d` pour « diagonal » |

En voici une utilisation :

```latex
\documentclass{standalone}
\begin{document}
$
\begin{array}{ccc}
   x_{11} & \cdots & x_{1p} \\
   \vdots & \ddots & \vdots \\
   x_{n1} & \cdots & x_{np}
\end{array}
$
\end{document}
\end{document}
```

En utilisant l'extension <ctanpkg:graphics> (ou <ctanpkg:graphicx>), on peut inverser la diagonale suivie par `\ddots`, pour écrire les [matrices antisymétriques](wpfr:Matrice_antisymétrique), de cette façon :

```latex
\documentclass{standalone}
\usepackage{graphics}
\begin{document}
\large
$a \reflectbox{$\ddots$} z$
\end{document}
```

Cette méthode fonctionne, mais elle n'est pas recommandée pour obtenir le résultat souhaité (voir plus bas). Pour répondre aux besoins plus pointus, il existe au moins trois extensions, listées ci-après.

## Avec l'extension « amsmath »

L'extension [amsmath](ctanpkg:latex-amsmath) fournit une panoplie de points de suspension nommé en fonction de leur usage :

- `\dotsb` pour une utilisation entre des paires d'opérateurs **b**inaires,
- `\dotsc` pour une utilisation entre des paires de virgules (**c***omma*),
- `\dotsi` pour une utilisation avec des **i**ntégrales,
- `\dotsm` pour une utilisation dans un produit (**m**ultiplication),
- `\dotso` pour les autres cas (**o***ther*).

```latex
\documentclass[varwidth=6cm]{standalone}
\usepackage{amsmath}

\begin{document}
Soit la série $A_1, A_2, \dotsc$,
on peut en faire la somme $A_1+A_2 +\dotsb $,
le produit $A_1 A_2 \dotsm $, mais aussi
l'intégrer comme ceci :
\[\int_{A_1}\int_{A_2}\dotsi\]
\end{document}
```

## Avec l'extension « yhmath »

L'extension <ctanpkg:yhmath>, de Yannis Haralambous, définit une commande `\adots`, qui est l'analogue de `\ddots`, mais inclinée vers l'avant plutôt que vers l'arrière. L'extension <ctanpkg:yhmath> est fournie avec une police de caractères plutôt intéressante qui étend la police standard `cmex` ; les détails sont [dans la documentation](texdoc:yhmath).

## Avec l'extension « mathdots »

L'extension <ctanpkg:mathdots> est vraiment spécialisée dans les points de suspension. En plus de corriger le comportement des `\ddots` et `\vdots` de TeX et LaTeX lorsque la taille de caractère change (voir les tableaux comparatifs ci-dessous), elle fournit des points de suspension en « diagonale inverse », avec `\iddots` (qui fait donc le même travail que `\adots` de <ctanpkg:yhmath>, mais en y apportant ses corrections).

Comparaison du rendu :

**Avec** <ctanpkg:mathdots> :

```latex
\documentclass{standalone}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{mathdots}

\begin{document}
\def\dott#1{$#1$}
\def\dotts#1{$2^{#1}\quad 2^{2^{#1}}$}
\renewcommand\arraystretch{1.4}
\begin{tabular}{c|cccc}
\multicolumn{1}{c}{\textbf{Commande}}%
                 &\textbf{Large}          &\textbf{normal}   &\textbf{scriptsize}          &\textbf{en exposant}\\
\hline
\verb$\ddots$    & \Large\dott{\ddots}    & \dott{\ddots}    & \scriptsize\dott{\ddots}    & \dotts{\ddots}\\
\verb$\vdots$    & \Large\dott{\vdots}    & \dott{\vdots}    & \scriptsize\dott{\vdots}    & \dotts{\vdots}\\
\verb$\iddots$   & \Large\dott{\iddots}   & \dott{\iddots}   & \scriptsize\dott{\iddots}   & \dotts{\iddots}\\
\verb$\dddot{X}$ & \Large\dott{\dddot{X}} & \dott{\dddot{X}} & \scriptsize\dott{\dddot{X}} & \dotts{\dddot{X}}\\
\verb$\ddddot{X}$& \Large\dott{\ddddot{X}}& \dott{\ddddot{X}}& \scriptsize\dott{\ddddot{X}}& \dotts{\ddddot{X}}
\end{tabular}
\end{document}
```

**Sans** `mathdots` :

```latex
\documentclass{standalone}
\usepackage{xcolor}
\usepackage{amsmath}

\begin{document}
\def\dott#1{$#1$}
\def\dotts#1{$2^{#1}\quad 2^{2^{#1}}$}
\renewcommand\arraystretch{1.4}
\begin{tabular}{c|cccc}
\multicolumn{1}{c}{\textbf{Commande}}%
                 &\textbf{Large}           &\textbf{normal}   & \textbf{scriptsize}         & \textbf{en exposant}\\
\hline
\verb$\ddots$    & \Large\dott{\ddots}     & \dott{\ddots}    & \scriptsize\dott{\ddots}    & \dotts{\ddots}\\
\verb$\vdots$    & \Large\dott{\vdots}     & \dott{\vdots}    & \scriptsize\dott{\vdots}    & \dotts{\vdots}\\
\color{black!30!white}\verb$\iddots$ & \multicolumn{4}{c}{\color{black!30!white}Non disponible} \\
\verb$\dddot{X}$ & \Large\dott{\dddot{X}}  & \dott{\dddot{X}} & \scriptsize\dott{\dddot{X}} & \dotts{\dddot{X}}\\
\verb$\ddddot{X}$& \Large\dott{\ddddot{X}} & \dott{\ddddot{X}}& \scriptsize\dott{\ddddot{X}}& \dotts{\ddddot{X}}
\end{tabular}
\end{document}
```

:::{tip}
Les commandes `\dddot` ($\dddot{X}$) et `\ddddot` ($\ddddot{X}$) sont fournies par <ctanpkg:amsmath> et corrigées par <ctanpkg:mathdots>. Il vous faut donc charger les deux extensions :

```latex
% !TEX noedit
\usepackage{amsmath}
\usepackage{mathdots}
```
:::

______________________________________________________________________

*Sources :*

- [Ellipses](faquk:FAQ-mathlips),
- [Points de suspension](wpfr:Points_de_suspension)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mode mathématique,composition des mathématiques,ponctuation,points de suspension,ellipsis,trois petits points,points de suite
```
