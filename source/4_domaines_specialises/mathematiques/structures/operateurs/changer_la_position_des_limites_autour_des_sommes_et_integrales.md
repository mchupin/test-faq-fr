# Comment positionner les limites des grands opérateurs ?

Quand on écrit à la main des intégrales (∫), des sommes (Σ) ou autres « grands opérateurs », on place généralement leurs bornes physiquement au-dessous et au-dessus du symbole de l'opérateur. Dans (La)TeX, on utilise des indices et des exposants appliqués à l'opérateur `\int` ou `\sum`, mais le rendu dans la sortie de TeX n'est pas toujours celui dont on a l'habitude à la main.

Il y a une raison à ce placement : quand une somme ou un intégrale apparaît dans du texte courant (en mode `\textstyle`, donc [non mise en exergue hors du paragraphe en cours](/4_domaines_specialises/mathematiques/a_quoi_sert_displaystyle)), le fait de placer les bornes de cette manière pourrait rendre l'interlignage irrégulier, et donc le texte difficile à lire. Il est donc courant (dans ce mode `\textstyle`) de placer les bornes comme on le ferait pour les indices et les exposants des variables, décalées vers la droite :

```latex
\documentclass[14pt]{extarticle}
  \pagestyle{empty}

\begin{document}
$\sum_{n=1}^{n} ...$
\end{document}
```

Cette méthode n'est pas toujours satisfaisante, c'est pourquoi il existe une primitive `\limits` :

```latex
\documentclass[14pt]{extarticle}
  \pagestyle{empty}

\begin{document}
$\sum\limits_{n=1}^{n} ...$
\end{document}
```

qui placera les bornes juste au-dessous et au-dessus du symbole (tant pis pour l'interlignage...).

À l'inverse, vous pouvez souhaiter modifier la disposition des bornes lorsque vous êtes en mode `\displaystyle`. Pour cela, il existe une primitive `\nolimits` :

```latex
\documentclass[14pt]{extarticle}
  \pagestyle{empty}

\begin{document}
\[\sum\nolimits_{n=1}^{m} ...\]
\end{document}
```

qui placera les bornes comme elles le seraient en mode `\textstyle`.

On peut aussi manipuler le mode `\textstyle`/`\displaystyle` des mathématiques. Pour obtenir le placement des bornes comme s'il y avait un `\limits` dans une formule composée en ligne :

```latex
\documentclass[14pt]{extarticle}
  \pagestyle{empty}

\begin{document}
$\displaystyle\sum_{n=1}^{m} ...$
\end{document}
```

ou comme s'il y avait un `\nolimits` dans une formule en mode `\displaystyle` :

```latex
\documentclass[14pt]{extarticle}
  \pagestyle{empty}

\begin{document}
\[\textstyle\sum_{n=1}^{m} ...\]
\end{document}
```

L'une ou l'autre de ces formes peut avoir des effets autres que sur le grand opérateur en question, mais certains préfèrent ces façons de faire.

:::{tip}
N'oubliez pas que si vous [définissez votre propre opérateur](/4_domaines_specialises/mathematiques/structures/operateurs/definir_un_nouvel_operateur), les fonctions de l'extension <ctanpkg:amsmath> (que vous devriez utiliser) vous permettront de choisir dans la définition comment les bornes seront affichées.
:::

:::{note}
Notez que la macro `\int` a déjà un `\nolimits` intégré dans sa définition.

Il existe un exemple dans le TeX​book montrant à quel point `\int\limits` donne un rendu étrange.
:::

______________________________________________________________________

*Source :* [Sub- and superscript positioning for operators](faquk:FAQ-limits)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,équation,grands opérateurs,bornes d'une intégrale
```
