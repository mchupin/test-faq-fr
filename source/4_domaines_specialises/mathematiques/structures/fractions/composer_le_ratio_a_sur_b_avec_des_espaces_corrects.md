# Comment écrire proprement « a/b » ?

- Pour qu'à l'impression le numérateur « a » soit légèrement décalé vers le haut et vers la gauche et que le dénominateur « b » soit légèrement décalé vers le bas et vers la droite, les deux étant plus petits que la police courante, il existe la commande `\sfrac` de l'extension <ctanpkg:xfrac> :

```latex
\documentclass{article}
  \usepackage{xfrac}
  \pagestyle{empty}

\begin{document}
\Large Votez pour ma fraction : $\sfrac{a}{b}$.
\end{document}
```

:::{note}
Cette commande a été, à une époque, fournie par l'extension <ctanpkg:tugboat>.
:::

## Pourquoi ne pas utiliser « nicefrac » ?

- L'extension <ctanpkg:nicefrac> fournit également une commande pour composer des fractions :

```latex
\documentclass{article}
  \usepackage{nicefrac}
  \pagestyle{empty}

\begin{document}
\Large Votez pour ma fraction : $\nicefrac{a}{b}$.
\end{document}
```

mais cette extension a été développée quand la police [Computer modern](ctanpkg:Computer_Modern) était la plus fréquemment utilée, et peut poser problème avec d'autres polices.

Voici un exemple en [Palatino](ctanpkg:palatino-nfss) et <ctanpkg:nicefrac> à gauche, et son équivalent avec <ctanpkg:xfrac> à droite :

```latex
\documentclass{article}
  \usepackage{mathpazo}
  \usepackage{nicefrac}
  \pagestyle{empty}

\begin{document}
\Large$\nicefrac{a}{b}$
\end{document}
```

```latex
\documentclass{article}
  \usepackage{mathpazo}
  \usepackage{xfrac}
  \pagestyle{empty}

\begin{document}
\Large$\sfrac{a}{b}$
\end{document}
```

En fonction des polices, la différence est plus ou moins visible. De façon générale, l'extension <ctanpkg:xfrac> corrige beaucoup des problèmes de <ctanpkg:nicefrac> et devrait toujours être utilisée à sa place.

```{eval-rst}
.. meta::
   :keywords: LaTeX,écrire une fraction en LaTeX,hauteur du numérateur et du dénominateur,positions des termes dans une fraction
```
