# Comment obtenir des notes de bas de page dans une formule mathématique ?

Pour obtenir des [notes de bas de page](/3_composition/texte/pages/footnotes/start), vous pouvez utiliser :

- directement la commande `\footnote` ;
- les commandes spécifiques `\footnotemark` et `\footnotetext` si vous voulez fixer vous-même les valeurs des compteurs. Ici, `\footnotemark` permet de placer dans le texte la référence à la note et `\footnotetext` permet d'insérer le texte correspondant en bas de page :

```latex
\documentclass{article}
\usepackage[width=5cm,height=5cm]{geometry}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\[
\textrm{Capacité} =
P_l\footnotemark[1] +
P_o\footnotemark[2] +
P_d\footnotemark[3]
\]

\footnotetext[1]{La place libre\dots}
\footnotetext[2]{La place occupée\dots}
\footnotetext[3]{La place défaillante\dots}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,mathématiques,notes de bas de page,note de bas de formule
```
