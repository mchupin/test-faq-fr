# Où sont rassemblés les fichiers et documentations concernant TeX ?

Pour faciliter l'archivage et la recherche de fichiers liés à TeX, un groupe de travail du TUG a développé le [Comprehensive TeX Archive Network](/1_generalites/glossaire/qu_est_ce_que_le_ctan) (CTAN). Les différents serveurs hébergeant le CTAN sont synchronisés entre eux, et offrent les mêmes fichiers (ce sont des « miroirs »). Il y a de quoi faire, une copie complète du CTAN représentant 45 Go (en juin 2021); en particulier, presque tout ce qui est évoqué dans cette FAQ est archivé le CTAN (vous n'avez qu'à suivre les liens fournis dans chaque réponse).

Le site principal du CTAN se trouve en Allemagne [^footnote-1], mais en tant qu'utilisateur, vous avez intérêt à accéder aux fichiers via [le redirecteur du CTAN](http://mirror.ctan.org/), qui vous connecte automatiquement à un miroir proche de vous géographiquement.

L'adresse de ce redirecteur est [mirror.ctan.org](http://mirror.ctan.org/). Donc pour accéder à un élément particulier, il suffit de placer le chemin CTAN après l'adresse de base ; ainsi <http://mirror.ctan.org/macros/latex/contrib/footmisc/> vous connectera au répertoire contenant l'extension <ctanpkg:footmisc> d'un des miroirs CTAN de votre zone géographique (cette FAQ utilise ce mécanisme).

Pour plus de détails sur les différentes façons de trouver des fichiers sur le CTAN, voir la question « [Où trouver des fichiers (La)TeX?](/5_fichiers/comment_trouver_un_fichier) ».

Les utilisateurs de TeX qui n'auraient pas accès à internet peuvent se procurer le [DVD « TeX collection »](/6_distributions/installation/dvd_texcollection), qui contient une copie des fichiers les plus utiles du CTAN. Bien entendu, comme le disque n'est mis à jour qu'une fois par an, son contenu devient rapidement obsolète au fil des mois, mais il reste probablement plus à jour que si vous vous contentez des fichiers distribués avec votre système d'exploitation, par exemple.

______________________________________________________________________

*Source :* [Repositories of TeX material](faquk:FAQ-archives)

```{eval-rst}
.. meta::
   :keywords: LaTeX,Comprehensive TeX Archive Network,archives TeX,fichiers de LaTeX
```

[^footnote-1]: À l'origine, il y avait trois serveurs principaux, en Allemagne, au Royaume-Uni et aux États-Unis.
