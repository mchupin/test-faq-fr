# Comment visualiser des fichiers PDF ?

[Le format PDF](wpfr:Portable_Document_Format) a été établi en 1993, et dès la fin des années 90, il est devenu un standard supporté par un grand nombre de programmes.

À partir d'un document LaTeX, les fichiers PDF sont produits soit directement par un programme tel `pdflatex`, `lualatex` ou `XeTeX`, soit en deux étapes :

- à partir des fichiers PS avec `ps2pdf`,
- à partir des fichiers DVI avec `dvipdfm`.

## Sous Mac OS

### Adobe Acrobat

[Adobe Acrobat](https://acrobat.adobe.com/fr/fr/), présenté ci-dessous pour Windows, est disponible pour MacOS X.

### Aperçu (ou Apple Preview)

Mac OS est fourni par défaut avec le logiciel [Aperçu](wpfr:Aper%C3%A7u_(OS_X)) (*Apple Preview*, en anglais), qui permet de visualiser les fichiers PDF, ainsi que de nombreux formats d'images.

### Skim

[Skim](https://skim-app.sourceforge.io/) est un logiciel *opensource* multifonction pour manipuler des fichiers PDF sous MacOS. Il est très bien intégré au système, permet d'annoter les fichiers, et interagit aisément avec LaTeX, `SyncTeX`, and `PDFSync`.

## Sous Unix ou Linux

### gv, ggv et gnome-gv

Ils sont décrits à la question « [Comment visualiser des fichiers PDF](/6_distributions/visualisateurs/visualisateurs_postscript) » .

### okular, evince et gpdf

Les programmes `okular`, `evince` et `gpdf` sont de bons choix.

:::{todo} *À compléter et détailler.*
:::

### xpdf

Le programme `xpdf` est rapide et efficace et qui n'a plus rien à envier à Acrobat Reader (la version 3.0 promet une vitesse 3 fois supérieure à la précédente). Il permet par contre de lire des textes utilisant les fontes de type 3 qui sont celles créées avec MetaFont, en plus des fontes truetype et Postscript. Il tourne sur les systèmes Unix, OS/2, VMS. Il contient aussi des programmes annexes qui permettent l'extraction des textes des fichiers `pdf`, un convertisseur `pdf` vers `ps`, ainsi que d'autres utilitaires. Vous pouvez le trouver sur son [site dédié](http://www.foolabs.com/xpdf/).

## Sous Windows

(adobe-acrobat-1)=

### Adobe Acrobat

[Adobe Acrobat](https://acrobat.adobe.com/fr/fr/) (anciennement appelé [Acrobat Reader](wpfr:Adobe_Acrobat)) propriétaire mais gratuit, est édité par Adobe, le créateur des formats de fichiers PS et PDF. Il est disponible pour Windows et MacOS X. Son support pour Linux a été interrompu en avril 2013. Le rendu avec des fontes issues de Metafont n'est pas terrible

:::{todo} Ce dernier paragraphe appelle une révision : « Est-ce toujours le cas?). » Avec des fontes vectorielles, le rendu est impeccable. Certaines fonctionnalités d'interaction et d'animation ne sont malheureusement disponibles qu'avec *Adobe Acrobat*.
:::

### Foxit Reader

[Foxit Reader](wpfr:Foxit_Reader) est un logiciel propriétaire, disponible pour Windows et Mac OS. Certaines des fonctionnalités appréciées des utilisateurs ont été retirées dans les versions diffusées à partir de 2020.

### SumatraPDF

[SumatraPDF](https://www.sumatrapdfreader.org/) est un excellent logiciel *opensource* pour Windows. Son usage est [recommandé par l'administration française](wpfr:Sumatra_PDF), modernisation globale de ses systèmes d’informations depuis 2016. Il intègre la synchronisation entre les sources TeX et le PDF qui en résulte. Enfin, il s'intègre aisément à [Docear](https://fr.m.wikipedia.org/wiki/Docear), outil polyvalent de gestion de bibliographie, lui-même compatible avec le format `BibTeX`.

## Cas du mode présentation, avec deux écrans

Certains visualisateurs ont été développés spécialement pour les présentations <ctanpkg:Beamer>. Ils permettent d'afficher les dapositives sur un vidéoprojecteur, et des notes de présentation et une horloge sur un second écran [voir aussi ici](/4_domaines_specialises/diaporama/mode_presentateur_pour_beamer)) :

- [Présentation.app](http://iihm.imag.fr/blanch/software/osx-presentation/) et [SlidePilot](https://slidepilotapp.com/) sous MacOS,
- [PDF presenter console](https://pdfpc.github.io/) sous Linux (des instructions sont [disponibles pour l'utiliser sous Windows](https://github.com/pdfpc/pdfpc)),
- [Pympress](https://pypi.org/project/pympress/) sous Linux (instructions disponibles pour MacOS et Windows sur la même page).

______________________________________________________________________

*Sources :*

- [Is there a nice solution to get a « presenter mode » for Latex presentations?](https://tex.stackexchange.com/questions/21777/is-there-a-nice-solution-to-get-a-presenter-mode-for-latex-presentations)
- [Portable Document Format](wpfr:Portable_Document_Format).

```{eval-rst}
.. meta::
   :keywords: Portable document format,LaTeX,Postscript,visualisateur
```
