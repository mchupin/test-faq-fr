# Existe-t-il des alternatives à TeX ?

L'idée d'écrire ses documents « comme des programmes » ne se limite pas à TeX, et de nombreuses pistes d'implémentation ont été explorées au fil des ans, dont certaines de façon fructueuse. Quelques unes ont été antérieures à TeX, d'autres cherchent à le remplacer, enfin d'autres sont relativement complémentaires. L'utilisation d'une syntaxe de type (La)TeX se retrouve dans un certain nombre d'entre elles, mais pas dans toutes.

:::{note}
Les projets énumérés ici sont entièrement distincts de TeX ou de ses dérivés (ce ne sont pas des [programmes « TeX-like »](/6_distributions/annexes/projets_derives_de_tex)).
:::

## Projets actifs

```{image} https://sile-typesetter.org/images/logo.png
:alt: https://sile-typesetter.org/images/logo.png
:class: align-top
:width: 170px
```

### SILE

[SILE](http://sile-typesetter.org/) est un système de mise en page écrit en Lua et utilisant la [bibliothèque HarfBuzz](wpfr:HarfBuzz) pour le rendu des caractères. Sa syntaxe d'entrée est largement inspirée de LaTeX, par exemple :

```latex
% !TEX noedit
\begin{document}
Hello SILE!
\end{document}
```

est un document SILE valide (remarquez l'absence de préambule ici).

SILE peut également prendre du XML en entrée (en fait, ce qui précède est lu comme du XML avec une syntaxe particulière).

SILE est programmable en Lua. Puisque XML est un langage de description raisonnable, et que Lua est un vrai langage de programmation, l'écriture d'extensions et de commandes pour SILE tend à être plus facile et plus rapide que pour TeX.

SILE supporte complètement [Unicode](wpfr:Unicode) et les polices [TrueType](wpfr:TrueType)/[OpenType](wpfr:OpenType). Comme il s'appuie sur des bibliothèques de mise en forme modernes, il prend facilement en charge plusieurs langues et tous les types d'écritures (par exemple l'arabe et le japonais). Il sait également mettre en forme les équations mathématiques.

Bien que SILE n'ait pas la pléthore d'extensions dont dispose TeX, il possède quelques fonctionnalités qui sont absentes de TeX :

- le support de mises en pages complexes à base de cadres, inspiré d'InDesign;
- la composition du texte sur une grille (pour éviter les problèmes de transparence quand on imprime sur papier fin).

Par ailleurs, la possibilité de prendre du XML en entrée permet d'automatiser la création de catalogues, de dictionnaires, etc.

```{image} https://www.speedata.de/images/logo-int.png
:alt: https://www.speedata.de/images/logo-int.png
:class: align-top
:width: 180px
```

### Speedata Publisher

[Speedata Publisher](https://www.speedata.de/en/) est, comme SILE, écrit en [Lua](wpfr:Lua), mais utilise [LuaTeX](/1_generalites/glossaire/qu_est_ce_que_luatex) pour toute la partie interne du traitement du document (nous parlons bien de LuaTeX et non de Lua pur). Il comporte une couche externe basée sur TeX, mais une fois que les données sont passées à Lua, TeX n'est plus impliqué.

:::{todo} Préciser clarifier la structure de la chaîne de traitement, car le paragraphe précédent est confus.
:::

Speedata Publisher est particulièrement bien adapté à certains domaines dans lesquels TeX a un peu de mal, par exemple les documents comportant de nombreuses images, comme les catalogues de produits. Il sait utiliser XML et peut parcourir une base de données.

```{image} https://raw.githubusercontent.com/wiki/gfngfn/SATySFi/img/satysfi-logo.png
:alt: https://raw.githubusercontent.com/wiki/gfngfn/SATySFi/img/satysfi-logo.png
:class: align-top
:width: 170px
```

### SATySFi

[SATySFi](https://github.com/gfngfn/SATySFi) (prononcé de la même manière que le verbe « *satisfy* » en anglais) est un nouveau système de mise en page basé sur un langage de programmation fonctionnelle à typage statique. Il se compose principalement de deux couches : la couche texte et la couche programme. La première sert à écrire des documents dans une syntaxe semblable à celle de LaTeX. La seconde, dont la syntaxe est semblable à celle d'OCaml, sert à définir les fonctions et les commandes. SATySFi vous permet d'écrire des documents balisés avec des commandes de votre cru. De plus, les erreurs de typage sont signalées avec des messages informatifs, ce qui est une aide précieuse quand on écrit ses propres fonctions.

```{image} https://patoline.github.io/images/titleduck.png
:alt: https://patoline.github.io/images/titleduck.png
:class: align-top
:width: 170px
```

### Patoline

[Patoline](http://patoline.github.io/) est un système de mise en page écrit en [OCaml](wpfr:OCaml) qui utilise un mélange de syntaxe de type TeX et d'« échappements » vers OCaml pour fournir le contrôle de la mise en forme. Patoline vise une conception modulaire, et un traitement rapide des documents par la machine.

(Le projet Patoline était précédemment hébergé à <http://patoline.org/>)

### Lout

[Lout](https://savannah.nongnu.org/projects/lout/) est un formateur de documents automatique, écrit par Jeffrey H. Kingston. Il prend en entrée une description de haut niveau d'un document, dans un style similaire à celui de LaTeX. Lout reprend certains des algorithmes de mise en forme de TeX mais utilise comme langage de personnalisation un langage de programmation fonctionnelle de haut niveau, au lieu d'un langage de macros. Lout a été publié en même temps que LaTeX2ε au début des années 1990. Il n'a jamais eu une base d'utilisateurs aussi vaste que celle de LaTeX, mais il est toujours maintenu.

:::{tip}
Plus de renseignements sur [la page wikipedia de Lout](wpfr:Lout).
:::

### troff/nroff/groff

[groff](https://www.gnu.org/software/groff/) : la [famille des outils de mise en forme](wpfr:Roff_(langage_informatique)) « `*roff` » est apparue avant TeX et a beaucoup influencé sa conception. Ils font partie intégrante d'Unix et de tous les systèmes de ce type, comme linux.

Notamment, [les pages de manuel](wpfr:Man_(Unix)) sont composées avec `groff` et ses dérivés, aussi bien pour leur affichage sur écran que pour leur impression.

## Projets historiques

### ANT typesetting system

*ANT* est l'acronyme de « \**A*NT is\* **n***ot* **T***eX* ».

Le [projet ANT](ctanpkg:ant), d'Achim Blumensath, ne visait pas à reproduire TeX avec une implémentation simplement différente, mais plutôt à fournir un remplacement de TeX, utilisant des algorithmes de composition semblables à ceux de TeX, mais offrant un environnement de programmation très différent, basé sur [OCaml](wpfr:OCaml) et ayant une conception flexible et modulaire,

Ainsi, bien qu'ANT possède un langage de balisage essentiellement identique à celui de (La)TeX, le langage servant à définir la mise en forme des documents est un langage de programmation bien établi et assez répandu, de sorte que la conception des documents ne dépend plus des compétences de quelques experts en programmation de macros TeX.

La version la plus récente de ANT est la 0.8, publiée [en décembre 2007](http://mirrors.ctan.org/systems/ant/manual.pdf). Son développement a été [brièvement repris en 2016](https://github.com/Miko4/ant).

______________________________________________________________________

*Sources :*

- [Alternatives to TeX](faquk:FAQ-alternatives),
- [Are there any news about Patoline typesetting engine?](https://discuss.ocaml.org/t/are-there-any-news-about-patoline-typesetting-engine)
- [Typesetting product catalogs and otherdatabase-driven documents with the Speedata Publisher](https://tug.org/TUGboat/tb41-2/tb128gundlach-speedata.pdf), Patrick Gundlach, TUGboat **41**:2 (2020).

```{eval-rst}
.. meta::
   :keywords: LaTeX,autres logiciels de composition de documents,LaTeX et Groff,outils de mise en page,chaînes de production de documents
```
