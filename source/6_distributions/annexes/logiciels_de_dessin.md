# Quels logiciels de dessin utiliser avec LaTeX ?

Les logiciels de dessin se décomposent en deux grandes catégories :

- les [logiciels de dessin bitmap](wpfr:Image_matricielle)
- et les [logiciels de dessin vectoriel](wpfr:Image_vectorielle).

Concernant les images bitmap, vous incluerez le fichier JPEG ou PNG dans votre document avec une commande du genre :

```latex
% !TEX noedit
\includegraphics[width=6cm]{mon_image}
```

L'interaction entre le logiciel de dessin bitmap et LaTeX est donc quasiment inexistante, et tout logiciel peut convenir. Nous fournissons quelques pointeurs en fin de page.

## Pour le dessin vectoriel

Si vous avez le choix, il est quasiment toujours recommandé de préférer le dessin vectoriel pour inclure des images dans un document LaTeX. Cela permet un redimensionnement sans perte de qualité, et une impression parfaite.

Vous avez deux possibilités pour inclure une image vectorielle dans votre document :

- l'export de l'image dans un fichier PDF ou PostScript, puis son inclusion dans le document avec `\includegraphics`. PDF et PS sont des formats « finaux », et LaTeX ne peut quasiment plus rien modifier à votre image. Notamment, vous ne pouvez pas modifier les couleurs ou la police de caractères éventuellement utilisées dans l'image, pour les faire correspondre à celles de votre document (même si l'extention [PSfrag](ctanpkg:psfrag) fait des tentatives pour modifier les images PS depuis LaTeX). En revanche, LaTeX n'a quasiment aucun travail à faire pour inclure l'image, même si celle-ci est très grosse, donc cela ne ralentit pas la compilation.
- l'export de l'image sous forme de code LaTeX (avec [TikZ](ctanpkg:pgf) ou [PStricks](ctanpkg:pstricks)). LaTeX se chargera de la compilation de ce code, ce qui peut être un peu long, mais vous pourrez faire dépendre l'apparence de votre image de variables contenues dans votre document, pour en déterminer les couleurs, la police ou les dimensions au moment de la compilation. Autre avantage : votre document LaTeX n'a pas besoin de fichiers externes.

### Logiciel Inkscape

[Inkscape](https://inkscape.org/fr/) est un logiciel de dessin vectoriel gratuit et open-source, qui peut exporter directement du code Ti*k*Z (en plus de PDF et PS). L'étendue de ses fonctionnalités en fait un outil très puissant, compatible avec vos habitudes de travail avec LaTeX, mais également recommandé dans d'autres contextes. Il existe pour Linux, MacOS et Windows.

### Logiciel TikZiT

[TikZiT](https://tikzit.github.io/index.html) : une interface graphique multiplateforme pour dessiner des graphes en Ti*k*Z.

### Logiciel XFig

[Xfig](https://sourceforge.net/projects/mcj/files/) est un logiciel de dessin vectoriel gratuit et open-source pour Linux. Il peut exporter du code Ti*k*Z ou PSTricks, mais aussi [MetaFont](/1_generalites/glossaire/qu_est_ce_que_metafont) et [MetaPost](/1_generalites/glossaire/qu_est_ce_que_metapost), ce qui rend très simple l'intégration des images dans un document LaTeX. Bien sûr, les formats PostScript et PDF sont également disponibles. Il est [également disponible depuis le CTAN](ctanpkg:xfig).

Son interface graphique est un peu datée, mais XFig s'intègre très bien à un travail avec (La)TeX. Notamment, il est possible d'include du code LaTeX dans le dessin (par exemple pour les légendes de la figure).

L'outil `transfig` s'occupe de la conversion du format interne `fig` en d'autres formats.

Il n'existe pas de version de XFig officiellement supportée sous MS Windows, mais il est apparemment possible de le faire tourner avec [Cygwin](wpfr:Cygwin) et son serveur X. Le logiciel [jfig](https://tams-www.informatik.uni-hamburg.de/applets/hades/classdoc/overview-summary.html) a été considéré comme un remplaçant de XFig sous Windows, écrit en Java, mais son développement s'est arrêté vers 2006.

### Logiciel LaTeXDraw

[LaTeXDraw](http://latexdraw.sourceforge.net/) peut produire du code PStricks, ou bien des fichier PDF ou PS, que vous pourrez inclure dans votre document. Il fonctionne sous Linux, MacOS et Windows.

### Logiciel TeXCAD

[TeXCAD](https://sourceforge.net/projects/texcad/) est un programme pour Windows qui vous permet de dessiner des diagrammes à la souris (ou avec les touches fléchées du clavier). Il produit du code à destination d'un environnement `picture` de LaTeX. Sa dernière version date de novembre 2018, et son développement semble s'être arrêté depuis. Il a, à une époque, été [porté sous Linux](ctanpkg:xtexcad), mais cette version n'est plus maintenue.

### Langages de description d'images

Outre [PGF/TikZ](ctanpkg:pgf) and <ctanpkg:PStricks>, parfaitement intégrés à LaTeX, il existe d'autres langages permettant de « programmer » ses images en écrivant des commandes.

`Asymptote` est un développement très apprécié du langage [MetaPost](/1_generalites/glossaire/qu_est_ce_que_metapost), qui permet de dessiner des diagrammes en 2D ou 3D, et peut intégrer du code LaTeX, par exemple pour les légendes d'une figure; une documentation abondante est disponible [sur le site Web d'asymptote](http://asymptote.sourceforge.net/).

## Pour le dessin bitmap

Le choix de logiciels est vaste, en fonction de votre besoin. Voulez-vous retraiter une photographie que vous avez prise ? Créer vous-même un dessin ? Numériser un document papier ? Retoucher une capture d'écran ?

[The Gimp](wpfr:GIMP) (*GNU Image Manipulation Program*) est un outil d'édition et de retouche d'image, gratuit et libre, qui existe pour la plupart des systèmes d'exploitation dont GNU/Linux, macOS et Microsoft Windows. Il est très polyvalent. Il peut utiliser la bibliothèque [SANE](wpfr:Scanner_Access_Now_Easy) pour piloter un scanner et numériser des documents papier. Disposant de très nombreuses fonctionnalités, sa prise en main peut demander un peu de temps.

[Adobe Photoshop](wpfr:Adobe_Photoshop) est son concurrent propriétaire et payant. Certaines personnes continuent de l'utiliser par habitude.

[Krita](wpfr:Krita) est un autre logiciel libre et gratuit d'édition et de retouche d'image, très adapté pour la [peinture numérique](wpfr:Peinture_numérique).

D'autres outils bitmap sont listés [sur Wikipédia](wpfr:%C3%89diteur_d%27image_matricielle).

______________________________________________________________________

*Sources :*

- [(La)TeX-friendly drawing packages](faquk:FAQ-xfigetc),
- [TeXCAD, a drawing package for LaTeX](faquk:FAQ-texcad),
- [Drawing packages for LaTeX](faquk:FAQ-texdraw).

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,illustrations,GUI,logiciels graphiques,dessin vectoriel,Illustrator
```
