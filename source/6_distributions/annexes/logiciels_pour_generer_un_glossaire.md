# Quels sont les logiciels permettant de créer un index, un glossaire ?

- Les distributions standards de LaTeX proposent `makeindex`. C'est un programme assez simple qui lit les entrées de l'index dans un fichier qui lui est passé en argument (généralement un fichier d'extension `.idx` créé par LaTeX au cours de la compilation du document où l'index sera inséré), trie ces entrées et les met en forme, et écrit le résultat dans un fichier (généralement d'extension `.ind`). Le programme `makeindex` est configurable par un fichier d'extension `.ist`, par exemple `gind.ist`. L'appel à `makeindex` se fera donc généralement de la façon suivante :

```bash
makeindex -s style.ist -o file.ind file.idx
```

Pour plus de renseignements sur la création d'index, voir la partie~\\vref{par=index}, qui est consacrée à ce sujet.

- `xindy` est un autre générateur d'index. Il est disponible à l'adresse <https://www.ctan.org/support/xindy/>. Il est un peu plus complexe que `makeindex`, mais également un peu plus puissant et plus général. Voir la question~\\vref{qu=xindy}.
- IdxTeX est une autre possibilité pour de générer un index. Il est disponible sur <https://www.ctan.org/indexing/glo+idxtex/>.
- `makeindex` permet également de générer des glossaires (c'est-à-dire un index dans lequel chaque mot est brièvement défini), à condition de lui donner les définitions par l'intermédiaire du fichier LaTeX, et d'utiliser un style correct (par exemple `nomencl.ist`). Voir la partie~\\vref{par=glossaire}.
- GloTeX, qui fonctionne de pair avec IdxTeX, est un outil permettant de générer un glossaire mais dont les définitions sont cette fois extraites d'une base de données ; c'est comparable à ce que fait BibTeX pour les références bibliographiques. On pourrait d'ailleurs configurer BibTeX pour qu'il fasse ce que fait GloTeX.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
