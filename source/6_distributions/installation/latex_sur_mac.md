# Quelles sont les distributions TeX pour les systèmes Mac ?

## MacTeX

Le DVD [TeX collection](/6_distributions/installation/dvd_texcollection) comprend `MacTeX`, une version [Mac](wpfr:Macintosh) de TeX Live, décrite sur le [site web](https://tug.org/mactex) du [TUG](/1_generalites/gutenberg). `MacTeX` dispose par exemple d'un gestionnaire graphique TeX Live personnalisé pour Mac, afin que vous puissiez maintenir votre distribution à jour.

Si vous ne disposez pas de ce disque, vous pouvez télécharger la distribution (notez qu'elle est assez volumineuse) depuis :

- la [page dédiée](https://tug.org/mactex) du TUG ;
- la [page dédiée](ctanpkg:mactex) du CTAN.

Notez que l'installation de `MacTeX` nécessite le privilège `root`. C'est dommage car il propose plusieurs extras non disponibles sur une TeX Live standard et donc non accessibles aux personnes qui n'ont pas ce privilège `root`. Pour ces derniers, l'alternative revient donc à utiliser l'installateur `tlinstall` de TeX Live.

De plus amples informations sont disponibles sur la [page d'aide](http://www.tug.org/mactex/gettinghelp.html) de `MacTeX`.
