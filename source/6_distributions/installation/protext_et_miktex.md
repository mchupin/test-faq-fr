# Quelle est la différence entre proTeXt et MiKTeX ?

Sous [Microsoft Windows](wpfr:Microsoft_Windows), on a actuellement le choix entre trois distributions pour installer LaTeX :

- [MiKTeX](https://miktex.org/), développée par Christian Schenk,
- [proTeXt](https://www.tug.org/protext/), développée par Thomas Feuerstack,
- [TeXlive](https://www.tug.org/texlive/).

Toutes les trois sont des distributions libres, gratuites, complètes et d'excellente qualité. Si TeXlive a l'avantage d'être multiplate-forme, les deux autres distributions sont développées spécialement pour Windows, et sont donc généralement mieux intégrées à l'environnement, et souvent plus faciles à installer.

## proTeXt vs. MiKTeX

MiKTeX est la distribution la plus ancienne du lot, puisqu'en 2000, Christian Schenk en publiait déjà la version 1.20. Elle contient l'éditeur de texte [TeXworks](wpfr:TeXworks).

proTeXt est un pack qui contient, entre autres, MiKTeX elle-même et différents logiciels indépendants, comme l'éditeur de texte [TeXstudio](wp:TeXstudio) et [Ghostscript](wpfr:Ghostscript), outil de visualisation/manipulation de fichiers PostScript.

À l'installation, MiKTeX occupe environ 170 Mo sur le disque dur, tandis que proTeXt prend près de 1200 Mo, ce qui peut surprendre. En fait, l'installation par défaut de MiKTeX ne copie sur le disque dur que les fichiers les plus courants. Tout le reste sera téléchargé à la volée au fil de l'utilisation, si nécessaire. La version de MiKTeX incluse dans proTeXt peut aussi fonctionner de cette façon, mais par défaut elle propose une installation « complète ».

- Donc si vous êtes novice en LaTeX et que vous avez une bonne connexion internet (ou au contraire que vous travaillez souvent sans possibilité de vous connecter), **il vaut mieux opter pour proTeXt** parce qu'il fournit tout qui est utile pour commencer et que l'installation sera faite une fois pour toutes.
- Si vous savez où vous allez, et que vous avez accès à internet en permanence (par exemple sur un ordinateur fixe), l'installation de base de MiKTeX suffira. On pourra la compléter avec un éditeur de son choix.

:::{important}
proTeXt a longtemps proposé l'éditeur de texte TeXnicCenter, mais est passé à TeXstudio vers 2013.

Il reste tout à fait possible d'installer indépendamment [TeXnicCenter](http://www.texniccenter.org/).
:::

______________________________________________________________________

*Sources :*

- [What to download, proTeXt or MiKTeX?](https://tex.stackexchange.com/questions/26018/what-to-download-protext-or-miktex)
- [ProTeXt](wp:ProTeXt),
- [MiKTeX](wp:MiKTeX),
- [(Modern) Windows systems](faquk:FAQ-syswin32)

```{eval-rst}
.. meta::
   :keywords: LaTeX,installer LaTeX,installation sous Windows
```
