# Quelles sont les particularités de la distribution kerTeX ?

<https://kertex.kergis.com/img/quixante_half.png> [kerTeX](https://kertex.kergis.com/fr/index.html) est une distribution du système TeX (à savoir non seulement TeX proprement dit mais aussi [MetaFont](/1_generalites/glossaire/qu_est_ce_que_metafont), les fontes, [MetaPost](/1_generalites/glossaire/qu_est_ce_que_metapost) de John Hobby et les utilitaires dont [dvips de Tomas Rokicki](/5_fichiers/postscript/generer_un_fichier_postscript_a_partir_d_un_dvi)), système composé des logiciels de typographie écrits par D.E. Knuth ou de leurs dérivés.

Le préfixe « ker » vient de « kernel » parce que la base de ce qui est installé est le noyau (le terme a été utilisé par D.E. Knuth dans un des articles annonçant la révision 2021 du système) à savoir les moteurs (en anglais : *engines*) qui font l'essentiel du travail et sur lesquels « tournent » les macros.

Afin qu'un tel système soit disponible partout et pour tous les systèmes d'exploitation, y compris dans des environnements restreints, le système ne dépend que de la disponibilité d'un compilateur C et d'une [libc](wpfr:GNU_C_Library) pour le système.

Le cœur du système étant installé sur un OS hôte, ajouter des extensions (par exemple LaTeX, qui n'est pas un moteur, mais un ensemble de macros tournant sur un moteur) se fait via le gestionnaire de paquets de kerTeX, le but étant de n'avoir à écrire qu'une seule version du paquet qui peut s'installer alors sur n'importe quel système puisque c'est kerTeX qui s'en charge et pas le système hôte. Pour ce faire, le gestionnaire de paquets n'utilise qu'un sous-ensemble très restreint d'utilitaires POSIX.2, qui existent pour pratiquement tous les systèmes (on les trouve, par exemple, avec MSYS pour MS Windows et ils sont normalement disponibles sur tous les systèmes de type Unix ou dérivés d'une base Unix).

Contrairement à ce que beaucoup pourraient croire, LaTeX n'est pas un moteur, mais un ensemble de macros et une infrastructure d'écriture d'extensions --- du niveau utilisateur (*user*) pour reprendre l'image d'un système typographique --- dont les moteurs seraient le noyau sur lequel « tournent » des programmes. C'est donc une application tierce (pour la base) et des applications tierces (pour les extensions) : des paquets.

Jusqu'à 2020, LaTeX pouvait tourner sur le moteur TeX tel qu'écrit par D.E. Knuth. À partir de 2020, LaTeX nécessitait les [extensions e-TeX](/1_generalites/glossaire/qu_est_ce_que_etex) écrites par l'équipe NTS et ne pouvait plus tourner sur TeX. Puis, en 2021, e-TeX ne suffisait plus et LaTeX nécessitait des extensions (primitives) supplémentaires.

Ce pourquoi désormais au sein de kerTeX, a été développé [Prote](https://kertex.kergis.com/fr/prote.html) (c'est un mot français...), qui reprend le principe de e-TeX, à savoir qu'il s'agit d'extensions à TeX + e-TeX. Prote est donc compatible TeX, compatible e-TeX, et peut servir de moteur aux dernières versions de LaTeX.

À terme, l'objectif de kerTeX est de fournir un système typographique pour les OS, entre autres à la place de [\*roff](/6_distributions/annexes/alternatives_a_tex.md#troffnroffgroff), pouvant être utilisé non seulement pour les pages de manuel et la documentation du système, mais également par les utilisateurs (dont les utilisateurs de LaTeX). Pour cela, il doit rester minimal, tant au niveau de la taille, qu'au niveau des dépendances (juste une libc) ainsi qu'au niveau de la licence (ouverte de type BSD).

[La documentation du projet](http://downloads.kergis.com/kertex/doc/LISEZ.MOI) (en français) détaille abondamment historique et objectifs.

______________________________________________________________________

*Source :* [Site web de kerTeX.](https://kertex.kergis.com/fr/index.html)

```{eval-rst}
.. meta::
   :keywords: distributions LaTeX,kerTeX,projet kerTeX,distributions alternative,distribution LaTeX embarquée
```
