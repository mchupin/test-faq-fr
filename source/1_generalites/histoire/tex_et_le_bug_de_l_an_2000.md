# TeX a-t-il subi le bug de l'an 2000 ?

:::{note}
Le [passage à l'an 2000](wpfr:Passage_informatique_%C3%A0_l%27an_2000) a suscité de sérieuses inquiétudes dans le monde de l'informatique à cause de problèmes de conception et de programmation portant sur le format de la date dans les mémoires des ordinateurs et par conséquent dans les matériels informatiques, ainsi que dans les logiciels.

Dans un souci d'économie de mémoire, l'année avait souvent été stockée sur deux chiffres (`78` pour `1978`), de sorte que le passage de 1999 à 2000 allait être vu comme un passage de `99` à `00` (ou `1999` à `1900`), ce qui allait causer de nombreux dysfonctionnements (tris erronés, dates considérées comme doublons à un siècle d'intervalle...).

Cet épisode de l'histoire informatique a été familièrement appelé « bug de l'an 2000 ».

La présente page a été écrite en novembre 1998.
:::

Cette page résume les problèmes de l'an 2000 concernant TeX et METAFONT, sur la base des discussions du newsgroup `comp.text.tex` et de la liste de diffusion `tex-implementors@ams.org`. Nous nous efforçons ici d'exposer simplement les principales questions en jeu, sans aborder les aspects controversés des solutions à adopter.

## Faut-il craindre des plantages ?

Les programmes TeX et METAFONT eux-mêmes ne se planteront pas à cause des dates.

Cependant, chaque exécutable dépend d'un système d'exploitation et de ses bibliothèques, qui doivent eux-mêmes être audités pour le passage à l'an 2000.

## Que va-t-il se passer avec l'horodatage ?

Une année à deux chiffres est :

- écrite dans les fichiers journaux,
- et stockée dans les *timestamps* du fichier de format et du fichier de base.

Ces éléments ne devraient pas poser de problème général, car ils sont destinés à des lecteurs humains et non à d'autres programmes.

Le 24 novembre 1998, Donald Knuth a accordé la permission inhabituelle de modifier TeX et METAFONT, pour qu'on puisse utiliser des horodatages à 4 chiffres (en fait, presque toutes les implémentations, comme web2c, le faisaient déjà), en disant :

*Je conviens qu'il serait maintenant préférable de supprimer le `mod 100` du module TeX 1328 et du module MF 1200 (et de METAPOST à l'endroit correspondant). Par la présente, j'autorise les responsables de l'implémentation à effectuer ces changements dans leurs fichiers. Aucune modification des numéros de version n'est nécessaire.* \[rapporté par Barbara Beeton sur la liste de diffusion `tex-implementors`\]

Cette permission signifie que les horodatages à 4 chiffres, même s'ils modifient légèrement la sortie de TeX et METAFONT par rapport aux signatures actuelles, respectent toujours les normes édictées par Knuth et requises pour qu'un logiciel ait le droit de s'appeler « TeX » ou « METAFONT ».

## Et la primitive « \\year » ?

La [certification TRIP](/1_generalites/bases/verifier_la_conformite_de_son_compilateur) de TeX, au sens le plus strict, n'exige pas que `\year` renvoie une valeur significative (car TeX peut être implémenté de manière certifiable sur des plateformes qui ne fournissent même pas d'information de date, comme le Pascal standard). Le TeXbook définit l'année comme « l'année courante de notre Seigneur », ce qui est la seule signification correcte de l'année pour les implémentations qui peuvent fournir une valeur significative, (c'est-à-dire la quasi-totalité, en fait).

En bref, les implémentations TeX doivent fournir dans `\year` une valeur donnant l'[année de l'ère commune](wpfr:Anno_Domini) sur quatre chiffres, ou la valeur `1776` si la plate-forme ne supporte pas de fonction de date. TeX ne fournit aucune variable d'état pour indiquer si `\year` contient une valeur significative, et bien que `1776` aurait pu être considéré comme une valeur de signal pour un manque de signification de `\year`, ce n'est pas une exigence normalisée.

## Et en ce qui concerne les logiciels externes ?

L'écosystème TeX contient de nombreux programmes accessoires, tels que des extensions et des pilotes DVI, qui peuvent calculer des dates à partir de la valeur de `\year` (ou, plus rarement, à partir de *timestamps*). Ces programmes accessoires doivent être vérifiés individuellement pour s'assurer que leur comportement est correct lorsque `\year` est supposé renvoyer une valeur `\year` correcte sur 4 chiffres avant et après 2000. Les programmes accessoires suffisamment robustes se comporteront de manière raisonnable lorsque `\year` contiendra une valeur à deux chiffres ou une valeur dénuée de sens telle que `1776`.

______________________________________________________________________

*Source :* [TeX Year 2000 (Y2K) Issues in Summary](http://www.truetex.com/y2k.htm) (mise à jour du 24 novembre 1998).

```{eval-rst}
.. meta::
   :keywords: TeX,bogue de l'an 2000,développement,moteur TeX
```
