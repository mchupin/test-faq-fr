# Qu'est-ce qu'ε-TeX ?

Les extensions ε-TeX de TeX sont une gamme de nouvelles fonctionnalités ajoutées par le [NTS](/1_generalites/histoire/developpement_du_moteur_tex) au programme TeX original de Donald Knuth.

Dans une distribution TeX moderne, les extensions sont activées pour presque tous les formats sauf TeX de Knuth lui-même. En tant que tel, la plupart des utilisateurs n'utiliseront jamais un système ne comportant pas ε-TeX : en effet, LaTeX en a besoin depuis 2017.

______________________________________________________________________

*Sources :*

- [What is ε-TeX?](faquk:FAQ-etex)
- <https://www.tug.org/TUGboat/tb29-1/tb91reutenauer.pdf>

```{eval-rst}
.. meta::
   :keywords: LaTeX,histoire
```
