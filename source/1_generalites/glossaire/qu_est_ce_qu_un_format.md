# Qu'est-ce qu'un format de TeX ?

Certaines séquences de contrôle basiques de TeX sont fastidieuses à utiliser directement : elles sont principalement destinés à servir de blocs de construction pour des abstractions de plus haut niveau et donc plus conviviales. Par exemple, il n'y a aucun moyen direct de spécifier à TeX qu'un morceau de texte doit être composé dans une police plus grande. Au lieu de cela, il faut garder une trace de la taille et de la police actuelles, charger cette police dans une taille plus grande, et dire à TeX d'utiliser cette nouvelle police jusqu'à instruction contraire. Heureusement, comme TeX est programmable, il est possible d'écrire une commande qui cache cette complexité derrière une nouvelle séquence de contrôle simple. Dans le cas, il est possible de définir `\large{mon texte}` pour composer l'expression « mon texte » avec une taille de police immédiatement supérieure à la taille actuelle.

Alors que certains utilisateurs écrivent leur propre ensemble de commandes personnalisées --- qu'ils réutilisent ensuite généralement dans de nombreux documents --- il est beaucoup plus courant de s'appuyer sur un *format*, une collection de commandes TeX écrites par des experts. Pour faciliter la vie de l'utilisateur, ces *formats* sont souvent combinés avec le moteur TeX de base dans un programme autonome. Voici quelques-uns de ces *formats* que vous êtes susceptible de rencontrer.

## Plain TeX

- Exécutable : `tex`. Notez que l'exécutable Plain TeX s'appelle `tex`. Le moteur TeX de base est parfois fourni avec un exécutable séparé tel que [initex](/1_generalites/histoire/qu_est_devenu_initex) ou sous la forme d'une option `-ini` à placer dans la ligne de commande de `tex`.
- Pour plus d'informations, voir « [Quels livres lire sur TeX et Plain TeX ?](/1_generalites/documentation/livres/documents_sur_tex) », « [Quels document en ligne lire sur TeX ?](/1_generalites/documentation/documents/documents_sur_tex) » et « [Dois-je utiliser TeX ou LaTeX ?](/1_generalites/bases/dois_je_utiliser_tex_ou_latex) ».

## LaTeX

- Exécutable : `latex`. Il existe deux versions majeures de LaTeX :

  - LaTeX fait référence. Voir « [Qu'est-ce que lire LaTeX 2ε ?](/1_generalites/glossaire/qu_est_ce_que_latex2e) » ;
  - LaTeX 2.09 est la version obsolète depuis 1994.

- Pour plus d'informations, voir « [Où trouver des introductions à LaTeX ?](/1_generalites/documentation/documents/tutoriels/initiations_a_latex_sur_internet) », « [Que lire sur LaTeX 2ε ?](/1_generalites/documentation/documents/documents_sur_latex2e) » et « [Où trouver des tutoriels sur (La)TeX ?](/1_generalites/documentation/documents/tutoriels/tutoriaux) ».

## ConTeXt

- Exécutable : `context`.
- Pour plus d'informations, voir « [Qu'est-ce que ConTeXt ?](/1_generalites/glossaire/qu_est_ce_que_context) ».

## Texinfo

- Exécutables : `tex`, `makeinfo`. Le programme `makeinfo` convertit les documents Texinfo en HTML, DocBook, info Emacs, XML et texte brut. Par ailleurs, `tex` (ou ses proches tels que `texi2dvi` et `texi2pdf`) produisent l'un des formats de sortie habituels de TeX tels que DVI ou PDF. Ces derniers chargent par défaut les macros Plain TeX et non celles de Texinfo. Aussi, un document Texinfo doit commencer par `\input texinfo` pour charger explicitement les commandes Texinfo.
- Pour plus d'informations, voir « [Qu'est-ce que Texinfo ?](/1_generalites/glossaire/qu_est_ce_que_texinfo) ».

## Eplain (Extended Plain TeX)

- Exécutable : `eplain`.
- Pour plus d'informations, voir « [Qu'est-ce qu'Eplain ?](/1_generalites/glossaire/qu_est_ce_que_eplain) ».

______________________________________________________________________

*Source :* [Things with « TeX » in the name](faquk:FAQ-texthings)

```{eval-rst}
.. meta::
   :keywords: LaTeX,TeX,glossaire,vocabulaire,format
```
