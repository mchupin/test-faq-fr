# Qu'est-ce que pdfTeX ?

Depuis quelques années, `pdfTeX` est le programme de référence des distributions TeX : la plupart des utilisateurs de LaTeX l'utilisent, parfois même sans le savoir.

Ce programme est un développement de TeX pensé pour pouvoir générer directement des fichiers PDF et non des fichiers DVI comme le compilateur `tex` d'origine, ce qui l'a rendu incontournable. Mais `pdfTeX` a aussi mis en œuvre d'autres idées, notamment dans le domaine des détails typographiques fins (par exemple, son support pour [la microtypographie](/1_generalites/notions_typographie/microtype), utilisée pour [améliorer le positionnement des sauts de ligne](/3_composition/texte/paragraphes/latex_fait_des_lignes_trop_longues)).

______________________________________________________________________

*Sources :*

- [What is pdfTeX?](faquk:FAQ-pdftex)
- [Cahiers Gutenberg n°28-29 (1998)](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_1998___28-29_197_0.pdf), pages 197-210 : *The pdfTeX Program*, par Hàn Thế Thành (en anglais),
- [Cahier Gutenberg n°32 (1999)](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_1999___32_21_0.pdf), pages 21-28 : *Améliorer la typographie de TeX*, par Hàn Thế Thành (en français).
- [Microtypographic extensions to the TeX typesetting system](https://www.tug.org/TUGboat/tb21-4/tb69thanh.pdf), thèse de doctorat de Hàn Thế Thành, octobre 2000 (Faculty of Informatics, Masaryk University -- Brno, République tchèque) (en anglais).

```{eval-rst}
.. meta::
   :keywords: LaTeX,glossaire,vocabulaire,pdfLaTeX,pdfTeX,microtypographie,compiler un document en PDF,histoire de pdfLaTeX,extensions microtypographiques
```
