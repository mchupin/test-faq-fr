# Quelle ponctuation mettre dans une énumération ?

Chaque paragraphe d'une énumération introduite par un [deux-points](wpfr:Deux-points) commence par une minuscule et (à l'exception du dernier paragraphe) se termine par un [point-virgule](wpfr:Point-virgule), quelle que soit la ponctuation que contient le paragraphe. Si un de ces paragraphes contient une autre énumération, chaque paragraphe de celle-ci (sauf le dernier) se terminera par une [virgule](wpfr:Virgule).

Voici un exemple d'énumération :

```latex
\documentclass{article}
  \usepackage[width=7cm]{geometry}
  \usepackage{lmodern}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \pagestyle{empty}
\begin{document}
Une liste à quatre éléments contient :
\begin{itemize}
\item le premier élément ;
\item le deuxième ;
\item le troisième, qui peut parfois
se subdiviser en sous-éléments, par
exemple :
  \begin{itemize}
  \item un sous-élément,
  \item et un second ;
  \end{itemize}
\item le quatrième et dernier.
\end{itemize}
\end{document}
```

______________________________________________________________________

*Sources :*

- [Ramat de la Typographie](wpfr:Le_Ramat_de_la_typographie), Aurel Ramat, 2017 ([site web](https://www.ramat.ca/)) ;
- [Comment faire des listes impeccables](https://lesmotsclairs.com/faire-listes-impeccables/).

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,ponctuation dans une liste,ponctuation dans une énumération,point-virgule,virgule ou point-virgule
```
