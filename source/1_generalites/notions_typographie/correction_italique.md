# Qu'est ce qu'une correction d'italique ?

À cause de leur inclinaison, certaines lettres montantes en italique peuvent avoir tendance à toucher la lettre suivante, notamment quand il s'agit d'un caractère lui aussi haut en romain (apostrophe, parenthèse...). Il est donc parfois nécessaire d'ajouter une espace fine, appelée *correction d'italique* pour éviter les chevauchements.

Avec LaTeX, la commande `\/` permet d'ajouter cette espace à la main. Les commandes `\textit` et `\emph` ajoutent automatiquement cette correction à la suite de leur argument, si nécessaire.

Comparer l'espacement entre le mot « pareil » et la parenthèse fermante dans ces deux paragraphes :

```latex
\documentclass[french]{article}
\usepackage[width=9cm]{geometry}
\pagestyle{empty}
\begin{document}
\Large
Le problème peut survenir à
l'intérieur d'une parenthèse (ou dans
un crochet, c'est {\itshape pareil}).

\medskip
Quand on ajoute la correction
d'italique, c'est mieux (ici, ce
n'est pas {\itshape pareil}\/). On
peut aussi laisser \LaTeX{} corriger
le problème tout seul (par rapport à
l'exemple précédent, c'est
\emph{pareil}).
\end{document}
```

Cette correction automatique est une des raisons de préférer la commande `\textit` aux commandes `\it` (de TeX) ou `\itshape` (de LaTeX).

______________________________________________________________________

*Sources :*

- [Petites leçons de typographie](https://jacques-andre.fr/faqtypo/lessons.pdf), Jacques André, 2017 (page 29).
- <https://tex.stackexchange.com/questions/8053/is-there-a-difference-between-textit-and-itshape>
- <https://tex.stackexchange.com/questions/516/does-it-matter-if-i-use-textit-or-it-bfseries-or-bf-etc>

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie,italique,correction d'italique,collision de caractères
```
