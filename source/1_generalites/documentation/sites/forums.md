# Où trouver des forums sur (La)TeX ?

## En français

- [MathemaTeX](https://www.mathematex.fr/) se présente comme un *Forum francophone relatif aux mathématiques avec support MathJax, LaTeX et Asymptote*.
- [Developpez.com>LaTeX](https://www.developpez.net/forums/f149/autres-langages/autres-langages/latex/) : Le site *Developpez.net* regroupe une communauté de développeurs dans une grande diversité de langages de programmation... dont LaTeX.
- [TeXnique.fr](https://texnique.fr/osqa/) : Ce site, à l'image de Stack Exchange cité ci-après, est centré sur la recherche de réponses aux questions que se posent des utilisateurs sur TeX et LaTeX. Pour bien échanger sur ce forum, pensez à consulter d'abord les questions « [Comment obtenir de l'aide en ligne ?](/1_generalites/documentation/listes_de_discussion) » et « [Comment faire un « exemple minimal » ?](/1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal) ».

## En anglais

- [TeX - LaTeX Stack Exchange](https://tex.stackexchange.com/) : Ce site, appelé *TeX.SE* par ses habitués, se présente comme un forum de discussion orienté sur la résolution de questions techniques. Il dispose d'un système de vote sur la qualité des réponses, ce qui permet d'obtenir un classement par pertinence des différentes réponses publiées. Par ailleurs, Stack Exchange propose des [conseils](https://tex.meta.stackexchange.com/questions/1436/welcome-to-tex-sx) que tout utilisateur devrait au moins parcourir avant de demander de l'aide : le but principal de ces conseils est de maximiser vos chances d'obtenir rapidement des informations utiles. Pour bien échanger sur ce forum, pensez à consulter les questions « [Comment obtenir de l'aide en ligne ?](/1_generalites/documentation/listes_de_discussion) » et « [Comment faire un « exemple minimal » ?](/1_generalites/documentation/listes_de_discussion/comment_faire_un_exemple_complet_minimal) ».
- [TopAnswers TeX](https://topanswers.xyz/tex) : Ce site fonctionne également sur le principe des questions-réponses, mais en y associant une messagerie instantanée qui donne un côté beaucoup plus humain aux échanges. De plus, il est dépourvu de publicités commerciales.
- [LaTeX Community](https://latex.org/forum/) : Ce forum offre une large variété de sujets regroupés par catégorie.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,forums de discussion,forums d'aide,forum web,questions-réponses sur LaTeX
```
