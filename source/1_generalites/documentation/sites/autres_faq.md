# Où trouver des FAQ sur (La)TeX ?

:::{warning}
LaTeX est utilisé depuis 40 ans. Beaucoup de documentations obsolètes se sont accumulées, notamment si vous [cherchez « FAQ LaTeX » sur Google](https://www.google.com/search?q=faq+latex), vous allez trouver une majorité de versions de 1999, 2001, etc.

La présente FAQ s'est beaucoup basée sur ces sources, en s'efforçant de les rassembler, traduire en français et mettre à jour.
:::

## En français

- La FAQ du groupe de discussion `fr.comp.text.tex` (noté `fctt` par les habitués) : [faqfctt.fr.eu.org](http://faqfctt.fr.eu.org/). Sa dernière mise à jour date de novembre 2004, avec un gros travail éditorial de Benjamin Bayart. Tout le contenu de cette FAQ a été importé pour constituer la présente version. Son contenu reprenait largement celui de la version [publiée chez Vuibert par Marie-Paule Kluth en 1998 et 2000](https://www.leslibraires.fr/livre/969775-faq-latex-2eme-edition-foire-aux-questions-latex-marie-paule-kluth-vuibert).
- La [FAQ LaTeX du forum Developpez.com](https://latex.developpez.com/faq/).
- [Les fiches à Bébert](http://typo.lesfichesabebert.fr/), maintenues par Bertrand Masson.
- Historiquement, il convient de citer [l'article de Bobby Bodenheimer](http://cahiers.gutenberg.eu.org/cg-bin/article/CG_1992___13_55_0.pdf) dans le [Cahier GUTenberg n°13](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1992___13) (en 1992) revu dans le [Cahier GUTenberg n°23](http://cahiers.gutenberg.eu.org/cg-bin/feuilleter?id=CG_1996___23) (en 1996).

:::{tip}
Le package <ctanpkg:LaTeX-FAQ-francaise> n'est rien d'autre que le contenu de la présente FAQ, régulièrement uploadé sur le CTAN pour encourager sa réutilisation.
:::

## En anglais

- La **FAQ de référence** en anglais est celle du TUG : [texfaq.org](https://texfaq.org/). La présente FAQ base certaines de ces pages sur elle, comme on peut le voir par le biais de lien tel [Where to find FAQs](faquk:FAQ-whereFAQ) en bas de cette page. Une grande partie des réponses date de 2006, mais les mainteneurs actuels ont fait un énorme travail de mise à jour. Le site du CTAN propose [son contenu sous forme PDF](ctanpkg:texfaq) (vision à 2014).

  - Source initiale de la FAQ anglaise, l'article de Bobby Bodenheimer, cité plus haut, était associé au groupe de discussion `comp.text.tex`. Sa dernière version de 1996 est conservée à titre d'archive sur le [site du CTAN](ctanpkg:old-faq-en).
  - Toujours en lien avec cette première FAQ, la [visual LaTeX FAQ](ctanpkg:visualfaq) de Scott Pakin se propose de mettre en lien du texte mis en forme avec des questions de la FAQ anglaise. Ainsi cliquer sur un graphique vous renverra vers les questions traitant des graphiques.

- Le dossier [help](https://www.ctan.org/help) du CTAN est organisé comme une FAQ.

- Le Wikibook anglophone sur LaTeX [contient une courte FAQ](https://en.wikibooks.org/wiki/LaTeX/FAQ).

- L'*Open Directory Project (ODP)* maintient une [liste de liens par thème](https://www.dmoztools.net/Computers/Software/Typesetting/TeX/) sur TeX et LaTeX, FAQ incluses.

- Le [(La)TeX navigator](http://tex.loria.fr). {octicon}`alert;1em;sd-text-warning` Ce site n'est plus mis à jour depuis 2005.

- La FAQ du groupe de discussion `comp.fonts`, surtout d'intérêt historique : <ctanpkg:comp-fonts-FAQ>.

- La documentation en ligne de LyX [une FAQ concernant en partie LaTeX](https://wiki.lyx.org/FAQ/FAQ). Attention que les réponses sont orientées vers l'utilisation de [LyX](wpfr:LyX), qui est une interface graphique permettant de composer des documents LaTeX, donc elles parlent souvent de menus et de modules LyX plutôt que de macros LaTeX.

## En d'autres langues

- En allemand, la FAQ du groupe de discussion `de.comp.text.tex` est disponible sur le [CTAN](ctanpkg:faq-de) ou [en format wiki](https://texfragen.de/).
- En italien, le groupe GuIT maintient [une FAQ](https://www.guitex.org/home/it/faq).
- En espagnol, est disponible la [FAQ LaTeX de CervanTeX](http://www.aq.upm.es/Departamentos/Fisica/agmartin/webpublico/latex/FAQ-CervanTeX/FAQ-CervanTeX.html).

______________________________________________________________________

*Source :* [Where to find FAQs](faquk:FAQ-whereFAQ)

```{eval-rst}
.. meta::
   :keywords: LaTeX,FAQ,FAQs,FAQ LaTeX,FAQ LaTeX en français,FAQ TeX,foire aux questions,frequently asked questions,documentation
```
