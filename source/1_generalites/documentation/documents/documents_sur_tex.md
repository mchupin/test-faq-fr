# Que lire sur TeX ?

## En français

- Paul W. Abrahams, Karl Berry et Kathryn A. Hargreaves, [TeX pour l'impatient](ctanpkg:impatient). Il s'agit de la traduction du livre cité ci-dessous dans la section des livres en anglais.
- Michael Doob, [Introduction à TeX - Manuel d'auto-formation](https://tex.loria.fr/general/doob-tex-book.pdf). Ce document est la traduction de l'ouvrage cité ci-dessous dans la section des livres en anglais.

## En anglais

### Manuels d'introduction

- Michael Doob, [A Gentle Introduction to TeX](https://ctan.org/tex-archive/info/gentle).
- David R. Wilkins, [Getting started with TeX](http://www.ntg.nl/doc/wilkins/pllong.pdf).

### Autres ouvrages

- Paul W. Abrahams, Karl Berry et Kathryn A. Hargreaves, [TeX for the Impatient](ctanpkg:impatient), Addison-Wesley, 1990, ISBN-10 0-2015-1375-7. Ce livre est épuisé en version papier.
- David Bausum, [TeX Primitive Control Sequences](http://www.tug.org/utilities/plain/cseq.html).
- [Victor Eijkhout](http://eijkhout.net/), [TeX by Topic](ctanpkg:texbytopic), Addison-Wesley, 1992, ISBN-10 0-2015-6882-9. Ce document est considéré comme complémentaire au TeX​book. Cet ouvrage est aussi accessible directement par ce [lien](texdoc:texbytopic).
- [Joseph H. Silverman](https://www.math.brown.edu/johsilve/), [TeX Reference Card](https://www.math.brown.edu/johsilve/ReferenceCards/TeXRefCard.v1.5.pdf).
- [Norman Walsh](https://nwalsh.com/), [Making TeX Work](http://makingtexwork.sourceforge.net/mtw/), 1994, O'Reilly. Le document est épuisé en version papier mais mis à disposition sous licence GNU. Une [page GIThub](https://github.com/ndw/MakingTeXWork) lui est également consacrée.

______________________________________________________________________

*Source :* ["Online introductions : Plain TeX"](faquk:FAQ-man-tex)

```{eval-rst}
.. meta::
   :keywords: TeX,documentation,libre
```
