# À quoi sert la classe « ltxdoc » ?

- La classe <ctanpkg:ltxdoc> est un peu particulière : elle sert à produire de la documentation pour les packages ou les classes. Le package ou la classe sont alors fournis sous forme d'un «kit d'installation», contenant un fichier `.ins` et un fichier `.dtx` ou `.doc`. Le premier contient les sources des fichiers nécessaires ainsi que la documentation (on parle de [Literate Programming](/5_fichiers/web/literate_programming)). Ces fichiers pourront être extraits en compilant le second fichier dit « d'installation ».

## Que puis-je lire sur la classe « ltxdoc » ?

- La documentation de la classe <ctanpkg:ltxdoc> est disponible dans toutes les distributions, sous la forme d'un fichier `.dtx` nommé `ltxdoc.dtx`. Compilé, ce fichier produira la documentation de la classe <ctanpkg:ltxdoc> sous la forme du fichier `ltxdoc.dvi`.

Le [LaTeX Companion](https://www.latex-project.org/help/books/#french) contient également une section à propos de la classe <ctanpkg:ltxdoc>.

## Y a-t-il des alternatives à « ltxdoc » ?

- Oui, plusieurs classes ont été développées récemment pour remplacer `ltxdoc` en y ajoutant des fonctionnalités. Notamment :
- <ctanpkg:skdoc> de Simon Sigurdhsson, [activement développée](https://github.com/urdh/skdoc),
- <ctanpkg:ydoc> de Martin Scharrer, qui est en version alpha et dont le développement semble s'être arrêté en 2011.

______________________________________________________________________

*Sources :*

- [Classes for LaTeX documentation](https://tex.stackexchange.com/questions/45421/classes-for-latex-documentation).

```{eval-rst}
.. meta::
   :keywords: LaTeX,programmation,documentation,literate programming,packages
```
