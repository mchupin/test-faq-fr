# Où trouver la documentation française de KOMA-Script ?

Le package <ctanpkg:koma-script>, de Markus Kohm, fournit des classes de remplacement pour les classes standards `article`, `report` et `book`, plus adaptées aux habitudes européennes. Il offre aussi une classe pour les lettres (mais pour le courrier français, la classe <ctanpkg:lettre> de Denis Mégevand semble plus appropriée).

KOMA-Script est très polyvalent, Markus Kohm y a intégré les bonnes pratiques de typographie, mais la documentation de référence incluse dans le paquet est en allemand, et même la version anglaise n'est pas aussi complète.

En 2017, Raymond Rochedieu a publié sous forme d'un livre libre une traduction et adaptation en français de cette documentation : <https://framabook.org/koma-script/> :

<https://framabook.org/wordpress/wp-content/uploads/2017/10/Prem_couvKomascript-286x400.png>

L'auteur donne également une liste de documentations utiles en français sur le blog de Framasoft : <https://framablog.org/2017/10/17/papiray-fait-du-komascript/>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation libre,Papiray,livre,Framasoft
```
