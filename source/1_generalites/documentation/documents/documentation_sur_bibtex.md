# Que lire sur BibTeX ?

[BibTeX](ctanpkg:bibtex), un programme initialement conçu pour produire des bibliographies en conjonction avec LaTeX, est expliqué dans la section 4.3 et l'annexe B du [LaTeX reference manual](/1_generalites/documentation/livres/documents_sur_latex)// de Leslie Lamport (on pourrait aussi citer le / [LaTeX Companion](/1_generalites/documentation/livres/documents_sur_latex) qui évoque également l'écriture de fichiers de style [BibTeX](ctanpkg:bibtex)). Il existe cependant plusieurs documents libres sur le sujet.

## En anglais

- Alexander Feder, [BibTeX.org -- Your BibTeX resource](http://www.bibtex.org). Ce site propose une introduction solide, mais n'entre pas dans les détails.
- David Hoadley et Michael Shell, [BibTeX Tips and FAQ](ctanpkg:bibtex). Ce document fait partie de la documentation officielle de [BibTeX](ctanpkg:bibtex).
- Nicolas Markey, [Tame the BeaST (The B to X of BibTeX)](https://ctan.org/pkg/tamethebeast/). Ce tutoriel décrit la procédure complète d'utilisation de [BibTeX](ctanpkg:bibtex).
- Oren Patashnik, [BibTeXing](texdoc:bibtex). Il s'agit de la documentation officielle de [BibTeX](ctanpkg:bibtex). Elle développe le chapitre du livre de Lamport.
- Oren Patashnik, [Designing BibTeX Styles](texdoc:btxhak). Ce document explique le langage utilisé pour écrire les styles BibTeX (fichiers `bst`). Ce document fait partie de la documentation officielle de [BibTeX](ctanpkg:bibtex).
- Oren Patashnik et Howard Trickey, [BibTeX 'plain' family](http://mirrors.ctan.org/biblio/bibtex/base/btxbst.doc). Ce fichierdonne le modèle pour les quatre styles standard ([plain](ctanpkg:bibtex), [abbrv](ctanpkg:bibtex), [alpha](ctanpkg:bibtex) et [unsrt](ctanpkg:bibtex)). Il contient également leur documentation et fait partie de la documentation officielle de [BibTeX](ctanpkg:bibtex).

______________________________________________________________________

*Source :* [BibTeX Documentation](faquk:FAQ-BibTeXing)

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation
```
