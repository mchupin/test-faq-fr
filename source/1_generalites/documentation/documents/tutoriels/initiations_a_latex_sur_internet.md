# Où trouver des introductions à LaTeX ?

## En français

- Adrien Bouzigues, [Initiation à LaTeX - Pour débutants ou jeunes utilisateurs](ctanpkg:guide-latex-fr).
- Tobias Oetiker, [Une courte (?) introduction à LaTeX2ε](ctanpkg:lshort-french). Il s'agit de la traduction du document cité plus bas.
- Zeste de savoir, [Introduction à LaTeX, devenez des TeXniciens](https://zestedesavoir.com/tutoriels/826/introduction-a-latex/). Le site de *Zeste de savoir* [propose également d'autres pages sur LaTeX](https://zestedesavoir.com/bibliotheque/?tag=latex).

## En anglais

- Engelbert Buxbaum, [The LaTeX document preparation system](ctanpkg:latex-course). Cette présentation correspond à un cours universitaire.
- Mark van Dongen, [LaTeX and friends](http://csweb.ucc.ie/~dongen/LAF/LAF.html). Ce site présente le livre de l'auteur mais aussi un ensemble de présentations détaillant par thèmes les fonctionnalités de LaTeX.
- Peter Flynn, [A beginner's introduction to typesetting with LaTeX](ctanpkg:beginlatex). Ce document servait à l'origine à un cours.
- George Grätzer, [More Math into LaTeX](ctanpkg:math-into-latex-4). Il s'agit ici d'un extrait du [livre de l'auteur](https://www.springer.com/gp/book/9780387688527) pensé pour être un « cours réduit ».
- Harvey J. Greenberg, [Simplified Introduction to LaTeX](ctanpkg:simplified-latex).
- Jim Hefferon, [Getting something out of LaTeX](ctanpkg:first-latex-doc). Ce document est conçu pour donner une première idée de ce que fait LaTeX à quelqu'un qui ne l'a jamais utilisé du tout. Différent d'un tutoriel, ce document aide simplement l'utilisateur à décider de passer à un tutoriel, et de là à une utilisation « réelle » de LaTeX.
- Philip Hirschhorn, [Getting up and running with AMSLaTeX](ctanpkg:amslatex-primer). Il s'agit d'une brève introduction à LaTeX lui-même, suivie d'une introduction substantielle à l'utilisation des classes AMS et de l'extension <ctanpkg:amsmath>. Cela inclut aussi des éléments potentiellement intéressants pour ceux qui écrivent des documents contenant des mathématiques.
- Edith Hodgen, [LaTeX, a Braindump](https://web.archive.org/web/20081014015530/http://homepages.mcs.vuw.ac.nz/~david/latex/notes.pdf) (sur [Internet Archive](https://web.archive.org/)). Ce document part de presque rien, donnant même un tutorial basique sur l'utilisation de `Linux`.
- Tobias Oetiker, [(Not so) Short Introduction to LaTeX 2ε](ctanpkg:lshort-english). Ce document est régulièrement modifié et traduit en différentes langues dont le français ; la dernière mise à jour date de 2018.
- Andrew Roberts, [Getting to Grips with LaTeX](http://www.andy-roberts.net/misc/latex/). Ce site donne une courte introduction agréable à l'utilisation de TeX et LaTeX.
- Nicola Talbot, [LaTeX for complete novices](https://www.dickimaw-books.com/latex/novices/)//. Ce tutoriel fait partie d'un ensemble de [tutoriels d'introduction](https://www.dickimaw-books.com/latex/) qui incluent des exercices : [Using LaTeX to Write a PhD Thesis](https://www.dickimaw-books.com/latex/thesis/) (utiliser LaTeX pour rédiger une thèse de doctorat) et //[LaTeX for Administrative Work](https://www.dickimaw-books.com/latex/admin/) (LaTeX pour le travail administratif).
- David R. Wilkins, [Getting started with LaTeX](http://www.maths.tcd.ie/~dwilkins/LaTeXPrimer/).

______________________________________________________________________

*Source :* ["Online introductions : LaTeX"](faquk:FAQ-man-latex)

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,initiation à LaTeX,apprendre LaTeX,découvrir LaTeX
```
