# Où trouver des tutoriels sur (La)TeX ?

:::{todo} *La notion de tutoriel est ici assez vague, les présentes références mélangent des pages de liens, des exemples et des documents complets...*
:::

## En français

- Philippe d’Anfray, [LaTeX pour l’amateur... Un peu de poésie, diable !](https://www.gutenberg.eu.org/IMG/pdf/que_diable-philippe_d_anfray-00-qd3-5.pdf).
- Marc Bailly-Bechet, [LaTeX et communication scientifique](http://www.unice.fr/mbailly/comm_sci.html). Série de cours donnés [à Lyon](https://pbil.univ-lyon1.fr/members/mbailly/Comm_Scientifique/) puis à Nice, inspirés de présentations de Sandrine Charles.
- Manuel Pégourié-Gonnard, [Apprentissage et pratique de LaTeX](http://pgoutet.free.fr/latex/poly.pdf). Philippe Goutet, qui a donné le cours à la suite de Manuel, a mis à disposition sur un site [différents documents complémentaires](http://pgoutet.free.fr/latex/index.html) à ce polycopié.
- Mark Trettin, [Liste des péchés des utilisateurs de LaTeX2e](ctanpkg:l2tabu-french) (traduit par Bernard Alfonsi). Un tutorial souvent cité car développant une liste de « tabous » autrement dit de techniques à éviter avec LaTeX.
- Vincent Zoonekynd, [Pages de titre](http://zoonek.free.fr/LaTeX_samples_title/0.html)//, [Têtes de chapitre](http://zoonek.free.fr/LaTeX/LaTeX_samples_chapter/0.html) et //[Têtes de section](http://zoonek.free.fr/LaTeX/LaTeX_samples_section/0.html). Ces trois pages fournissent toute une série d'exemples de style, graphiques ou pas, présentant le code et son résultat.

## En anglais

### Sur (La)TeX en général

- *Indian TUG*, [Online tutorials on LaTeX](https://www.tug.org/tutorials/tugindia). Le TUG indien a mis une série de 17 documents dont 2 servent d'introduction.
- Tim Love, [Text Processing using LaTeX](http://www-h.eng.cam.ac.uk/help/tpl/textprocessing/). Ce documentaliste du département d'ingénieurie de l'université de Cambridge met à disposition de nombreux liens organisés par thème ainsi que ses propres pages d'aide et d'exercices.
- Mike Shell, [The Testflow User’s Guide](ctanpkg:testflow). Ce document peut servir de guide de configuration étape par étape pour établir un « flux de tâches » dans votre système TeX ou LaTeX, afin que la sortie apparaisse à la taille et à la position correctes sur du papier de format standard et que la qualité d'impression soit satisfaisante. Certains points traités sont également décrits dans la question « [Comment obtenir un PDF de qualité ?](/5_fichiers/pdf/generer_un_fichier_pdf_de_qualite) ».
- Mark Trettin, [An essential guide to LaTeX2e usage](ctanpkg:l2tabu) (traduit par Jürgen Fenn). Un tutorial souvent cité car développant une liste de « tabous » autrement dit de techniques à éviter avec LaTeX.

### Sur les mathématiques

- Michael Downes et Barbara Beeton, [Short Math Guide for LaTeX](ctanpkg:short-math-guide). Ce document est mis à disposition par l [AMS](/1_generalites/glossaire/que_sont_ams-tex_et_ams-latex) qui le cite d'ailleurs sur [son site](http://www.ams.org/tex/amslatex.html).
- Peter Smith, [LaTeX for Logicians](http://www.logicmatters.net/latex-for-logicians/). Ces pages web couvre un domaine restreint mais propose également beaucoup de liens vers des documents pertinents.
- Herbert Voß, [Math mode](ctanpkg:voss-mathmode). Ce document est un guide complet des mathématiques sous LaTeX et est devenu un livre par la suite [un livre](/1_generalites/documentation/livres/documents_sur_latex). L'auteur considère que le document est obsolète, en particulier pour des raisons typographiques.

### Sur l'utilisation des fontes

[La page dédiée du TUG](http://www.tug.org/fonts/) dévéloppe largement le sujet et propose des liens vers de nombreux documents (dont les suivants).

- Sebastian Rahtz, [Essential NFSS2](http://www.tug.org/TUGboat/Articles/tb14-2/tb39rahtz-nfss.pdf), [TUGboat 14-2](http://www.tug.org/TUGboat/tb14-2/). Ce document de 1993 décrit l'utilisation des fontes dans TeX.
- Walter Schmidt, [Font selection in LaTeX - The most frequently asked questions](https://tug.org/pracjourn/2006-1/schmidt/schmidt.pdf), [The PracTeX Journal 2006-1](https://tug.org/pracjourn/2006-1/). Le document se présente sous forme de FAQ.

### Sur les graphiques

- Jean Crémer, [A very minimal introduction to TikZ](http://cremeronline.com/LaTeX/minimaltikz.pdf) (2011).
- Stefan Kottwitz, [TeXample.net](http://www.texample.net/). Ce site web est dédié à l'utilisation des packages de dessin [PGF et TikZ](/3_composition/illustrations/dessiner_avec_tex).
- Keith Reckdahl, [Using Imported Graphics in LaTeX2e](ctanpkg:epslatex) (2006). Il existe une [version en français, datée de 2001](texdoc:epslatex-fr). Ce document est une excellente introduction à l'utilisation des graphiques. Il est disponible sur le CTAN, mais pas dans les distributions TeX Live ou MiKTeX, faute de sources.

______________________________________________________________________

*Source :* [(La)TeX tutorials](faquk:FAQ-tutbitslatex)

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,didacticiels
```
