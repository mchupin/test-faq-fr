# Où trouver l'ensemble des commandes ?

## En français

- Karl Berry *et al*, [Manuel de référence officieux de LaTeX2ε](http://tug.ctan.org/info/latex2e-help-texinfo-fr/latex2e-fr.html), traduction en français par Vincent Belaïche du *LaTeX2ε unofficial reference manual* mentionné ci-dessous. Ce vaste document détaille toutes les commandes disponibles.
- Philippe Goutet, [Liste de commandes LaTeX](http://pgoutet.free.fr/latex/liste_commandes.pdf). Ce document accompagne un [cours donné par l'auteur](http://pgoutet.free.fr/latex/index.html) et la liste est donc partielle.

## En anglais

- David Bausum, [TeX Primitive Control Sequences](http://www.tug.org/utilities/plain/cseq.html). Ce site présente une liste des primitives de TeX avec un lien pour chaque primitive menant à une description détaillée et comprenant des références aux pages du TeX​book.
- Karl Berry, Stephen Gilmore et Torsten Martinsen, et Vincent Belaı̈che (trad.) [LaTeX2ε : Un manuel de référence officieux](ctanpkg:latex2e-help-texinfo-fr).
- Martin Scharrer, [List of internal LaTeX2ε Macros useful to Package Authors](ctanpkg:macros2e). Ce document est une bonne aide pour les personnes souhaitant écrire une classe ou une extension.
- [Goddard Institute for Space Studies (NASA)](https://www.giss.nasa.gov/), [Hypertext Help with LaTeX](http://www.giss.nasa.gov/tools/latex/ltx-2.html). Cette page donne accès à trois listes (sujets, commandes, environnements) où chaque entrée est détaillée dans une page dédiée. {octicon}`alert;1em;sd-text-warning` *Le site indique que la page n'est plus maintenue et que son contenu est conservé pour des raisons d'historique.*
- [LaTeX2ε help](http://www.emerson.emory.edu/services/latex/latex2e/latex2e_toc.html) (Emerson Center of Emory University).

______________________________________________________________________

*Source :* [Reference documents](faquk:FAQ-ref-doc)

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,commandes de LaTeX,syntaxe de LaTeX,apprendre LaTeX
```
