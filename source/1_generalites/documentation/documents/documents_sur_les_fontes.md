# Que lire sur les fontes ?

## En français

:::{todo} Ajouter des références sur les fontes en français.
:::

## En anglais

- Il existe une FAQ `comp.fonts` disponible sur <http://www.nwalsh.com/comp.fonts/FAQ/index.html>
- La note sur les fontes Postscript [dans la documentation](texdoc:psnfss) de l'extension [PSNFSS](ctanpkg:psnfss).

```{eval-rst}
.. meta::
   :keywords: LaTeX,documentation,Postscript,polices de caractères
```
