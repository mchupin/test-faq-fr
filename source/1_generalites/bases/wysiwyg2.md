# Quels logiciels choisir pour préparer mes documents TeX ?

La page « [Pourquoi TeX n'est pas WYSIWYG ?](/1_generalites/bases/wysiwyg) » présente les raisons (ou les excuses) de l'énorme disparité des interfaces utilisateur entre les environnements TeX « courants » et les traitements de texte commerciaux.

Aujourd'hui, il existe tout un éventail d'outils qui tentent de combler ce fossé. Une première catégorie vise modestement à fournir à l'utilisateur un document source lisible. Le niveau le plus bas d'assistance est la coloration syntaxique : marquer les tokens TeX, les commentaires et autres choses avec des couleurs spécifiques. De nombreux éditeurs gratuits peuvent s'adapter à TeX de cette manière. À l'autre extrême, nous avons [TeXmacs](http://www.texmacs.org), un processeur de documents utilisant les algorithmes et les polices de TeX à la fois pour l'affichage pendant l'édition et pour l'impression. TeXmacs n'utilise pas le langage TeX lui-même (bien que, parmi d'autres formats, LaTeX puisse être exporté et importé). Un peu plus proche de LaTeX est [LyX](http://www.lyx.org/), qui a son propre affichage (quasi-WYSIWYG) et ses propres formats de fichiers, mais qui produit sa sortie imprimée en exportant vers LaTeX. L'interface est très similaire à un traitement de texte classique, mais vous avez la possibilité d'entrer votre propre code LaTeX. Si vous utilisez des constructions que LyX ne comprend pas, il les affichera simplement comme texte source marqué en rouge, mais les exportera correctement.

Il faut un travail considérable pour créer de zéro un éditeur qui soit réellement bon pour l'édition (et pour TeX), et ce n'est peut-être pas un hasard si plusieurs approches ont été mises en œuvre en se fondant sur l'éditeur Emacs, réputé pour son extensibilité. Il existe cependant un très grand nombre d'éditeurs spécifiques à (La)TeX : voir <https://tex.stackexchange.com/q/339/> pour une liste (non exhaustive).

Un autre type de logiciels vise à rendre plus immédiate la prévisualisation du rendu final de votre document. Ceci est typiquement géré en utilisant un système appelé SyncTeX, qui permet de relier les lignes du code LaTeX et les emplacements dans le fichier PDF résultant de la compilation. Ainsi, il est possible de naviguer très facilement entre le code-source (potentiellement long) et la sortie PDF, et *vice versa*. Un très grand nombre des éditeurs mentionnés ci-dessus utilisent cette technologie.

Les différentes approches offrent des choix variés, qui diffèrent par l'immédiateté de leur réponse, la zone de l'écran sur laquelle ils travaillent (source ou fenêtre séparée), le degré de correspondance de l'affichage avec la sortie finale, et l'équilibre qu'ils trouvent entre aide visuelle et distraction visuelle.

______________________________________________________________________

*Sources :*

- [The TeX document preparation environment](faquk:FAQ-WYGexpts),
- [What exactly is SyncTeX?](https://tex.stackexchange.com/questions/118489/what-exactly-is-synctex)

```{eval-rst}
.. meta::
   :keywords: LaTeX,éditeurs de texte,éditeurs LaTeX,interface graphique pour LaTeX
```
