# Comment être sûr que c'est vraiment TeX ?

TeX, MetaFont et MetaPost sont écrits dans un langage de [programmation lettrée](/5_fichiers/web/literate_programming) appelé « Web » qui est conçu pour être portable sur une large gamme de systèmes informatiques. Comment, alors, une nouvelle version de TeX est-elle validée ?

Bien sûr, tout programmeur de logiciel sensé aura sa propre batterie de tests pour vérifier que son logiciel fonctionne. Les personnes qui portent TeX et assimilés sur d'autres plateformes suivent cette logique et effectuent de tels tests.

Donald Knuth, cependant, a fourni des « tests de conformité » (*conformance tests*) : [trip](ctanpkg:tex) (documenté [ici](texdoc:tripman)) pour TeX et [trap](ctanpkg:metafont) (documenté [ici](texdoc:trapman)) pour MetaFont. Il les désigne comme des tests de torture. De fait, ils ne sont pas conçus pour vérifier les choses évidentes qu'utilisent les documents typographiques ordinaires, ou les polices de caractères. Ces tests explorent plutôt les sentiers moins balisés du code de TeX et ils sont, pour le lecteur occasionnel, assez incompréhensibles.

Une fois qu'une implémentation de TeX a passé son test [trip](ctanpkg:tex), ou qu'une implémentation de MetaFont a passé son test [trap](ctanpkg:metafont), alors il peut en principe être distribué comme version de travail. Dans la pratique, des tests avec des documents classiques ou des polices sont aussi effectués.

______________________________________________________________________

*Source :* [How can I be sure it's really TeX?](faquk:FAQ-triptrap)

```{eval-rst}
.. meta::
   :keywords: LaTeX,conformité,tests,torture
```
