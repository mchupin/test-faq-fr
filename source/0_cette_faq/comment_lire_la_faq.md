# Comment lire la FAQ ?

## Probablement pas dans l'ordre

Cette FAQ ne se lit pas comme un roman et n'est pas vraiment à but pédagogique, en ce sens que les questions du début ne sont pas vraiment plus simples que les questions de la fin.

Nous avons plutôt essayé de ranger la FAQ par thèmes, par sous-thèmes, par problème, etc. Du coup, deux questions très voisines comme « Comment ajouter une espace verticale ? » et « Comment séparer un flottant de son titre (caption) ? » ne sont pas forcément côte à côte : la première est générale à un paragraphe dans un document LaTeX, la seconde est, ou semble être, liée à la notion de flottant. Cependant, elles renvoient, a priori, l'une à l'autre.

## En regardant au moins les pages titres

La table des matières permet de s'approprier un peu plus la structure de la FAQ et d'estimer l'emplacement probable de chaque question (il n'est pas exclu que ce ne soit pas parfaitement rangé...). De plus chaque thème pourra contenir une courte introduction sur la façon dont il est rangé sous le titre « Quel ordre a été retenu ? ».

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
