# Quels langages de description graphique peut-on utiliser avec LaTeX ?

Certains langages permettant de présenter des graphiques peuvent être utilisés directement dans le code source de documents LaTeX pour travailler plus efficacement. En voici quelques exemples.

Notez bien que, dans la plupart des cas (hors <ctanpkg:asymptote>), ces solutions nécessitent que vous puissiez [exécuter des programmes externes à partir de votre document](/2_programmation/compilation/write18).

## Asymptote

L'extension <ctanpkg:asymptote> définit un environnement `asy` qui fait en sorte que son contenu soit disponible pour le traitement par le logiciel `asymptote` pour être ensuite placé dans le document (après un nombre suffisant de compilations). Pour cela, il faut par exemple écrire dans votre fichier source :

```latex
% !TEX noedit
\begin{asy}
⟨code asymptote⟩
\end{asy}
```

Ensuite, il faut exécuter ici les commandes suivantes (on suppose ici qu'il est nommé `document.tex`) :

```bash
latex document
asy document-*.asy
latex document
```

## GNUplot

L'extension <ctanpkg:egplot> vous permet d'incorporer des instructions `GNUplot` dans votre document, pour un traitement en dehors de LaTeX. Elle fournit des commandes qui permettent à l'utilisateur de faire des calculs dans `GNUplot` et de restituer les résultats dans le graphique à tracer.

## MetaPost

Trois extensions vous permettent d'inclure du code MetaPost dans votre code LaTeX :

- <ctanpkg:gmp> avec lequel les paramètres associés transmis par l'appel de l'environnement ;
- <ctanpkg:emp>, extension plus ancienne que <ctanpkg:gmp>, qui fournit des fonctionnalités similaires ;
- <ctanpkg:mpgraphics>.

______________________________________________________________________

*Source :*

- [In-line source for graphics applications](faquk:FAQ-inlgrphapp)

```{eval-rst}
.. meta::
   :keywords: LaTeX,dessins,illustrations,schéma,dessiner en LaTeX
```
