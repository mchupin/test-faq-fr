# Comment créer une ombre sous une image ?

## Avec l'extension « TikZ »

[Ludovic Vimont](https://borntocode.fr/latex-differents-effets-sur-vos-images-grace-au-package-tikz/) propose cette solution avec [TikZ](ctanpkg:pgf) :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{tikz}
\usepackage{graphicx}
\usetikzlibrary{shadows,calc}

\def\shadowshift{3pt,-3pt}
\def\shadowradius{5pt}

\colorlet{innercolor}{black!60}
\colorlet{outercolor}{gray!05}

\newcommand\drawshadow[1]{
    \begin{pgfonlayer}{shadow}
        % Effet d'ombre en forme de cercle en bas à gauche
        \shade[outercolor, inner color=innercolor, outer color=outercolor]
            ($(#1.south west)+(\shadowshift)+(\shadowradius/2 , \shadowradius/2)$)
            circle (\shadowradius);
        % Effet d'ombre en forme de cercle en bas à droite
        \shade[outercolor, inner color=innercolor, outer color=outercolor]
            ($(#1.south east)+(\shadowshift)+(-\shadowradius/2 , \shadowradius/2)$)
            circle (\shadowradius);
        % Effet d'ombre en forme de cercle en haut à droite
        \shade[outercolor,inner color=innercolor,outer color=outercolor]
            ($(#1.north east)+(\shadowshift)+(-\shadowradius/2 , -\shadowradius/2)$)
            circle (\shadowradius);

        % Dégradé de haut en bas sur la partie sud du rectangle
        \shade[top color=innercolor,bottom color=outercolor]
            ($(#1.south west)+(\shadowshift)+(\shadowradius/2,-\shadowradius/2)$)
            rectangle ($(#1.south east)+(\shadowshift)+(-\shadowradius/2,\shadowradius/2)$);
        % Dégradé de gauche à droite sur le côté droit du rectangle
        \shade[left color=innercolor,right color=outercolor]
            ($(#1.south east)+(\shadowshift)+(-\shadowradius/2,\shadowradius/2)$)
            rectangle ($(#1.north east)+(\shadowshift)+(\shadowradius/2,-\shadowradius/2)$);

        % On remplit le rectangle créé avec une couleur noire
        \filldraw
            ($(#1.south west)+(\shadowshift)+(\shadowradius/2,\shadowradius/2)$)
            rectangle ($(#1.north east)+(\shadowshift)-(\shadowradius/2,\shadowradius/2)$);
    \end{pgfonlayer}
}

\pgfdeclarelayer{shadow}
\pgfsetlayers{shadow,main}

\newcommand\shadowimage[2][]{
    \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[#1]{#2}};
        \drawshadow{image}
    \end{tikzpicture}
}

\begin{document}

\shadowimage[width=10cm]{test.jpg}

\end{document}
```

______________________________________________________________________

*Source :* [Jouer avec vos images grâce au package tikz](https://borntocode.fr/latex-differents-effets-sur-vos-images-grace-au-package-tikz/)

```{eval-rst}
.. meta::
   :keywords: LaTeX,ombre,shading,relief,effet 3D
```
