# Comment tracer une courbe ?

- Avec le package <ctanpkg:pst-plot> faisant partie de la distribution <ctanpkg:pstricks>.

On peut tracer des fonctions par la commande `\psplot` ou tracer des courbes paramétriques par la commande `\parametricplot`. La définition mathématique des fonctions à dessiner est à rentrer en langage Postscript, dont on peut trouver une documentation intégrale à l'adresse <http://partners.adobe.com/asn/developer/PDFS/TN/PLRM.pdf> , ou un résumé à l'adresse <http://www.cs.indiana.edu/docproject/programming/postscript/postscript.html> . Une des choses à savoir sur Postscript est que c'est un langage qui se sert de la notation polonaise inverse. Ainsi, pour calculer `\(\exp{\frac{x^2}4}\)`, on saisira `x x mul 4 div exp`. On trouvera un exemple de tracé de courbe ci-dessous

```latex
% !TEX noedit
\documentclass{article}
% Pour les marges
\usepackage[a4paper,margin=2cm]{geometry}

\usepackage{pst-plot}

\begin{document}

\begin{pspicture}(-8.2,-8.2)(8.2,8.2)
% On dessine les axes. On met les labels des
% abscisses à la main pour avoir le sigle \pi
\psaxes[linewidth=.5\pslinewidth,%
        Dx=3.1415,%
        labels=y]%
       {->}(0,0)(-8.2,-8.2)(8.2,8.2)
% \SpecialCoor permet de donner les coordonnées
% d'un point en utilisant du code PostScript avec
% un point d'exclamation !
\SpecialCoor
% (! 3.1415 2 mul 0) est le code PostScript pour
% dire (-2pi,0)
% Cela évite la calculatrice et permet
% d'automatiser l'affichage des labels formels
% contenant par exemple \pi ou \mathrm{e} en
% utilisant \multido (exemple à faire).
\uput[-90](! 3.1415 2 mul neg 0){$-2\pi$}
\uput[-90](! 3.1415 neg 0){$-\pi$}
\uput[-90](! 3.1415 0){$\pi$}
\uput[-90](! 3.1415 2 mul 0){$2\pi$}
% Dessin de y=x
\psplot[linecolor=red]{-8}{8}{x}
% Dessin de y=(sin x)/x
% En PostScript, l'argument de sin
% est en degrés. Ici, on veut des
% radians, d'où la conversion.
\psplot[linecolor=green,plotpoints=500]{-8}{8}{%
  x 360 3.1415 div mul sin x mul}
% Dessin de y=arctan x
% Pour obtenir arctan x, le code PostScript
% est x 1 atan car atan donne arctan du rapport
% des deux éléments du dessus de la pile
% Attention, la fonction atan de
% Postcript n'est pas bien définie...
% puisque pour x<0, x 1 atan donne quelque chose
% à valeur dans [3\pi/2,\pi]
\psplot[linecolor=blue,linestyle=dashed,
  plotpoints=500]{-8}{8}{%
  x 1 atan 3.1415 360 div mul}
% Il faut donc tracer arctan par morceaux
% en rusant un peu...
\psplot[linecolor=blue,plotpoints=250]
  {-8}{-0.0001}%
  {x 1 atan 3.1415 360 div mul 3.1415 sub}
\psplot[linecolor=blue,plotpoints=250]
  {0.0001}{8}{x 1 atan 3.1415 360 div mul}
\end{pspicture}

\end{document}
```

:::{todo} Parler de :ctanpkg:`PGFplots <pgfplots>`.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,PStricks,Postscript
```
