# Comment changer la forme des points avec PGFplots ?

:::{todo} Donner un exemple simple.
:::

## Pour avoir encore plus de formes

- Le package <ctanpkg:oPlotSymbl> fournit un grand nombre de formes supplémentaires :

Ici, la liste des hexagones disponibles :

```latex
\documentclass{article}
  \usepackage[latin1]{inputenc}
  \usepackage{tikz}
  \usepackage{oplotsymbl}
  \pagestyle{empty}

\begin{document}
\begin{tabular}{cll}
\hline
Symbole       & Commande                             & Description                       \\ \hline
\hexago       & \texttt{\textbackslash hexago}       & Forme standard                    \\
\hexagofill   & \texttt{\textbackslash hexagofill}   & Hexagone plein                    \\
\hexagodot    & \texttt{\textbackslash hexagodot}    & Hexagone avec un point            \\
\hexagolinev  & \texttt{\textbackslash hexagolinev}  & Hexagone avec un trait vertical   \\
\hexagolineh  & \texttt{\textbackslash hexagolineh}  & Hexagone avec un trait horizontal \\
\hexagolinevh & \texttt{\textbackslash hexagolinevh} & Hexagone avec une croix grecque   \\
\hexagocross  & \texttt{\textbackslash hexagocross}  & Hexagone avec une croix décussée  \\
\hexagofillha & \texttt{\textbackslash hexagofillha} & Hexagone à moitié plein (haut)    \\
\hexagofillhb & \texttt{\textbackslash hexagofillhb} & Hexagone à moitié plein (bas)     \\
\hexagofillhr & \texttt{\textbackslash hexagofillhr} & Hexagone à moitié plein (droite)  \\
\hexagofillhl & \texttt{\textbackslash hexagofillhl} & Hexagone à moitié plein (gauche)  \\
\hline
\end{tabular}
\end{document}
```

Voir [sa documentation](texdoc:oPlotSymbl) pour une liste complète.

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphique,forme des points,apparence des points,format du graphique,statistiques,TikZ,tracer une fonction,mathématiques
```
