# Comment inclure des formules LaTeX dans Xfig ?

- L'inclusion de formules mathématiques se fait assez simplement :
- on tape le texte qu'on veut, avec les commandes LaTeX voulues. Le texte final commencera au même endroit, mais n'occupera pas forcément la même place après avoir été compilé par LaTeX ;
- on exporte le dessin (disons `dessin.fig`) au format combiné Postscript/LaTeX. Deux fichiers sont ainsi créés : le fichier `dessin.pstex`, un fichier PS encapsulé qui contient la partie « graphique » du dessin, et le fichier `dessins.pstex_t`, un fichier LaTeX qui contient le texte et les commandes LaTeX, ainsi que les coordonnées des points où ils doivent être positionnées. Ce deuxième fichier contient également une commande qui inclut le premier ;
- on inclut, dans le document final, le fichier LaTeX créé précédemment, avec la commande `\input{dessin}`. Cela nécessite d'inclure le package <ctanpkg:epsfig>.
- il existe aussi un autre logiciel de dessins, comparable à Xfig mais offrant plus de possibilités : Tgif. Il permet en particulier d'insérer des formules créées par LaTeX, et qui seront compilées par Tgif lui-même avant d'être incluses dans la figure. C'est complètement différent : on voit le résultat final, on peut facilement placer le texte au bon endroit. Mais le texte n'est pas recompilé par LaTeX dans le document final.

```{eval-rst}
.. meta::
   :keywords: LaTeX,Postscript
```
