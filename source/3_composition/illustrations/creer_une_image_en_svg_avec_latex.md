# Comment créer une image indépendante en SVG ?

Une fois que l'on a pris l'habitude d'utiliser LaTeX pour dessiner ou représenter ses données, notamment avec les extensions <ctanpkg:TikZ> et <ctanpkg:pgfplots>, il est tentant d'y avoir recours dans d'autres contextes que la préparation d'un document LaTeX.

La classe <ctanpkg:standalone> permet de produire facilement une image indépendante, contenue dans un document PDF faisant juste sa taille. Elle propose aussi des options pour générer des fichiers dans d'autres formats graphiques, comme SVG ou PNG.

## Comment créer une image indépendante ?

Il suffit d'appeler la classe <ctanpkg:standalone>. Il devient inutile de préciser la taille du papier : celle-ci sera ajustée exactement à la taille finale de l'image.

Avec l'option de classe `tikz`, le chargement de Ti*k*Z se fait automatiquement :

```latex
\documentclass{article}
  % L'exemple ci-dessus a besoin d'une version de TeXlive assez récente
  % donc on triche pour avoir le résultat dans la FAQ.
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\begin{tikzpicture}
  \fill[orange] (0,0) circle (9ex) ;
  \node[font=\bf] at (0ex,3ex) {SVG} ;
\end{tikzpicture}
\end{document}
```

Si vous avez besoin de charger des bibliothèques Ti*k*Z, vous pouvez le faire avec `\usetikzlibrary{...}` dans le préambule de votre document :

```latex
% !TEX noedit
\documentclass[tikz]{standalone}
  \usetikzlibrary{arrows,snakes}
```

:::{warning}
La classe <ctanpkg:standalone> ne doit pas être confondue [avec la classe "minimal"](https://faq.gutenberg.eu.org/2_programmation/extensions/classe_minimal) :

- `minimal` est un simple exemple théorique d'une classe minimale, qui n'est pas destinée à être utilisée.
- `standalone` vous fournit au contraire de nombreuses options pour produire des documents indépendants.
:::

## Comment produire un fichier SVG à partir d'un fichier TeX ?

### Avec les options de l'extension « standalone »

L'extension <ctanpkg:standalone> peut se charger elle-même de la conversion du document produit en un autre format graphique. L'option de classe `convert` est faite pour cela. En utilisant en plus la primitive TeX `\jobname`, le nom du fichier SVG créé pourra être le même que celui du document PDF :

```latex
% !TEX noedit
\documentclass[tikz,convert={outfile=\jobname.svg}]{standalone}
```

La conversion de format n'est pas implémentée directement en TeX. La classe `standalone` appelle en fait des convertisseurs externes, comme [ImageMagick](wpfr:ImageMagick) ou `pdf2svg`, qui doivent être installés.

Si lors de la compilation, vous avez l'avertissement :

```text
Class standalone Warning : Conversion failed! Please ensure that shell escape
(standalone)              is enabled (e.g. use '-shell-escape').
```

cela veut dire que votre installation de LaTeX empêche par défaut l'exécution de sous-programmes externes, pour des raisons de sécurité.

Comme indiqué dans le message d'avertissement, pour autoriser manuellement cette exécution, vous pouvez utilisez l'option de ligne de commande `-shell-escape` pour lancer la compilation :

```bash
pdflatex -shell-escape fichier.tex
```

:::{tip}
Vous pouvez rencontrer d'autres messages d'avertissement lors de la compilation de votre document vers le format SVG :

```text
Output written on fichier.pdf (1 page, 13115 bytes).
Transcript written on fichier.log.
convert : attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/422.
convert : no images defined `fichier.svg' @ error/convert.c/ConvertImageCommand/3285.
system returned with code 256

Class standalone Warning : Conversion unsuccessful!
(standalone)              There might be something wrong with your
(standalone)              conversation software or the file permissions!
```

Le problème est lié à une faille de sécurité dans le traitement des fichiers PDF par GhostScript ([repérée en 2018](https://www.kb.cert.org/vuls/id/332928/)). Pour limiter les conséquences de cette faille de sécurité, les développeurs d'ImageMagick ont temporairement configuré leur logiciel pour refuser de traiter les fichiers PDF.

Si vous avez des versions récentes de tous ces logiciels (postérieures à 2019), vous pouvez réactiver les fonctionnalités concernées d'ImageMagick. Pour cela, cherchez sur votre ordinateur le fichier `/etc/ImageMagick/policy.xml` (le répertoire peut aussi s'appeler `/etc/ImageMagick-7/` ou `/etc/ImageMagick-7/`), puis ajoutez la ligne suivante juste avant la dernière ligne, qui devrait contenir `</policymap>` :

```xml
<policy domain="coder" rights="read | write" pattern="PDF" />
```
:::

### En ligne de commande

Vous pouvez également appeler ImageMagick vous-même, en ligne de commande, avec :

```bash
convert fichier.pdf fichier.svg
```

______________________________________________________________________

*Sources :*

- [How can I use TikZ to make standalone (SVG) graphics?](https://tex.stackexchange.com/questions/51757/how-can-i-use-tikz-to-make-standalone-svg-graphics)
- [ImageMagick security policy ‘PDF’ blocking conversion](https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion).

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphiques,dessins,image sur une page,image indépendante
```
