# Comment insérer un graphique produit avec SigmaPlot ?

[SigmaPlot](wp:SigmaPlot) est un logiciel propriétaire pour l'analyse de données et leur représentation graphique. Il a été très populaire pour la production de graphiques à insérer dans les articles scientifiques. Il n'existe que pour Windows.

Dans certaines versions, l'exportation directe en PDF ne fonctionne pas bien.

- Certains utilisateurs contournent le problème en exportant leur graphique en EPS. Mais même dans ce cas :
- certains caractères spéciaux ne sont pas supportés,
- la police est parfois changée.
- Une solution plus robuste semble être de produire le fichier PDF par impression virtuelle, plutôt que par export direct. En « imprimant en PDF » avec le pilote d'impression d'Adobe, les caractères spéciaux sont corrects. Il peut être nécessaire de modifier les polices avec un logiciel de dessin vectoriel tel qu'[Inkscape](wpfr:Inkscape) (ou Illustrator).
- Solution indirecte : Sigmaplot insère sans problème ses graphiques dans MS Word. Donc il est possible de passer par un document Word, puis de le transformer en PDF.

:::{tip}
**Alternatives à SigmaPlot**

Si vous démarrez un nouveau projet, vous avez sans doute intérêt à regarder du côté des alternatives *open-source* à SigmaPlot. Par exemple :

- [R](wpfr:R_(langage)),
- [SciDAVis](wp:SciDAVis),
- [LabPlot](wp:LabPlot).

Sans oublier l'extension LaTeX <ctanpkg:PGFplots>, qui permet de tracer la plupart des graphiques directement dans son document LaTeX, sans logiciel extérieur.
:::

______________________________________________________________________

*Sources :*

- [Sigmaplot to EPS to PDF to LaTeX](https://golatex.de/sigmaplot-to-eps-to-pdf-tp-latex-t6159.html) (en allemand).
- [EPS plot to LaTeX with special characters](https://tex.stackexchange.com/questions/371865/eps-plot-to-latex-with-special-characters).

```{eval-rst}
.. meta::
   :keywords: LaTeX,figures,graphiques,analyse statistique,SigmaPlot,Sigma Plot
```
