# Comment insérer des images avec « pdfLaTeX » ?

## Les formats acceptés par défaut

Le programme [pdfTeX](/1_generalites/glossaire/qu_est_ce_que_pdftex) dispose d'une gamme assez large de formats qu'il peut incorporer directement dans son flux PDF de sortie :

- JPEG (fichiers `jpg`) pour les photographies et images similaires ;
- PNG pour les images bitmap artificielles ;
- PDF pour les dessins vectoriels.

Les anciennes versions de `pdfTeX` (antérieures à la version 1.10a) prenaient en charge le format TIFF (fichiers `tif`) comme alternative aux fichiers PNG. Mieux vaut ne pas compter sur cette fonctionnalité, même si vous utilisez une version suffisamment ancienne de `pdfTeX`.

En plus des formats « natifs », la configuration standard de l'extension <ctanpkg:graphicx> provoque le chargement des commandes de l'extension <ctanpkg:pdf-mps-supp> de [Hans Hagen](https://www.ctan.org/author/hagen) : ces commandes sont capables de traduire la sortie de MetaPost en PDF à la volée. Dès lors, la sortie MetaPost (fichiers `mps`) peut également être incluse dans les documents compliés avec `pdfLaTeX`.

## Le format Postscript encapsulé (EPS)

Le problème le plus couramment rencontré par les utilisateurs est qu'il n'y a pas de moyen simple pour inclure des fichiers [EPS](/5_fichiers/postscript/postscript_encapsule), ces derniers étant largement utilisés historiquement. Puisque `pdfTeX` ne contient en effet aucun moyen de convertir PostScript en PDF.

### Avec le programme « epstopdf »

La solution classique consiste à convertir le fichier EPS en un fichier PDF approprié. Le programme `epstopdf` traite ce point et est disponible soit en tant qu'exécutable Windows, soit en tant que script Perl pour s'exécuter sur Unix et d'autres systèmes similaires. L'extension <ctanpkg:epstopdf> peut être utilisée pour générer les fichiers PDF requis à la volée. Si ce principe est bien pratique, il nécessite toutefois que vous supprimiez un des contrôles de sécurité de TeX : n'autorisez donc pas son utilisation avec des fichiers provenant de sources en lesquelles vous n'avez pas entièrement confiance.

### Avec les extensions « pst-pdf » et « auto-pst-pdf »

L'extension <ctanpkg:pst-pdf> vous permet d'aller un peu plus loin avec des éléments Postscript. Elle fonctionne un peu comme `BibTeX` : vous compilez votre fichier en utilisant `pdfLaTeX`, puis vous utilisez LaTeX, `dvips` et `ps2pdf` successivement pour produire un fichier secondaire à saisir pour votre prochaine exécution de `pdfLaTeX`. Des scripts sont fournis pour faciliter la production du fichier secondaire.

Pour sa part, l'extension <ctanpkg:auto-pst-pdf> génère un PDF de manière transparente, en générant une tâche pour traiter les sorties comme <ctanpkg:pst-pdf> le fait. Si votre installation de `pdfLaTeX` ne le permet pas automatiquement --- voir la question « [Comment lancer un sous-processus pendant la compilation ?](/2_programmation/compilation/write18) » --- vous devez alors exécuter `pdfLaTeX` avec l'instruction suivante (en changeant le nom du `fichier.tex`) :

```latex
% !TEX noedit
pdflatex -shell-escape fichier.tex
```

### Avec le programme « purifyeps »

Une solution alternative consiste à utiliser `purifyeps`, un script Perl qui utilise `pstoedit` et MetaPost pour convertir votre fichier EPS en « quelque chose qui ressemble au PostScript encapsulé issu de MetaPost » et peut donc être inclus directement. Malheureusement, `purifyeps` ne fonctionne pas pour tous les fichiers EPS.

### Autres solutions

Une large description de ce sujet est présentée dans la [page traitant des PDF](http://pstricks.tug.org/main.cgi?file=pdf/pdfoutput) de [Herbert Voß](https://www.ctan.org/author/voss) qui évoque l'utilisation de <ctanpkg:pstricks> avec `pdfLaTeX` ainsi que l'extension <ctanpkg:pdftricks>. Une alternative récente (non couverte dans la page d'Herbert Voß) est l'extension <ctanpkg:pdftricks2> qui offre des fonctionnalités similaires à <ctanpkg:pdftricks> avec quelques variantes utiles.

______________________________________________________________________

*Source :* [Imported graphics in pdfLaTeX](faquk:FAQ-pdftexgraphics)

```{eval-rst}
.. meta::
   :keywords: LaTeX,graphics,Postscript
```
