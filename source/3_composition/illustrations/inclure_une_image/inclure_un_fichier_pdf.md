# Comment insérer un document PDF de plusieurs pages ?

:::{note}
Toutes les réponses à cette question ne sont pas valables pour LaTeX, mais pour `pdfLaTeX`. En effet, le format LaTeX accepte seulement les inclusions en EPS, et il n'est pas possible d'y inclure des documents au format PDF.
:::

## Avec l'extension « graphicx »

Si le document à inclure ne comporte qu'une seule page, il est possible d'utiliser la commande `\includegraphics` de l'extension <ctanpkg:graphicx>. Se reporter à la question « [Comment insérer une image ?](/3_composition/illustrations/inclure_une_image/inclure_une_image) » pour plus d'informations sur cette commande.

## Avec l'extension « pdfpages »

L'extension <ctanpkg:pdfpages> a été écrit pour ça. Elle définit les commandes suivantes :

- `\includepdf` qui sert à insérer les pages d'un document `pdf` ;
- `\includepdfmerge` qui peut insérer les pages de plusieurs documents `pdf` ;
- `\includepdfset` permet de définir des options communes à tous les appels à `\includepdf`.

Ces commandes disposent de nombreuses options décrites dans la [documentation](texdoc:pdfpages) de l'extension <ctanpkg:pdfpages>. L'exemple ci-dessous montre une utilisation possible de la commande `\includepdf` (cet exemple produira deux pages comportant chacune 4 pages du document `faqfr.pdf`).

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{pdfpages}

\begin{document}

\includepdf[pages=3-10,nup=2x2]{faqfr.pdf}

\end{document}
```

### Comment afficher le numéro des pages avec « pdfpages » ?

Par défaut, lorsqu'on inclut un document PDF avec l'extension <ctanpkg:pdfpages>, le numéro des pages du document englobant est masqué.

L'option `pagecommand` permet d'exécuter du code sur chaque page lors de l'inclusion, et peut être utilisée de cette façon pour faire réapparaître les numéros de pages :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{pdfpages}

\begin{document}

\includepdf[pages=-,
            pagecommand={\thispagestyle{plain}}
           ]{fichier.pdf}

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,inclure un article dans un document LaTeX,inclure un document multipage,inclure un article dans une thèse,numéros de pages avec "pdfpages"
```
