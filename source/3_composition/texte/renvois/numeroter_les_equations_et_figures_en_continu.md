# Comment obtenir une numérotation des équations, figures et tables indépendante des chapitres ?

De nombreuses classes LaTeX (y compris la classe standard <ctanpkg:book>) numérotent les objets par chapitre ; les figures du chapitre 1 sont donc numérotés 1.1, 1.2, etc. Mais ce comportement ne convient pas à tous les utilisateurs.

## Avec l'extension « chngcntr »

À moins de réécrire toute la classe, il est possible d'utiliser l'extension <ctanpkg:chngcntr> qui fournit les commandes `\counterwithin` (qui établit l'imbrication de numérotation) et `\counterwithout` (qui l'annule).

Supposons que vos figures numérotées par chapitre sont 1.1, 1.2, 2.1, ... et que vous saisissiez la commande suivante :

```latex
% !TEX noedit
\counterwithout{figure}{chapter}
```

Alors, vos figures seront numérotées 1, 2, 3, ... Vous pourrez noter que la commande a également retiré le numéro de chapitre de la définition du compteur.

Voici un autre exemple permettant de changer les éléments numérotés par section (ici les équations) en éléments numérotés par chapitre :

```latex
% !TEX noedit
\counterwithout{equation}{section}
\counterwithin{equation}{chapter}
```

## Avec la commande « `\@removefromreset` »

L'utilisation de l'extension <ctanpkg:chngcntr> n'implique pas beaucoup de programmation. Ici, un programmeur enthousiaste pourrait choisir d'essayer la technique que nous devions utiliser avant l'arrivée de cette extension. Historiquement, chacune des extensions <ctanpkg:removefr> et <ctanpkg:remreset> (explicitement [obsolète](/1_generalites/histoire/liste_des_packages_obsoletes)) a défini une commande `\@removefromreset`. Cette commande fait partie, depuis 2018, des commandes du noyau LaTeX. Elle permet le code suivant qui supprime la rénumérotation automatique :

```latex
% !TEX noedit
\makeatletter
\@removefromreset{figure}{chapter}
\makeatother
```

Vous pourrez alors avoir besoin de rédéfinir la manière dont le numéro de la figure (dans ce cas) est présenté :

```latex
% !TEX noedit
\makeatletter
\renewcommand{\thefigure}{\@arabic\c@figure}
\makeatother
```

Cette technique peut également être utilisée pour traiter le cas de réinitialisation d'une numérotation à plusieurs niveaux niveaux. Supposons que la numérotation de vos figures se présente sous la forme ⟨chapitre⟩.⟨section⟩.⟨figure⟩, et que vous voulez des figures numérotées par chapitre, essayez alors :

```latex
% !TEX noedit
\makeatletter
\@removefromreset{figure}{section}
\@addtoreset{figure}{chapter}
\renewcommand{\thefigure}{\thechapter.\@arabic\c@figure}
\makeatother
```

La commande `\@addtoreset` fait partie des commandes usuelles de LaTeX.

______________________________________________________________________

*Source :* [Running equation, figure and table numbering](faquk:FAQ-running-nos)

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
