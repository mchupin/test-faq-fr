# Comment obtenir des références croisées à partir de plusieurs sources ?

Lors de la production d'un ensemble de documents interdépendants, vous souhaiterez probablement faire des références croisées *entre* ces documents ; mais, par défaut, LaTeX ne le permet pas.

## Avec l'extension « xr »

L'extension <ctanpkg:xr> permet de répondre à ce besoin. Essayez de compiler les fichiers `volume1.tex` et `volume2.tex` qui servent d'exemple.

```latex
\documentclass{article}

\begin{document}
\section{Ma section.\label{masect}}
\end{document}
```

```latex
\documentclass{article}
\usepackage{xr}
\externaldocument{volume1}

\begin{document}
Pour plus de détails, voir le document \textit{essai}, paragraphe~\ref{masect}.
\end{document}
```

Dans le détail, la ligne suivante va charger toutes les références du `volume1` dans votre document actuel :

```latex
% !TEX noedit
\externaldocument{volume1}
```

Cet exemple présuppose que `volume1.tex` et `volume2.tex` sont dans le même répertoire. Si ce n'est pas le cas, il faut spécifier le chemin relatif ou absolu de `volume1.tex` :

```latex
% !TEX noedit
\externaldocument{../Volume-1/volume1}
```

{octicon}`alert;1em;sd-text-warning` Il ne faut pas indiquer l'extension `.tex` dans l'argument de la commande `\externaldocument`, car `xr` utilise cet argument pour construire le nom du fichier `.aux` auquel il va se référer. Si vous écrivez `\externaldocument{volume1.tex}`, vous obtiendrez l'erreur suivante :

```
Package xr Warning :
No file volume1.tex.aux
LABELS NOT IMPORTED
```

{octicon}`alert;1em;sd-text-warning` Parce que `xr` utilise le fichier `.aux` du document extérieur, `volume1.tex` doit être compilé avant `volume2.tex`.

### Le cas des étiquettes ayant le même nom dans deux documents

L'extension fournit un moyen de transformer toutes les étiquettes importées, de sorte que vous n'avez pas à changer les noms d'étiquette dans l'un ou l'autre des documents. Ceci se fait au niveau de l'argument optionnel de la commande `\externaldocument` qui permet de placer un préfixe devant chaque étiquette d'un document extérieur. Voici un exemple où les étiquettes du document `volume1` sont préfixées par le terme `V1-` :

```latex
% !TEX noedit
\usepackage{xr}
\externaldocument[V1-]{volume1}
...
... l'introduction du volume 1 (\ref{V1-introduction})...
```

### Le cas particulier des hyperliens

Pour que les fonctionnalités de <ctanpkg:xr> fonctionnent avec <ctanpkg:hyperref>, vous avez besoin de <ctanpkg:xr-hyper>. Voici un exemple pour faire un hyperlien simple (c'est-à-dire vers un fichier PDF local que vous venez de compiler) :

```latex
% !TEX noedit
\usepackage{xr-hyper}
\usepackage{hyperref}
\externaldocument[V1-]{volume1}
...
... \nameref{V1-introduction}...
```

Ici, le nom de la référenc apparaîtra comme un lien actif vers le chapitre « Introduction » de `volume1.pdf`.

Pour faire un lien vers un document PDF sur le web (pour lequel vous disposez du fichier `.aux`), il faudra modifier votre saisie. En voici un exemple :

```latex
% !TEX noedit
\usepackage{xr-hyper}
\usepackage{hyperref}
\externaldocument[V1-]{volume1}[http://livres.net/volume1.pdf]
...
... \nameref{V1-introduction}...
```

## Avec l'extension « zref-xr »

L'ensemble expérimental <ctanpkg:zref> de Heiko Oberdiek comprend un mécanisme d'hyper-références croisées utilisant son extension [zref-xr](ctanpkg:zref). L'utilisation est étroitement calquée sur celle de <ctanpkg:xr> et de <ctanpkg:xr-hyper>. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{zref-xr,zref-user}
\zexternaldocument*{xr02}

\begin{document}
Pour plus de détails, voir \zref{foo}.
\end{document}
```

L'extension fournit toutes les fonctionnalités des anciennes extensions et peut traiter à la fois les étiquettes LaTeX « traditionnelles » et son propre style d'étiquettes.

______________________________________________________________________

*Source :* [Referring to labels in other documents](faquk:FAQ-extref)

```{eval-rst}
.. meta::
   :keywords: LaTeX,renvois,labels,références
```
