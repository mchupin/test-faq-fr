# Comment faire référence plusieurs fois à la même note de bas de page ?

## Avec l'extension « fixfoot »

L'extension <ctanpkg:fixfoot> permet de définir une note en bas de page, sous forme de commande utilisable plusieurs fois. Par exemple :

```latex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[total={7cm,3cm}]{geometry}

\usepackage{fixfoot}
\DeclareFixedFootnote{\blah}{Notez c'est important !}

\begin{document}
Donald Knuth\blah{} a déclaré qu'il ne développe
plus \TeX ; il ne traite désormais plus que la correction des erreurs\blah{} qui
lui sont remontées\blah{}.
\end{document}
```

## Avec la décomposition de la commande \\footnote

La commande `\footnote` peut être *décomposée* en deux :

- la première, `\footnotemark[⟨numéro⟩]`, place la marque de renvoi à la note de bas de page ;
- la seconde, `\footnotetext[⟨numéro⟩]{⟨texte⟩}`, crée la note elle-même.

Ainsi, la solution intuitive serait la suivante :

```latex
% !TEX noedit
Ici, du bla-bla\footnotemark[1] avec un renvoi vers
la première note\footnotemark[2]. Si je veux à
nouveau faire référence à la première
note\footnotemark[1], c'est pas compliqué.
\footnotetext[1]{À propos de bla-bla.}
\footnotetext[2]{Au sujet d'autre chose...}
```

Cependant, cette solution a le désagréable défaut d'avoir le numéro « en dur » dans le code. Lors de la création d'une nouvelle note de bas de page avant celle qui a été traitée, il va falloir changer le numéro de celle qui a été traitée à la main. C'est totalement contraire à l'esprit de LaTeX.

Une solution plus propre consiste à faire une référence croisée entre les notes de bas de page. L'exemple précédent deviendra donc :

```latex
% !TEX noedit
Ici, du bla-bla\footnote{À propos de bla-bla.
\label{footnote}} avec un renvoi vers la première
note\footnote{Au sujet d'autre chose...}. Si je
veux à nouveau faire référence à la
première note\footnotemark[\ref{footnote}], c'est
pas compliqué.
```

Cependant, cette solution ne fonctionne pas à la première compilation, car la commande `\footnotemark` ne reçoit pas comme argument optionnel un numéro. Pour éviter ce problème, on pourra utiliser la commande suivante :

```latex
% !TEX noedit
\makeatletter
\def\myref#1{%
  \expandafter\ifx\csname r@#1\endcsname\relax
    0\@latex@warning{Reference `#1' on page
              \thepage \space undefined}%
  \else
    \ref{#1}%
  \fi}
\makeatother

Ici, du bla-bla\footnote{À propos de bla-bla.
\label{footnote}} avec un renvoi vers la première
note\footnote{Au sujet d'autre chose...}. Si je
veux à nouveau faire référence à la
première note\footnotemark[\myref{footnote}],
c'est pas compliqué.
```

La commande `\myref` affiche la référence si elle existe, sinon elle écrit \$0\$ tout en avertissant l'utilisateur.

```{eval-rst}
.. meta::
   :keywords: LaTeX,notes de bas de page,footnote
```
