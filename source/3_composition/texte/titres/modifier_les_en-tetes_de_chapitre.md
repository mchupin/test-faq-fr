# Comment modifier le style des titres de chapitre ?

Cette question complète les éléments plus généraux (concernant aussi les chapitres) présentés dans la question «[Comment modifier le style des titres de sectionnement ?](/3_composition/texte/titres/modifier_le_style_des_titres) ».

## Avec l'extension « fncychap »

L'extension <ctanpkg:fncychap> fournit une intéressante collection de styles de chapitre prédéfinis, portant tous un nom : `Sonny`, `Glenn`, `Conny`, `Rejne` et `Bjarne`. Voici un exemple d'utilisation :

```latex
\documentclass{report}
  \usepackage[width=6cm,height=8cm]{geometry}
  \usepackage[Lenny]{fncychap}

\begin{document}
\chapter{Introduction}
De tout temps, les hommes\dots
\end{document}
```

## Avec l'extension « anonchap »

L'extension <ctanpkg:anonchap> fournit un moyen simple de présenter des chapitres au même format que des sections (c'est-à-dire sans le mot « Chapitre »).

## Avec l'extension « tocbibind »

L'extension <ctanpkg:tocbibind> fournit les mêmes commandes que l'extension <ctanpkg:anonchap> mais pour répondre à l'origine à d'autres besoins.

## Avec des modifications manuelles

Pour les chapitres, il faut modifier la commande `\@makechapterhead`, ou `\@makeschapterhead` selon que vous vous intéressez à `\chapter` ou à `\chapter*`. Voici un exemple de ce type de modification.

```latex
% !TEX noedit
\makeatletter
\def\@makechapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright \normalfont
    \interlinepenalty\@M
    \ifnum \c@secnumdepth >\m@ne
        \Huge\bfseries \thechapter\quad
    \fi
    \Huge \bfseries #1\par\nobreak
    \vskip 40\p@
  }}

\def\@makeschapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    \Huge \bfseries  #1\par\nobreak
    \vskip 40\p@
  }}
\makeatother
```

Dans ce cadre, Vincent Zoonekynd propose des exemples illustrés montrant comment obtenir différents [styles de chapitre](http://zoonek.free.fr/LaTeX/LaTeX_samples_chapter/0.html).

```{eval-rst}
.. meta::
   :keywords: LaTeX,chapitre,sectionnement,apparence des titres de chapitres,format des titres de chapitres,chapitre sans titre,retirer le mot chapitre
```
