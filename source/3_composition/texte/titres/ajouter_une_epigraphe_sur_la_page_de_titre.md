# Comment modifier la page de « partie » ?

Il est possible de modifier la commande `\part`. Voici ici un exemple pour que cette commande ccepte un argument de plus, permettant d'insérer quelque-chose entre le titre de la partie et la fin de la page.

```latex
% !TEX noedit
\makeatletter
\def\@part[#1]#2#3{%
   \ifnum \c@secnumdepth >-2\relax
     \refstepcounter {part}%
     \addcontentsline{toc}{part}
                     {\thepart \hspace {1em}#1}%
   \else
     \addcontentsline {toc}{part}{#1}%
   \fi
   \markboth {}{}{%
   \centering \vfill
   \interlinepenalty \@M \normalfont
   \ifnum \c@secnumdepth >-2\relax
     \Huge \bfseries \partname ~\thepart \par
     \vskip 20\p@
   \fi
   \Huge \bfseries #2
   \par}%
   {\centering
   \vfill #3 \vfill}%
   \@endpart}
\makeatother
```

Le titre sera alors au tiers de la page, et le deuxième argument aux deux-tiers. Cette nouvelle commande s'utilise alors ainsi :

```latex
% !TEX noedit
\part[titre court]{un titre long pour la page}{ce qu'on veut inclure, par exemple un résumé, une image\dots}
```

:::{warning}
Cette nouvelle définition de `\part` introduit des incompatibilités avec l'extension <ctanpkg:minitoc>. Pour remédier à cela, il est préférable d'utiliser l'extension <ctanpkg:epigraph>.
:::

```{eval-rst}
.. meta::
   :keywords: LaTeX,partie,page,redéfinition
```
