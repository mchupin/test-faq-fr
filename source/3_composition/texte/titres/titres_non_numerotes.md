# Comment gérer des chapitres de préface, d'introduction ou de conclusion non numérotés ?

## Avec des commandes de base

Chaque commande de sectionnement possède une version *étoilée*, telle `\chapter*{}`, qui indique à LaTeX de ne pas numéroter le titre en question et de ne pas l'inclure dans la table des matières ni dans les en-têtes des pages. Par exemple, dans le cas ci-dessous, la deuxième sous-section et la deuxième section sont non numérotées par l'utilisation de leur version étoilée :

```latex
\documentclass{article}
\usepackage[frenchb]{babel}
\pagestyle{empty}
\begin{document}
\section{Ga}
\subsection{Bu}
\subsection*{Zo}
\section*{Meu}
\end{document}
```

Les limitations de ces commandes peuvent être contournées :

- la commande `\addcontentsline` permet d'ajouter cette entrée dans la table des matières mais avec les limites présentées à la question « [Comment ajouter une entrée dans la table des matières ?](/3_composition/annexes/tables/ajouter_une_entree_a_une_table_des_matieres) » ;
- la commande `\markboth` permet d'ajouter des en-têtes comme le précise la question « [Comment définir les hauts et bas de page ?](/3_composition/texte/pages/entetes/composer_des_en-tetes_et_pieds_de_page) ».

## Avec la classe « book »

La classe <ctanpkg:book> permet de faire cela à l'aide des commandes `\frontmatter`, `\mainmatter` et `\backmatter` :

- dans la zone *frontmatter*, généralement utilisée pour le début du document, les chapitres ne sont pas numérotés et les numéros de page sont écrits en chiffres romains, en minuscules. La numérotation des pages recommence à un à l'appel de `\mainmatter` ;
- dans la zone *mainmatter*, les pages sont numérotées en chiffres arabes, les chapitres sont numérotés ;
- dans la zone *backmatter*, les chapitres ne sont plus numérotés, les pages sont numérotées dans la continuité de la zone *mainmatter*.

Voici un exemple rudimentaire d'utilisation, que vous pouvez tester :

```latex
% !TEX noedit
\documentclass[10pt]{book}
\usepackage[french]{babel}

\begin{document}
\frontmatter
\chapter{Prologue}
\mainmatter
\chapter{Logue}
\backmatter
\chapter{Et puis logue !}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,textes,titres,titres de sectionnement,titres non numérotés
```
