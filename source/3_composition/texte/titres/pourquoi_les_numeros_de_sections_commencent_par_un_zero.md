# Pourquoi mes sections sont numérotées « 0.1 » et ainsi de suite ?

Une numérotation commençant par un zéro est générée lorsque :

- votre document utilise une classe standard <ctanpkg:book> ou <ctanpkg:report> (ou une classe similaire) ;
- et que vous placez une commande `\section` avant votre première commande `\chapter`.

En effet, ces classes numérotent les sections sous la forme « ⟨n° du chapitre⟩.⟨n° de section⟩ ». Or, jusqu'à ce que le premier `\chapter` apparaisse, le numéro de chapitre est 0, d'où l'affichage. D'ailleurs, si vous utilisez la commande `\chapter*` qui ne numérote pas le chapitre qu'elle produit, le problème persiste.

Pour éviter cette présentation, plusieurs solutions existent :

- utilisez la classe <ctanpkg:article> si vous n'avez pas besoin de la notion de chapitre ;
- mettez un `\chapter` avant vos sections ;
- supprimez la numérotation des sections en utilisant à la place `\section*`. Une autre solution traitant de la numérotation est présentée dans la question « [Comment placer des titres non numérotés en table des matières ?](/3_composition/annexes/tables/chapitres_non_numerotes_dans_la_table_des_matieres5) ».

______________________________________________________________________

*Source :* [Why are my sections numbered 0.1... ?](faquk:FAQ-zerochap)

```{eval-rst}
.. meta::
   :keywords: LaTeX,titres,sectionnement,sections,numéroter
```
