# Comment gérer des abréviations ?

La notion d'abréviation recouvre plusieurs cas, décrits ci-après.

## Des abréviations dans le code

### Avec l'extension xspace

LaTeX permet simplement de définir des commandes qui peuvent raccourcir la saisie au clavier de textes courts et sécuriser aussi cette saisie en ayant une fois pour toute une version bien écrite à un endroit du document. Prenons l'exemple de l'expression « [Inégalité de Bienaymé-Tchebychev](wpfr:In%C3%A9galit%C3%A9_de_Bienaym%C3%A9-Tchebychev) ». Voici un code qui, intuitivement, devrait répondre au besoin.

```latex
% !TEX noedit
\documentclass{report}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev}
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

```latex
\documentclass{report}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev}
\pagestyle{empty}
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

L'exemple ci-dessus montre que notre commande gère mal l'espacement après elle (du moins, elle suit la méthode usuelle de LaTeX). Une solution pourrait alors consister à insérer une espace dans la commande. Ce qui donne le résultat suivant :

```latex
% !TEX noedit
\documentclass{report}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev }
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

```latex
\documentclass{report}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev }
\pagestyle{empty}
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

Là, c'est l'espace avant le point qui est gênante. De fait, pour traiter ce point, il existe ici deux possibilités :

- ajouter « `{}` » immédiatement après le nom de la commande ;
- utiliser l'extension <ctanpkg:xspace>. Cet extension crée une commande `\xspace` qui gère l'espacement après votre commande.

```latex
% !TEX noedit
\documentclass{report}
\usepackage{xspace}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev}
\newcommand{\InBTc}{inégalité de Bienaymé-Tchebychev\xspace}
\begin{document}
Parlons donc de l'\InBT{}. L'\InBT{} est une inégalité de concentration...

Parlons donc de l'\InBTc. L'\InBTc est une inégalité de concentration...
\end{document}
```

```latex
\documentclass{report}
\usepackage{xspace}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev}
\newcommand{\InBTc}{inégalité de Bienaymé-Tchebychev\xspace}
\pagestyle{empty}
\begin{document}
Parlons donc de l'\InBT{}. L'\InBT{} est une inégalité de concentration...

Parlons donc de l'\InBTc. L'\InBTc est une inégalité de concentration...
\end{document}
```

## Des abréviations dans le texte

### Avec l'extension abbrevs

L'extension <ctanpkg:abbrevs> permet d'avoir une mécanique d'abréviation dans le code mais aussi dans le texte :

- elle gère l'espacement qui doit être placé après la commande ;
- et elle permet également de définir des abréviations dont le texte s'adapte : elles sont développées d'une certaine façon la première fois qu'elles sont utilisées, et d'une autre façon ensuite.

Voici un exemple d'utilisation :

```latex
% !TEX noedit
\documentclass{report}
\usepackage{abbrevs}
\newabbrev\InBT{Inégalité de Bienaymé-Tchebychev (IBT)}[IBT]
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

```latex
\documentclass{report}
\usepackage{abbrevs}
\pagestyle{empty}
\newabbrev\InBT{Inégalité de Bienaymé-Tchebychev (IBT)}[IBT]
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

Ici (et ce n'est pas systématique), nous nous retrouvons avec le problème vu précédemment : un espacement mal géré après la commande ! Pour ce cas, l'extension <ctanpkg:xspace> n'est d'aucune aide. Il faut modifier le code de l'extension <ctanpkg:abbrevs> en rédéfinissant une mécanique interne (voir pour plus détail les sources en bas de cette page).

```latex
% !TEX noedit
\documentclass{report}
\usepackage{abbrevs}
\newabbrev\InBT{Inégalité de Bienaymé-Tchebychev (IBT)}[IBT]
\makeatletter
\renewcommand\maybe@space@{%
  % \@tempswatrue % <= le code d'origine
  \maybe@ictrue   % <= la correction apportée
  \expandafter   \@tfor
    \expandafter \reserved@a
    \expandafter :%
    \expandafter =%
                 \nospacelist
                 \do \t@st@ic
  % \if@tempswa % <= le code d'origine
  \ifmaybe@ic   % <= la correction apportée
    \space
  \fi
}
\makeatother
\begin{document}
Parlons donc de l'\InBT. L'\InBT est une inégalité de concentration...
\end{document}
```

```latex
\documentclass{report}
% Le correctif ne compile pas bien (peut-être une question de mise à jour). On remet un code qui restitue une sortie propre.
\usepackage{xspace}
\newcommand{\InBT}{inégalité de Bienaymé-Tchebychev\xspace}
\pagestyle{empty}
\begin{document}
Parlons donc de l'\InBT (IBT). L'IBT est une inégalité de concentration...
\end{document}
```

### Des acronymes, avec des listes d'acronymes

L'extension <ctanpkg:acronym> permet de placer une liste des acronymes utilisés dans le document, et de s'assurer que tous sont écrits au moins une fois sous forme développée.

### Des abréviations avec une mise en exposant

La commande `\textsuperscript` permet d'obtenir un exposant sans passer en mode mathématique. Il est également possible de définir une nouvelle commande `\abrev` qui compose son argument en texte petit et surélevé. Les deux méthodes sont illustrées dans l'exemple ci-dessous.

```latex
% !TEX noedit
\documentclass{report}
\newcommand{\abrev}[1]{\raisebox{1ex}{\footnotesize #1}}

\begin{document}
Voici un premier \abrev{test}, ainsi qu'un second \textsuperscript{test}.
\end{document}
```

```latex
\documentclass[french]{article}
\pagestyle{empty}
\newcommand{\abrev}[1]{\raisebox{1ex}{\footnotesize #1}}

\begin{document}
Voici un premier \abrev{test}, ainsi qu'un second \textsuperscript{test}.
\end{document}
```

______________________________________________________________________

*Source :*

- <https://tex.stackexchange.com/questions/59840/how-to-prevent-getting-a-space-after-an-abbreviation-using-the-abbrevs-package>

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
