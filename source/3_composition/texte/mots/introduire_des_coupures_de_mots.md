# Comment définir certaines césures ?

## Localement

Pour permettre la coupure d'une chaîne de caractères, il faut utiliser la commande `\-` à l'endroit souhaité. Dans ce cas, le mot ne pourra être coupé qu'aux endroits où un `\-` a été inséré.

```latex
% !TEX noedit
Le mot suivant pourra seulement être découpé aux endroits fixés : anticonstitutionnellement.
Du moins pas, ici, car on laisse \LaTeX{} faire.

Le mot suivant pourra seulement être découpé aux endroits fixés : anticonsti\-tutionnelle\-ment.
Étonnant, non ?
```

```latex
Le mot suivant pourra seulement être découpé aux endroits fixés : anticonstitutionnellement.
Du moins pas, ici, car on laisse \LaTeX{} faire.

Le mot suivant pourra seulement être découpé aux endroits fixés : anticonsti\-tutionnelle\-ment.
Étonnant, non ?
```

## Globalement

Pour indiquer à LaTeX comment couper certains mots, on peut utiliser la commande `\hyphenation` dans laquelle on écrit le mot avec un « `-` » pour indiquer chacune des césures possibles. Elle est normalement placée dans le préambule du document.

```latex
% !TEX noedit
\hyphenation{anti-constitution-nelle-ment}
(...)
Le mot suivant pourra seulement être découpé à l'endroit fixé : anticonstitutionnellement.
Mais cela reste à nos risques et périls car nous pouvons obtenir des lignes qui débordent
dans les marges (ici, du fait d'un nombre de césures que nous avons trop limité).
```

```latex
\hyphenation{anti-constitution-nelle-ment}
Le mot suivant pourra seulement être découpé à l'endroit fixé : anticonstitutionnellement.
Mais cela reste à nos risques et périls car nous pouvons obtenir des lignes qui débordent
dans les marges (ici, du fait d'un nombre de césures que nous avons trop limité).
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,typographie
```
