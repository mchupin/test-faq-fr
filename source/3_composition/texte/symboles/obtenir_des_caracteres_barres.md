# Comment barrer ou biffer du texte ?

Barrer du texte (*to overstrike*, en anglais) peut être utilisé, par exemple, pour indiquer du texte supprimé au cours de la révision d'un document.

## Pour du texte courant

### Avec l'extension « ulem »

L'extension <ctanpkg:ulem> permet de barrer du texte avec la commande `\sout`. Plus largement, elle fournit par ailleurs des commandes permettant d'obtenir différents types de soulignement.

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{ulem}

\begin{document}
Votre comportement \sout{parfaitement inconvenant} a surpris !
\end{document}
```

```latex
\documentclass{article}
  \usepackage{ulem}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Votre comportement \sout{parfaitement inconvenant} a surpris !
\end{document}
```

### Avec l'extension « soul »

L'extension <ctanpkg:soul> permet de barrer du texte avec la commande `\st`. Comme l'extension <ctanpkg:ulem>, elle propose des commandes de soulignement mais elle s'en distingue en proposant aussi des commandes pour gérer l'espacement entre les lettres (sur ce dernier point, voir la question « [Comment modifier l'espacement entre caractères ? »](/3_composition/texte/mots/modifier_l_espacement_entre_les_caracteres)

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{soul}

\begin{document}
Votre comportement \st{parfaitement inconvenant} a surpris !
\end{document}
```

```latex
\documentclass{article}
  \usepackage{soul}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Votre comportement \st{parfaitement inconvenant} a surpris !
\end{document}
```

Si l'extension <ctanpkg:xcolor> est chargée, la macro `\setstcolor` permet de changer la couleur du trait. La [documentation de l'extension](texdoc:soul) fournit de nombreux exemples très clairs.

```latex
% !TEX noedit
\setstcolor{red}
Votre comportement \st{parfaitement inconvenant} a surpris !
```

```latex
\documentclass{article}
  \usepackage{xcolor}
  \usepackage{soul}
  \pagestyle{empty}

\begin{document}
\setstcolor{red}
Votre comportement \st{parfaitement inconvenant} a surpris !
\end{document}
```

### Avec l'extension « cancel »

L'extension <ctanpkg:cancel> permet de biffer des mots en oblique, avec la macro `\cancel` :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{cancel}

\begin{document}
Votre comportement \cancel{parfaitement inconvenant} a surpris !
\end{document}
```

```latex
\documentclass{article}
  \usepackage{cancel}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Votre comportement \cancel{parfaitement inconvenant} a surpris !
\end{document}
```

Cette extension propose d'autres possibilités pour barrer en sens inverse (avec `\bcancel` comme **b***ack*) ou en croix (avec `\xcancel`) :

| `\cancel`                                                                                                                                                           | `\bcancel`                                                                                                                                                           | `\xcancel`                                                                                                                                                           |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| :raw-latex:\` documentclass\{article} usepackage\{cancel} usepackage\{lmodern} pagestyle\{empty}begin\{document} cancel{parfaitement inconvenant} end\{document} \` | :raw-latex:\` documentclass\{article} usepackage\{cancel} usepackage\{lmodern} pagestyle\{empty}begin\{document} bcancel{parfaitement inconvenant} end\{document} \` | :raw-latex:\` documentclass\{article} usepackage\{cancel} usepackage\{lmodern} pagestyle\{empty}begin\{document} xcancel{parfaitement inconvenant} end\{document} \` |

### Avec l'extension « pdfcomment »

L'extension <ctanpkg:pdfcomment> permet de barrer un texte et d'associer un commentaire : vous pouvez ici donner différents arguments optionnels pour affiner cette opération. Le texte du commentaire est incorporé à l'aide d'une « annotation » Adobe. Malheureusement, la prise en charge de ces annotations n'est pas une caractéristique usuelle des visionneuses PDF. Dans le cas où vos lecteurs utilisent « Acrobat Reader », vous devriez être tranquille. Cette fonction fournit ce « surlignement » supplémentaire fréquemment retenu par les utilisateurs professionnels.

## Pour des mathématiques

### Avec l'extension « amssymb », pour un symbole

Pour barrer un symbole mathématique, on pourra utiliser la commande `\not`, pour marquer la négation. Il existe de nombreux caractères barrés définis dans le package <ctanpkg:amssymb>, comme par exemple `\notin`, `\nleq` ou `\lneq`.

```latex
% !TEX noedit
Il faut noter la différence entre \(a \not\in A\) et \(a \notin A\).

Pour dire <<\,strictement supérieur\,>> en insistant sur le \emph{strictement},
on peut utiliser le symbole \(\gneq\).
```

```latex
\documentclass{article}
  \usepackage{amssymb}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Il faut noter la différence entre
\(a \not\in A\) et \(a \notin A\).

Pour dire <<\,strictement supérieur\,>> en insistant sur le \emph{strictement},
on peut utiliser le symbole \(\gneq\).
\end{document}
```

### Avec d'autres extensions, pour des expressions

Une solution plus générale, permettant de biffer des expressions entières, est indiquée en réponse à la question « [Comment biffer des termes dans des expressions mathématiques ?](/4_domaines_specialises/mathematiques/barrer_des_termes) ».

______________________________________________________________________

*Source :* [Overstriking characters](faquk:FAQ-overstrike)

```{eval-rst}
.. meta::
   :keywords: LaTeX,barrer des caractères,barrer un mot,rayer du texte,supprimer du texte,overstrike,rature en LaTeX
```
