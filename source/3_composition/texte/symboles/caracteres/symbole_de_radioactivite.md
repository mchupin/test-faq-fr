# Comment obtenir le symbole « radioactivité » ?

## Avec l'extension marvosym

L'extension <ctanpkg:marvosym> définit la commande `\Radioactivity` :

```latex
\documentclass{article}
\usepackage{marvosym}
\pagestyle{empty}
\begin{document}
\LARGE\Radioactivity
\end{document}
```

## Avec l'extension ifsym

L'extension <ctanpkg:ifsym>, chargé avec l'option `misc`, donne un symbole avec la commande `\Radiation` :

```latex
% !TEX noedit
\documentclass{article}
\usepackage[misc]{ifsym}

\begin{document}
\LARGE\Radiation
\end{document}
```

:::{todo} // Restitution LaTeX à obtenir.//
:::

La [documentation de ifsym](texdoc:ifsym) est malheureusement uniquement disponible en allemand.

```{eval-rst}
.. meta::
   :keywords: LaTeX,radioactivité,symbole de radioactivité,symboles de danger
```
