Comment obtenir les symboles « mâle » et « femelle » ? ==================================================

# Avec l'extension « marvosym »

L'extension <ctanpkg:marvosym> fournit les commandes `\Female`, `\Male` et quelques autres sur la même thématique. En voici les différentes possibilités :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{marvosym}

\begin{document}
Voici de la variété : \Female (ou \FEMALE), \Male (ou \MALE), \Hermaphrodite
(ou \HERMAPHRODITE), \Neutral, \FemaleFemale, \FemaleMale ou bien encore \MaleMale.
\end{document}
```

```latex
\documentclass{article}
\usepackage{marvosym}
\pagestyle{empty}
\begin{document}
Voici de la variété : \Male (ou \MALE), \Female (ou \FEMALE), \Hermaphrodite
(ou \HERMAPHRODITE), \Neutral, \FemaleFemale, \FemaleMale ou bien encore \MaleMale.
\end{document}
```

# Avec l'extension « fontawesome »

L'extension <ctanpkg:fontawesome> fournit tout autant de variété avec `\faVenus`, `\faMars` et quelques autres sur la même thématique. En voici les différentes possibilités :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{fontawesome}

\begin{document}
Voici de la variété : \faVenus, \faMars, \faTransgender, \faNeuter, \faGenderless,
\faMarsStroke, \faMarsStrokeH, \faMarsStrokeV, \faMarsDouble, \faVenusMars,
\faVenusDouble\ ou bien \faTransgenderAlt.
\end{document}
```

```latex
\documentclass{article}
\usepackage{fontawesome}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Voici de la variété : \faVenus, \faMars, \faTransgender, \faNeuter, \faGenderless,
\faMarsStroke, \faMarsStrokeH, \faMarsStrokeV, \faMarsDouble, \faVenusDouble,
\faVenusMars\ ou bien \faTransgenderAlt.
\end{document}
```

# Avec l'extension « stix »

L'extension <ctanpkg:stix> présente quatre symboles mathématiques sur ce thème dont `\female` et `\male`. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{stix}

\begin{document}
Voici un peu moins de variété : $\female$, $\male$, $\Hermaphrodite$ et $\neuter$.
\end{document}
```

```latex
\documentclass{article}
\usepackage{stix}
\pagestyle{empty}
\begin{document}
Voici un peu moins de variété : $\female$, $\male$, $\Hermaphrodite$ et $\neuter$.
\end{document}
```

# Avec l'extension « wasysym »

L'extension <ctanpkg:wasysym> donne deux symboles : `\female` et `\male`. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{wasysym}

\begin{document}
Voici encore moins de variété : \female{} et \male.
\end{document}
```

```latex
\documentclass{article}
\usepackage{wasysym}
\pagestyle{empty}
\begin{document}
Voici encore moins de variété : \female{} et \male.
\end{document}
```

# Avec l'extension « mathabx »

L'extension <ctanpkg:mathabx> se limite également à deux symboles à mettre en mode mathématique : `\girl` et `\boy`. En voici un exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{mathabx}

\begin{document}
Voici encore moins de variété : $\girl$ et $\boy$.
\end{document}
```

```latex
\documentclass{article}
\usepackage{mathabx}
\pagestyle{empty}
\begin{document}
Voici encore moins de variété : $\girl$ et $\boy$.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,symboles,mâle,femelle,symboles de genres
```
