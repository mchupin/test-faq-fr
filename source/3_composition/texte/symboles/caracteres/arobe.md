# Comment obtenir une arobase ?

L'[arobase](wpfr:arobase), arrobase, arobe, arrobe ou arrobas sont les noms français du symbole@''. On parle aussi de « A commercial » ou, par anglicisme, de symbole « *at* ». Voir, à ce sujet, la note 2 de l’article [Lexique anglo-français du Companion](http://cahiers.gutenberg.eu.org/cg-bin/fitem?id=CG_2007___49_19_0) dans les *Cahiers GUTenberg*, n° 49.

Elle est traitée de manière particulière par LaTeX :

- dans un document, elle est normalement accessible en tapant tout simplement «@'' ». Si, cas rare, cela ne marche pas, il est possible de définir une commande pour obtenir ce caractère, comme dans l'exemple ci-dessous la commande `\at` ;
- dans les noms de commande, elle a un rôle spécifique demandant un peu de prudence. Ce rôle est précisé dans les questions « [Comment bien nommer ses commandes et environnements ?](/2_programmation/macros/bien_nommer_ses_macros_et_environnements) » et « [À quoi servent « \\makeatletter » et « \\makeatother » ?](/2_programmation/macros/makeatletter_et_makeatother) ».

```latex
\documentclass{article}
  \usepackage{lmodern}
  \pagestyle{empty}

\newcommand{\at}{\string@}

\begin{document}
Vos adresses sont jean@domaine.fr
et j.jean\at domaine.fr.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,arobase,arobas,A commercial,A rond,symbole at,caractère at,symbole de mail
```
