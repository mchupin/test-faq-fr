# Comment obtenir les différents tirets ?

La typographie distingue plusieurs types de [tiret](wpfr:Tiret). Ils sont listés par la suite avec certains de leurs usages (parfois sujets à discussion).

## Le tiret long

Le tiret long ou tiret cadratin (du fait de sa longueur d'un [cadratin](wpfr:Cadratin)), « --- », est utilisé pour les dialogues ou pour faire des incises (bien que ces usages soient désormais moins fréquents). Ce tiret est celui qui est utilisé comme symbole d'énumération par défaut lorsque l'extension <ctanpkg:babel> est utilisée avec son option `french`.

Il s'obtient avec le code `-``-``-` ou la commande `\textemdash`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\begin{document}
Posément, le typographe :
\begin{itemize}
\item parcourut rapidement la nouvelle tentative de ses élèves ;
\item puis pris une grande respiration.
\end{itemize}

Et, là --- sans sourciller --- il hurla :

\textemdash{} Confondre encore les tirets ? Après tout ce temps ?
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Posément, le typographe :
\begin{itemize}
\item parcourut rapidement la nouvelle tentative de ses élèves ;
\item puis pris une grande respiration.
\end{itemize}

Et, là --- sans sourciller --- il hurla :

\textemdash{} Confondre encore les tirets ? Après tout ce temps ?
\end{document}
```

## Le tiret moyen

Le tiret moyen ou tiret demi-cadratin, « -- » sert :

- à indiquer un intervalle ;
- à énumérer ;
- à traiter les cas historiquement liés au tiret cadratin.

Il s'obtient avec le code `-``-` ou la commande `\textendash`.

L'exemple ci-dessous montre comment modifier localement le symbole de l'énumération pour obtenir ce tiret (au lieu du tiret cadratin proposé par l'extension <ctanpkg:babel> avec son option `french`).

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}

\begin{document}
Dans cet ouvrage -- sans doute un peu désuet --, vous verrez au niveau des pages 30--34 qu'il faut :
\begin{itemize}
\item[--] respecter les tirets ;
\item[--] et ne pas les confondre.
\end{itemize}
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
Dans cet ouvrage -- sans doute un peu désuet --, vous verrez au niveau des pages 30--34 qu'il faut :
\begin{itemize}
\item[--] respecter les tirets ;
\item[--] et ne pas les confondre.
\end{itemize}
\end{document}
```

## Le signe moins

Le signe moins, « \$-\$ », est distinct du tiret moyen (en étant plus court) et du tiret court (en étant plus long) : ce dernier sert pourtant parfois à le remplacer mais il s'agit d'une tolérance.

Il s'obtient avec le code `$`-`$` ;

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Voici donc la différence entre --, $-$ et -. Ainsi que + et $-$ placés l'un à côté de l'autre.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Voici donc la différence entre --, $-$ et -. Ainsi que + et $-$ placés l'un à côté de l'autre.
\end{document}
```

## Le tiret court

Le tiret court ou trait d'union, « - » sert à l'écriture des mots composés, à la césure et parfois à remplacer le signe moins.

Il est codé simplement par `-`.

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Malheureusement, le tiret court est devenu une sorte de passe-partout typographique du fait de sa disponibilité sur le clavier.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Malheureusement, le tiret court est devenu une sorte de passe-partout typographique du fait de sa disponibilité sur le clavier.
\end{document}
```

## Le tiret bas

Le [tiret bas](wpfr:Tiret%20bas) (aussi nommé souligné, trait de soulignement, « tiret du 8 » ou, en anglais, *underscore*) est habituellement utilisé dans LaTeX pour procéder à une mise en indice en mode mathématique. Dès lors, si vous tapez « _ » seul, au cours d'un texte ordinaire, LaTeX s'en plaindra.

Deux commandes permettent d'obtenir ce symbole :

- `\textunderscore` où il convient de faire attention à l'espace qui le suit ;
- `\_` qui ne pose pas de souci pour l'espacement.

En voici une illustration :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Ceci est un exemple \textunderscore très\_ pertinent mais pas \_très\textunderscore{} lisible.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Ceci est un exemple \textunderscore très\_ pertinent mais pas \_très\textunderscore{} lisible.
\end{document}
```

### Avec l'extension « underscore »

Si vous écrivez un document qui contiendra un grand nombre de caractères de soulignement, la perspective de taper `\_` à chaque fois pourra décourager.

Des programmeurs débrouillards peuvent facilement mettre en place une bidouille pour que taper « `_` » signifie pour LaTeX « soulignement de texte » (voir sur ce point « [Comment changer un caractère en commande ?](/2_programmation/macros/definir_un_caracteres_comme_une_macro) » qui utilise cet exemple comme illustration). Cependant, ce type de code reste délicat et peut être leurré. De ce fait, l'extension <ctanpkg:underscore> apporte ici une solution plus efficace et générale.

Cependant, il y a un problème : les polices de texte OT1 ne contiennent pas de tiret bas, à moins qu'elles ne soient dans la version « machine à écrire » de l'encodage (utilisée par les polices à largeur fixe telles que *cmtt*). À la place de ce caractère, LaTeX (en encodage OT1) utilise un trait court pour la commande `\textunderscore`, mais cela pose des problèmes pour les systèmes qui interprètent le PDF (comme les systèmes de lecture audio de PDF).

Aussi, vous devez vous assurer que le tiret bas apparait bien dans la police « machine à écrire », ou vous devez utiliser un encodage plus moderne, tel que T1, qui dispose du tiret bas dans chaque police. Voici un exemple passant par le choix de l'encodage T1 :

```latex
% !TEX noedit
\usepackage{lmodern}        % (1) sélection d'une fonte disponible en T1 (exemple)
\usepackage[T1]{fontenc}    % (2) specification de l'encodage
\usepackage{textcomp}       % (3) chargement des définitions de symboles
```

Ceci met à disposition une commande `\textunderscore` qui sélectionne le bon caractère. L'extension <ctanpkg:underscore>, mentionnée plus haut, utilisera cette commande.

______________________________________________________________________

*Source :* [How to typeset an underscore character](faquk:FAQ-underscore)

```{eval-rst}
.. meta::
   :keywords: LaTeX,signes de ponctuation,trait d'union,trait de soulignement,tiret du 6,tiret du 8,différents traits,tirets disponibles,longueur du tiret,tiret cadratin,tiret quadratin,tiret demi-cadratin,tiret demi-quadratin
```
