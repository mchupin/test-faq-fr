# Comment obtenir les symboles pour-cent, pour-mille... ?

Le pour-cent, « % », est un des [caractères réservés](/3_composition/texte/symboles/caracteres/caracteres_reserves) de LaTeX qui sert à introduire des commentaires. Aussi, pour l'obtenir dans un texte, il faut passer par une commande dédiée.

Sans aucune extension particulière, et même en TeX pur, ce caractère s'obtient avec `\%`.

LaTeX fournit deux autres commandes :

- `\textperthousand` pour le pour-mille (‰) ;
- `\textpertenthousand` pour le pour-dix-mille (‱).

Voici un exemple d'utilisation où il convient de noter la présence d'une espace insécable entre la valeur et le symbole du pour-cent.

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Avoir 0,02~\% ou 0,2~\textperthousand{} ou bien encore 2~\textpertenthousand{} de
chance, est-ce si différent ?
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Avoir 0,02~\% ou 0,2~\textperthousand{} ou bien encore 2~\textpertenthousand{} de chance, est-ce si différent ?
\end{document}
```

## Avec l'extension « wasysym »

L’extension <ctanpkg:wasysym> fournit `\permil`, le pour-mille.

Il faut faire attention au fait que ce symbole a un aspect très *computer modern* qui ne se marie pas forcément bien avec d'autres fontes.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{wasysym}

\begin{document}
Avoir 0,02~\% ou 0,2~\permil{} de chance, est-ce si différent ?
\end{document}
```

```latex
\documentclass{article}
\usepackage{wasysym}
\pagestyle{empty}
\begin{document}
Avoir 0,02~\% ou 0,2~\permil{} de chance, est-ce si différent ?
\end{document}
```

## Avec l'extension « siunitx »

L’extension <ctanpkg:siunitx> fournit `\percent`. Elle permet de gérer convenablement l'espace entre le nombre et le signe.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{siunitx}

\begin{document}
Avoir \SI{2}{\percent} de chance, est-ce si important ?
\end{document}
```

```latex
\documentclass{article}
\usepackage{siunitx}
\pagestyle{empty}
\begin{document}
Avoir \SI{2}{\percent} de chance, est-ce si important ?
\end{document}
```

## Avec l'extension « textcomp »

{octicon}`alert;1em;sd-text-warning` *L’extension* <ctanpkg:textcomp> *est classée comme* [obsolète](/1_generalites/histoire/liste_des_packages_obsoletes)*. Ce qui suit est informatif.*

Le symbole pour-mille est accessible par la commande `\textperthousand` du package <ctanpkg:textcomp>. De même que le symbole pour-dix-mille `\textpertenthousand`.

```{eval-rst}
.. meta::
   :keywords: LaTeX,signe pourcent,symbole pourcent,signe pour cent,symbole pour cent,symbole pour mille,fractions
```
