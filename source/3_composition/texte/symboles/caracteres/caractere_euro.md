# Comment obtenir le symbole de l'euro ?

Lorsque la monnaie européenne, l'euro (€), est apparue pour la première fois, la composition de son [symbole](wpfr:€) se révélait un problème sérieux pour les utilisateurs de TeX et de LaTeX. Les choses sont simplifiées depuis lors : la plupart des polices ont un moyen de fournir ce symbole.

Initialement, le symbole de l'euro avait une [représentation figée](wpfr:%E2%82%AC#/media/Fichier:Euro-Construction.svg) par la Commission européenne. Toutefois, cette rigidité a été décriée par les typographes et ils ont fini par adapter le symbole à leurs [polices de caractères](wp:Euro_sign#/media/File:Moreeurofonts.svg) comme sont adaptés les symboles du dollar, du yen ou de la livre sterling. De fait, l'utilisation du symbole original de l'euro ne vaut finalement que si vous travaillez sur des documents officiels de la Commission européenne ou si vous vous décidez à fabriquer de faux billets (la seconde option étant quelque peu déconseillée : la Commission serait sans doute bien plus contrariée).

Les versions les plus récentes de LaTeX proposent la saisie directe du symbole dans votre texte ou l'utilisation de la commande `\texteuro`. Notez bien que la saisie directe du symbole peut parfois poser difficulté.

Voici un exemple fonctionnel :

```latex
% !TEX noedit
\documentclass{article}
\begin{document}
Cet exemple ne vaut pas 10~€. Pas même 1~\texteuro{} d'ailleurs.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Cet exemple ne vaut pas 10~€. Pas même 1~\texteuro{} d'ailleurs.
\end{document}
```

Et voici un exemple dysfonctionnel :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{fourier}
\begin{document}
Cet exemple ne vaut pas 10~€. Pas même 1~\texteuro{} d'ailleurs.
\end{document}
```

```latex
\documentclass{article}
\usepackage{fourier}
\pagestyle{empty}
\begin{document}
Cet exemple ne vaut pas 10~€. Pas même 1~\texteuro{} d'ailleurs.
\end{document}
```

Aussi, cette page fournit un résumé des méthodes « au cas où ».

## Avec l'extension « marvosym »

L'extension <ctanpkg:marvosym> fournit, entre autres glyphes, plusieurs symboles de l'euro (dont l'officiel avec la commande `\EUR`). La police sous-jacente est disponible aux formats Adobe Type 1 et TrueType.

```latex
% !TEX noedit
\documentclass{article}
\usepackage{marvosym}
\begin{document}
Cet exemple ne vaut pas 10~\EUR. Pas même 1~\EURcr{} d'ailleurs.
\end{document}
```

```latex
\documentclass{article}
\usepackage{marvosym}
\pagestyle{empty}
\begin{document}
Cet exemple ne vaut pas 10~\EUR. Pas même 1~\EURcr{} d'ailleurs.
\end{document}
```

## Avec l'extension « eurosym »

L'extension <ctanpkg:eurosym>, utilisant MetaFont, fournit le symbole officiel de l'euro avec la commande `\euro`. Sa documentation indique également comment faire de ce symbole le symbole par défaut lorsque vous saisissez `€` dans le code source. Enfin, elle propose une commande `\EUR` pour afficher des montants avec une espace correcte entre le montant et le symbole (cette commande tient compte de la langue du document).

```latex
% !TEX noedit
\documentclass{article}
\usepackage[french]{babel}
\usepackage{eurosym}
\begin{document}
Cet exemple ne vaut pas 10~\euro. Pas même \EUR{1} d'ailleurs.
\end{document}
```

```latex
\documentclass{article}
\usepackage[french]{babel}
\usepackage{eurosym}
\pagestyle{empty}
\begin{document}
Cet exemple ne vaut pas 10~\euro. Pas même \EUR{1} d'ailleurs.
\end{document}
```

## Avec l'extension « eurofont »

L'extension <ctanpkg:eurofont> propose des commandes pour configurer la source des symboles à utiliser pour afficher l'euro. Elle est sans doute moins utile de nos jours.

## Avec l'ensemble de fontes « euro-ce »

L'ensemble de fontes <ctanpkg:euro-ce>, basé sur MetaFont, fournit différents symboles euro. Son fichier « euro-ce.tex » offre des conseils sur la manière dont un utilisateur de Plain TeX peut utiliser ces polices.

## Avec l'extension « textcomp »

{octicon}`alert;1em;sd-text-warning` *L’extension* <ctanpkg:textcomp> *est classée comme* [obsolète](/1_generalites/histoire/liste_des_packages_obsoletes)*. Ce qui suit est informatif.*

Les polices *Text Companion* (TC) encodées TS1 fournies dans le cadre de la distribution de polices EC fournissent des symboles euro. L'extension <ctanpkg:textcomp> fournit une commande `\texteuro` qui sélectionne le symbole correspondant au texte environnant. Ces symboles des polices TC ne sont pas universellement appréciés... Néanmoins, utilisez la version police TC du symbole si vous produisez des documents en utilisant les polices *Computer Modern* de Knuth.

## Avec l'extension « china2e »

De façon anecdotique, l'extension <ctanpkg:china2e> (dont le but principal est de mettre en forme des calendriers chinois) fournit aussi un symbole euro avec la commande `\Euro`...

______________________________________________________________________

*Source :* [Typesetting the Euro sign](faquk:FAQ-euro)

```{eval-rst}
.. meta::
   :keywords: LaTeX,euro,symbole de l'euro,euro arrondi,symboles monétaires
```
