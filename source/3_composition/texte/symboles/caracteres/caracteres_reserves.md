# Quels sont les caractères réservés ?

Comme tout langage de programmation, TeX (comme LaTeX) attribue des rôles particuliers à certains caractères dit alors « caractères réservés » ou « caractères spéciaux ». Il faut donc passer par des commandes dédiées pour pouvoir les afficher dans un texte. Le tableau ci-dessous rappelle à quoi servent ces caractères et quelles sont les commandes à utiliser pour les afficher.

| Caractère | Utilisation                     | Commande pour l'obtenir dans le texte                                                                                |
| --------- | ------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| `%`       | début de commentaire            | `\%`, voir la [page dédiée](/3_composition/texte/symboles/caracteres/pour-cent)                                |
| `_`       | (mode math) mise en indice      | `\_` *ou* `\textunderscore`, voir la [page dédiée](/3_composition/texte/symboles/caracteres/tiret)             |
| `^`       | (mode math) mise en exposant    | `\^{}` *ou* `\textasciicircum`                                                                                       |
| `$`       | passage en mode mathématique    | `\$`                                                                                                                 |
| `&`       | (tableau) séparateur de cellule | `\&`                                                                                                                 |
| `#`       | (macro) identifiant d’argument  | `\#`                                                                                                                 |
| `{`       | début de groupe                 | `\{`                                                                                                                 |
| `}`       | fin de groupe                   | `\}`                                                                                                                 |
| `~`       | espace insécable                | `\~{}` *ou* `\textasciitilde`, voir la [page dédiée](/3_composition/texte/symboles/caracteres/caractere_tilde) |
| `\`       | appel de commande               | `\textbackslash` *ou* `$\backslash$`, voir la [page dédiée](/3_composition/texte/symboles/caracteres/backslash)|

Ainsi, mis à part « `^` », « `~` » et « `\` », tous s'obtiennent en les faisant précéder d'une contre-oblique.

Voici donc un exemple illustrant le principe des caractères réservés avec ici le pour-cent :

```latex
% !TEX noedit
\documentclass{article}

\begin{document}
Voici ce qu'il ne faut pas faire pour afficher le pour-cent. %, la preuve !

Et ce qu'il faut faire pour l'afficher. \%, la preuve !

Plus largement, voici les caractères réservés de \TeX{} :
\%\_\^{}\$\&\#\{\}\textasciitilde\textbackslash.
\end{document}
```

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Voici ce qu'il ne faut pas faire pour afficher le pour-cent. %, la preuve !

Et ce qu'il faut faire pour l'afficher. \%, la preuve !

Plus largement, voici les caractères réservés de \TeX{} :
\%\_\^{}\$\&\#\{\}\textasciitilde\textbackslash.
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
