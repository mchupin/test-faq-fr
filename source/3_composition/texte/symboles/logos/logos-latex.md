# Comment obtenir les logos de TeX, LaTeX et compagnie ?

À tout seigneur... : la macro `\TeX` donne TeX.

On obtient le --- bien connu ? --- logo de LaTeX LaTeX avec la macro `\LaTeX`.

Si on veut être plus précis, `\LaTeXe` donne LaTeX.

## Extensions utiles

- <ctanpkg:metalogo> fournit les commandes `\XeTeX`, `\XeLaTeX`, `\luaTeX` et `\luaLaTeX` utilisables même depuis `latex` ou `pdflatex`. `metalogo` permet également de personnaliser, dans une certaine mesure, l’aspect de ces logos ;
- <ctanpkg:metalogox> reprend l'extension `metalogo` en ajustant automatiquement l'apparence du logo selon la police détectée ou l'option spécifiée.
- <ctanpkg:texlogos> fournit plusieurs logos comme celui de metapost ainsi que d’autres symboles ;
- <ctanpkg:hologo> fournit la macro `\hologo` qui permet d’écrire `\hologo{LaTeX}` pour avoir LaTeX et plus d’une cinquantaine de logos du monde TeX ;
- <ctanpkg:bxtexlogo> reprend l'extension `hologo` en utilisant une façon plus simple d'entrer les commandes (`\BibTeX` à la place de '' \\hologo\{BibTeX}''). Il fournit aussi des logos supplémentaires, pour des logiciels liés à TeX et populaires au Japon.
- <ctanpkg:mflogo> permet d’écrire les logos de MetaFont et MetaPost avec la police idoine créée par D. E. Knuth pour MetaFont.

## Considérations sur les logos de TeX et compagnie

Lorsqu'il a défini le logo de TeX (TeX), Knuth a conçu un exemple démonstratif des capacités du logiciel. Malheureusement (de l'avis de certains), il a aussi ouvert les vannes, poussant les auteurs à définir toute une série de logos d'assez mauvais goût pour des entités TeX telles que AMSTeX, PicTeX, BibTeX, etc., produits dans une multitude de polices, de tailles et de lignes de base différentes --- en fait, tout ce qu'on pourrait imaginer pour entraver le processus de lecture. En particulier, lorsque Leslie Lamport a inventé LaTeX, il a aussi défini le logo LaTeX (assez bête en soi, avec un petit "A" surélevé et un "E" abaissé) et l'apport marketing d'Addison-Wesley a conduit au logo actuel encore plus étrange pour LaTeX2e (LaTeX), qui ajoute la lettre grecque "ε" (epsilon) abaissée.

Les utilisateurs avisés ne sont pas obligés de suivre ce genre de choses partout. Les logos de MetaFont et MetaPost peuvent être définis dans des polices que LaTeX2e connaît (afin qu'ils s'adaptent au texte environnant) en utilisant l'extension <ctanpkg:mflogo> ; mais sachez que des pièges entourent l'utilisation de la police Knuthian pour MetaPost (vous pourriez obtenir quelque chose comme "META O T"). Il ne faut cependant pas désespérer --- la plupart des versions de la police du logo distribuées de nos jours contiennent les lettres manquantes, et l'auteur lui-même n'utilise que "MetaPost".

Un ensemble bien conçu de macros est fourni par l'extension <ctanpkg:hologo> (`ho` sont les intiales de son auteur, Heiko Oberdiek), qui définit une commande `\hologo`, que l'on utilise comme (par exemple) `\hologo{pdfLaTeX}` pour ce que vous pourriez obtenir en tapant `pdf\LaTeX`, ainsi qu'une version capitalisée `\Hologo{pdfLaTeX}` pour `Pdf\LaTeX`.

L'extension <ctanpkg:metalogo> traite du problème de ces myriades de logos, souvent ignoré de nos jours : la géométrie des caractères de différentes polices est (évidemment) différente, et ils s'assemblent naturellement différemment. Cette extension vous permet d'ajuster l'espacement entre les lettres de l'un de ces logos bizarres (même le "E" particulièrement bizarre en miroir dans [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex)).

Pour ceux qui ne se préoccupent pas d'utiliser les "bons" logos, la chose canonique à faire est d'écrire `AMS-\TeX{}` pour AMSTeX, `Pic\TeX{}` pour PicTeX, `Bib\TeX{}` pour BibTeX, et ainsi de suite.

______________________________________________________________________

*Source :* [Typesetting all those TeX-related logos](faquk:FAQ-logos)

```{eval-rst}
.. meta::
   :keywords: LaTeX,logo LaTeX,logo TeX,logo METAFONT,logo XeTeX,logo pdfTeX
```
