# Que faire lorsqu'une police ne supporte pas certaines diacritiques en petites majuscules ?

Malheureusement, il se peut que certaines fontes ne fournissent pas une version en petites majuscules pour tous les caractères qu'elle supporte en bas de casse et en majuscule. Dans ce cas, la version en bas de casse est utilisée :

```latex
\documentclass{article}
\usepackage{fontspec}
\setmainfont{Libertinus Serif}
\pagestyle{empty}

\begin{document}
AḤMAD B. FAŻLĀN B. AL-ʿABBĀS B. RAŠĪD

Aḥmad b. Fażlān b. al-ʿAbbās b. Rašīd

\textsc{Aḥmad b. Fażlān b. al-ʿAbbās b. Rašīd}
\end{document}
```
