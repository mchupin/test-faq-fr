# Comment avoir d'autres tailles de police de base ?

Les classes standard de LaTeX ont une notion de « taille de base » pour les caractères d'un document; cette taille est indiquée dans la ligne `\documentclass`, et toutes les autres tailles de police (de `\tiny` à `\Huge`) sont déterminées à partir d'elle :

**10pt**

```latex
% !TEX noedit
\documentclass[10pt]{article}

\begin{document}
Taille de base (10\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[10pt]{article}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Taille de base (10\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

**12pt**

```latex
% !TEX noedit
\documentclass[12pt]{article}

\begin{document}
Taille de base (12\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[12pt]{article}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Taille de base (12\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

Les classes sont conçues en supposant qu'elles ne seront pas utilisées avec des tailles autres que celles proposées par défaut par LaTeX (10, 11 ou 12pt), mais en pratique, on constate qu'on a régulièrement besoin d'autres tailles. La meilleure réponse à ce besoin serait de concevoir une nouvelle maquette pour le document (éventuellement une nouvelle classe), mais peu de gens ont envie de se lancer là-dedans.

D'autre part, si vous essayez d'utiliser une autre taille que 10, 11 ou 12pt avec les classes LaTeX de base, la compilation réussira, mais votre demande (ici `8pt`) n'aura aucun effet, le document sera mis en forme en taille 10pt (taille par défaut), et vous aurez un avertissement dans le `log` :

```text
LaTeX Warning : Unused global option(s) :
    [8pt].
```

Mais il existe plusieurs solutions :

## Avec les classes de l'extension « extsizes »

A simple solution is to use the <ctanpkg:extsizes> bundle. This bundle offers "extended" versions of the article, report, book and letter classes, at sizes of 8, 9, 14, 17 and 20pt as well as the standard 10--12pt. Since little has been done to these classes other than to adjust font sizes and things directly related to them, they may not be optimal --- but they are at least practical.

Une solution simple consiste à utiliser l'extension <ctanpkg:extsizes>, qui propose des versions « étendues » des classes de base :

- `extarticle` à la place de `article`,
- `extreport` à la place de `report`,
- `extbook` à la place de `book`,
- `extletter` à la place de `letter` et
- `extproc` à la place de `proc`

Ces classes étendues supportent des tailles de caractère de base de 8, 9, 14, 17 et 20 pt, en plus des tailles standard de 10, 11 et 12 pt. Comme ces classes n'ont guère été modifiées par rapport aux classes standard de LaTeX, si ce n'est pour ajuster la taille des polices et les éléments qui y sont directement liés, elles ne sont peut-être pas optimales, mais elles sont au moins pratiques car on peut les appeler en ne changeant que la ligne de `\documentclass` :

**8pt**

```latex
% !TEX noedit
\documentclass[8pt]{extarticle}

\begin{document}
Taille de base (8\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[8pt]{extarticle}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Taille de base (8\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

**20pt**

```latex
% !TEX noedit
\documentclass[20pt]{extarticle}

\begin{document}
Taille de base (20\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

```latex
\documentclass[20pt]{extarticle}
  \usepackage{lmodern}
  \pagestyle{empty}

\begin{document}
Taille de base (20\,pt)

{\footnotesize Footnotesize}

{\LARGE LARGE}
\end{document}
```

## Avec les classes de l'extension « KOMA-script » et « memoir »

Les classes <ctanpkg:KOMA-script> sont plus satisfaisantes, car elles sont vraiment conçues pour fonctionner correctement avec les options de classe fournies par <ctanpkg:extsizes> (voir ci-dessus). La classe <ctanpkg:memoir> possède, elle, ses propres options pour les tailles de police 9pt--12pt, 14pt, 17pt, 20pt, 25pt, 30pt, 36pt, 48pt et 60pt.

L'extension <ctanpkg:scrextend> permet même d'étendre les fonctionnalités de <ctanpkg:KOMA-script> à n'importe quelle classe et n'importe quelle taille de caractère :

```latex
% !TEX noedit
\usepackage[fontsize=12.3]{scrextend}
```

va en effet configurer la police de base du document pour qu'elle ait une taille de 12,3 pt et régler le `baselineskip` de façon appropriée. Le paquet « connaît » les tailles par défaut de *KOMA-script*, et pour les tailles excentriques comme dans l'exemple, il produira un avertissement :

```latex
% !TEX noedit
Using fallback calculation to setup font sizes
```

Mais gardez votre sang-froid : L'extension <ctanpkg:scrextend> souffre du même problème que <ctanpkg:extsizes> : les tailles de police sont la seule caractéristique du document qui est modifiée, et l'apparence du document résultant ne sera probablement pas aussi bonne que si la classe de document avait été conçue pour être utilisée à la taille choisie.

## Avec des classes spécialisées

De nombreuses classes, conçues pour produire des mises en page sur une taille de papier « non-ordinaire », possèdent leurs propres mécanismes et plages de tailles de police. C'est le cas, par exemple, des classes [conçues pour des affiches](/4_domaines_specialises/communication_scientifique/mettre_en_page_un_poster) (telles que <ctanpkg:a0poster> ou <ctanpkg:beamerposter>), et des classes [pour préparer des diapositives](/4_domaines_specialises/diaporama/classes_pour_des_presentations) (comme <ctanpkg:beamer>).

______________________________________________________________________

*Source :* [Other "document font" sizes?](faquk:FAQ-extsizes)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,taille de caractère,police plus grosse,police plus petite,gros caractères,agrandir la police
```
