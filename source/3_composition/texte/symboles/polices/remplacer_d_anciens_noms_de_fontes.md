# Comment utiliser d'anciennes commandes comme `\tenrm` ?

LaTeX 2.09 a défini un vaste ensemble de commandes pour accéder aux polices dont il disposait. Par exemple, la police *Computer Modern* (`cmr`) peut apparaître sous la forme `\fivrm`, `\sixrm`, `\sevrm`, `\egtrm`, `\ninrm`, `\tenrm`, `\elvrm`, `\twlrm`, `\frtnrm`, `\svtnrm`, `\twtyrm` et `\twfvrm`, selon la taille voulue. Ces commandes n'ont jamais été documentées mais certaines extensions les ont néanmoins utilisées pour obtenir les effets dont elles avaient besoin.

Ces commandes n'étant pas publiques, elles n'ont pas été incluses dans LaTeX. Pour changer cela, vous devez également inclure le paquet <ctanpkg:rawfonts> (qui fait partie de la distribution LaTeX).

______________________________________________________________________

*Source :* [Old LaTeX font references such as \\tenrm](faquk:FAQ-oldfontnames)

```{eval-rst}
.. meta::
   :keywords: LaTeX,ancienne commande,\\tenrm,polices de caractères,compatibilité avec LaTeX 2.09,anciens documents
```
