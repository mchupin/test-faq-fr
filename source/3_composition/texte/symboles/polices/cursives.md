# Comment obtenir des lettres cursives ?

## En mode texte

### Avec l'extension « calligra »

L’extension [calligra](ctanpkg:fundus-calligra) permet l’utilisation de la police calligraphique de texte <ctanpkg:calligra>. Cette police fournit capitales et minuscules, avec lettres accentuées, ç, œ et ÿ, bref ce qu’il faut pour écrire français.

### Avec l'extension « frcursive »

L’extension <ctanpkg:frcursive> donne accès à une police calligraphique dont les lettres sont *droites* contrairement à la précédente dont les lettres sont très penchées. Elle fournit également la macro `\seyes` permettant de composer le texte en cursive sur des lignes de cahier.

### Avec d'autres « extensions »

On trouvera d’autres polices calligraphiques en suivant le lien [polices calligraphiques sur le CTAN](http://www.ctan.org/topic/font-calligraphic).

L'extension <ctanpkg:aurical> donne accès à trois polices en codage T1.

L'extension <ctanpkg:lobster2> donne accès à deux polices dans plusieurs codages dont T1. Elles possèdent des ligatures et des formes terminales que l’on peut appeler avec Xe(La)TeX ou Lua(La)TeX.

## En mode mathématique

On obtient des lettres cursives avec `\mathcal` en mode mathématique.

```latex
% !TEX noedit
$\mathcal{AZ}$
```

donne :        $\mathcal{AZ}$

On ne peut obtenir ainsi que des capitales (de A à Z). La police obtenue est la police calligraphique par défaut.

### Avec l'ensemble « mnsymbol »

L'ensemble <ctanpkg:mnsymbol> fournit (parmi de nombreux autres symboles) un ensemble de lettres calligraphiques, bien qu'elles soient assez similaires à l'ensemble par défaut de *Computer Modern*.

### Avec l'extension « euscript »

L’extension <ctanpkg:euscript> définit la commande `\EuScript` que l’on utilise en mode mathématique pour avoir accès à une police calligraphique dessinée par Hermann Zapf :

```latex
\documentclass{article}
\usepackage{fixltx2e}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{xspace,ifthen}
\usepackage{lmodern}
\usepackage{euscript}
\usepackage[french]{babel}
\pagestyle{empty}
\begin{document}
\newcounter{Lettre}%
\newcommand\LaLettre[1]{%
  \stepcounter{Lettre}
  \(\csname #1\endcsname{\Alph{Lettre}}\)
}%

Avec \verb+\EuScript+ : \par
\noindent
\whiledo{\value{Lettre}<26}{%
  \LaLettre{EuScript}%
  \ifthenelse{\value{Lettre}=13}{\par\noindent}{}}

\setcounter{Lettre}{0}%
Avec \verb+\mathcal+ : \par
\noindent
\whiledo{\value{Lettre}<26}{%
  \LaLettre{mathcal}%
  \ifthenelse{\value{Lettre}=13}{\par\noindent}{}}
\end{document}
```

### Avec l'extension « eucal »

L’extension [eucal](ctanpkg:amsfonts) fait partie de l'ensemble `amsfonts`. Elle redéfinit la commande `\mathcal` pour qu’elle soit équivalente à la commande `\EuScrip` présentée ci-dessus. Les versions de type 1 des polices sont disponibles dans la distribution des polices AMS.

### Avec l'extension « euler »

L’extension <ctanpkg:euler> change la police calligraphique obtenue avec `\mathcal` si elle est chargée avec l’option `mathcal` :

```latex
% !TEX noedit
\usepackage[mathcal]{euler}
```

### Avec l'extension « mathrsfs »

L’extension <ctanpkg:mathrsfs> fournit une autre police calligraphique sophistiquée (dont le nom signifie *Ralph Smith's Formal Script*), avec la commande `\mathscr`. Des versions de type 1 de la police sont également fournis, grâce à Taco Hoekwater.

### Avec l'ensemble « rsfso »

L'ensemble <ctanpkg:rsfso> fournit une version moins oblique des polices RSFS : le résultat s'avère assez satisfaisant et similaire à l'effet de la police de script (commerciale) dans la collection *Adobe Mathematical Pi*.

### Avec l'extension « mathabx »

L’extension <ctanpkg:mathabx> fournit une autre police calligraphique, accessible avec la macro `\mathcal`, contenant également les minuscules. La [documentation de l’extension](texdoc:mathabx) présente l’ensemble des glyphes qu’elle définit. Ces polices ont été développées en MetaFont mais une version au format Adobe Type 1 est disponible.

### Avec la fonte Zapf Chancery

La fonte [Zapf Chancery](wp:ITC_Zapf_Chancery) fournit aussi des lettres cursives, majuscules et minuscules, et des chiffres. Voir l'exemple ci-dessus. Il faut définir cet alphabet mathématique *à la main*, en mettant dans le préambule :

```latex
% !TEX noedit
\DeclareMathAlphabet{mathpzc}{OT1}{pzc}{m}{it}
```

On peut ensuite utiliser dans le corps du documents la commande `\mathpzc`.

```latex
% !TEX noedit
\[\mathpzc{ABCDEFGHIJKLMNOPQRSTUVWXYZ}\]
\[\mathpzc{abcdefghijklmnopqrstuvwxyz}\]
\[\mathpzc{1234567890}\]
```

Si vous trouvez la police un peu trop grosse, vous pouvez en utiliser une version à l'échelle comme celle-ci :

```latex
% !TEX noedit
\DeclareFontFamily{OT1}{pzc}{}
\DeclareFontShape{OT1}{pzc}{m}{it}{<-> s * [0.900] pzcmi7t}{}
\DeclareMathAlphabet{\mathscr}{OT1}{pzc}{m}{it}
```

### Avec l'extension « urwchancal »

*Adobe Zapf Chancery* (que les exemples ci-dessus utilisent) est distribué avec la plupart des imprimantes PostScript, sauf les plus basiques. Une police sensiblement identique (dans la mesure où les mêmes métriques peuvent être utilisées) est disponible auprès d'URW, appelée URW Chancery L : elle est distribuée dans le cadre de l'ensemble « URW base35 ». L'extension <ctanpkg:urwchancal> (qui inclut des polices virtuelles pour modifier l'apparence) prévoit son utilisation comme police calligraphique.

### Avec l'ensemble « Tex Gyre »

La famille de polices *TeX Gyre* comprend également un remplacement à *Zapf Chancery*, « Chorus » ; utilisez-le avec [tgchorus](ctanpkg:tex-gyre) (et ignorez les plaintes concernant la nécessité de changer la forme de la police).

(avec-dautres-extensions-1)=

### Avec d'autres extensions

Comme souvent, on pourra regarder la [liste complète](ctanpkg:comprehensive) des symboles, etc., accessibles avec LaTeX.

______________________________________________________________________

*Source :* [Better script fonts for maths](faquk:FAQ-scriptfonts)

```{eval-rst}
.. meta::
   :keywords: LaTeX,fonte cursive,police cursive,écriture manuelle,écriture de tableau noir
```
