# Comment mettre en évidence une portion de texte ?

- Utiliser la commande `\emph`. Cette commande est définie dans la classe de votre document, et sert spécifiquement à mettre en évidence un mot, une expression ou toute une phrase. La plupart du temps, elle se contente de mettre en italique votre texte, mais vous pouvez lui donner une nouvelle définition avec `\renewcommand` (mettre en rouge, en gras...).

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
