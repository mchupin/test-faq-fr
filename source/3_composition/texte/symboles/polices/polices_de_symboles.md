# Quelles sont les polices de symboles disponibles sous LaTeX ?

Voici deux listes de symboles très utiles qui mentionnent des polices de symboles :

- [The Comprehensive LaTeX Symbol List](ctanpkg:comprehensive) (« la liste complète des symboles de LaTeX ») de Scott Pakin *et al.* illustre plus de 2000 symboles et détaille les commandes et extensions nécessaires pour les produire. Elle liste plus d'une cinquantaine de polices de symboles ;
- si vous utilisez des fontes mathématiques Unicode avec [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex)'' ou ' [LuaTeX](/1_generalites/glossaire/qu_est_ce_que_luatex), votre distribution devrait vous fournir une table des symboles mathématiques Unicode dénommée `unimath-symbols.pdf`. Elle liste les éléments disponibles dans les fontes mathématiques courantes. Si ce fichier n'est pas présent sur votre installation, il se retrouve dans l'extension <ctanpkg:unicode-math>.

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole,police
```
