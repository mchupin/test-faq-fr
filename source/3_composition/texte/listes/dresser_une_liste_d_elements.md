```{role} latexlogo
```

```{role} tttexlogo
```
# Comment composer des listes ?

LaTeX fournit trois environnements de liste différents :

- `itemize` ;
- `description` ;
- et `enumerate`.

Ils partagent une logique commune : chaque élément de ces listes est introduit par la commande `\item`. Mais cette commande affiche un élément différent pour chaque environnement. Dans le cas particulier de l'environnement `description`, l'argument optionnel de la commande `\item` devient le nom de l'élément introduisant l'élément de la liste (en gras).

Voici un exemple de chacun de ces environnements :

```latex
\begin{itemize}
   \item un élément,
   \item un autre élément.
\end{itemize}

\begin{description}
   \item[Genre] Le genre peut être féminin ou masculin
   \item[Nombre] Le nombre peut être singulier ou pluriel
\end{description}

\begin{enumerate}
   \item premier élément,
   \item second élément.
\end{enumerate}
```

L'environnement `itemize` a une particularité. Son format est modifié par l'extension <ctanpkg:babel> pour correspondre aux normes de chaque langue. L'exemple ci-dessus et l'exemple ci-dessous illustrent les modifications que cela génère : en français, le symbole pour les éléments de la liste devient un tiret cadratin et l'espacement entre les éléments de la liste est réduit :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[french]{babel}
  \pagestyle{empty}

\begin{document}
\begin{itemize}
   \item un élément,
   \item un autre élément.
\end{itemize}
\end{document}
```

:::{note}
Il existe en fait un quatrième environnement de liste fourni par LaTeX : l'environnement `list`.

Il n'est jamais utilisé directement lorsque l'on écrit un document, car c'est un environnement générique, qui sert à [construire des listes plus spécialisées](/3_composition/texte/listes/modifier_les_environnements_de_liste). Les environnements `itemize`, `description` et `enumerate` font appel à lui en interne, par exemple, ainsi que `quote` et les commandes qui produisent les bibliographies.
:::

______________________________________________________________________

*Sources :*

- [Latex – Customisation de listes à puces](https://borntocode.fr/latex-customisation-de-listes-a-puces/),
- [How can I define bullet style for itemize with french option babel?](https://tex.stackexchange.com/questions/123668/how-can-i-define-bullet-style-for-itemize-with-french-option-babel)
- [Manuel de référence officieux de LaTeX2ε : « list »](http://tug.ctan.org/info/latex2e-help-texinfo-fr/latex2e-fr.html#list).

```{eval-rst}
.. meta::
   :keywords: LaTeX,listes,itemize,enumerate,description,liste de choses,environnement de liste,liste à puce,bullet point,tiret
```
