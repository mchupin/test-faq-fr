# Comment obtenir un soulignement de plusieurs lignes de texte ?

LaTeX n'a pas été pensé pour gérer par défaut le soulignement du texte. En effet, la mise en valeur de texte devrait typographiquement se faire plutôt par l'usage de l'italique. Toutefois, une solution a été trouvée par certains pour compenser cette absence de prise en charge : utiliser la commande mathématique `\underline`. Cette technique a cependant un gros défaut : le texte sera positionné dans une boîte, sans césure possible, et débordera dans la marge. Qui plus est, la position du souligné n'est pas constante, selon que des lettres descendantes sont soulignées ou pas. Voici un exemple :

```latex
\documentclass{article}
\pagestyle{empty}
\begin{document}
Je dois vous le dire : votre comportement typographique \underline{parfaitement
inconvenant} a surpris ! Nombreux sont ceux qui se sont même dit scandalisés
par ce soulignement qui dépasse les bornes.
\end{document}
```

Plusieurs extensions permettent de corriger ce point.

## Avec l'extension « ulem »

L'extension <ctanpkg:ulem> définit la commande `\uline` pour souligner son argument ; le texte souligné ainsi produit se comporte comme un texte accentué ordinaire, et se coupera à la fin d'une ligne. Notez bien que la césure peut parfois ne pas fonctionner. Dans l'exemple ci-dessous, elle est indiquée car, sans cela, le mot « inconvenant » n'aurait pas été coupé.

```latex
\documentclass{article}
\usepackage{ulem}
\pagestyle{empty}
\begin{document}
Je dois vous le dire : votre comportement typographique \emph{parfaitement
in\-convenant} a surpris ! Nombreux sont ceux qui se sont même dit scandalisés
par ce soulignement qui dépasse les bornes.
\end{document}
```

Notez ici un point : par défaut, l'extension <ctanpkg:ulem> convertit la commande `\emph` en commande de soulignement comme `\uline` ; cela peut être évité en chargeant l'extension avec :

```latex
% !TEX noedit
\usepackage[normalem]{ulem}
```

Par ailleurs, les commandes `\normalem` et `\ULforem` permettent de passer du mode `\emph` classique au mode `\emph` souligné. En mode souligné, la commande devient paramétrable pour changer le style du souligné ou biffer des mots.

```latex
\documentclass{article}
\usepackage[french]{babel}
\usepackage{ulem}
\pagestyle{empty}
\begin{document}
\normalem Voici le mode \emph{emphasize} usuel.
\ULforem Voici le mode \emph{emphasize} souligné.

D'autres possibilités existent :
\begin{itemize}
   \item vagues : \uwave{texte} ;
   \item barré : \sout{texte} ;
   \item rayé : \xout{texte}.
\end{itemize}
\end{document}
```

## Avec l'extension « soul »

L'extension <ctanpkg:soul> fournit la macro `\ul`. Ici, l'anomalie de césure n'est pas ici présente.

```latex
\documentclass{article}
\usepackage{soul}
\pagestyle{empty}
\begin{document}
Je dois vous le dire : votre comportement
typographique \ul{parfaitement inconvenant} a
surpris ! Nombreux sont ceux qui se sont même dit
scandalisés par ce soulignement qui dépasse les
bornes.
\end{document}
```

Si l'extension <ctanpkg:xcolor> est chargée, on peut modifier la couleur du trait par `\setulcolor{⟨nom d'une couleur⟩}`. Bien entendu, la couleur ne sera, en général, *pas* visible avec un visualiseur de DVI mais elle sera bien dans le PS ou PDF produit. On peut régler d'autres aspects du soulignement à l'aide de `\setul{⟨profondeur⟩}{⟨épaisseur du trait⟩}`. La documentation de <ctanpkg:soul> fournit de nombreux exemples dont s'inspire celui-ci :

```latex
\documentclass{article}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{color,soul}
\usepackage[frenchb]{babel}
\definecolor{darkblue}{rgb}{0,0,0.5}
\setulcolor{darkblue}
\definecolor{bleuclair}{rgb}{.90,.95,1}
\sethlcolor{bleuclair}
\pagestyle{empty}
\begin{document}
Pour le bal des horreurs, voici venir :
\begin{itemize}
\item \ul{des soulignements ;}%
\setuldepth{a}%
\item \ul{des soulignements ;}%
\setuldepth{g}%
\item \ul{des soulignements ;}
\item \setul{}{.75ex}
\ul{des soulignements \emph{un peu trop épais ;}}
\item \setul{1.2ex}{.05ex}
\ul{des soulignements \emph{un peu trop bas.}} \\
\end{itemize}

On peut également surligner \hl{en bleu clair.} \\

On dispose aussi de petites capitales espacées :
\begin{itemize}
\item \textsc{Un exemple assez long} ;
\item \caps{Un exemple assez long}.
\end{itemize}
\end{document}
```

:::{todo} Des problèmes entre <ctanpkg:xcolor> et <ctanpkg:soul> ont été mentionnés.
:::

## Avec l'extension « Lua-UL »

L'extension [Lua-UL](ctanpkg:lua-ul), de Marcel Krüger, met à profit les nouvelles possibilités du moteur LuaTeX pour fournir des soulignement, biffage, mise en évidence, etc., qui n'empêchent pas les ligatures, le crénage des caractères ou la césure.

Les commandes principales sont :

- `\underLine` pour le soulignement,
- `\highLight` pour la mise en évidence (ne fonctionne que si <ctanpkg:luacolor> est chargé),
- `\strikeThrough` pour biffer.

Par exemple :

```latex
% !TEX noedit
\documentclass{article}
\usepackage{lua-ul}

\begin{document}
Cette extension est \strikeThrough{inutile}\underLine{géniale}!
\end{document}
```

:::{todo} Compiler le code de cet exemple quand le serveur aura le package :ctanpkg:`Lua-UL <lua-ul>`.
:::

Cette extension permet une très grande personnalisation des signes de biffure et de soulignement (on peut même souligner avec de petits dessins).

Elle propose un mode de compatibilité avec <ctanpkg:soul> : si la chargez avec l'option `soul`, vous pourrez utiliser les commandes `\ul` et `\ul`.

```latex
% !TEX noedit
\documentclass{article}
\usepackage[soul]{lua-ul}

\begin{document}
Cette extension est \st{inutile}\ul{géniale}!
\end{document}
```

______________________________________________________________________

*Source :* [Underlined text won't break](faquk:FAQ-underline)

```{eval-rst}
.. meta::
   :keywords: LaTeX,soulignement,barrer du texte,souligner du texte,mettre du texte sur fond coloré
```
