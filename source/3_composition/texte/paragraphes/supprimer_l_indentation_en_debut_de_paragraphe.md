# Comment obtenir des paragraphes sans indentation ?

Par convention, le texte courant se compose sans séparation entre les paragraphes et la première ligne de chaque paragraphe est en retrait. Cependant, il n'est pas rare de trouver une convention alternative dans laquelle il n'y a aucune mise en retrait de la première ligne des paragraphes : ce style s'observe dans des manuels techniques ou des documents avec des chartes graphiques spécifiques.

En l'absence de retrait de la première ligne, il est nécessaire que les paragraphes soient séparés par un espace vide. Sans cela, il serait parfois impossible de voir les changements de paragraphes.

## Avec des rédéfinitions de longueur

Une approche pour obtenir ce format est de redéfinir certains paramètres de LaTeX comme suit :

```latex
% !TEX noedit
\setlength{\parindent}{0pt}           % Taille de l'indentation de début de paragraphe
\setlength{\parskip}{\baselineskip}   % Taille de l'espace vertical après une fin de paragraphe
```

## Avec l'extension parskip

Dans les cas simples, la solution ci-dessus est utile. Cependant, la commande `\parskip` interfère avec la présentation des listes et environnements similaires. Cela peut faciliment conduire à un résultat laid. L'extension <ctanpkg:parskip> corrige la plupart des problèmes de cette méthode (mais n'est pas parfaite).

## Avec les classes artikel3 et rapport3

Les classes définies par le [NTG](http://www.ntg.nl/), le TUG néerlandais, incluent deux classes qui proposent un retrait de paragraphe nul et un saut de paragraphe différent de zéro :

- un équivalent de la classe <ctanpkg:article> : [artikel3](ctanpkg:ntgclass) ;
- un équivalent de la classe <ctanpkg:report> : [rapport3](ctanpkg:ntgclass).

______________________________________________________________________

*Source :* [Zero paragraph indent](faquk:FAQ-parskip)

```{eval-rst}
.. meta::
   :keywords: LaTeX,indentation
```
