# Comment changer le retrait en début de paragraphe ?

LaTeX ajoute une espace prédéfinie en début de paragraphe (ici en rouge) :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \usepackage{microtype}
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\noindent\tikz\path[fill=red] (0,0) rectangle (\parindent,1.5ex);%
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{}
\end{document}
```

Avec <ctanpkg:babel> et l'option `french`, ce retrait mesure `1,5em` par défaut.

## Avec les commandes de base

Vous pouvez modifier la longueur de ce retrait en changeant la valeur de la variable `\parindent` :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \usepackage{microtype}
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\setlength{\parindent}{1.2cm}

\noindent\tikz\path[fill=red] (0,0) rectangle (\parindent,1.5ex);%
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{}
\end{document}
```

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \usepackage{microtype}
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\setlength{\parindent}{0ex}

\noindent\tikz\path[fill=red] (0,0) rectangle (\parindent,1.5ex);%
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{}
\end{document}
```

:::{important}
On pourrait être tenté d'utiliser `\hspace*{⟨longueur⟩}` en début de chaque paragraphe (avec `⟨longueur⟩` exprimée dans les [unités usuelles](/2_programmation/syntaxe/longueurs/unites_de_mesure_de_tex) de LaTeX). Cette solution est déconseillée, sauf s'il s'agit de modifier un unique paragraphe. En effet, le document résultant deviendrait compliqué à maintenir, avec une mauvaise séparation du fond et de la forme.

Inversement, si vous souhaitez ponctuellement retirer ce retrait sans changer la mise en forme du reste de votre document, faites précéder votre paragraphe de `\noindent` :

```latex
\documentclass{article}
  \usepackage[T1]{fontenc}
  \usepackage[width=7cm,height=6cm]{geometry}
  \usepackage{lmodern}
  \usepackage[french]{babel}
  \usepackage{microtype}
  \usepackage{tikz}
  \pagestyle{empty}
\begin{document}
\noindent%
Longtemps, je me suis couché de bonne heure. Parfois, à peine ma bougie éteinte, mes yeux se fermaient si vite que je n'avais pas le temps de me dire : \og{}Je m'endors.\fg{}
\end{document}
```
:::

## Avec l'extension « indentfirst »

En typographie anglaise usuelle, ce retrait n'est pas inséré au début du premier paragraphe **d'une nouvelle section**. C'est donc le comportement par défaut de LaTeX.

Si votre document est en français, avec :

```latex
% !TEX noedit
\usepackage[french]{babel}
```

[le module Babel-french](texdoc:frenchb) s'occupe de restaurer ce retrait et vous n'avez rien de plus à faire.

Si vous écrivez en anglais et souhaitez tout de même avoir ce retrait, l'extension <ctanpkg:indentfirst> permet de forcer LaTeX à le faire. C'est notamment utile pour avoir une mise en forme homogène dans un document bilingue anglais/français.

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en forme des paragraphes,alinéa,renfoncement de la première ligne,début de paragraphe,typographie anglaise,typographie française
```
