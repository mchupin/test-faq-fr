# Comment insérer du code LaTeX dans un document LaTeX ?

- Le package <ctanpkg:faqexs> qui propose de nombreux types d'exemples, d'un petit bout de code à des documents complets, est celui qui a été développé pour la mise en forme de [la FAQ fctt au format LaTeX](http://faqfctt.fr.eu.org/).
- Le package <ctanpkg:example> offre un environnement `example` qui permet en ne tapant qu'une seule fois le code d'avoir côte à côte le code LaTeX et son résultat après compilation, un peu à la manière des exemples que l'on voit dans cette FAQ. Attention, ce package est considéré comme obsolète et risque de poser certains soucis.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
