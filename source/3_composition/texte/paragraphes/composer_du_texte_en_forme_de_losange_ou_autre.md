# Comment écrire du texte en forme de losange ou autre ?

L'extension <ctanpkg:shapepar> définit plusieurs commandes pour écrire des paragraphes dans des formes :

- `\diamondpar` pour un losange ;
- `\heartpar` pour un cœur ;
- `\starpar` pour une étoile à cinq branches ;
- `\circlepar` pour un disque ;
- et bien d'autres, d'autant plus que l'extension permet de définir des formes personnalisées.

Voici un exemple pour le losange :

```latex
\documentclass{article}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage[french]{babel}
\usepackage{shapepar}
\pagestyle{empty}
\begin{document}
\diamondpar{Mon si beau paragraphe en forme de losange (vous imaginez bien que c'est plus joli quand le paragraphe est assez long).}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,texte dans une forme,disque,étoile,losange
```
