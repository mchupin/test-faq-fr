# Comment modifier l'alignement du texte ?

Sous LaTeX, un texte est normalement [justifié](wpfr:Justification_(typographie)), autrement dit toutes les lignes de texte complètes sont de même longueur. Le changement de ce comportement peut être obtenu avec plusieurs solutions.

## Avec les commandes de base

Trois commandes et trois environnement permettent d'obtenir trois effets classiques :

- l'alignement à gauche (ou en « drapeau droit »), avec la commande `\raggedright` ou l'environnement `flushleft` ;
- l'alignement à droite (ou en « drapeau gauche »), avec la commande `\raggedleft` ou l'environnement `flushright`;
- l'alignement au centre, avec la commande `\centering` ou l'environnement `center`.

```latex
\documentclass{article}
\usepackage[width=7cm]{geometry}
\pagestyle{empty}
\begin{document}
Du texte aligné à droite :

\raggedleft
cet exemple ne brille sans
doute pas par sa complexité.

Clair, non ?
\end{document}
```

```latex
\documentclass{article}
\usepackage[width=7cm]{geometry}
\pagestyle{empty}
\begin{document}
Du texte aligné à droite :
\begin{flushright}
cet exemple ne brille sans
doute pas par sa complexité.
\end{flushright}

Clair, non ?
\end{document}
```

Les deux exemples ci-dessus permettent de noter que la commande et l'environnement ne donnent pas le même résultat :

- la commande `raggedleft` (comme `raggedright` et `centering`) ne peut s'achever que lorsque le groupe qui la contient s'achève : la phrase « Clair, non ? » reste donc alignée à droite car le groupe contenant `raggedleft` n'a pas été fermé. La question « [Pourquoi mon paramètre de paragraphe est-il ignoré ?](/3_composition/texte/paragraphes/parametres_non_appliques_au_paragraphe) » détaille ce point.
- l'environnement `flushleft` (comme `flushright` et `center`) crée lui un espace vertical avant et après lui. Ceci explique pourquoi certains utilisateurs ne l'utilise pas, préférant les commandes précédentes pour ne pas avoir d'autre effet visuel que le changement de l'alignement du texte.

## Avec l'extension « ragged2e »

Les commandes et environnements vus ci-dessus ont tendance à créer souvent des lignes ridiculement courtes. Historiquement, la version Plain TeX de la commande `\raggedright` n'avait pas de ce défaut car elle répugnait à créer une espace trop grande à la fin de la ligne même si dans certaines circonstances --- comme quand [la césure est supprimée](/3_composition/langues/cesure/desactiver_completement_la_cesure) --- des espaces douloureusement larges pouvaient apparaître.

L'extension <ctanpkg:ragged2e> de Martin Schröder offre le meilleur des deux mondes : sa gestion de la justification est construite sur le modèle Plain TeX et est facilement configurable. Elle définit des commandes et environnements avec des noms proches, à quelques majuscules près, des commandes (par exemple, `\RaggedRight`) et d'environnement (par exemple, `FlushLeft`) de base. La [documentation de l'extension](texdoc:ragged2e) illustre d'ailleurs le gain à utiliser ces commandes.

```{eval-rst}
.. meta::
   :keywords: LaTeX,justifier du texte,centrer du texte,changer l'alignement,lignes irrégulières
```
