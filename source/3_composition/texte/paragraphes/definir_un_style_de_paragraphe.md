# Comment définir un style de paragraphe ?

Pour agir sur tout le document, les paramètres de définition d'un paragraphe sont :

- `\parindent` pour fixer la longueur du retrait d'alinéa ;
- `\parskip` pour gérer l'espace entre les paragraphes.

Voici un exemple d'effet bien visible (mais pas forcément très heureux) en TeX :

```latex
% !TEX noedit
\parindent=-10em
```

Une syntaxe plus cohérente avec LaTeX sera :

```latex
% !TEX noedit
\setlength{\parindent}{-10em}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
