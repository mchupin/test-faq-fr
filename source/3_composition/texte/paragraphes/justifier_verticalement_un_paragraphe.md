# Comment justifier verticalement un paragraphe ?

## Avec la commande \\parbox ou l'environnement minipage

La commande `\parbox` et l'environnement `minipage` permettent de mettre en forme un paragraphe d'une largeur donnée (par exemple la largeur de la colonne de texte en cours, `\columnwidth`) et de régler différents paramètres, dont entre autres la hauteur du texte produit et la façon dont il doit être aligné dans cette hauteur (en haut, en bas, centré, étiré).

```latex
% !TEX noedit
\parbox[pos][hauteur][vpos]{largeur}{texte}

\begin{minipage}[pos][hauteur][vpos]{largeur}
Texte
\end{minipage}
```

Dans les deux cas,

- `texte` est le texte à mettre en forme ;
- `largeur` est la largeur du texte à produire ;
- `pos` spécifie l'alignement de la boîte produite par rapport au texte qui se trouve à sa droite et à sa gauche, s'il y en a, en choisissant l'endroit du texte formaté qui servira à aligner sur la ligne commune : `t` la ligne du haut, `b` la ligne du bas, `c` le centre ;
- `hauteur` est la hauteur de la boîte à produire ;
- `vpos` est la façon dont le texte doit être aligné dans sa hauteur : `t` en haut, `b` en bas, `c` centré, `s` étiré. Dans le cas où la boîte doit être étirée en hauteur, il faut placer suffisament d'espaces variables pour que cela fonctionne bien.

## Avec un environnement dédié

L'environnement `vcenterpage` ci-dessous permet de centrer verticalement un texte sur une page seule.

```latex
% !TEX noedit
\newenvironment{vcenterpage}
   {\newpage\vspace*{\fill}}
   {\vspace*{\fill}\par\pagebreak}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
