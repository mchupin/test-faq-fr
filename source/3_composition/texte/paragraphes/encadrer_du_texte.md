# Comment encadrer du texte ?

## Avec la commande de base \\fbox

La commande `\fbox` met son argument dans une boîte (comme `\parbox`) puis dessine un cadre autour :

```latex
\documentclass{article}
\usepackage[width=9cm,height=1cm]{geometry}
\pagestyle{empty}
\begin{document}
Je souhaite \fbox{encadrer} un mot dans un paragraphe.
\end{document}
```

Mais attention : tout le texte passé à la commande va se retrouver sur une seule ligne, sans coupure de mot possible !

Pour encadrer tout un paragraphe, une solution est de le placer dans un environnement `minipage` :

```latex
\documentclass{article}
\usepackage[width=9cm,height=7cm]{geometry}
\usepackage{microtype}
\pagestyle{empty}
\begin{document}

\fbox{%
\begin{minipage}{0.75\textwidth}
   Je souhaite encadrer tout un
   paragraphe, sur plusieurs lignes.
\end{minipage}
}

\end{document}
```

La commande \\fbox est soumise à quelques paramètres : par exemple, l'épaisseur du trait et l'espace entre le texte et le trait est défini respectivement par les longueurs `\fboxrule` et `\fnboxsep` :

```latex
% !TEX noedit
Je souhaite \fbox{encadrer} un mot
dans un paragraphe.

{% Cette paire d'accolades permet
 % que la modification n'ait
 % qu'un effet local.
 \setlength{\fboxrule}{2pt}
 Je souhaite \fbox{encadrer} un mot
 dans un paragraphe.
}

{%
 \setlength{\fboxsep}{1.5ex}
 Je souhaite \fbox{encadrer} un mot
 dans un paragraphe.
}
```

```latex
\documentclass{article}
\usepackage[width=9cm,height=7cm]{geometry}
\pagestyle{empty}
\begin{document}

Je souhaite \fbox{encadrer} un mot
dans un paragraphe.

{% Cette paire d'accolades permet
 % que la modification n'ait
 % qu'un effet local.
 \setlength{\fboxrule}{2pt}
 Je souhaite \fbox{encadrer} un mot
 dans un paragraphe.
}

{%
 \setlength{\fboxsep}{1.5ex}
 Je souhaite \fbox{encadrer} un mot
 dans un paragraphe.
}

\end{document}
```

## Avec l'environnement tabular

Avoir du texte encadré revient à avoir un tableaau avec une unique cellule :

```latex
% !TEX noedit
\begin{tabular}{|p{5cm}|}
\hline
On ne dirait pas, mais il s'agit
ici d'un tableau. \\
\hline
\end{tabular}
```

```latex
\documentclass{article}
\usepackage[width=9cm,height=7cm]{geometry}
\pagestyle{empty}
\begin{document}

\begin{tabular}{|p{5cm}|}
\hline
On ne dirait pas, mais il s'agit ici d'un tableau. \\
\hline
\end{tabular}

\end{document}
```

## Exemples avancés avec d'autres extensions

Les méthodes ci-dessous fournissent des cadres d'apparence très classique. D'autres extensions dessinent des cadres adaptés à des documents au look plus moderne, avec couleurs et icônes.

Certaines extensions permettent également d'encadrer des pages entières, ou des blocs de texte s'étendant sur plusieurs pages.

### L'extension fancybox

L'extension <ctanpkg:fancybox> définit des commandes telles que `\shadowbox`, `\doublebox` et `\ovalbox`, qui fonctionnent de la même manière que `\fbox` ci-dessus :

```latex
% !TEX noedit
\usepackage{fancybox}

\shadowbox{Texte ombré.}
\doublebox{Texte doublement encadré.}
\ovalbox{Texte dans un cadre
  aux coins arrondis.}
```

```latex
\documentclass{article}
\usepackage[width=9cm,height=7cm]{geometry}
\usepackage{fancybox}
\pagestyle{empty}
\begin{document}

\shadowbox{Texte ombré.}
\smallskip

\doublebox{Texte doublement encadré.}
\smallskip

\ovalbox{Texte dans un cadre
  aux coins arrondis.}
\end{document}
```

### L'extension awesomebox

L'extension <ctanpkg:awesomebox> ne dessine pas de cadres à proprement parler, mais peut mettre en valeur un bloc de texte avec une icône et un filet coloré :

```latex
\documentclass{article}
  \usepackage[width=9cm,height=7cm]{geometry}
  \usepackage{awesomebox}
  \pagestyle{empty}

\begin{document}

\notebox{Notez bien ceci !}
\smallskip

\importantbox{Lisez bien ce paragraphe
avant de passer à la suite de ce document
sinon, vous risquez de perdre votre temps
en considérations inutiles !}
\smallskip

\end{document}
```

[Awesomebox](ctanpkg:awesomebox) a cinq dessins de base, utilisables sous forme d'une commande ou d'un environnement :

| Pour...                 | Commande        | Environnement                                     | Exemple                                      |
|-------------------------|-----------------|---------------------------------------------------|----------------------------------------------|
| Une note                | `\notebox`      | `\begin{noteblock}`...`\end{noteblock}`           | ![alt](/_static/images/svg/awesomebox_1.svg) |
| Une suggestion          | `\tipbox`       | `\begin{tipblock}`...`\end{tipblock}`             | ![alt](/_static/images/svg/awesomebox_2.svg) |
| Un avertissement        | `\warningbox`   | `\begin{warningblock}`...`\end{warningblock}`     | ![alt](/_static/images/svg/awesomebox_3.svg) |
| Une mise en garde       | `\cautionbox`   | `\begin{cautionblock}`...`\end{cautionblock}`     | ![alt](/_static/images/svg/awesomebox_4.svg) |
| Une remarque importante | `\importantbox` | `\begin{importantblock}`...`\end{importantblock}` | ![alt](/_static/images/svg/awesomebox_5.svg) |

:::{tip}
Il est également très facile de définir vos propres boîtes avec la commande `\awesomebox`. Les icônes peuvent être choisies parmi celles de l'extension <ctanpkg:fontawesome5>, et vous pouvez mettre des filets horizontaux avant et/ou après votre bloc de texte :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{awesomebox}

\begin{document}

\awesomebox[white][\abShortLine]{0pt}{\faGrinBeam[regular]}{pink}{Ceci va vous rendre heureux\dots}

\end{document}
```
:::

### L'extension tcolorbox

L'extension <ctanpkg:tcolorbox> utilise [TikZ/PGF](ctanpkg:tikz) pour dessiner ses cadres. Si vous avez l'habitude de Ti*k*Z, vous apprécierez la syntaxe clefs-valeurs qui permet de paramétrer finement l'apparence des cadres (couleurs, formes, structures...).

À cause de ses possibilités de configuration immenses, [sa documentation](texdoc:tcolorbox) fait plus de 500 pages (en anglais). Mais les deux exemples ci-dessous montrent qu'il n'est vraiment pas compliqué de dessiner des cadres déjà adaptés à pas mal de circonstances, et les nombreuses illustrations de la documentation devraient vous aider à aller plus loin :

```latex
% !TEX noedit
\documentclass{article}
  \usepackage{tcolorbox}

\begin{document}

\begin{tcolorbox}
Le cadre par défaut
\end{tcolorbox}
\smallskip

\begin{tcolorbox}[colback=red!5!white,
                  colframe=red!75!black,
                  title=Cadre sexy
                 ]
Un autre cadre \textbf{plus coloré}, séparé
en deux parties, et portant un titre.
\tcblower
Demain, \textit{j'enlève le bas}.
\end{tcolorbox}

\end{document}
```

```latex
\documentclass{article}
  \usepackage[width=6cm,height=6cm]{geometry}
  \usepackage{microtype}
  \usepackage[french]{babel}
  \usepackage{tcolorbox}
  \pagestyle{empty}

\begin{document}

\begin{tcolorbox}
Le cadre par défaut
\end{tcolorbox}
\smallskip

\begin{tcolorbox}[colback=red!5!white,
                  colframe=red!75!black,
                  title=Cadre sexy
                 ]
Un autre cadre \textbf{plus coloré}, séparé
en deux parties, et portant un titre.
\tcblower
Demain, \textit{j'enlève le bas}.
\end{tcolorbox}

\end{document}
```

### L'extension boites

Pour encadrer un texte pouvant s'étendre sur plusieurs pages, on peut utiliser l'environnement `breakbox` de l'extension <ctanpkg:boites>. Cette extension définit également, par le biais du fichier `boites_exemples.sty` (à charger car <ctanpkg:boites> ne le précharge pas), des environnements qui proposent différentes méthodes d'encadrement :

- `boiteepaisseavecuntitre` ;
- `boitenumeroteeavecunedoublebarre` ;
- `boiteavecunelignequiondulesurlecote` ;
- et `boitecoloriee`.

Ces environnements peuvent tous être modifiés par deux commandes  :

- `bkcounttrue` qui numérote les lignes de ces boîtes ;
- `bkcountfalse` qui ne les numérote plus (option par défaut).

Il est également possible d'emboîter ces environnements.

### L'extension niceframe

L'extension <ctanpkg:niceframe> permet de définir des cadres pleine page.

:::{todo} Détailler <ctanpkg:nieframe>.
:::

### L'extension boxedminipage

L'extension <ctanpkg:boxedminipage> correspond à un vieux style LaTeX 2.09 qui a été actualisé.

:::{todo} Détailler <ctanpkg:boxedminipage>.
:::

### L'extension bclogo

:::{todo} Détailler <ctanpkg:bclogo>.
:::

## Avec des environnements « faits maison »

Les extensions présentées ci-dessus sont évidemment bien plus plus puissantes mais, à titre pédagogique, voici un exemple d'environnement « fait main » pour encadrer des paragraphes de texte :

```latex
\newsavebox{\fmbox}
\newenvironment{fmpage}[1]
    {\begin{lrbox}{\fmbox}\begin{minipage}{#1}}
    {\end{minipage}\end{lrbox}\fbox{\usebox{\fmbox}}}
```

Et voici un exemple d'utilisation de ce fichier :

```latex
% !TEX noedit
\input{fmpage.sty}

\begin{fmpage}{3cm}
   Texte à encadrer dans une boîte
   ne dépassant pas 3 centimètres
   de large.
\end{fmpage}
```

```latex
\documentclass{article}
\usepackage[width=6cm,height=7cm]{geometry}
\pagestyle{empty}

\newsavebox{\fmbox}
\newenvironment{fmpage}[1]
    {\begin{lrbox}{\fmbox}\begin{minipage}{#1}}
    {\end{minipage}\end{lrbox}\fbox{\usebox{\fmbox}}}

\begin{document}

\begin{fmpage}{3cm}
   Texte à encadrer dans une boîte
   ne dépassant pas 3 centimètres
   de large.
\end{fmpage}

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,entourer du texte,mettre un cadre,dessiner un cadre,dessiner des filets
```
