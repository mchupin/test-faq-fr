# Comment supprimer les en-têtes et bas de page de pages vierges ?

Lorsqu'on utilise l'option de classe `openright` pour faire débuter un nouveau chapitre sur une page de droite dans un document recto-verso, pour ne pas afficher les en-têtes et bas de page sur une page de gauche restée vierge, on peut utiliser la commande suivante :

```latex
% !TEX noedit
\newcommand{\clearemptydoublepage}{%
        \newpage{\pagestyle{empty}\cleardoublepage}}
```

Il est alors nécessaire d'utiliser cette commande avant la commande `\chapter` (cette dernière peut aussi être rédéfinie pour prendre en compte la nouvelle commande).

Une autre version est proposée par la petite extension whitecdp dont le code source est donné ci-après. Elle redéfinit la commande `\cleardoublepage` pour que les pages insérées soient vides autrement dit sans en-tête ni bas de page.

```latex
% whitecdp (formerly schulzrinne.sty) --provide for blank pages
% between chapters
% This redefinition of the \cleardoublepage command provides
% for a special pagestyle for the "extra" pages which are generated
% to ensure that the chapter opener is on a recto page.
% The pagestyle is "chapterverso"; for many publishers, this should be
% identical to "empty", so that's the default.
\def\cleardoublepage{\clearpage
 \if@twoside
  \ifodd\c@page\else
   \null\thispagestyle{chapterverso}\newpage
   \if@twocolumn\null\newpage\fi
   \fi
  \fi
 }%
\def\ps@chapterverso{\ps@empty}%
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,en-tête,page blanche,bas de page
```
