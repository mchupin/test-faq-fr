# Comment obtenir une page de garde ?

Les commandes permettant de définir les éléments d'une page de garde sont :

- `\title` pour le titre ;
- `\author` pour le ou les auteurs (dans ce dernier cas leurs noms sont séparés par la commande `\and`) ;
- `\date` pour la date. Si son argument est vide, la date n'est pas affichée. En l'absence de cette commande, c'est la date du jour qui est affichée ;
- `\thanks` utilisée dans l'une des commandes ci-dessus permet d'obtenir une note de bas de page sur la page de garde. Elle se place dans une commande citée précédemment.

La page de garde est ensuite générée par la commande `\maketitle` appelée dans le corps du document (généralement juste après la balise `\begin{document}`).

Voici un exemple de page de garde :

```latex
% !TEX noedit
\documentclass{report}
\usepackage[frenchb]{babel}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\newlength{\larg}
\setlength{\larg}{14.5cm}

% La commande \title... bien chargée !
\title{
{\rule{\larg}{1mm}}\vspace{7mm}
\begin{tabular}{p{4cm} r}
   & {\Huge {\bf {FAQ} \LaTeX{} française}} \\
   & \\
   & {\huge Pour débutants et confirmés}
\end{tabular}\\
\vspace{2mm}
{\rule{\larg}{1mm}}
\vspace{2mm} \\
\begin{tabular}{p{11cm} r}
   & {\large \bf Version 2.0} \\
   & {\large  \today}
\end{tabular}\\
\vspace{5.5cm}
}
% La commande \author... bien chargée aussi !
\author{\begin{tabular}{p{13.7cm}}
Marie-Paule Kluth
\end{tabular}\\
\hline }
\date{}
\begin{document}

\maketitle
\end{page}
\begin{page}
Voici mon document.

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,titre,page de garde,auteur
```
