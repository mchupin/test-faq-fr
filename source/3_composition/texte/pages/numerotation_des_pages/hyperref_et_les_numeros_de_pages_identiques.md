# Comment utiliser hyperref avec des numéros de page répétés ?

La classe <ctanpkg:book>, comme les classes associées, modifie automatiquement l'affichage des numéros de page dans la partie du document placée entre les commandes `\frontmatter` et `\mainmatter` : ces numéros sont alors composés en minuscules romaines. Si cela peut satisfaire des lecteurs humains, pour l'extension <ctanpkg:hyperref>, il en va autrement. L'existence de pages ayant le même numéro de page peut en effet poser des problèmes. Heureusement, les options de configuration de l'extension qui traitent ce point sont (par défaut) configurées pour éviter les problèmes. Les deux options en question sont :

- « `plainpages = false` » qui gère les ancres de page en utilisant la version mise en forme du numéro de page. Avec cette option, <ctanpkg:hyperref> écrit des ancres différentes pour les pages « ii » et « 2 ». Et il s'agit bien de la valeur par défaut de l'option, ce qui est *une bonne chose*. Si l'option vaut `true`, <ctanpkg:hyperref> écrit toutes les ancres de page avec les chiffres arabes, ce qui n'est généralement pas approprié.
- « `pdfpagelabels` » qui définit les étiquettes de page PDF, c'est-à-dire qu'elle communique la valeur de `\thepage` au fichier PDF afin que lecteur de PDF puisse afficher le numéro de page comme, par exemple, « ii (4 sur 40) » plutôt que simplement « 4 sur 40 ».

Ces deux options doivent être utilisées chaque fois que la numérotation des pages diffère du simple « 1 ... N ». Elles sont d'ailleurs rarement employées séparément.

Cette méthode n'est hélas pas parfaite : elle repose sur le fait que la valeur de `\thepage` est différente pour chaque page du document. Un problème fréquent survient quand, après une page de titre non numérotée, les numéros de page sont réinitialisés : l'avertissement pdfTeX de [duplicate ignored](/2_programmation/erreurs/d/pdftex_destination_ignored) se produira alors, quelles que soient les options.

______________________________________________________________________

*Source :* [Hyperref and repeated page numbers](faquk:FAQ-pdfpagelabels)

```{eval-rst}
.. meta::
   :keywords: LaTeX,numéro de page répété,style de numéro de page,hyperref,pdf
```
