# Comment faire référence à une note de bas de page ?

Il faut pour cela mettre un `\label` *à l'intérieur* de l'argument de `\footnote`. Par exemple :

```latex
% !TEX noedit
du texte\footnote{Ici, une note, avec un label.\label{mafootnote}}
et encore du texte.
```

Vous pouvez également lire « [Comment faire référence plusieurs fois à la même note de bas de page ?](/3_composition/texte/renvois/faire_reference_plusieurs_fois_a_une_note_de_bas_de_page) ».

```{eval-rst}
.. meta::
   :keywords: LaTeX,note de bas de page,référence
```
