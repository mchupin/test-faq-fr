# Comment tracer des hirondelles ?

- Si vous préparez un document (livre, flyer, affiche...) pour un imprimeur, il est probable que celui-ci prévoie de l'imprimer sur du papier plus grand que le format final. Pour le calage des différentes machines lors de la fabrication et la découpe au bon format, il aura besoin de repères imprimés dans les marges : [les hirondelles](wpfr:Hirondelle_(imprimerie)) et traits de coupes.

Pour placer ces repères d'alignement dans la marge, vous pourrez utiliser le package <ctanpkg:crop>, dont c'est exactement la fonction. Vous aurez intérêt à le combiner avec le package <ctanpkg:geometry> pour définir la taille de votre papier et de la surface imprimée.

- Si vous utilisez la classe <ctanpkg:memoir>, elle contient déjà les macros nécessaires pour tracer les traits de coupe, et l'option de classe `[showtrims]` :

```latex
\documentclass[showtrims]{memoir}
\mag=200

\setstocksize{216mm}{146mm}
\settrimmedsize{210mm}{140mm}{*}
\settrims{3mm}{3mm}
\setlrmarginsandblock{20mm}{15mm}{*}
\setulmarginsandblock{20mm}{20mm}{*}
% \trimLmarks
\checkandfixthelayout
\usepackage{lipsum}

\begin{document}
\lipsum[1-3]
\end{document}
```

______________________________________________________________________

*Sources :*

- [How to create crop marks](faquk:FAQ-crop),
- [How to adjust memoir crop marks line length](https://tex.stackexchange.com/questions/232543/how-to-adjust-memoir-crop-marks-line-length).

```{eval-rst}
.. meta::
   :keywords: LaTeX,imprimerie,traits de coupe,massicotage
```
