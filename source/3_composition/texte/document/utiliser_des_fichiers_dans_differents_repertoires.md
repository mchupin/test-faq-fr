# Comment inclure des fichiers sans modifier leurs liens internes ?

La construction d'un document volumineux se fait souvent par division de celui-ci en un ensemble de fichiers (par exemple, un par chapitre) et par conservation de tout ce qui concerne chacun de ces fichiers dans un sous-dossier dédié. Malheureusement, TeX n'a pas de « répertoire courant » modifiable, de sorte que tous les fichiers auxquels vous faites référence doivent être rédigés (ou amendés) en tenant compte du dossier où est présent le fichier principal et non là où est leur propre dossier. Ce qui est plutôt contre-intuitif.

Il peut être approprié d'utiliser la technique « d'extension de chemin » [utilisée dans les installations temporaires](/5_fichiers/tds/installation_temporaire) pour résoudre ce problème. Cependant, s'il y a plusieurs fichiers avec le même nom dans votre document, tels que `chapitre1/fig1.eps` et `chapitre2/fig1.eps`, vous n'aidez guère TeX au moment où, dans le fichier du chapitre principal, vous utiliser une commande `\input{fig1}`. Bien que la résolution soit évidente avec des fichiers préparés manuellement (ne les nommez juste pas tous de la même manière), les fichiers produits automatiquement ont souvent des noms répétitifs et les renommer peut causer bien des erreurs.

## Avec l'extension import

L'extension <ctanpkg:import> vient à votre aide ici : elle définit une commande `\import` qui accepte un nom de chemin complet avec le nom d'un fichier dans ce répertoire et organise tout pour que cela fonctionne correctement. Ainsi, par exemple, si `/home/ami/results.tex` contient :

```latex
% !TEX noedit
Graphique : \includegraphics{image}
\input{explication}
```

alors `\import{/home/ami/}{results}` inclura à la fois un graphique et une explication présents dans le dossier `/home/ami/`. Une commande `\subimport` fait le même genre de chose pour un sous-répertoire (un chemin relatif plutôt qu'un chemin absolu). L'extension définit également les commandes `\include` et `\subincludefrom`.

## Avec l'extension chapterfolder

L'extension <ctanpkg:chapterfolder> fournit des commandes pour gérer son modèle d'inclusion de fichier dans un document. Elle fournit les commandes `\cfpart`, `\cfchapter`, `\cfsection` et `\cfsubsection`, chacune prenant des arguments de répertoire et de fichier, par exemple :

```latex
% !TEX noedit
\cfpart[pt 2]{Partie deux}{partie2}{partie}
```

Cette commande va émettre une commande classique `\part[pt 2]{Partie deux}` puis inclure le fichier `partie2/partie.tex` pour lequel `partie2/` est le dossier courant. Il existe également des commandes de la forme `\cfpartstar` qui correspondent aux versions « étoilées » (`\part*` dans le cas présent).

Une fois que vous êtes au sein d'un document <ctanpkg:chapitrefolder>, vous pouvez utiliser la commande `\cfinput` pour inclure un élément relatif au dossier courant ou vous pouvez utiliser la commande `\input`, en utilisant `\cfcurrentfolder` pour obtenir un chemin vers le fichier. De manière similaire, sont définis quelques autres chemins, par exemple `\cfcurrentfolderfigure` pour un sous-dossier `figure/`.

______________________________________________________________________

*Source :* [Bits of document from other directories](faquk:FAQ-docotherdir)

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
