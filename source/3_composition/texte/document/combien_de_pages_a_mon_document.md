# Comment compter le nombre de pages d'un document ?

## Avec quelques commandes

Il est aussi possible de définir soi-même une étiquette sur la dernière page « à la main », en insérant la commande \\label\{PageFin} sur la dernière page (`PageFin` n'étant ici qu'un exemple de nom possible).

On peut aussi, dans le préambule, faire cette déclaration dans le préambule :

```latex
% !TEX noedit
\AtEndDocument{\label{PageFin}}
```

Il suffit ensuite de faire référence à la page de cette étiquette avec `\pageref{PageFin}`.

Ces deux solutions sont à réserver à des documents simples.

## Avec l'extension lastpage

Les documents simples (ceux qui commencent à la page 1 et qui n'ont aucune interruption dans leur numérotation de page jusqu'à leur dernière page) ne présentent aucun problème particulier. Le nombre de pages est indiqué par l'extension <ctanpkg:lastpage> dans son étiquette `LastPage` que l'on peut afficher avec `\pageref{LastPage}`.

Pour les documents plus compliqués (tels des livres avec une première séquence de numéros de page différents), cette approche simple ne fonctionnera pas.

## Avec l'extension count1to

L'extension <ctanpkg:count1to> définit une étiquette `TotalPages`. C'est la valeur de sa copie de `\count1`, un registre de compte réservé de TeX, à la fin du document. Cette extension nécessite l'extension <ctanpkg:everyshi>.

## Avec l'extension totpages

L'extension <ctanpkg:totpages> définit une étiquette `TotPages` et elle rend également disponible le registre qu'elle utilise comme compteur LaTeX, `TotPages`, par le biais de la commande `\theTotPages`. Bien sûr, le compteur `TotPages` est asynchrone à l'image des numéros de page, mais des captures de cette valeur peuvent être faites en toute sécurité lors de la routine de sortie. Cette extension nécessite l'extension <ctanpkg:everyshi>.

## Avec l'extension memoir

La classe <ctanpkg:memoir> définit deux compteurs `lastpage` et `lastsheet`, qui sont déterminés (après la première exécution d'un document) à l'image des étiquettes `LastPage` et `TotalPages`.

______________________________________________________________________

*Source :* [How many pages are there in my document?](faquk:FAQ-howmanypp)

```{eval-rst}
.. meta::
   :keywords: LaTeX,usage
```
