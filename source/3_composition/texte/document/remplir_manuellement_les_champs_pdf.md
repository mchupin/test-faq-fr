# Comment renseigner manuellement les métadonnées du document en format PDF ?

Les métadonnées d'un fichier PDF (auteur, date, etc.) peuvent être remplies facilement avec les commandes fournies par le package <ctanpkg:hyperref> (options de chargement du package ou commande `\hypersetup`) :

```latex
% !TEX noedit
\usepackage[pdftitle={Mon beau document},
            pdfauthor={Moi},
            pdfsubject={Passionnant},
            pdfkeywords={mots, clefs}
           ]{hyperref}
```

Mais il peut arriver que vous souhaitiez les remplir à la main, par exemple parce que vous ne souhaitez pas charger `hyperref` pour si peu. Cela se fait facilement avec la primitive `\pdfinfo` de `pdfTeX`.

```latex
% !TEX noedit
\ifnum\pdfoutput>0 \pdfinfo
{
/Title (Mon beau document)
/Author (Moi)
/Subject (Passionnant)
/Keywords (mots, clefs)
}
\fi
```

L'exemple parle de lui-même. Notez deux points :

- par prudence ce code n'est inséré qu'en mode pdf, `\pdfinfo` causant un warning sinon ;
- l'espace entre 0 et `\pdfinfo` correspond à une règle générale en TeX. Il est prudent de laisser un espace entre un nombre et un nom de commande (autre que `\relax`) car sinon TeX va chercher à voir si la commande ne contient pas la suite du nombre, ce qui peut avoir des effets surprenants.

______________________________________________________________________

*Copie archivée :* <https://web.archive.org/web/20170314080827/https://elzevir.fr/imj/latex/tips.html#pdfinfo>

```{eval-rst}
.. meta::
   :keywords: LaTeX,document,métadonnées,PDF
```
