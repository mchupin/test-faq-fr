# Comment obtenir un document hypertexte avec (La)TeX ?

Si vous voulez un faire un document hypertexte en ligne avec une source (La)TeX, il y a actuellement trois possibilités à considérer.

## Avec une conversion de LaTeX en HTML

Vous pouvez partir d'une source (La)TeX et utiliser une des nombreuses techniques pour [traduire (plus ou moins) directement en HTML](/5_fichiers/xml/convertir_du_latex_en_html);

## Avec l'extension hyperref

Vous pouvez partir d'une source (La)TeX et utiliser `pdfTeX`, [XeTeX](/1_generalites/glossaire/qu_est_ce_que_xetex)'' ou ' [LuaTeX](/1_generalites/glossaire/qu_est_ce_que_luatex) pour produire des PDF, tout en vous servant de l'extension <ctanpkg:hyperref> pour construire des hyperliens.

## Avec texinfo

Vous pouvez partir d'une source [texinfo](/1_generalites/glossaire/qu_est_ce_que_texinfo) et utilisez le visualiseur `info` ou vous pouvez convertir la source <ctanpkg:texinfo> en HTML en utilisant `texi2html`.

______________________________________________________________________

*Source :* [Making hypertext documents from TeX](faquk:FAQ-hyper)

```{eval-rst}
.. meta::
   :keywords: LaTeX,hypertexte,hyperref,HTML,conversion,LaTeX pour le web
```
