# Comment présenter un tableau comme dans les livres ?

## Avec l'extension « booktabs »

L'extension <ctanpkg:booktabs> permet de mettre en page un tableau comme dans un ouvrage professionnel. Sa documentation est [disponible en français](texdoc:booktabs-fr).

Son auteur préconise quelques règles de bon sens (qui peuvent éventuellement demander quelques adaptations car il raisonne sur la typographie anglo-saxonne) :

- ne jamais utiliser de ligne verticale ;
- ne jamais utiliser de ligne double ;
- mettre les unités dans l'en-têtes du tableau ;
- ne pas oublier le zéro devant la virgule de « \$,10\$ » ;
- ne pas utiliser de signe pour dire que la valeur précédente est répétée, mais plutôt laisser la case vide (les tableaux français peuvent utiliser un guillemet pour indiquer une case vide ou sans objet, un tiret long pour indiquer une répétition, source <http://listetypo.free.fr/ortho/guillemets.html>).

En voici un exemple d'utilisation :

```latex
\documentclass{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[total={4cm,4cm}]{geometry}

  \usepackage{lmodern}
  \usepackage{booktabs}
  \usepackage{eurosym}

  \pagestyle{empty}

\begin{document}

\begin{tabular}{ @{} l l r @{} }
  \toprule
  \multicolumn{2}{c}{Article} \\
  \cmidrule(r){1-2}
  Animal   & Description & Prix (\euro) \\
  \midrule
  Moustique& par gramme  & 13,65 \\
           & la pièce    &  0,01 \\
  Gnou     & empaillé    & 92,50 \\
  Chameau  & empaillé    & 33,33 \\
  Tatou    & surgelé     &  8,99 \\
  \bottomrule
\end{tabular}
\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableaux,mise en forme de tableaux,beaux tableaux,filets horizontaux
```
