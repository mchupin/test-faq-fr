# Comment éviter que du texte de grande taille ne touche le cadre des cellules ?

- On peut « dilater » l'espace autour du texte dans un tableau, en augmentant la valeur retournée par la commande `\arraystretch` (qui vaut 1 par défaut), avec :

```latex
% !TEX noedit
\renewcommand{\arraystretch}{1.5}
```

Ce qui donne ce résultat :

```latex
% !TEX noedit
% Pas de \renewcommand
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

```latex
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

```latex
% !TEX noedit
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

```latex
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

- Si l'on a changé de police dans un tableau (ou simplement de taille de police), il se peut que la hauteur de la cellule n'ait pas augmenté en conséquence. On pourra alors utiliser la commande `\strut`, qui crée un caractère invisible (d'épaisseur nulle, en fait) qui s'adapte à la taille de la fonte. Voici un exemple qui montre la différence que cela apporte :

```latex
% !TEX noedit
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

```latex
\begin{tabular}{|l|}
   \hline
   {\large HAUT} bas \\
   \hline
\end{tabular}
```

```latex
% !TEX noedit
\begin{tabular}{|l|}
   \hline
   {\large\strut HAUT} bas \\
   \hline
\end{tabular}
```

```latex
\begin{tabular}{|l|}
   \hline
   {\large\strut HAUT} bas \\
   \hline
\end{tabular}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,tableau,grand texte dans un tableau,texte qui atteint le bord
```
