# Comment insérer une figure large dans un document en deux colonnes ?

Les flottants comme les figures et les tableaux ont normalement la même largeur que la page, mais dans les documents à deux colonnes, ils sont limités à la largeur de la colonne. Si vous souhaitez des flottants de la largeur de la page dans un document à deux colonnes, utilisez les versions étoilées : `figure*` contient une figure flottante de la largeur de la page, et `table*` un tableau de la largeur de la page.

Les environnements de flottants étoilés ne peuvent apparaître qu'en haut d'une page, ou sur une page entière : les options de placement `h` ou `b` sont simplement ignorées.

## Comment insérer une équation large ?

Malheureusement, les équations de la largeur de la page ne peuvent être insérées dans un document en colonnes que sous forme de flottants. Vous devrez donc les inclure dans des environnements `figure*`, ou utiliser le package <ctanpkg:float> ou <ctanpkg:ccaption> pour définir un nouveau type de flottant pour les equations.

______________________________________________________________________

*Source :* [Wide figures in two-column documents](faquk:FAQ-widefigs)

```{eval-rst}
.. meta::
   :keywords: LaTeX,mise en page,mise en forme,documents en deux colonnes,figures,images,flottants
```
