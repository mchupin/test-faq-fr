# Comment lier le placement des flottants aux sections ?

Les chapitres faisant généralement un `\clearpage`, le problème ne se pose pas pour eux, puisque cette commande « vide » l'ensemble des figures qui étaient en réserve. Par contre, pour les sections, c'est différent, puisqu'elles ne commencent pas forcément sur une nouvelle page.

## Avec l'extension « placeins »

L'extension <ctanpkg:placeins> définit une commande `\FloatBarrier` qui interdit aux flottants qui la précèdent (dans le source) de la dépasser (dans le document final). Il faudra donc placer cette commande à la fin de la section concernée.

Pour que cela soit fait automatiquement pour toutes les sections du document, il faut utiliser l'option `section` lors du chargement de l'extension :

```latex
% !TEX noedit
\usepackage[section]{placeins}
```

Si vous trouvez cette extension trop stricte et vous souhaitez autoriser l'apparition d'une figure après la nouvelle section si l'ancienne débute sur la même page, il suffit d'utiliser l'option `below` pour obtenir la présentation souhaitée.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
