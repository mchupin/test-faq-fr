# Comment utiliser une base de données de définitions pour mes glossaires ?

L'extension <ctanpkg:bib2gls> fournit un outil (écrit en Java) qui extrait des entrées de glossaire stockées dans un fichier `bib`, sous cette forme :

```bibtex
@entry{oiseau,
   name={oiseau},
   description = {animal à plumes}
}

@abbreviation{sncf,
   short="sncf",
   long={Société nationale des chemins de fer français}
}

@symbol{v,
   name={$\vec{v}$},
   text={\vec{v}},
   description={un vecteur}
}
```

De cette façon, vous pouvez par exemple utiliser [JabRef](https://www.jabref.org/) pour gérer votre base de données, puis <ctanpkg:bib2gls> pour extraire les défintions utilisées dans votre document. Plus de détails [dans la documentation](texdoc:bib2gls).

:::{todo} Ajouter un exemple plus concret.
:::

______________________________________________________________________

*Sources :*

- <https://latex.net/sorting-glossaries-with-bib2gls/>

```{eval-rst}
.. meta::
   :keywords: LaTeX,glossaire,glossarie,bibtex,fichier bib,dictionnaire,définition,lexique
```
