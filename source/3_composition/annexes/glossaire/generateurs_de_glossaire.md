# Quels sont les générateurs de glossaire ?

## Les programmes makeindex ou xindy

Les programmes <ctanpkg:makeindex> et <ctanpkg:xindy> sont les plus couramment utilisés.

## Le programme GloTeX

Le programme [GloTeX](ctanpkg:glotex) permet de créer un glossaire à partir d'une base de données. Cela permet de réutiliser les définitions et d'avoir des glossaires cohérents d'un document à un autre.

## Le programme glosstex

Le programme <ctanpkg:glosstex> avec l'extension du même nom fonctionne lui aussi à partir d'une base de données. Il permet de générer plusieurs listes (définitions, acronymes...).

## L'extension gloss avec BibTeX

L'extension <ctanpkg:gloss> utilise également des bases de données pour générer le contenu du glossaire, mais elle utilise `BibTeX` pour générer ce contenu.

```{eval-rst}
.. meta::
   :keywords: LaTeX,annexes,glossaires,glossaire,makeindex,xindy,BibTeX
```
