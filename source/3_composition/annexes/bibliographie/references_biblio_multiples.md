# Comment fusionner des entrées dans la bibliographie ?

Une convention parfois utilisée dans les revues de physique consiste à « fusionner » un groupe de citations liées en une seule entrée dans la bibliographie. `BibTeX`, par défaut, ne peut pas gérer cette mise en forme. Cependant des extensions peuvent traiter ce point.

## Avec l'extension « mcite »

L'extension <ctanpkg:mcite> surcharge la commande `\cite` pour qu'elle reconnaîsse un `*` placé au début d'une clé. Voici un exemple :

```latex
% !TEX noedit
\cite{article1,*article2}
```

Ici, l'appel de la référence renvoit à une unique référence qui fusionne les deux entrées `article1` et `article2` en une unique entrée dans la bibliographie elle-même. L'extension fusionne l'netrée étoilées ou les entrées étoilées consécutives avec l'entrée non étoilée qui les précèdent. Dès lors, il est possible de mélanger des références « réduites » avec des références « ordinaires », comme dans

```latex
% !TEX noedit
\cite{article0,article1,*article2,*article3,article4}
```

qui apparaîtra dans le document, par exemple, comme 3 références « \[4,7,11\] » :

- le « 4 » fera référence à `article0` ;
- « 7 » fera référence à une entrée fusionnée pour `article1`, `article2` et `article3` ;
- « 11 » fera référence à l'article 3.

Notez que vous devez apporter une petite modification au fichier de style de bibliographie (`bst`) que vous utilisez afin de bien gérer le séparateur entre les entrées bibliographiques agrégées en une unique entrée. La [documentation](texdoc:mcite) de l'extension <ctanpkg:mcite> vous indique comment procéder.

:::{todo} // Ajouter l'exemple visuel (celui de la documentation de mcite est très explicite). //
:::

## Avec l'extension « mciteplus »

L'extension <ctanpkg:mciteplus> ajoute des fonctionnalités telles l'ajout d'une gestion de sous-listes d'entrées ou la comptabilité avec l'extension <ctanpkg:natbib>. Elle résout également quelques problèmes posés par <ctanpkg:mcite>. Toutefois, les fichiers `bst` classiques ne fonctionneront pas avec <ctanpkg:mciteplus>. Une nouvelle fois, la [documentation](texdoc:mciteplus) de l'extension explique comment corriger un style `BibTeX` existant.

## Avec l'ensemble « revtex »

L'ensemble <ctanpkg:revtex> contient une classe et différents éléments de style pour présenter des documents issus de trois organismes américains : l'*American Physical Society*, l'*American Institute of Physics* et l'*Optical Society of America*. En conjonction avec l'extension <ctanpkg:natbib>, cet ensemble fournit un support pour les citations fusionnées et n'a même plus besoin de <ctanpkg:mciteplus>.

## Avec l'extension « collref »

L'extension <ctanpkg:collref> adopte une approche assez différente du problème et fonctionne avec la plupart des extensions `BibTeX`. L'extension repère les sous-ensembles communs dans les appels de références. Voici un exemple :

```latex
% !TEX noedit
\cite{article0,article2,article3,article4}
(...)
\cite{article1,article2,article3,article5}
```

Dans ce cas, l'extension considére `article2` et `article3` comme des entrées à condenser en une unique entrée.

______________________________________________________________________

*Source :* [Multiple citations](faquk:FAQ-mcite)

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```
