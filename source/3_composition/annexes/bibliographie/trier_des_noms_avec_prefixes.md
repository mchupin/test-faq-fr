# Comment concilier le tri de BibTeX avec certaines formes de noms ?

`BibTeX` reconnaît un grand nombre de préfixes de noms (principalement ceux dérivés des noms de langues européennes) et il les ignore lors du tri de la bibliographie. Dès lors, il place bien l'entrée « Ludwig van Beethoven » à la lettre B, et non à la lettre V.

Cependant, `BibTeX` ne traite pas par défaut un cas comme « Lord Rayleigh » ou des noms de langues non prises en compte lorsqu'il a été conçu tel « al-Wakil » (transcrit de l'arabe). Résoudre ce point demande une « clé de tri » distincte du nom lui-même, mais `BibTeX` n'autorise cela que dans les citations d'éléments qui n'ont ni auteur ni éditeur. Ici, [Oren Patashnik](wpfr:Oren_Patashnik), l'un des concepteurs de `BibTeX`, suggère d'intégrer la clé de tri dans le nom de l'auteur, tout en empêchant la composition de cette clé. Pour cela, il propose dans le fichier « .bib » de définir une commande nommée `\noopsort` (pour *no-output-sortkey*, autrement dit « clé de tri non affichée »), qui est définie comme suit :

```latex
% !TEX noedit
@PREAMBLE{ {\providecommand{\noopsort}[1]{}} }
```

Dès lors, dans ce fichier « .bib », vous pouvez indiquer par exemple :

```latex
% !TEX noedit
...
@ARTICLE{Rayleigh1,
author = "{\noopsort{Rayleigh}}{Lord Rayleigh}",
...
}
```

Notez que cette commande s'applique au nom de famille. Dans le cas d'un auteur avec un nom arabe, cela donnerait donc :

```latex
% !TEX noedit
...
author = "Ali {\noopsort{Hadiidii}}{al-Hadiidii}",
...
```

Cette commande peut également être utilisée pour traiter des questions d'ordre des noms, comme dans le nom vietnamien ci-dessous :

```latex
% !TEX noedit
...
author = "\noopsort{Thanh Han The}{Han The Thanh}",
...
```

Enfin, elle peut permettre de classer certaines références en fin de bibligraphie, par exemple :

```latex
% !TEX noedit
...
author = "\noopsort{ZZZ}{Anonyme}",
...
```

______________________________________________________________________

*Source :* [BibTeX sorting and name prefixes](faquk:FAQ-bibprefixsort)

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```
