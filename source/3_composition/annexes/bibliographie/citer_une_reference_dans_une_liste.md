# Comment utiliser la commande « `\cite` » dans une commande « `\item` » ?

Un problème peut survenir, parce qu'on « emboîte » deux commandes ayant un argument optionnel. Par exemple :

```latex
% !TEX noedit
\item[\cite[chapitre 13]{Companion}] ...
```

Dans ce cas, le crochet fermant après « `chapitre 13` » sera considéré comme la fin de l'argument optionnel de la commande `\item`. Pour éviter la confusion, on ajoutera donc un niveau d'accolades :

```latex
% !TEX noedit
\item[{\cite[chapitre 13]{Companion}}] ...
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,arguments optionnels,protéger,accolades,crochets,référence bibliographique,citation
```
