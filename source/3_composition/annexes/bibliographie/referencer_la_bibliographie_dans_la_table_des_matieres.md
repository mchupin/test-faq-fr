# Comment référencer la bibliographie dans la table des matières ?

La question « [Comment ajouter une entrée dans la table des matières ?](/3_composition/annexes/tables/ajouter_une_entree_a_une_table_des_matieres) » aborde ce cas particulier.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,table des matières
```
