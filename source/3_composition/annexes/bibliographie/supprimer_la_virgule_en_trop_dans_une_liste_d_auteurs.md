# Comment supprimer la virgule en trop dans une liste d'auteurs ?

Pour les entrées ayant plusieurs auteurs, les fichiers de style bibliographiques (`.bst`) font généralement précéder le dernier auteur d'un *and* (ou d'un *et* dans les versions francisées), lui-même séparé de ce qui précède par une virgule s'il y a plus de trois auteurs.

Pour supprimer cette virgule, il faut suivre les instructions suivantes :

- copier et renommer le style bibliographique de départ ;
- chercher la ligne `FUNCTION{ format.names }` ;
- à l'intérieur de cette fonction, chercher des lignes ressemblant à ce que suit pour les placer en commentaire (le caractère de commentaire étant le symbole `%`) :

```
numnames #2 >
         { "," * }
         'skip$
if$
```

- et, bien entendu, utiliser ce nouveau style.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,références bibliographiques,ponctuation dans les bibliographies,virgule supplémentaire,virgule surnuméraire,retirer la virgule
```
