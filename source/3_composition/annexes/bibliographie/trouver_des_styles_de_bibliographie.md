# Où trouver des styles de bibliographie ?

Il existe de nombreux styles disponibles dans le dossier [des contributions à BibTeX](http://www.ctan.org/tex-archive/biblio/bibtex/contrib) sur le site du CTAN. Attention, certains de ces styles bibliographiques nécessitent l'utilisation d'une extension, comme par exemple les styles <ctanpkg:apalike> ou <ctanpkg:natbib>.

Par ailleurs, le programme interactif <ctanpkg:custom-bib> permet de créer un style bibliographique sans avoir à se plonger dans le langage définissant ces styles.

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie
```
