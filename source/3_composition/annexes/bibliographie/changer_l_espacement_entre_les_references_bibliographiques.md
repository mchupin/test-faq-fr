# Comment changer l'espace entre les items ?

Comme une bibliographie est en fait un environnement de type `list`, l'idée revient à simplement augmenter la valeur de la variable `\itemsep` en charge de cet espace entre deux items consécutifs. Toutefois, cette modification doit être faite *à l'intérieur* de l'environnement `thebibliography`. Voici deux possibilités découlant de cette idée.

## Modification du fichier « .bbl »

Ajouter la commande suivante dans le fichier « `.bbl` », juste après le début de l'environnement `thebibliography`.

```latex
% !TEX noedit
\addtolength\itemsep{1ex}
```

Il faut ici noter que toute mise à jour de la bibliographie supprimera cette ligne (le fichier « `.bbl` » étant totalement regénéré) et qu'elle n'est donc utile que pour un document finalisé.

## Redéfinition de l'environnement « thebibliography »

Voici un exemple d'une telle redéfinition (ici, en langage TeX comme le montre la présence de la commande `\let`) :

```latex
% !TEX noedit
\let\oldtb=\thebibliography
\def\thebibliography#1{\oldtb{#1}%
           \addtolength\itemsep{1ex}}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographie,espace,items
```
