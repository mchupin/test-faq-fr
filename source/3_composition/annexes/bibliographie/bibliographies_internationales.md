# Comment mettre en forme une bibliographie en langue non-anglaise ?

Comme la plupart des premiers logiciels de la famille TeX, `BibTeX` reposait sur des principes propres à ce que son auteur connaissait le mieux, à savoir les articles scientifiques en anglais (plus précisément les articles mathématiques). Les styles standard de `BibTeX` sont donc tous conçus pour ce contexte, laissant à l'utilisateur qui écrit dans une autre langue (ou qui travaille dans d'autres disciplines que les mathématiques) le soin de se débrouiller avec d'autres outils.

De fait, pour l'utilisateur dont la langue n'est pas l'anglais, il existe plusieurs alternatives.

## Les solutions monolingues

### Avec l'extension « BibLaTeX » (et « biber »)

La plus simple est sans doute de passer à l'utilisation de <ctanpkg:biblatex>, qui est très flexible et peut produire des bibliographies dans un grand nombre de langues, y compris plusieurs à la fois. Cependant, <ctanpkg:biblatex> est plutôt volumineux et possède une documentation à l'avenant (bien qu'elle soit très bien écrite et mise en page), son adoption prend donc un peu de temps.

### Avec BibTeX

Si vous préférez utiliser [BibTeX](ctanpkg:bibtex), le plus simple consiste à utiliser des traductions des styles `BibTeX` dans la langue voulue :

- <ctanpkg:bib-fr> pour le français (sans doute le plus complet de tous). Ces versions traduisent principalement le « and » en « et », les noms des mois et quelques autres petites choses. Cependant, elles ne changent pas les règles typographiques appliquées au document, notamment les règles de césure. Les éléments d'une bibliographie étant des paragraphes, les règles typographiques à appliquer pour chaque entrée peuvent être modifiées localement en utilisant d'autres extensions. Voir à ce sujet la question « [Comment franciser un document LaTeX ?](/3_composition/langues/composer_un_document_latex_en_francais) » ;
- <ctanpkg:spain> pour un style de citation espagnol traditionnel ;
- <ctanpkg:germbib> ou <ctanpkg:dinat> pour l'allemand ;
- <ctanpkg:finplain> pour le finnois ;
- <ctanpkg:dk-bib> pour le danois ;
- <ctanpkg:norbib> pour le norvégien ;
- <ctanpkg:swebib> pour le suédois.

## Les solutions multilingues

Ces approches statiques ci-dessus résolvent le problème pour les langues concernées, mais avec une telle approche, chaque langue demande un travail considérable. Voici des approches plus génériques.

### Avec l'extension « babelbib »

L'extension <ctanpkg:babelbib> (qui reprend les idées de l'extension <ctanpkg:germbib>) coopère avec <ctanpkg:babel> pour contrôler la langue des références bibliographiques (potentiellement au niveau des éléments individuels des références). L'extension dispose d'un ensemble intégré de langues qu'elle « connaît », mais sa [documentation](texdoc:babelbib) propose des instructions pour définir des commandes pour d'autres langues. Point qui peut sembler limitant, <ctanpkg:babelbib> a son propre ensemble de styles bibliographiques. Cependant, elle peut être reliée à <ctanpkg:custom-bib>, ce qui retire cette limitation.

### Avec le générateur de style « custom-bib »

Le menu [makebst](ctanpkg:custom-bib) de <ctanpkg:custom-bib> vous permet de choisir une langue pour le style `BibTeX` que vous souhaitez générez (parmi 14 langues). Apparemment, « [spain.bst](ctanpkg:spain) », mentionné ci-dessus, a été généré de cette façon. Si vous choisissez de ne pas spécifier de langue, l'outil vous demande si vous souhaitez que le style interagisse avec <ctanpkg:babelbib> ; si vous le faites, vous obtenez le meilleur des deux mondes : la liberté de formatage de <ctanpkg:custom-bib> et la liberté linguistique via l'extensibilité de <ctanpkg:babelbib>.

### Avec l'extension « mlbib »

L'extension <ctanpkg:mlbib> permet de gérer des bibliographies multilingues. Sa documentation est cependant en allemand.

______________________________________________________________________

*Source :* [Non-english bibliographies](faquk:FAQ-i18nbib)

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,références bibliographiques,règles françaises pour les bibliographies,langue de la bibliographie,multilingue
```
