# Comment utiliser BibTeX avec Plain TeX ?

Le fichier `btxmac.tex`, qui fait partie des fichiers du [format](/1_generalites/glossaire/qu_est_ce_qu_un_format) [Eplain](ctanpkg:eplain), contient des commandes et de la documentation pour utiliser `BibTeX` avec Plain TeX, soit directement, soit avec [Eplain](/1_generalites/glossaire/qu_est_ce_que_eplain).

______________________________________________________________________

*Source :* [Using BibTeX with Plain TeX](faquk:FAQ-bibplain)

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies
```
