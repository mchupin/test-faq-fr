# Comment définir des initiales de prénom regroupant au moins deux lettres ?

Certains styles bibliographiques abrègent les prénoms. Ainsi, par exemple, « Christophe Colomb » devient « C. Colomb », ce que certains jugent fâcheux, préférant « Ch. Colomb ». Bien entendu, ces derniers souhaitent également conserver le prénom complet si le style bibliographique n'abrège pas les prénoms.

Pour obtenir l'affichage d'une initiale qualifiée ici de « multiple », deux solutions sont possibles et demandent à bien rédiger les noms sous la forme « *nom*, *liste des prénoms* »

## Avec une commande de préambule

Une solution consiste à remplacer l'initiale multiple par une commande qui va
afficher en entière les différentes lettres de l'intiale. Pour garder un fichier
bibliographique portable, vous devez ajouter cette commande à votre
bibliographie avec la commande `@preamble` :

```bibtex
% !TEX noedit
@preamble{ {\providecommand{\BIBCh}{Ch} } }

@article{bouquin,
   author   = "Colomb, {\BIBCh}ristophe",
   (...)
}
```

Si vous devez constituer de nombreuses commandes de ce type, vous pourrez éventuellement les mettre dans un fichier séparé et appeler ce fichier avec la commande `\input` depuis la directive `@preamble` de votre fichier bibliographique.

## Avec les commandes de base

Une alternative consiste à faire ressembler notre initiale multiple à un accent aux yeux de `BibTeX`. Pour cela nous avons besoin d'une séquence de contrôle qui ne fait rien :

```bibtex
% !TEX noedit
author = "Colomb, {\relax Ch}ristophe"
```

Comme pour la première solution, cela implique une saisie supplémentaire fastidieuse. Savoir laquelle des deux techniques est préférable pour une bibliographie donnée sera déterminé par les noms qu'elle contient. Il convient juste de noter qu'un préambule qui introduit beaucoup de commandes « étranges » est généralement indésirable si la bibliographie est partagée.

______________________________________________________________________

*Source :* [Multi-letter initials in BibTeX](faquk:FAQ-bibtranscinit)

```{eval-rst}
.. meta::
   :keywords: LaTeX,bibliographies,noms d'auteurs dans les bibliographies,initiales et bibtex
```
