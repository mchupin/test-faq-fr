# Comment obtenir des titres de sectionnement non numérotés en table des matières ?

Il peut être parfois utile d'ajouter des chapitres ou sections non numérotées dans un document : par exemple, une préface, des remerciements, un résumé. Là, l'apparition des titres de ces éléments peut se révéler importante.

Dans ce cadre, les solutions passant par l'utilisation des commandes de sectionnement étoilées comme `\section*` peuvent être approximatives car, si la suppression de la numérotation se fait bien, l'ajout en table des matières peut poser difficulté. Voir sur ce point la question « [Comment ajouter une entrée dans la table des matières ?](/3_composition/annexes/tables/ajouter_une_entree_a_une_table_des_matieres) ». Les méthodes ci-dessous évitent ces approximations.

## Avec les commandes de base

La façon dont fonctionnent les commandes de sectionnement permet d'y voir plus clair. Voici comment la commande `\chapter` procède par le biais du compteur `secnumdepth` (décrit dans l'annexe C du [LaTeX - A Document Preparation System](/1_generalites/documentation/livres/documents_sur_latex)) :

```
- mettre quelque chose dans le fichier ''aux'', qui apparaîtra dans la ''toc'' ;
- si le compteur ''secnumdepth'' est supérieur ou égal à zéro, augmenter le compteur du chapitre et l'écrire ;
- écrire le titre du chapitre.
```

Les autres commandes de sectionnement sont similaires, mais avec d'autres valeurs utilisées dans le test.

Ainsi, un moyen simple d'obtenir dans la table des matières des titres de sectionnement non numérotés consiste à utiliser le compteur :

```latex
% !TEX noedit
\setcounter{secnumdepth}{-1}
\chapter{Préface}
```

Malheureusement, vous devez remettre `secnumdepth` à sa valeur habituelle (qui est 2 dans les styles standard) avant de faire un titre de sectionnement que vous souhaitez numéroter.

La valeur du compteur `tocdepth` contrôle quels en-têtes seront finalement imprimés dans la table des matières. Il est normalement défini dans le préambule et devrait être une constante du document. L'extension <ctanpkg:tocvsec2> vous évite d'avoir à changer les valeurs du compteur `secnumdepth` et/ou `tocdepth` à n'importe quel point du corps du document : les commandes `\setsecnumdepth` et `\settocdepth` effectuent les changements en fonction du *nom* de l'unité de sectionnement (`chapitre`, `section`, etc.).

## Avec des extensions dédiées

Quelques extensions offrent des solutions à ce sujet :

- l'extension <ctanpkg:abstract> propose une option pour ajouter la référence au résumé dans la table des matières. Cette extension est présentée également dans la question « [Comment obtenir un résumé en pleine largeur dans un document à deux colonnes ?](/3_composition/texte/paragraphes/resume_sur_une_colonne_dans_un_document_en_deux_colonnes) ») ;
- l'extension <ctanpkg:tocbibind> a des options pour inclure les titres de la table des matières elle-même, de la bibliographie, de l'index, etc., à la table des matières.

## Avec la classe « book »

La classe <ctanpkg:book> utilise des opérations similaires à celles vues en début de page lors de l'utilisation des commandes `\frontmatter`, `\mainmatter` et `\backmatter`. Ces commandes qui permettent d'obtenir trois zones distinctes de mise en forme dans le document sont décrites dans la question « [Comment gérer des chapitres de préface, d'introduction ou de conclusion non numérotés ?](/3_composition/texte/titres/titres_non_numerotes) ».

## Avec les classes « KOMA-Script »

Les classes <ctanpkg:KOMA-Script> mettent à disposition les commandes `\addchap` et `\addled` fonctionnant comme `\chapter` et `\section` mais ne présentant pas de numérotation.

## Avec la classe « memoir »

La classe <ctanpkg:memoir> intègre les fonctionnalités des trois extensions <ctanpkg:abstract>, <ctanpkg:tocbibind> et <ctanpkg:tocvsec2> évoquées ci-dessus.

______________________________________________________________________

*Source :* [Unnumbered sections in the Table of Contents](faquk:FAQ-secnumdep)

```{eval-rst}
.. meta::
   :keywords: LaTeX,titre de sectionnement,table des matières,index
```
