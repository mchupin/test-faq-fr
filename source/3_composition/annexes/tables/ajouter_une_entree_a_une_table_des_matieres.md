# Comment ajouter une entrée dans la table des matières ?

## Cas général

La commande `\addcontentsline{toc}{section}{nom de la section}` permet d'insérer des entrées dans la table des matières. Dans cette commande :

- le premier argument indique dans quelle table on veut ajouter une entrée. Il peut s'agir de la table des matières (`toc`), de la liste des figures (`lof`), de la liste des tableaux (`lot`) ;
- le deuxième argument indique le niveau de sectionnement auquel on veut placer la nouvelle entrée. Cet argument peut donc valoir `part`, `chapter`, `section`, `subsection`, `subsubsection`. Il peut valoir aussi `figure` ou `table` pour les listes de figures ou les listes de tableaux ;
- le troisième argument est le texte qui doit apparaître, généralement le titre de la section ajoutée.

Dans la pratique, cette commande est souvent utilisée pour insérer des titres de sectionnement lors de l'utilisation de commandes de sectionnement étoilées comme `\section*`. En effet, ces commandes empêchent la numérotation du titre et ne génèrent pas d'entrée en table des matière. Voici un exemple de ce correctif :

```latex
% !TEX noedit
\section*{Un titre non numéroté}
\addtontentsline{toc}{section}{Un titre non numéroté}
```

Toutefois, il faut faire attention au numéro de page obtenu en table des matières (un correctif revenant alors à placer la commande `\addcontentsline` après la commande `\section*` si la section est mal référencée.) La question « [Comment obtenir des titres de sectionnement non numérotés en table des matières ?](/3_composition/annexes/tables/chapitres_non_numerotes_dans_la_table_des_matieres5) » donne une solution bien plus robuste.

## Cas des titres de bibliographie, d'index ou d'autres annexes spécifiques

### Avec les commandes de base

Les classes standards de LaTeX (et bien d'autres) utilisent `\section*` ou `\chapter*` pour mettre en forme des parties auto-générées du document (les tables des matières, les listes de figures et de tableaux, la bibliographie et l'index). Comme vu ci-dessus, ces éléments ne sont donc pas numérotés (ce qui ne dérange pas la plupart des gens) mais n'apparaissent pas dans la table des matières.

Supposons que notre document soit basé sur des chapitres (classe <ctanpkg:report> ou <ctanpkg:book>, par exemple), la solution suivante pourrait être envisagée :

```latex
% !TEX noedit
\bibliography{mabiblio}
\addcontentsline{toc}{chapter}{Bibliographie}
```

Cependant, si la bibliographie s'étale sur plus d'une page, la numérotation en table des matières sera *fausse* (en référençant la dernière page de la bibliographie). Mettre la commande `\addcontentsline` avant la commande `\bibliography` ne résout pas pour autant le problème puisque la commande serait effectuée \\emph\{avant} la commande `\bibliography`, donc avant le début du nouveau chapitre et, par conséquent, peut-être pas sur la bonne page. Pour corriger cela, il convient d'utiliser la suite de commande suivante :

```latex
% !TEX noedit
\cleardoublepage
\addcontentsline{toc}{chapter}{Bibliographie}
\bibliography{mabiblio}
```

Notez que `\cleardoublepage` fait ce qu'il faut, à savoir faire un saut de page permettant d'accéder à une page de début de chapitre, même si votre document n'est pas en mode recto-verso (dans ce cas, c'est un simple synonyme de `\clearpage`). Toutefois, s'assurer que l'entrée fait référence au bon endroit reste délicat dans le cas d'une classe basée sur la commande `\section`.

Si vous utilisez conjointement l'extension <ctanpkg:hyperref> (gérant des liens hypertextes dans le document), un léger ajustement est nécessaire :

```latex
% !TEX noedit
\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{Bibliographie}
\bibliography{mabiblio}
```

La commande supplémentaire, `\phantomsection`, donne à <ctanpkg:hyperref> un point à référencer lors de la création du lien.

### Avec l'extension « tocbibind »

La solution courante consiste à utiliser l'extension <ctanpkg:tocbibind> qui fournit de nombreuses fonctionnalités pour contrôler la manière dont ces entrées apparaissent dans la table des matières.

### Avec les classes « KOMA-script »

Les classes <ctanpkg:KOMA-script> fournissent cette fonctionnalité sous la forme d'un ensemble d'options de classe (par exemple, `bibtotoc` pour ajouter la bibliographie à la table des matières).

### Avec la classe « memoir »

La classe <ctanpkg:memoir> fait directement appel à l'extension <ctanpkg:tocbibind> elle-même.

______________________________________________________________________

*Source :* [Bibliography, index, etc., in TOC](faquk:FAQ-tocbibind)

```{eval-rst}
.. meta::
   :keywords: LaTeX,table des matières,titre de sectionnement,entrée,index,toc
```
