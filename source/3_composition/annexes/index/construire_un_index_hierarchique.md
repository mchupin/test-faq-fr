# Comment construire un index hiérarchique ?

Pour cela, il faut utiliser la construction suivante :

```latex
% !TEX noedit
\index{niveau un!niveau deux}
```

Cela va donner dans ce cas une entrée « niveau deux » au-dessous de l'entrée « niveau un ». Des entrées de niveau trois, quatre et suivants sont possibles en suivant la même logique, le symbole « `!` » indiquant le passage du niveau « N » au niveau « N+1 ». L'exemple ci-dessous, plus complet, illustre en particulier deux cas d'entrées de niveau trois :

```latex
% !TEX noedit
\documentclass{report}
\usepackage[french]{babel}
\usepackage{makeidx}
\makeindex

\begin{document}

\chapter{Sports.}
Le sport\index{Sport} c'est fantastique ! Mes sports préférés sont :
\begin{itemize}
   \item l'escalade\index{Sport!Escalade} et surtout les sorties en falaise ;
   \item l'équitation\index{Sport!Equitation} et en particulier les disciplines de
     dressage\index{Sport!Equitation!Dressage} et de complet\index{Sport!Equitation!Complet} ;
   \item le judo\index{Sport!Judo}.
\end{itemize}

\printindex

\end{document}
```

```{eval-rst}
.. meta::
   :keywords: LaTeX,index,hiérarchie,index hiérarchique
```
