# Où trouver une traduction de la documentation de CTeX ? (pour le chinois)

- Ci-dessous! ↓

:::{important}
Cette page est une traduction du [Manuel de CTeX](texdoc:ctex) (version v2.5.8, du 2021/12/12).

> <ctanpkg:CTeX> est un framework général de composition LaTeX pour la composition du chinois, fournissant des fonctions pour les documents LaTeX en chinois tels que le support de sortie des caractères chinois, la compression de la ponctuation, les commandes de taille de police, la sinisation du texte de l'en-tête, l'ajustement du style aux usages chinois, la conversion des dates numériques, etc. Il peut être adapté à différents types de documents en chinois tels que des articles, des rapports, des livres, des diaporamas, etc.
:::

L'extension CTeX prend en charge de nombreuses méthodes de compilation telles que LaTeX, pdfLaTeX, XƎLaTeX, LuaLaTeX et upLaTeX, et fournit une interface unifiée pour celles-ci. L'essentiel des fonctionnalités est mis en œuvre par l'extension `ctex` elle-même et les classes de documents en chinois : `ctexart`, `ctexrep`, `ctexbook` et `ctexbeamer`.

## Introduction

### Histoire

L'extension CTeX a deux origines : la classe de document `cjkbook` écrite par Wang Lei et `GB.cap` écrite par Wu Lingyun. Ces débuts n'ont pas été conçus de façon très rigoureuse ni systématique et n'avaient pas de documentation utilisateur, ce qui n'en facilitait pas la maintenance et l'amélioration.

En 2003, Wu Lingyun a remanié l'ensemble du projet à l'aide de <ctanpkg:doc> et <ctanpkg:DocStrip> et a ajouté de nombreuses nouvelles fonctionnalités, pour devenir l'extension <ctanpkg:CTeX>. En 2007, Oseen et Wang Yue ont ajouté la prise en charge du codage UTF-8 à CTeX et ont développé l'extension `ctexutf8`.

En mai 2009, nous avons construit le projet [ctex-kit](http://code.google.com/p/ctex-kit/) dans Google Code (ce lien est maintenant obsolète), intégrant `ctex` et les scripts associés, et ajoutant le support du moteur XƎTeX. Lors du développement de la nouvelle version, nous avons abandonné doc et DocStrip pour faciliter le développement et le débogage en collaboration et avons adopté l'approche consistant à écrire directement le code du jeu de macros.

En mars 2014, afin de s'adapter aux plus récents développements de LaTeX, notamment aux nouvelles fonctionnalités de [LaTeX3](/1_generalites/histoire/c_est_quoi_latex3), Qing Li a remanié le code de l'ensemble des macros pour utiliser LaTeX3 et utilisé de nouveau les outils <ctanpkg:doc> et <ctanpkg:DocStrip> pour la gestion du code, faisant passer le numéro de version à 2.0 et le renommant «*CTeX macro set* ».

En mars 2015, le projet `ctex-kit` a été [migré vers GitHub](https://github.com/CTeX-org/ctex-kit) en raison de l'arrêt imminent des services Google Code.

Initialement, Knuth n'a pas conçu et développé TeX avec un support multi-langue en tête, en particulier pour les [idéogrammes CJK](wpfr:Chinois,_japonais,_coréen_et_vietnamien) à plusieurs octets. C'est pourquoi le support du chinois dans TeX et plus tard dans LaTeX n'a jamais été très bon. Même après que l'extension <ctanpkg:CJK> ait résolu le problème de la gestion des caractères chinois, les utilisateurs chinois devaient encore faire face à de nombreuses difficultés lors de l'utilisation de LaTeX. La plus importante de ces difficultés est la gestion des titres de chapitre. En raison des différences entre les habitudes d'écriture chinoises et occidentales, il est difficile pour les utilisateurs d'utiliser la structure de code des classes de documents standards pour écrire les titres chinois. Par conséquent, les utilisateurs doivent apporter des modifications importantes aux classes de documents standards. En outre, des détails tels que le formatage de la date, l'indentation de la première ligne, la taille du texte en chinois et l'espacement des caractères ont dû être réglés avec précision. L'un des objectifs de la conception de l'extension CTeX était de résoudre ces difficultés dans la sinisation des documents LaTeX.

D'autre part, comme le moteur TeX et les extensions LaTeX continuent d'évoluer, les façons dont LaTeX peut prendre en charge la langue chinoise ont évolué depuis les premiers jours des systèmes dédiés (tels que CCT) vers un éventail d'implémentations en fonction du moteur utilisé (par exemple : les extensions <ctanpkg:CJK> et <ctanpkg:zhmCJK> pour le moteur pdfTeX, l'extension <ctanpkg:xeCJK> pour le moteur XƎTeX et l'extension <ctanpkg:LuaTeX-ja> pour le moteur LuaTeX). Il existe un certain nombre de différences dans la mise en œuvre et l'utilisation de ces approches, tandis que des conditions d'utilisation telles que les différents systèmes d'exploitation et les environnements linguistiques introduisent encore d'autres différences dans le détail. Un autre de nos principaux objectifs dans la conception du jeu de macros CTeX est de réduire autant que possible l'impact de ces différences, afin que les utilisateurs puissent utiliser une interface unifiée pour les différentes méthodes de prise en charge de la langue chinoise, de sorte que le même document puisse être échangé et utilisé dans différents environnements.

De nombreux détails de la mise en œuvre de l'ensemble de macros CTEX n'auraient pu être réalisés sans la discussion d'amis enthousiastes sur le forum `bbs.ctex.org` (ce forum CTeX est fermé depuis 2018, pour une raison inconnue et ce lien est désormais mort), et nous tenons à remercier ceux qui ont participé.

### Une remarque sur le nom de l'ensemble de macros

Le nom CTeX est une combinaison de l'initiale « C » du mot anglais *China* ou *Chinese* et de « TEX ». En mode texte brut, le nom doit être écrit `CTeX`.

L'extension CTeX est une collection de fichiers de style LaTeX et de classes de documents initiée et maintenue par la communauté CTeX. La communauté publie également une distribution TeX appelée *CTeX Suite*, qui n'est pas la même chose que le jeu de macros CTeX décrit dans ce document.

`ctex` désigne le fichier du `ctex.sty` fourni par l'extension. Ce nom entièrement minuscule a également été utilisé dans le passé pour désigner l'ensemble des macros CTeX, mais il fait désormais spécifiquement référence au jeu de macros `ctex.sty`. Il peut également suivre l'ancienne convention consistant à se référer à l'ensemble des macros sans créer d'ambiguïté.

## Bref tutoriel

### Composition du jeu de macros CTeX

Afin de répondre aux différents besoins des utilisateurs, nous avons organisé les principales fonctions de l'extension CTeX en quatre classes de documents pour le chinois, et trois fichiers de style, dont la composition spécifique est présentée dans le tableau ci-dessous.

| Catégorie          | Document          | Description                                                                                                                                         |
| ------------------ | ----------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| Classe de document | `ctexart.cls`     | Version chinoise de la classe standard `article`, généralement adaptée aux articles courts.                                                         |
|                    | `ctexrep.cls`     | Version chinoise de la classe standard `report`, généralement adaptée aux rapports de longueur moyenne.                                             |
|                    | `ctexbook.cls`    | Version chinoise de la classe standard `book`, généralement pour les livres longs.                                                                  |
|                    | `ctexbeamer.cls`  | Version chinoise de la classe de document `beamer`, pour les présentations type diapositives.                                                       |
| Fichier de style   | `ctex.sty`        | Fournit toutes les fonctions, mais par défaut les titres de chapitre ne sont pas activés, ils doivent être activés en utilisant l'option `heading`. |
|                    | `ctexsize.sty`    | La définition et l'ajustement des caractères chinois peuvent être appelés séparément à partir du fichier `ctex.sty` ou de la classe CTeX.           |
|                    | `ctexheading.sty` | Le réglage du titre de la section est disponible (voir section 7) et peut être invoqué séparément du fichier `ctex.sty` ou de la classe CTeX.       |

### Installation et mise à jour du jeu de macros CTeX

Les distributions TeX les plus courantes (TeX Live et MiKTeX) incluent déjà l'extension CTeX et ses collections de macros, et ses dépendances.

:::{warning}
L'extension `zhmCJK` constitue une exception. Lorsque l'utilisateur spécifie explicitement l'option `zhmap = zhmCJK`, CTeX essaie de la charger. Comme elle n'est pas incluse dans TeX Live ni MiKTeX, les utilisateurs devront peut-être suivre la documentation pour l'installer eux-mêmes.
:::

Si une installation locale de TeX Live ou MiKTeX n'est pas la version complète, il peut être nécessaire d'installer les extensions via les gestionnaires de paquets fournis par ces deux distributions.

#### Avec TeX Live

Le gestionnaire de paquets pour TeX Live est `tlmgr` (*TeX Live Manager*). L'utilisateur peut l'exécuter à partir de la ligne de commande du système :

```bash
tlmgr gui
```

:::{note}
La ligne de commande de Windows est l'invite de commande CMD. Vous pouvez utiliser la combinaison de touches *Windows + R* pour ouvrir la boîte de dialogue « Exécuter », puis taper `cmd` pour confirmer la fenêtre de l'invite de commande.
:::

Lancez l'interface graphique du gestionnaire (les utilisateurs de Windows peuvent également l'ouvrir via TeX Live 20XX (année) *TeX Live Manager* dans le menu *Démarrer*). Après s'être connecté au dépôt distant, recherchez `ctex` pour l'installer. L'interface graphique de `tlmgr` est écrite en Perl/Tk et s'est avérée dans la pratique encline à planter. Les utilisateurs qui rencontrent ce problème peuvent également installer l'extension CTeX directement à partir de la ligne de commande du système en exécutant la commande suivante :

```bash
tlmgr install ctex
```

Les utilisateurs d'Unix/Linux peuvent avoir besoin des droits de superutilisateur (sudo) pour installer correctement CTeX.

#### Avec MiKTeX

MiKTEX s'occupe automatiquement d'installer une extension quand celle-ci manque pour compiler un document. Pour une installation manuelle, vous pouvez utiliser la console MiKTeX, qui vous permet d'ouvrir le gestionnaire, de choisir un serveur distant, de rechercher `ctex` dans l'onglet *Package* et de l'installer. Enfin, vous pouvez aussi utiliser `mpm` (*MiKTeX Package Manager*) en ligne de commande et exécuter :

```bash
mpm --admin --install=ctex
```

pour installer l'extension CTeX.

Pour plus d'informations sur les dépendances spécifiques de CTeX ou sur la manière de l'installer manuellement, voir la section 12.

#### Mises à jour

À chaque mise à jour de CTeX sur le CTAN, une fois le catalogue local également mis à jour, l'utilisateur peut installer la nouvelle version localement via le gestionnaire de paquets.

Pour TeX Live, ceci peut être fait à partir de l'interface graphique `tlmgr` en cliquant sur le bouton *update all installed* ou en ligne de commande, avec

```bash
tlmgr update --all
```

pour mettre à jour complètement les fichiers installés.

Pour MiKTeX, trouvez l'onglet *Updates* dans la console MiKTeX, vérifiez les mises à jour et sélectionnez les extensions à mettre à jour. Alternativement, vous pouvez utiliser `mpm` en exécutant la ligne de commande

```bash
mpm --admin --update
```

pour effectuer la mise à jour.

### Utilisation des classes de documents CTeX

Si vous devez ajouter la prise en charge de la langue et de la mise en page chinoises aux trois classes de documents standards ou à la classe <ctanpkg:Beamer>, nous vous recommandons d'utiliser directement les quatre classes de documents chinois fournies par l'extension CTeX.

CTeX fournit quatre classes de documents dédiées au chinois :

- `ctexart`,
- `ctexrep`,
- `ctexbook`
- et `ctexbeamer`,

qui correspondent respectivement aux classes de documents standards de LaTeX : `article`, `report`, `book` et `beamer`. Pour les utiliser, tous les fichiers sources impliqués doivent être enregistrés avec un encodage UTF-8 (il est également possible d'utiliser l'encodage GBK lors de l'utilisation de (pdf)LaTeX, mais cela n'est pas recommandé --- voir section 4.2).

```latex
\documentclass{ctexart}
  \pagestyle{empty}

\begin{document}
中文文档类测试。你需要将所有源文件保存为 UTF-8 编码。

你可以使用 XeLaTeX、 LuaLaTeX 或 upLaTeX编译， 也可以使用 (pdf)LaTeX 编译。
推荐使用 XeLaTeX 或 LuaLaTeX 编译。 对高级用户， 我们也推荐使用 upLaTe X编译。
\end{document}
```

Voici un exemple d'utilisation de la classe de document `ctexbeamer` pour préparer des diapositives en chinois :

```latex
\documentclass{ctexbeamer}

\begin{document}
\begin{frame}{中文演示文档}

\begin{itemize}
  \item 你需要将所有源文件保存为 UTF-8 编码
  \item 你可以使用 XeLaTeX、 LuaLaTeX 或 upLaTeX 编译
  \item 也可以使用 (pdf)LaTeX 编译
  \item 推荐使用 XeLaTeX 或 LuaLaTeX 编译
  \item 对高级用户， 我们也推荐使用 upLaTeX 编译
\end{itemize}
\end{frame}

\end{document}
```

### Utilisation de l'extension « ctex »

On peut utiliser l'extension `ctex` si on a besoin d'ajouter le support de la langue chinoise et de ses spécificités de mise en page lorsqu'on utilisent des classes de documents ou des styles beamers non standards.

Pour les classes de documents construites à partir des classes de documents standards de LaTeX, l'extension `ctex` peut être utilisée avec l'option `heading` pour définir les titres de section en style chinois :

```latex
\documentclass{ltxdoc}
  \usepackage[heading = true]{ctex}
  \pagestyle{empty}

\begin{document}
\section{简介}
章节标题中文化的 \LaTeX{} 手册。
\end{document}
```

## 3. Options de l'extension et commande « \\ctexset »

L'extension CTeX a été adaptée et configurée pour le texte chinois et les conventions de mise en page chinoises, dans la mesure du possible, et ces configurations s'avèrent généralement adéquates. Par conséquent, nous ne recommandons pas aux utilisateurs réguliers de modifier les valeurs par défaut, sauf si cela est nécessaire. Si vous pensez que les macros CTeX pourraient être améliorées, vous pouvez faire un rapport de bug ou une suggestion sur la page d'accueil du projet et nous le faire savoir, et nous apporterons les améliorations nécessaires dans les versions ultérieures.

Toutefois, l'extension CTeX offre également un certain nombre d'options. Les utilisateurs peuvent utiliser ces options pour contrôler le comportement des macros de CTeX. Classées par type, ces options sont soit fournies de manière traditionnelle, soit sous forme `⟨clef⟩=⟨valeur⟩`. Elles peuvent être subdivisées en trois catégories comme suit.

- Les options avec un signe ☆ après leur nom, qui ne peuvent être utilisées que comme options d'extension ou de classe de document et doivent être spécifiées lorsque l'extension ou la classe de document est chargée.
- les options comportant un signe ★ après leur nom, qui ne peuvent être définies que par l'intermédiaire de l'interface utilisateur `\ctexset` fournie par l'extension CTeX.
- Les options dont le nom n'est pas suivi d'un symbole spécial peuvent être définies en tant qu'option d'extension ou de classe de document, ou via `\ctexset`.

Certaines options spéciales seront décrites dans suite de la documentation.

### Commande « \\ctexset »

La commande `\ctexset{⟨clefs-valeurs⟩}` est permet de choisir des options pour l'extension CTeX après son chargement. L'argument de `\ctexset` est une liste de clefs et valeurs, qui permet de définir chaque option avec une interface uniforme.

Cette liste de clefs et valeurs se présente sous forme d'une liste d'options séparées par des virgules, chacune dans un format `⟨clef⟩=⟨valeur⟩`. Par exemple, pour définir le titre du résumé et le titre de la bibliographie (voir section 6.2), on peut écrire :

```latex
% !TEX noedit
\ctexset{abstractname={Titre du résumé},
         bibname={Titre de la bibliographie}%
        }
```

`\ctexset` utilise des clefs du même format que LaTeX3 et supporte différents types d'options avec une structure hiérarchique, voir la section 7 pour un exemple.

## 4. Méthodes de compilation, codage et bibliothèques en langue chinoise

### 4.1 Méthodes de compilation

Selon le mode de compilation utilisé par l'utilisateur (parmi LaTeX, pdfLaTeX, XeLaTeX, LuaLaTeX et upLATEX), CTeX choisira quelle extension employer pour supporter la langue chinoise :

| Méthode de compilation | (pdf)LaTeX | XeLaTeX | LuaLaTeX  | upLaTeX  |
| ---------------------- | ---------- | ------- | --------- | -------- |
| Extension utilisée     | CJK        | xeCJK   | LuaTeX-ja | *Native* |

:::{note}
pLaTeX-ng (ou ApLaTeX) est compatible avec upLaTeX. Lors de la compilation avec pLaTeX-ng, CTeX utilise les mêmes paramètres que upLaTeX.
:::

Les différentes méthodes de compilation et le mode de support de la langue chinoise affecteront dans une certaine mesure le comportement de l'extension CTEX, par exemple en ce qui concerne l'encodage des documents, la sélection des polices, les espacements, la ponctuation, etc. Les détails seront abordés dans les sections suivantes de ce document.

% todo:: : À poursuivre.

### 4.2 Codage chinois

### 4.3 Bibliothèques en langue chinoise

## 5. Formatage de la mise en page

### 5.1 Taille de police par défaut du document

### 5.2 Styles d'en-tête de section

### 5.3 Options de schéma de composition

## 6. Sinisation des documents

### 6.1 Manipulation des dates

### 6.2 Personnalisation du titre du document

### 6.3 Mise en page des pages et sinisation

## 7. Mise en forme des titres de chapitre

### 7.1 En rapport avec la numérotation

### 7.2 Formatage lié

### 7.3 Espacement, lié à l'indentation

### 7.4 Table des matières, annexes

### 7.5 Commandes auxiliaires

### 7.6 Exemples

## 8. Commandes pratiques

### 8.1 Taille et espacement des caractères

### 8.2 Conversions numériques chinoises

### 8.3 Divers

## 9. Comment le chinois est pris en charge sous LuaLaTeX

Sous LuaLaTeX, CTeX s'appuie sur l'extension [LuaTeX-ja](ctanpkg:luatexja) pour la prise en charge de la langue chinoise. Cette extension a été développée par Hiroyori Kitagawa, Kazutaka Maeda, Takayuki Yaten et d'autres membres de la communauté TeX japonaise, et a été conçu pour mettre en œuvre (la plupart) des fonctionnalités du [moteur pTeX](/1_generalites/glossaire/ptex_et_uptex) japonais sous le moteur LuaTeX. Il apporte un certain nombre de modifications et d'extensions au [schéma NFSS](/5_fichiers/fontes/utiliser_des_fontes_non_standard_en_plain_tex) de LaTeX2𝜀 afin d'être compatible avec les habitudes d'utilisation de pLaTeX. Cela n'est pas nécessaire pour les utilisateurs de chinois simplifié, c'est pourquoi CTeX désactive la plupart de ses paramètres dans le format LaTeX, ne conservant que les éléments nécessaires. La méthode de définition des polices a également été modifiée de sorte que les commandes pertinentes sont plus ou moins les mêmes que dans le paquet de macros xeCJK.

Le paquet de macros LuaTeX-ja à partir de la version 20150420 prend désormais en charge la typographie verticale, mais CTeX ne le fait pas pour le moment.

### 9.1 Définition de polices alternatives sous LuaLaTeX

:::{todo} Section à compléter.
:::

## 10. Fichiers de configuration pour le jeu de macros CTeX

:::{todo} Section à compléter.
:::

### 10.1 Modifier les options par défaut des macro-paquets

:::{todo} Section à compléter.
:::

### 10.2 Configuration des macro-paquets après le chargement

:::{todo} Section à compléter.
:::

### 10.3 Configuration de la traduction chinoise du titre

:::{todo} Section à compléter.
:::

### 10.4 Personnalisation des jeux de polices

:::{todo} Section à compléter.
:::

## 11. Compatibilité avec les anciennes versions

:::{todo} Section à compléter.
:::

______________________________________________________________________

*Sources :*

- [Manuel de CTeX](texdoc:ctex) (en chinois),
- [How to write in Chinese on OverLeaf?](https://fr.overleaf.com/learn/latex/Chinese) (en anglais).

```{eval-rst}
.. meta::
   :keywords: LaTeX,écrire en chinois,idéogrammes,documentation en chinois simplifié
```
