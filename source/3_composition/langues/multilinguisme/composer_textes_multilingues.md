# Formats TeX et LaTeX spéciaux pour certaines langues ou groupes de langues

LaTeX supportant maintenant pleinement l'UFT-8, la plupart de ces outils ne devraient pas vous être utiles. Cette page a un intérêt plutôt historique.

:::{todo} Indiquer ce qui a encore un intérêt.
:::

## Pouvez-vous donner des exemples d'utilisation de `UCS-LaTeX` ?

## Pouvez-vous donner des exemples d'utilisation d'OMEGA ?

Non. Omega, de même que son successeur Aleph, n'est plus développé. Les fonctionnalités d'Aleph ont été intégrées dans LuaTeX.

## Pouvez-vous donner des exemples d'utilisation de `THAI-LaTeX` ?

## Pouvez-vous donner des exemples d'utilisation de `IvriTeX` ?

## Pouvez-vous donner des exemples d'utilisation de `CJK-LaTeX` ?

```{eval-rst}
.. meta::
   :keywords: LaTeX,textes multilingues
```
