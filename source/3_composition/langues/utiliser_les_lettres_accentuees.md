# Comment saisir les lettres accentuées ?

- Le moyen le plus simple de saisir des lettres accentuées sous LaTeX est de les taper sur son clavier de façon simple et classique : `é` donnera « é » après compilation.

## Saisie directe au clavier

- Pour que ça marche, il faut utiliser une fonte contenant toutes les lettres accentuées nécessaires (code 8 bits). Si besoin, on doit préciser un codage d'entrée via l'instruction :

```latex
% !TEX noedit
\usepackage[codage d'entrée]{inputenc}
```

L'option est rendue nécessaire par le fait que les codes de caractères au-delà de 127 sont différents sous DOS, MacOS et Unix... Ainsi, le codage à préciser est `[latin1]` pour un système Unix, `[ansinew]` pour un PC sous Windows, `[applemac]` sous MacOS adapté au français ou encore `[cp850]` pour le code-page 850 sur PC (sous MS-DOS). Il existe également `[latin9]` (`[latin1]` avec le symbole de l'euro), `[cp438]`, `[latin2]` selon les systèmes.

:::{note}
Il est important de noter que le codage `[latin1]` (ou `[latin9]`) fonctionnera aussi sous Windows dans la majorité des cas. L'avantage de sélectionner ce codage est de rendre le document portable sous Windows et Linux.
:::

:::{warning}
<ctanpkg:inputenc> *casse* les efforts faits par MlTeX lorsqu'on utilise les fontes codées en OT1 (fontes CMR ou Postscript).
:::

- <ctanpkg:mapcodes> de M. Piotrowski autorise également un grand nombre de codages de caractères (iso8859-1 (latin1), iso8859-2 (latin2), ibm850 ou 852, hproman8, etc.).

## Des macros pour accéder aux autres caractères

Si votre clavier ne propose pas les lettres accentuées dont vous avez besoin, ou si elles ne sont pas présentes dans votre police, vous pouvez utiliser ces macros TeX pour les saisir, disponibles *sans package particulier* :

| Codage    | Effet                                         |
|-----------|-----------------------------------------------|
| `\'x`     | ![alt](/_static/images/svg/x_accentue_01.svg) |
| `` \`x `` | ![alt](/_static/images/svg/x_accentue_02.svg) |
| `\^x`     | ![alt](/_static/images/svg/x_accentue_03.svg) |
| `\"x`     | ![alt](/_static/images/svg/x_accentue_04.svg) |
| `\~x`     | ![alt](/_static/images/svg/x_accentue_05.svg) |
| `\.x`     | ![alt](/_static/images/svg/x_accentue_06.svg) |
| `\=x`     | ![alt](/_static/images/svg/x_accentue_07.svg) |
| `\b x`    | ![alt](/_static/images/svg/x_accentue_08.svg) |
| `\c x`    | ![alt](/_static/images/svg/x_accentue_09.svg) |
| `\d x`    | ![alt](/_static/images/svg/x_accentue_10.svg) |
| `\H x`    | ![alt](/_static/images/svg/x_accentue_11.svg) |
| `\r x`    | ![alt](/_static/images/svg/x_accentue_12.svg) |
| `\t{xx}`  | ![alt](/_static/images/svg/x_accentue_13.svg) |
| `\u x`    | ![alt](/_static/images/svg/x_accentue_14.svg) |
| `\v x`    | ![alt](/_static/images/svg/x_accentue_15.svg) |

Les accents sont présentés sur la lettre « x » car aucun de ces caractères n’est précomposé dans unicode.

```{eval-rst}
.. meta::
   :keywords: LaTeX,écrire les lettres accentuées,signes diacritiques,français,caractères accentués,cédille,accents en LaTeX
```
