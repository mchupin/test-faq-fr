# Pourquoi la césure ne fonctionne pas ?

## Quelques règles pour régler les problèmes les plus fréquents

Vérifiez avant toute chose que vous avez bien sélectionné la langue du document via <ctanpkg:babel> ou <ctanpkg:polyglossia>.

- Si votre problème concerne un mot ou groupe de mots qui contient déjà un trait d'union, voir la question « [Comment obtenir une césure dans un mot ou groupe de mots qui contient déjà un trait d'union ?](/3_composition/langues/cesure/permettre_la_coupure_des_mots_contenant_un_trait_d_union) » ;

- S'il s'agit d'un mot étranger à la langue principale du document, deux solutions au choix :

  - Définir une règle spécifique pour ce mot, comme indiqué dans la question « [Comment définir certaines césures ?](/3_composition/langues/cesure/introduire_des_coupures_de_mots) » ;
  - Définir cette langue comme langue secondaire du document et utiliser la commande appropriée sur le passage concerné :

  %

  > - Pour <ctanpkg:polyglossia>, voir les commande `\setotherlanguage` et `\text<lang>{}` dans la [documentation](texdoc:polyglossia)) ;
  > - Pour <ctanpkg:babel>, voir un exemple à la question « [3_composition/langues/composer_un_document_latex_en_francais](/3_composition/langues/composer_un_document_latex_en_francais) ».

- Dans les autres cas, il s'agit probablement d'un mot pour lequel il n'y a pas de césures prédéfinies. Dans ce cas, définissez-les vous-mêmes comme le montre la question « [Comment définir certaines césures ?](/3_composition/langues/cesure/introduire_des_coupures_de_mots) ».

## Explications de césures fautives

### Césure dans une langue qui n'est pas celle souhaitée

Comme expliqué dans la question « [Comment fonctionne la césure en TeX ?](/3_composition/langues/cesure/comment_fonctionne_la_cesure) », il est peu probable que vous obteniez des résultats corrects en composant un document dans une langue en utilisant les règles de césure d'une autre langue. Si vous êtes un utilisateur de LaTeX, sélectionnez la langue appropriée en utilisant <ctanpkg:babel> (ce qui peut demander d'installer ce langage, comme présenté en question « [Comment utiliser un nouveau langage avec « babel » ?](/3_composition/langues/utiliser_une_nouvelle_langue_avec_babel) »).

### Césure de mots courts

Depuis la version 3.0 de TeX, les limites sur la distance à laquelle la césure d'un mot peut avoir lieu sont paramétrables (voir « [Pourquoi les coupures de mots sont bizarres ?](/3_composition/langues/cesure/cesures_bizarres3) »). TeX ne coupera pas à moins de `\lefthyphenmin` caractères après le début d'un mot, ni à moins de `\righthyphenmin` caractères avant la fin d'un mot. Dès lors, il ne coupera pas du tout un mot plus court que la somme de ces deux valeurs. Par exemple, puisque les minimums sont 2 et 3 pour l'anglais, TeX ne coupera pas un mot de moins de 5 lettres, s'il pense que le mot est anglais. Notez que, dans certains cas, les paramètres en question peuvent être modifié par erreur dans des commandes que vous utilisez.

### Césure de mots déjà coupés

TeX ne coupe pas un mot préalablement coupé par un trait d'union. Par exemple, le nom de famille anglais (caricatural) Smyth-Postlethwaite ne devrait pas avoir de césure (en anglais). Mais, si besoin est, vous pouvez remplacer le trait d'union dans le nom par une commande `\hyph`, définie `\def\hyph{-\ pénalité0\hskip0pt\relax}` (ce n'est évidemment pas recommandé sinon). L'extension <ctanpkg:hyphenat> définit un ensemble de telles commandes introduisant des points de césure à divers caractères de ponctuation. Voir aussi la question « [Comment obtenir une césure dans un mot ou groupe de mots qui contient déjà un trait d'union ?](/3_composition/langues/cesure/permettre_la_coupure_des_mots_contenant_un_trait_d_union) »

### Césure de mots accentués

Il peut y avoir des commandes d'accent dans le mot : ce sujet est détaillé à la question « [Comment corriger les coupures de mots accentués ?](/3_composition/langues/cesure/coupures_de_mots_accentues) ».

### Césure de mots non repérés par TeX

Il est aussi possible que la césure n'ait tout simplement pas été repérée. Si l'algorithme de TeX est bon, il n'est pas infaillible et il manque parfois des césures parfaitement correctes dans certaines langues. Lorsque cela se produit, vous devez donner à TeX des instructions *explicites* sur la façon de couper.

La commande `\hyphenation` permet de donner des instructions explicites. À condition que le mot ne soit pas parasité par les règles ci-dessus, la commande remplace tout ce que les modèles de césure pourraient dicter. Cela permet donc de créer de nouvelles césures ou d'annuler d'autres césures. Voir ici les questions « [Comment définir certaines césures ?](/3_composition/langues/cesure/introduire_des_coupures_de_mots) » et « [Comment supprimer certaines césures ?](/3_composition/langues/cesure/empecher_des_coupures_de_mots) » pour des exemples, en particulier si vous travaillez dans un document multilingue.

______________________________________________________________________

*Source :* [My words aren't being hyphenated](faquk:FAQ-nohyph)

```{eval-rst}
.. meta::
   :keywords: LaTeX,césure,problème de coupure de mots,coupure en fin de ligne,motifs de césure
```
